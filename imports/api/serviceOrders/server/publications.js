import { Meteor } from 'meteor/meteor';
import { ServiceOrders } from '../collection.js';

Meteor.publish("serviceOrders", function(){
    return ServiceOrders.find({});
})

Meteor.publish("serviceOrder", function(condition)
{
    return ServiceOrders.find(condition);
})
