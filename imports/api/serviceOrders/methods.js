import { Mongo } from 'meteor/mongo';
import { ServiceOrders } from './collection';

export const getNextServiceOrderId = function()
{
     let record = ServiceOrders.findOne({}, {sort:{index: -1}});
     console.log(record);
     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addServiceOrder: function(properties)
    {
        let user = Meteor.user();
        let id = getNextServiceOrderId();

        let record = Object.assign({
            _id: id,
            index: parseInt(id),
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date(),
            status:"In-Process"
        }, properties);

        ServiceOrders.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ServiceOrder',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updateServiceOrder: function(id, properties)
    {
        let user = Meteor.user();
        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        console.log(properties);

        ServiceOrders.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceOrder',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    updateServiceHasPurchase: function(id, purchaseOrderId, hasPurchase)
    {
        properties = {
            hasPurchase: hasPurchase,
            status:"Waiting for parts",
            purchaseOrderId: purchaseOrderId,
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        };

        ServiceOrders.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceOrder',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    removeServiceOrder: function(id)
    {
        ServiceOrders.remove({_id: id});
    },
    onApproveServiceOrder: function(id, status)
    {
        let user = Meteor.user();

        let properties = {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date(),
            status: status
        };

        ServiceOrders.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceOrder',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    updateReleaseSw: function(id, releaseSw)
    {
        ServiceOrders.update({_id: id}, {$set: {releaseSw: releaseSw}});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceOrder',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: {releaseSw: releaseSw}
                    });
    },
    addOrderFromPreOrder: function(properties)
    {
        let user = Meteor.user();
        let id = getNextServiceOrderId();

        let record = Object.assign({
            _id: id,
            index: parseInt(id),
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date(),
            status:"In-Process"
        }, properties);

        ServiceOrders.insert(record);

        Meteor.call("updatePreOrderisService", record.foreignId, true);

        if(record.releaseSw != "")
        {
            Meteor.call("addHistoryReleaseSw", {
                serviceOrderId: id,
                systemId: record.systemId,
                brandId: record.brandId,
                modelId: record.modelId,
                releaseSw: record.releaseSw
            });
        }

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ServiceOrder',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },

});
