import { Meteor } from 'meteor/meteor';
import { Missings } from '../collection.js';

Meteor.publish("missings", function()
{
	return Missings.find({});
})
