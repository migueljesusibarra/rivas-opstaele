import { Mongo } from 'meteor/mongo';
import { Missings } from './collection';

export const getNextMissingId = function()
{
     let record = Missings.findOne({}, {sort:{index: -1}});
     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextMissingPosId = function(receptionEquipmentId)
{
     let record = Missings.findOne({receptionEquipmentId: receptionEquipmentId}, {sort:{index: -1}});
     return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addMissing: function(properties)
  {
    let user = Meteor.user();
    let id = getNextMissingId();
    let pos = getNextMissingPosId(properties.receptionEquipmentId)

    let record = Object.assign({
          _id: id,
          index: parseInt(id),
          pos:pos,
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
    }, properties);

    Missings.insert(record);

    return id;
  },
  
  updateMissing: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       Missings.update({_id: id}, {$set: properties});
   },
  
  removeMissing: function(id)
   {
       Missings.remove({_id: id});
   }
});
