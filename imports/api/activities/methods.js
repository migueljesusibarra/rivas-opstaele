import { Mongo } from 'meteor/mongo';
import { Activities } from './collection';

export const getNextIndex = function()
{
    let record = Activities.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

Meteor.methods({
    addActivity: function(description)
    {
        let user = Meteor.user();
        let id = Random.id();

        let properties = {
                        _id: id,
                        index: getNextIndex(),
                        description: description,
                        createdByUserId: Meteor.userId(),
                        createdOn: new Date(),
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date()
        };

        Activities.insert(properties);


        Meteor.call("createHistory", {
             foreignId: id,
             module: 'Activity',
             channel: "PC",
             type: 'RECORD_CREATED',
             properties: properties
         });

        return id;
    },
    updateActivity: function(id, description)
    {
        let user = Meteor.user();
        let properties = {
                        description: description,
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date()
        };

        Activities.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Activity',
                channel: "PC",
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeActivity: function(id)
    {
        Activities.remove({_id: id});
    }
});
