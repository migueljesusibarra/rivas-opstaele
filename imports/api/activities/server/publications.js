import { Meteor } from 'meteor/meteor';
import { Activities } from '../collection.js';

Meteor.publish("activities", function(condition)
{
	return Activities.find(condition || {});
})