import { Meteor } from 'meteor/meteor';
import { ServicePrograms } from '../collection.js';

Meteor.publish("servicePrograms", function(condition) {
	return ServicePrograms.find(condition || {});
})
