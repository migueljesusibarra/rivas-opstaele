import { Mongo } from 'meteor/mongo';
import { ServicePrograms } from './collection';

export const getNextIndex = function()
{
    let record = ServicePrograms.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

export const getNextProgramPosId = function(serviceOrderId)
{
    let record = ServicePrograms.findOne({serviceOrderId: serviceOrderId}, {sort:{index: -1}});

    return ((record != null ? parseInt(record.postId) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addServiceProgram: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign(
        {
             _id: id,
             index: getNextIndex(),
             postId: getNextPartPosId(properties.serviceOrderId),
             createdByUserId: Meteor.userId(),
             createdOn: new Date(),
             modifiedByUserId: Meteor.userId(),
             modifiedOn: new Date()
        }, properties);

        ServicePrograms.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ServiceProgram',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updateServicePrograms: function(id, properties)
    {
        let user = Meteor.user();

        let record = Object.assign(properties, {
                modifiedByUserId: Meteor.userId(),
                modifiedOn: new Date()
        });

        ServicePrograms.update({_id: id}, {$set: {description: properties}});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceProgram',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    removeServicePrograms: function(id)
    {
        ServicePrograms.remove({_id: id});
    },
    addServicePrograms: function(protocols)
    {
        let user = Meteor.user();

         console.log("llego");

        for(let protocol of protocols)
        {
            let user = Meteor.user();
            let id = Random.id();

            let record = Object.assign(
            {
                 _id: id,
                 index: getNextIndex(),
                 postId: getNextProgramPosId(protocol.serviceOrderId),
                 createdByUserId: Meteor.userId(),
                 createdOn: new Date(),
                 modifiedByUserId: Meteor.userId(),
                 modifiedOn: new Date()

            }, protocol);

            ServicePrograms.remove({serviceOrderId: record.serviceOrderId, period: record.period, protocolId: record.protocolId})
            
            if(record.check)
                 ServicePrograms.insert(record);

            Meteor.call("createHistory", {
                    foreignId: id,
                    module: 'ServiceProgram',
                    channel: "PC",
                    type: 'RECORD_CREATED',
                    properties: protocol
                });
        }
    }
});
