import { Mongo } from 'meteor/mongo';
import { ReceptionEquipmentTestsPerformed } from './collection';

Meteor.methods({
    addReceptionEquipmentTestsPerformed: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign({
                            _id: id,
                            createdByUserId: Meteor.userId(),
                            createdOn: new Date(),
                            modifiedByUserId: Meteor.userId(),
                            modifiedOn: new Date()
                        }, properties);

        ReceptionEquipmentTestsPerformed.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ReceptionEquipmentTestsPerformed',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updateReceptionEquipmentTestsPerformed: function(id, properties)
    {
        let user = Meteor.user();
        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        ReceptionEquipmentTestsPerformed.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ReceptionEquipmentTestsPerformed',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    removeReceptionEquipmentTestsPerformed: function(id)
    {
        ReceptionEquipmentTestsPerformed.remove({_id: id});
    }
});
