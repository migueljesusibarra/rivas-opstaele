import { Meteor } from 'meteor/meteor';
import { ReceptionEquipmentTestsPerformed } from '../collection.js';

Meteor.publish("receptionEquimentTestsPerformed", function(condition)
{
	return ReceptionEquipmentTestsPerformed.find(condition || {});
})
