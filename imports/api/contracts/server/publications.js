import { Meteor } from 'meteor/meteor';
import { Contracts } from '../collection.js';

Meteor.publish("contracts", function(){
    return Contracts.find({});
});
