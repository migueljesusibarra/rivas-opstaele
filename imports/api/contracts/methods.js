import { Meteor } from 'meteor/meteor';
import { Contracts } from '../contracts/collection.js';

export const getNextContractId = function()
{
  let record = Contracts.findOne({}, {sort:{index: -1}});
  return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addContract: function(properties)
  {
    let id = getNextContractId();
    let user = Meteor.user();

    let record = Object.assign({
          _id: id,
          index: parseInt(id),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
    }, properties);

    Contracts.insert(record);

    Meteor.call("createHistory", {
            foreignId: id,
            module: 'Contract',
            channel: "PC",
            type: 'RECORD_CREATED',
            properties: properties
        });

    return id;
  },
  updateContract: function(id, properties)
  {
    let record = Object.assign({
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
    }, properties);

    Contracts.update({_id: id}, {$set: properties})

    Meteor.call("createHistory", {
            foreignId: id,
            module: 'Contract',
            channel: "PC",
            type: 'RECORD_UPDATED',
            properties: properties
        });
  }

});
