import { Mongo } from 'meteor/mongo';
import { ServiceOrderParts } from './collection';

export const getNextIndex = function()
{
    let record = ServiceOrderParts.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

export const getNextPartPosId = function(serviceOrderId)
{
    let record = ServiceOrderParts.findOne({serviceOrderId: serviceOrderId}, {sort:{index: -1}});

    return ((record != null ? parseInt(record.postId) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addServiceOrderPart: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign(
        {
             _id: id,
             index: getNextIndex(),
             postId: getNextPartPosId(properties.serviceOrderId),
             createdByUserId: Meteor.userId(),
             createdOn: new Date(),
             modifiedByUserId: Meteor.userId(),
             modifiedOn: new Date()
        }, properties);

        ServiceOrderParts.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ServiceOrderPart',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updateServiceOrderPart: function(id, properties)
    {
        let user = Meteor.user();
        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        ServiceOrderParts.update({_id: id}, {$set: {description: properties}});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'ServiceOrderPart',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: properties
                    });
    },
    removeServiceOrderPart: function(id)
    {
        ServiceOrderParts.remove({_id: id});
    },
    addServiceOrderParts: function(parts)
    {
        let user = Meteor.user();

        for(let part of parts)
        {
            let user = Meteor.user();
            let id = Random.id();

            let record = Object.assign(
            {
                 _id: id,
                 index: getNextIndex(),
                 postId: getNextPartPosId(part.serviceOrderId),
                 createdByUserId: Meteor.userId(),
                 createdOn: new Date(),
                 modifiedByUserId: Meteor.userId(),
                 modifiedOn: new Date()
            }, part);

            ServiceOrderParts.insert(record);

            Meteor.call("createHistory", {
                    foreignId: id,
                    module: 'ServiceOrderPart',
                    channel: "PC",
                    type: 'RECORD_CREATED',
                    properties: part
                });
        }
    }
});
