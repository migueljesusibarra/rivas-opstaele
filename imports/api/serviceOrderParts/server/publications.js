import { Meteor } from 'meteor/meteor';
import { ServiceOrderParts } from '../collection.js';

Meteor.publish("serviceOrderParts", function(condition)
{
    return ServiceOrderParts.find(condition || {});
})
