import { Meteor } from 'meteor/meteor';
import { OrderTypes } from '../collection.js';

Meteor.publish("orderTypes", function()
{
	return OrderTypes.find({});
})
