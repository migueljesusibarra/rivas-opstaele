import { Mongo } from 'meteor/mongo';
import { OrderTypes } from './collection';

export const getNextOrderTypeId = function()
{
   let record = OrderTypes.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addOrderType: function(properties)
      {
        let user = Meteor.user();
        let id = getNextOrderTypeId();

        let record = Object.assign({
                                        _id: id,
                                        index: parseInt(id),
                                        createdByUserId: Meteor.userId(),
                                        createdOn: new Date(),
                                        modifiedByUserId: Meteor.userId(),
                                        modifiedOn: new Date()
                                    }, properties);

        OrderTypes.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'OrderType',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
      },

    updateOrderType: function(id, properties)
       {
          let user = Meteor.user();
          let record = Object.assign(properties,
          {
              modifiedByUserId: Meteor.userId(),
              modifiedOn: new Date()
          });

          OrderTypes.update({_id: id}, {$set: properties});

          Meteor.call("createHistory", {
                  foreignId: id,
                  module: 'OrderType',
                  channel: "PC",
                  type: 'RECORD_UPDATED',
                  properties: properties
              });
       },

    removeOrderType: function(id)
    {
           OrderTypes.remove({_id: id});
     }
});
