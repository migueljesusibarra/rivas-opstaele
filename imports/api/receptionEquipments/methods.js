import { Mongo } from 'meteor/mongo';
import { ReceptionEquipments } from './collection';

export const getNextreceptionEquipmentId = function()
{
    let record = ReceptionEquipments.findOne({}, {sort:{index: -1}});

    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addReceptionEquipment: function(properties)
    {
        let user = Meteor.user();
        let id = getNextreceptionEquipmentId();

        let record = Object.assign({
            _id: id,
            index: parseInt(id),
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date(),
            status:"Borrador",
        }, properties);

        ReceptionEquipments.insert(record);

        Meteor.call("createHistory", {
             foreignId: id,
             module: 'ReceptionEquipment',
             type: 'RECORD_CREATED',
             properties: properties
         });

        return id;
    },
    updateReceptionEquipment: function(id, properties)
    {
        let user = Meteor.user();
        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        ReceptionEquipments.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'ReceptionEquipment',
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeReceptionEquipment: function(id)
    {
        ReceptionEquipments.remove({_id: id});
    },
    setReceptionEquipmentStatus: function(id, status)
    {
        ReceptionEquipments.update({_id: id}, {$set: {status: status}});

        let properties = {
                         modifiedByUserId: Meteor.userId(),
                         modifiedOn: new Date(),
                         status: status
                     };

         Meteor.call("createHistory", {
                 foreignId: id,
                 module: 'ReceptionEquipment',
                 type: 'RECORD_UPDATED',
                 properties: properties
             });
    },
    assignTechnician: function(receptionEquipmentId, technicianId)
    {
        ReceptionEquipments.update({_id: receptionEquipmentId}, {$set: {technicianId: technicianId}});

        let properties = {
                         modifiedByUserId: Meteor.userId(),
                         modifiedOn: new Date(),
                         technicianId: technicianId
                     };

         Meteor.call("createHistory", {
                 foreignId: receptionEquipmentId,
                 module: 'ReceptionEquipment',
                 type: 'RECORD_UPDATED',
                 properties: properties
             });
    },
    removeTechnicianFromReceptionEquipment: function(receptionEquipmentId)
    {
        ReceptionEquipments.update({_id: receptionEquipmentId}, {$set: {technicianId: ""}});

        let properties = {
                         modifiedByUserId: Meteor.userId(),
                         modifiedOn: new Date(),
                         technicianId: ""
                     };

         Meteor.call("createHistory", {
                 foreignId: receptionEquipmentId,
                 module: 'ReceptionEquipment',
                 type: 'RECORD_UPDATED',
                 properties: properties
             });
    }
});
