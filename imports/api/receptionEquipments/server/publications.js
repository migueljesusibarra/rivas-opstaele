import { Meteor } from 'meteor/meteor';
import { ReceptionEquipments } from '../collection.js';

Meteor.publish("receptionEquipments", function(condition) {
	return ReceptionEquipments.find(condition || {});
})
