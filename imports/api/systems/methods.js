import { Mongo } from 'meteor/mongo';
import { Systems } from './collection';

export const getNextIndex = function()
{
    let record = Systems.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 0;

};

Meteor.methods({
    addSystem: function(description)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = {
            _id: id,
            index: getNextIndex(),
            description: description,
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        };

        Systems.insert(record);

        return id;

    },
    updateSystem: function(id, description)
    {
        let user = Meteor.user();
        let record = {
            description: description,
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        };

        Systems.update({_id: id}, {$set: record});
    },
    removeSystem: function(id)
    {
        Systems.remove({_id: id});
    }
});
