import { Meteor } from 'meteor/meteor';
import { Systems } from '../collection.js';

Meteor.publish("systems", function(condition) {
	return Systems.find(condition || {});
})
