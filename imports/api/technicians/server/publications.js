import { Meteor } from 'meteor/meteor';
import { Technicians } from '../collection.js';

Meteor.publish("technicians", function() {
	return Technicians.find({});
})
