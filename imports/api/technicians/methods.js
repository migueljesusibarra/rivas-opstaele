import { Mongo } from 'meteor/mongo';
import { Technicians } from './collection';
import { Email } from 'meteor/email';
import { Templates } from '/imports/api/templates';

export const getNextTechnicianId = function()
{
    let record = Technicians.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addTechnician: function(properties)
    {
        let id = getNextTechnicianId();
        let user = Meteor.user();

        let record = Object.assign({
                            _id: id,
                            index: parseInt(id),
                            createdByUserId: Meteor.userId(),
                            createdOn: new Date(),
                            modifiedByUserId: Meteor.userId(),
                            modifiedOn: new Date()
                        }, properties);

        Technicians.insert(record);

        Meteor.call("createHistory", {
             foreignId: id,
             module: 'Technician',
             type: 'RECORD_CREATED',
             properties: properties
         });

        return id;
    },
    updateTechnician: function(id, properties)
    {
        let record = Object.assign({
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date()
                    }, properties);

        Technicians.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Technician',
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeTechnician: function(id)
    {
        Technicians.remove({_id: id});
    },
    setTechnicianActive: function(id, active)
    {
        Technicians.update({_id: id}, {$set: {active: active}});

        let record = {
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date(),
                        active: active
                    };

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Technician',
                type: 'RECORD_UPDATED',
                properties: record
            });

    },
    sendTechnicianEmail(technicianIds)
    {
        this.unblock();

        for(let technicianId of technicianIds)
        {
            let technician = Technicians.findOne({_id: technicianId});
            let to = technician.email;
            let subject = "Test Technician Send email";
            let text = "Hi";

            if(Meteor.isServer)
                Email.send(Templates.getBasicTemplateEmail(to, "no-reply@kodein.io", subject, text));
        }
    }
});
