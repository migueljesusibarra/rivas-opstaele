import { Mongo } from 'meteor/mongo';
import { Vendors } from './collection';

export const getNextVendorId = function()
{
    let record = Vendors.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addVendor: function(properties)
  {
      let id = getNextVendorId();
      let user = Meteor.user();

      let record = Object.assign({
          _id: id,
          index: parseInt(id),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

      Vendors.insert(record);

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'Vendor',
              channel: "PC",
              type: 'RECORD_CREATED',
              properties: properties
          });

      return id;
  },

  updateVendor: function(id, properties)
  {
    let record = Object.assign({
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
    }, properties);

    Vendors.update({_id: id}, {$set: properties})

    Meteor.call("createHistory", {
                    foreignId: id,
                    module: 'Vendor',
                    channel: "PC",
                    type: 'RECORD_UPDATED',
                    properties: properties
                });
  },
  removeVendor: function(id)
  {
      Vendors.remove({_id: id});
  },
  setStatusVendor: function(id, active)
    {
        Vendors.update({_id: id}, {$set: {active: active}});

        Meteor.call("createHistory", {
                        foreignId: id,
                        module: 'Vendor',
                        channel: "PC",
                        type: 'RECORD_UPDATED',
                        properties: {active: active}
                    });
    }
});
