import { Meteor } from 'meteor/meteor';
import { Vendors } from '../collection.js';

Meteor.publish("vendors", function() {
	return Vendors.find({});
})
