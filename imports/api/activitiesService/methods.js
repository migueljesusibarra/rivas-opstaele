import { Mongo } from 'meteor/mongo';
import { ActivitiesService } from './collection';

Meteor.methods({
  addActivitieService: function(properties)
   {
      let user = Meteor.user();
      let id = Random.id();

      let record = Object.assign(properties,
      {
           _id: id,
           createdByUserId: Meteor.userId(),
           createdOn: new Date(),
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
      });

      ActivitiesService.insert(record);

      Meteor.call("createHistory", {
           foreignId: id,
           module: 'ActivitiesService',
           channel: "PC",
           type: 'RECORD_CREATED',
           properties: properties
       });


      return id;
   },
   updateActivitieService: function(id, properties)
   {
      let user = Meteor.user();
      let record = Object.assign(properties,
      {
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
      });

      ActivitiesService.update({_id: id}, {$set: properties});

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'ActivitiesService',
              channel: "PC",
              type: 'RECORD_UPDATED',
              properties: properties
          });
   },
   removeActivitieService: function(id)
   {
      ActivitiesService.remove({_id: id});
   }
});
