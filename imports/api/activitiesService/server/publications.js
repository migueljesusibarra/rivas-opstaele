import { Meteor } from 'meteor/meteor';
import { ActivitiesService } from '../collection.js';

Meteor.publish("activitiesService", function(condition)
{
	return ActivitiesService.find(condition || {});
})