import { Mongo } from 'meteor/mongo';
import { Periods } from './collection';

export const getNextIndex = function()
{
    let record = Periods.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

Meteor.methods({
    addPeriod: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign({
                                       _id: id,
                                       index: getNextIndex(),
                                       createdByUserId: Meteor.userId(),
                                       createdOn: new Date(),
                                       modifiedByUserId: Meteor.userId(),
                                       modifiedOn: new Date()
                                   }, properties);

        Periods.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Period',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updatePeriod: function(id, properties)
    {
        let user = Meteor.user();

        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        Periods.update({_id: id}, {$set: record});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Period',
                channel: "PC",
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removePeriod: function(id)
    {
        Periods.remove({_id: id});
    }
});
