import { Meteor } from 'meteor/meteor';
import { Periods } from '../collection.js';

Meteor.publish("periods", function() {
	return Periods.find({});
})
