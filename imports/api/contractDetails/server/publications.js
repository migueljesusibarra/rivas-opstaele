import { Meteor } from 'meteor/meteor';
import { ContractDetails } from '../collection.js';

Meteor.publish("contractDetails", function(){
    return ContractDetails.find({});
})