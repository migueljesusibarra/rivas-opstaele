import { Meteor } from 'meteor/meteor';
import { ContractDetails } from './collection.js';

  export const getNextContractDetailId = function()
  {
       let record = ContractDetails.findOne({}, {sort:{index: -1}});
       return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
  };

  export const getNextContractDetailPosId = function(contractId)
  {
       let record = ContractDetails.findOne({contractId: contractId}, {sort:{index: -1}});
       return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
  };

  Meteor.methods({
    addContractDetail: function(properties)
    {
        let user = Meteor.user();
        let id = getNextContractDetailId();
        let pos = getNextContractDetailPosId(properties.contractId);

        let record = Object.assign({
                                    _id: id,
                                    index: parseInt(id),
                                    pos: pos,
                                    createdByUserId: Meteor.userId(),
                                    createdOn: new Date(),
                                    modifiedByUserId: Meteor.userId(),
                                    modifiedOn: new Date()
                                }, properties);

       ContractDetails.insert(record);

       Meteor.call("setInContract", record.equipmentId, true, record.contractId, record.contractNumber, function(error, result){});

      return id;
    },

   updateContractDetail: function(id, properties)
    {
      let record = Object.assign({
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

      ContractDetails.update({_id: id}, {$set: properties})

    },

   removeContractDetail: function(id, equipmentId)
   {
       ContractDetails.remove({_id: id});
       Meteor.call("setInContract", equipmentId, false, "", "", function(error, result){});
   },

  });
