import { Mongo } from 'meteor/mongo';
import { Configurations } from './collection';

export const getNextConfigurationId = function()
{
    let record = Configurations.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextConfigurationPosId = function(receptionEquipmentId)
{
    let record = Configurations.findOne({receptionEquipmentId: receptionEquipmentId}, {sort:{index: -1}});
    return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addConfiguration: function(properties)
    {
        let user = Meteor.user();
        let id = getNextConfigurationId();
        let pos = getNextConfigurationPosId(properties.receptionEquipmentId)

        let record = Object.assign({
                                        _id: id,
                                        index: parseInt(id),
                                        pos: parseInt(pos),
                                        createdByUserId: Meteor.userId(),
                                        createdOn: new Date(),
                                        modifiedByUserId: Meteor.userId(),
                                        modifiedOn: new Date()
                                    }, properties);

        Configurations.insert(record);

        Meteor.call("createHistory", {
             foreignId: id,
             module: 'Configuration',
             channel: "PC",
             type: 'RECORD_CREATED',
             properties: properties
         });

        return id;
    },
    updateConfiguration: function(id, properties)
    {
        let user = Meteor.user();

        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        Configurations.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Configuration',
                channel: "PC",
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeConfiguration: function(id)
    {
        Configurations.remove({_id: id});
    },
    addConfigurations: function(configurations)
    {
        let user = Meteor.user();

        for(let configuration of configurations)
        {
            let id = getNextConfigurationId();
            let pos = getNextConfigurationPosId(configuration.receptionEquipmentId)

            let record = Object.assign({
                                            _id: id,
                                            index: parseInt(id),
                                            pos: parseInt(pos),
                                            createdByUserId: Meteor.userId(),
                                            createdOn: new Date(),
                                            modifiedByUserId: Meteor.userId(),
                                            modifiedOn: new Date()
                                        }, configuration);

            Configurations.insert(record);

            Meteor.call("createHistory", {
                 foreignId: id,
                 module: 'Configuration',
                 channel: "PC",
                 type: 'RECORD_CREATED',
                 properties: properties
             });
        }

        return configurations;
    }
});
