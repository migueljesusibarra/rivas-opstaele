import { Meteor } from 'meteor/meteor';
import { Configurations } from '../collection.js';

Meteor.publish("configurations", function(condition) {
	return Configurations.find(condition || {});
})
