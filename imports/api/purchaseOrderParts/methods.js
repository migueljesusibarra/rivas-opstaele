import { Mongo } from 'meteor/mongo';
import { PurchaseOrderParts } from './collection';

export const getNextIndex = function()
{
    let record = PurchaseOrderParts.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

export const getNextPartPosId = function(purchaseOrderId)
{
    let record = PurchaseOrderParts.findOne({purchaseOrderId: purchaseOrderId}, {sort:{index: -1}});

    return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addPurchaseOrderPart: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();
        let pos = getNextPartPosId(properties.purchaseOrderId)

        let record = Object.assign({
            _id: id,
            index: getNextIndex(),
            pos:pos,
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        }, properties);

        PurchaseOrderParts.insert(record);

        return id;
    },
    addBacthPurchaseOrderPart: function(propertiesArray)
    {
        let user = Meteor.user();
        propertiesArray.forEach(function(properties)
        {
            let id = Random.id();
            let pos = getNextPartPosId(part.purchaseOrderId)

            let record = Object.assign({
                _id: id,
                index: getNextIndex(),
                pos: pos,
                createdByUserId: Meteor.userId(),
                createdOn: new Date(),
                modifiedByUserId: Meteor.userId(),
                modifiedOn: new Date()
            }, properties);

            PurchaseOrderParts.insert(record);

        });
    },
    updatePurchaseOrderPart: function(id, properties)
    {
        let user = Meteor.user();

        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        PurchaseOrderParts.update({_id: id}, {$set: properties});
    },
    removePurchaseOrderPart: function(id)
    {
        PurchaseOrderParts.remove({_id: id});
    },
    removePurchaseOrderParts: function(purchaseOrderId)
    {
        PurchaseOrderParts.remove({purchaseOrderId: purchaseOrderId});
    },
    addPurchaseOrderParts: function(parts, hasPurchase)
    {
        let user = Meteor.user();
        
        if(hasPurchase)
        {
            PurchaseOrderParts.remove({purchaseOrderId: parts[0].purchaseOrderId});
        }

        for(let part of parts)
        {
            let pos = getNextPartPosId(part.purchaseOrderId)
            let id = Random.id();

            let record = Object.assign({
                _id: id,
                index: getNextIndex(),
                pos: pos,
                createdByUserId: Meteor.userId(),
                createdOn: new Date(),
                modifiedByUserId: Meteor.userId(),
                modifiedOn: new Date()
            }, part);

            PurchaseOrderParts.insert(record);
        }

        return parts;
    }
});
