import { Meteor } from 'meteor/meteor';
import { PurchaseOrderParts } from '../collection.js';

Meteor.publish("purchaseOrderParts", function(condition)
{
	return PurchaseOrderParts.find(condition || {});
})
