import { Mongo } from 'meteor/mongo';
import { Emails } from './collection';

export const getNextIndex = function()
{
    let record = Emails.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

Meteor.methods({
  addEmail: function(properties)
 {
    let user = Meteor.user();
    let id = Random.id();

    let record = Object.assign({
          _id: id,
          index: getNextIndex(),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
    }, properties);


    Emails.insert(record);

    return id;
 },
 updateEmail: function(id, properties)
 {
    let user = Meteor.user();
    let record = Object.assign(properties,
    {
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
    });

    Emails.update({_id: id}, {$set: properties});
 },
 removeEmail: function(id)
 {
    Emails.remove({_id: id});
 }

});
