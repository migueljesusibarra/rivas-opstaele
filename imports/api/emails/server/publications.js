import { Meteor } from 'meteor/meteor';
import { Emails } from '../collection.js';

Meteor.publish("emails", function() {
	return Emails.find({});
});
