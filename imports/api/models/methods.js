import { Mongo } from 'meteor/mongo';
import { Models } from './collection';

export const getNextIndex = function()
{
    let record = Models.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

Meteor.methods({
    addModel: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign({
                                       _id: id,
                                       index: getNextIndex(),
                                       createdByUserId: Meteor.userId(),
                                       createdOn: new Date(),
                                       modifiedByUserId: Meteor.userId(),
                                       modifiedOn: new Date()
                                   }, properties);

        Models.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Model',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
    },
    updateModel: function(id, properties)
    {
        let user = Meteor.user();

        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        Models.update({_id: id}, {$set: record});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Model',
                channel: "PC",
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeModel: function(id)
    {
        Models.remove({_id: id});
    }
});
