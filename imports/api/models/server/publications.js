import { Meteor } from 'meteor/meteor';
import { Models } from '../collection.js';

Meteor.publish("models", function(condition) {
	return Models.find(condition || {});
})
