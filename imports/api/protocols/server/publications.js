import { Meteor } from 'meteor/meteor';
import { Protocols } from '../collection.js';
//Se utiliza en el catalogo
Meteor.publish("protocols", function(condition) {
	return Protocols.find(condition || {});
})
//Se utiliza en la conatiner ServiceProgramContainer
// Meteor.publish("modelProtocols", function(condition)
// {
// 	return Protocols.find({condition});
// })
