import { Mongo } from 'meteor/mongo';
import { Protocols } from './collection';

export const getNextProtocolId = function()
{
     let record = Protocols.findOne({}, {sort:{index: -1}});
     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
 addProtocol: function(properties)
 {
   let user = Meteor.user();
   let id = getNextProtocolId();

   let record = Object.assign({
          _id: id,
          index: parseInt(id),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

      Protocols.insert(record);

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'Protocol',
              channel: "PC",
              type: 'RECORD_CREATED',
              properties: properties
          });

      return id;
   },
   updateProtocol: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       Protocols.update({_id: id}, {$set: properties});

       Meteor.call("createHistory", {
                       foreignId: id,
                       module: 'Protocol',
                       channel: "PC",
                       type: 'RECORD_UPDATED',
                       properties: properties
                   });

   },

   updateIsParent: function(id, isParent)
   {
       Protocols.update({_id: id}, {$set: {isParent: isParent}});
   },

   removeProtocol: function(id)
   {
      Protocols.remove({_id: id});
   }
});
