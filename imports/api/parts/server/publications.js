import { Meteor } from 'meteor/meteor';
import { Parts } from '../collection.js';

Meteor.publish("parts", function(condition) {
	return Parts.find(condition || {});
})
