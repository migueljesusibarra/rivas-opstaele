import { Mongo } from 'meteor/mongo';
import { Parts } from './collection';

Meteor.methods({
    addPart: function(properties)
   {
       let user = Meteor.user();
       let id = Random.id();

       let record =  Object.assign({
           _id: id,
           createdByUserId: Meteor.userId(),
           createdOn: new Date(),
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
       }, properties);

       Parts.insert(record);

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'Part',
               channel: "PC",
               type: 'RECORD_CREATED',
               properties: properties
           });

       return id;
   },
   updatePart: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
       });

       Parts.update({_id: id}, {$set: properties});

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'Part',
               channel: "PC",
               type: 'RECORD_UPDATED',
               properties: properties
           });
   },
   removePart: function(id)
   {
       Parts.remove({_id: id});
   }
});
