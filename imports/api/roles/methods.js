import { Mongo } from 'meteor/mongo';
import { Roles } from './collection';

Meteor.methods({
    addRole: function(properties)
   {
       let user = Meteor.user();
       let id = Random.id();

       let record = {
           _id: id,
           description: properties,
           createdByUserId: Meteor.userId(),
           createdOn: new Date(),
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
       };

       Roles.insert(record);
       return id;
   },
   updateRole: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       Roles.update({_id: id}, {$set: {description: properties}});
   },
   removeRole: function(id)
   {
       Roles.remove({_id: id});
   }
});
