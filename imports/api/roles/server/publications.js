import { Meteor } from 'meteor/meteor';
import { Roles } from '../collection.js';

Meteor.publish("roles", function(){
    return Roles.find({});
})
