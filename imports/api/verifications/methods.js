import { Mongo } from 'meteor/mongo';
import { Verifications } from './collection';

export const getNextVerificationId = function()
{
     let record = Verifications.findOne({}, {sort:{index: -1}});
     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextVerificationPosId = function(receptionEquipmentId)
{
     let record = Verifications.findOne({receptionEquipmentId: receptionEquipmentId}, {sort:{index: -1}});
     return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addVerification: function(properties)
  {
    let user = Meteor.user();
    let id = getNextVerificationId();
    let pos = getNextVerificationPosId(properties.receptionEquipmentId)

    let record = Object.assign({
          _id: id,
          index: parseInt(id),
          pos: parseInt(pos),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

     Verifications.insert(record);

     return id;
  },

  addBacthVerificationDetail: function(propertiesArray)
    {
      let user = Meteor.user();

      propertiesArray.forEach(function(properties)
      {
        let id = getNextVerificationId();
        let pos = getNextVerificationPosId(properties.receptionEquipmentId)

         let record = Object.assign({
               _id: id,
               index: parseInt(id),
               pos: parseInt(pos),
               createdByUserId: Meteor.userId(),
               createdOn: new Date(),
               modifiedByUserId: Meteor.userId(),
               modifiedOn: new Date()
           }, properties);

          Verifications.insert(record);

      });
    },

  updateVerification: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       Verifications.update({_id: id}, {$set: properties});
   },

   updateVerificationMissing: function(id, isMissing)
    {
        Verifications.update({_id: id}, {$set: {isMissing: isMissing, modifiedByUserId: Meteor.userId(), modifiedOn: new Date()}});
    },

  removeVerification: function(id)
  {
      Verifications.remove({_id: id});
  }

});
