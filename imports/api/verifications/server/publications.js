import { Meteor } from 'meteor/meteor';
import { Verifications } from '../collection.js';

Meteor.publish("verifications", function(condition) {
	return Verifications.find(condition || {});
})
