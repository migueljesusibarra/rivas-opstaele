import { Mongo } from 'meteor/mongo';
import { PartsRequest } from './collection';
import { Email } from 'meteor/email';
import { Templates } from '/imports/api/templates';

export const getNextIndex = function()
{
    let record = PartsRequest.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

export const getNextPartPosId = function(serviceOrderId)
{
    let record = PartsRequest.findOne({serviceOrderId: serviceOrderId}, {sort:{index: -1}});
    return ((record != null ? parseInt(record.postId) || 0 : 0) + 1).toString();
};


Meteor.methods({

  addPartRequest: function(properties)
   {
      let user = Meteor.user();
      let id = Random.id();

      let record = Object.assign(
      {
           _id: id,
           index: getNextIndex(),
           postId: getNextPartPosId(properties.serviceOrderId),
           createdByUserId: Meteor.userId(),
           createdOn: new Date(),
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
      }, properties);

      PartsRequest.insert(record);

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'PartsRequest',
              channel: "PC",
              type: 'RECORD_CREATED',
              properties: properties
          });

      return id;
   },
   updatePartsRequest: function(id, properties)
   {
      let user = Meteor.user();
      let record = Object.assign(properties,
      {
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
      });

      PartsRequest.update({_id: id}, {$set: record});

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'PartsRequest',
              channel: "PC",
              type: 'RECORD_UPDATED',
              properties: properties
          });
   },
   removePartsRequest: function(id)
   {
      PartsRequest.remove({_id: id});
   },
   addPartsRequest: function(parts)
   {
       let user = Meteor.user();

       for(let part of parts)
       {
           let user = Meteor.user();
           let id = Random.id();

           let record = Object.assign(
           {
                _id: id,
                index: getNextIndex(),
                postId: getNextPartPosId(part.serviceOrderId),
                createdByUserId: Meteor.userId(),
                createdOn: new Date(),
                modifiedByUserId: Meteor.userId(),
                modifiedOn: new Date()
           }, part);

           PartsRequest.insert(record);

           Meteor.call("createHistory", {
                   foreignId: id,
                   module: 'PartsRequest',
                   channel: "PC",
                   type: 'RECORD_CREATED',
                   properties: part
               });
       }
   },
   sendPartsRequestEmail(id, email)
    {
        this.unblock();

        let partsRequest = PartsRequest.findOne({_id: id});
        let to = email;
        let subject = "Solicitud de Parte";
        let text = "Cantidad : " + partsRequest.quantity + " - " + "Parte" + partsRequest.description;

        if(Meteor.isServer)
            Email.send(Templates.getBasicTemplateEmail(to, "no-reply@kodein.io", subject, text));
    }

});
