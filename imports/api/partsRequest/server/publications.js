import { Meteor } from 'meteor/meteor';
import { PartsRequest } from '../collection.js';

Meteor.publish("partsRequest", function(condition)
{
	return PartsRequest.find(condition || {});
})
