import { Meteor } from 'meteor/meteor';
import { Customers } from '../collection.js';

Meteor.publish("customers", function(condition){
    return Customers.find(condition || {});
});
