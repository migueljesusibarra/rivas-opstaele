import { Meteor } from 'meteor/meteor';
import { Customers } from './collection.js';

export const getNextCustomerId = function()
{
    let record = Customers.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addCustomer: function(properties)
  {
      let id = getNextCustomerId();
      let user = Meteor.user();

      let record = Object.assign({
          _id: id,
          index: parseInt(id),
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

      Customers.insert(record);

      Meteor.call("createHistory", {
           foreignId: id,
           module: 'Customer',
           type: 'RECORD_CREATED',
           properties: properties
       });

      return id;
  },

  updateCustomer: function(id, properties)
  {
    let record = Object.assign({
                                   modifiedByUserId: Meteor.userId(),
                                   modifiedOn: new Date()
                               }, properties);

    Customers.update({_id: id}, {$set: record})

    Meteor.call("createHistory", {
            foreignId: id,
            module: 'Customer',
            type: 'RECORD_UPDATED',
            properties: properties
        });
  },
  setActive: function(id, active)
    {
        Customers.update({_id: id}, {$set: {active: active}});

        let record = {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date(),
            active:active
         };

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Customer',
                type: 'RECORD_UPDATED',
                properties: record
            });

    }
});
