import { Meteor } from 'meteor/meteor';
import { Locations } from '../collection.js';

Meteor.publish("locations", function() {
	return Locations.find({});
})
