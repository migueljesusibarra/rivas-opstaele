import { Mongo } from 'meteor/mongo';
import { Locations } from './collection';

Meteor.methods({
    addLocation: function(properties)
   {
     let user = Meteor.user();
     let id = Random.id();

     let record = {
         _id: id,
         description: properties,
         createdByUserId: Meteor.userId(),
         createdOn: new Date(),
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
     };

     Locations.insert(record);
     return id;
   },
   updateLocation: function(id, properties)
   {
     let user = Meteor.user();
     let record = Object.assign(properties,
     {
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
     });

       Locations.update({_id: id}, {$set: {description: properties}});
   },
   removeLocation: function(id)
   {
      Locations.remove({_id: id});
   }
});
