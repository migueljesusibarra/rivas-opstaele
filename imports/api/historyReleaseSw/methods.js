import { Mongo } from 'meteor/mongo';
import { HistoryReleaseSw } from './collection';

export const getHistoryReleaseSwId = function()
{
    let record = HistoryReleaseSw.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addHistoryReleaseSw: function(properties)
    {
        let id = getHistoryReleaseSwId();
        let user = Meteor.user();

        let record = Object.assign({
        _id: id,
        index: parseInt(id),
        createdByUserId: Meteor.userId(),
        createdOn: new Date(),
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
        }, properties);

        HistoryReleaseSw.insert(record);

        return id;
    }
});
