import { Meteor } from 'meteor/meteor';
import { HistoryReleaseSw } from '../collection.js';

Meteor.publish("historyReleaseSw", function(condition)
{
	return HistoryReleaseSw.find(condition || {});
});
