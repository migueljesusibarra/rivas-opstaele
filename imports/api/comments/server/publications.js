import { Meteor } from 'meteor/meteor';
import { Comments } from '../collection.js';

Meteor.publish("comments", function() {
	return Comments.find({});
})
