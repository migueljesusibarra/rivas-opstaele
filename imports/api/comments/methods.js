import { Mongo } from 'meteor/mongo';
import { Comments } from './collection';

Meteor.methods({
  addComment: function(properties)
   {
      let user = Meteor.user();
      let id = properties._id;

      let record = Object.assign({
              createdBy: {
                  id: user._id,
                  username: user.username,
                  name: user.name
              },
              createdOn: new Date()

             }, properties);

      Comments.insert(record);

       return id;
   },
  updateAttachments: function(id, attachments)
   {
      Comments.update({_id: id}, {$set: {attachments: attachments}});

   },

   removeCollectionComment: function(foreignId, collection)
   {
      let comments = Comments.find({foreignId: foreignId, collection: collection}).fetch();

      for (var i = 0; i < comments.length; i++)
         {
             removeComment(comments[i]._id);
         }

    },

   deleteComment: function(id)
    {
      removeComment(id);
    }
    
  });


export const removeComment = function(id)
{
  let comment = Comments.findOne({_id: id});
  let $inc = {};

  if(comment.attachments != null && comment.attachments.length > 0)
      $inc["attachments"] = -(comment.attachments.length);

  if(comment.text != null && comment.text.length > 0)
      $inc["comments"] = -1;

  Comments.remove({_id: id});

  if(comment != null && comment.attachments != null && comment.attachments.length > 0 && Meteor.settings)
  {

      if(Meteor.isServer)
      {
          AWS.config.update({
              accessKeyId: Meteor.settings.AWSAccessKeyId,
              secretAccessKey: Meteor.settings.AWSSecretAccessKey
          });

          let s3 = new AWS.S3();

          let params = {
              Bucket: 'rivas-opstaele'
          };

          comment.attachments.forEach(function(attachment){
              if(attachment.url)
              {
                  // Get the key from the attachment url
                  let key = 'attachments' + attachment.url.split('attachments')[1];

                  s3.deleteObject(Object.assign({Key: key}, params), function(error, data){
                      if(error)
                          console.log(error);
                      else
                          console.log(data);
                  });
              }
          });
      }
  }
};
