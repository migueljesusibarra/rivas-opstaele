import { Mongo } from 'meteor/mongo';
import { TestsPerformed } from './collection';

export const getNextIndex = function()
{
    let record = TestsPerformed.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 0;

};

Meteor.methods({
    addTestPerformed: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign({
        _id: id,
        index: getNextIndex(),
        createdByUserId: Meteor.userId(),
        createdOn: new Date(),
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
        }, properties);

        TestsPerformed.insert(record);
        return id;
    },
    updateTestPerformed: function(id, properties)
    {
        let user = Meteor.user();
        let record = Object.assign(properties,
        {
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()
        });

        TestsPerformed.update({_id: id}, {$set: properties});
    },
    removeTestPerformed: function(id)
    {
        TestsPerformed.remove({_id: id});
    }
});
