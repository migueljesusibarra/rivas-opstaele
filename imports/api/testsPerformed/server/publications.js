import { Meteor } from 'meteor/meteor';
import { TestsPerformed } from '../collection.js';

Meteor.publish("testsPerformed", function(condition)
{
	return TestsPerformed.find(condition || {});
})
