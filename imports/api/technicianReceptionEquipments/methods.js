import { Meteor } from 'meteor/meteor';
import { TechnicianReceptionEquipments } from './collection';

export const getNextTechnicianreceptionEquipmentId = function()
{
    let record = TechnicianReceptionEquipments.findOne({}, {sort:{index: -1}});

    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextTechnicianReceptionEquipmentPosId = function(receptionEquipmentId)
{
    let record = TechnicianReceptionEquipments.findOne({receptionEquipmentId: receptionEquipmentId}, {sort:{index: -1}});

    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addTechnicianReceptionEquipments: function(technicianReceptionEquipments)
  {
      let user = Meteor.user();

      for(let technicianReceptionEquipment of technicianReceptionEquipments)
      {
          let id = getNextTechnicianreceptionEquipmentId();
          let pos = getNextTechnicianReceptionEquipmentPosId(technicianReceptionEquipment.receptionEquipmentId)

          let record = Object.assign({
              _id: id,
              index: parseInt(id),
              pos:pos,
              createdByUserId: Meteor.userId(),
              createdOn: new Date(),
              modifiedByUserId: Meteor.userId(),
              modifiedOn: new Date()
          },technicianReceptionEquipment);

          TechnicianReceptionEquipments.insert(record);
      }

      return technicianReceptionEquipments;
  },
  removeTechnicianReceptionEquipments: function(id)
  {
     TechnicianReceptionEquipments.remove({_id: id});
  }
});
