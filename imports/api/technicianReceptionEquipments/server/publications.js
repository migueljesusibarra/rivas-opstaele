import { Meteor } from 'meteor/meteor';
import { TechnicianReceptionEquipments } from '../collection.js';

Meteor.publish("technicianReceptionEquipments", function(condition) {
	return TechnicianReceptionEquipments.find(condition || {});
})
