import { Mongo } from 'meteor/mongo';
import { PurchaseOrderDetails } from './collection';

export const getNextPurchaseOrderDetailId = function()
{
     let record = PurchaseOrderDetails.findOne({}, {sort:{index: -1}});

     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextDetailPosId = function(purchaseOrderId)
{
     let record = PurchaseOrderDetails.findOne({purchaseOrderId: purchaseOrderId}, {sort:{index: -1}});

     return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addPurchaseOrderDetail: function(properties)
   {
       let user = Meteor.user();
       let id = getNextPurchaseOrderDetailId();
       let pos = getNextDetailPosId(properties.purchaseOrderId) 

       let record = Object.assign({
                       _id: id,
                       index: parseInt(id),
                       pos:pos,
                       createdByUserId: Meteor.userId(),
                       createdOn: new Date(),
                       modifiedByUserId: Meteor.userId(),
                       modifiedOn: new Date()
                   },properties);

       PurchaseOrderDetails.insert(record);

       return id;
   },

  addBacthPurchaseOrderDetail: function(propertiesArray)
    {
      propertiesArray.forEach(function(properties)
      {
         let user = Meteor.user();
         let id = getNextPurchaseOrderDetailId();
         let pos = getNextDetailPosId(properties.purchaseOrderId)

         let record = Object.assign({
                         _id: id,
                         index: parseInt(id),
                         pos:pos,
                         createdByUserId: Meteor.userId(),
                         createdOn: new Date(),
                         modifiedByUserId: Meteor.userId(),
                         modifiedOn: new Date()
                     },properties);

          PurchaseOrderDetails.insert(record);

      });
    },

  updatePurchaseOrderDetail: function(id, properties)
   {       
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
       });

       PurchaseOrderDetails.update({_id: id}, {$set: properties});
   },
  removePurchaseOrderDetail: function(id)
   {
       PurchaseOrderDetails.remove({_id: id});
   },

  removeAllPurchaseOrderDetail: function(purchaseOrderId)
   {
       PurchaseOrderDetails.remove({purchaseOrderId: purchaseOrderId});
   }

});
