import { Meteor } from 'meteor/meteor';
import { PurchaseOrderDetails } from '../collection.js';

Meteor.publish("purchaseOrderDetails", function() {
	return PurchaseOrderDetails.find({});
})
