import { Mongo } from 'meteor/mongo';
import { PurchaseOrders } from './collection';

export const getNextPurchaseOrderId = function()
{
     let record = PurchaseOrders.findOne({}, {sort:{index: -1}});

     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
   addPurchaseOrder: function(properties)
   {
       let user = Meteor.user();
       let id = getNextPurchaseOrderId();

       let record = Object.assign({
                       _id: id,
                       index: parseInt(id),
                       createdByUserId: Meteor.userId(),
                       createdOn: new Date(),
                       modifiedByUserId: Meteor.userId(),
                       modifiedOn: new Date(),
                       status:"In-Process",
                       totalAmount: 0,
                       hasReceptionEquipment: false
                   },properties);

       PurchaseOrders.insert(record);

       Meteor.call("createHistory", {
            foreignId: id,
            module: 'PurchaseOrder',
            type: 'RECORD_CREATED',
            properties: properties
        });

       return id;
   },
   updatePurchaseOrder: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       PurchaseOrders.update({_id: id}, {$set: properties});

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'PurchaseOrder',
               type: 'RECORD_UPDATED',
               properties: properties
           });
   },
   removePurchaseOrder: function(id)
   {
       PurchaseOrders.remove({_id: id});
   },

   setApprove: function(id, status)
   {
         PurchaseOrders.update({_id: id}, {$set: {status: status}});

        let record = {
                         modifiedByUserId: Meteor.userId(),
                         modifiedOn: new Date(),
                         status: status
                     };

         Meteor.call("createHistory", {
                 foreignId: id,
                 module: 'PurchaseOrder',
                 type: 'RECORD_UPDATED',
                 properties: record
             });

         return status;
   },

   setReceptionEquipment: function(id, receptionEquipment)
   {
       PurchaseOrders.update({_id: id}, {$set: {hasReceptionEquipment: receptionEquipment}});

       let record = {
                       modifiedByUserId: Meteor.userId(),
                       modifiedOn: new Date(),
                       hasReceptionEquipment: receptionEquipment
                   };

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'PurchaseOrder',
               type: 'RECORD_UPDATED',
               properties: record
           });
   },

   setPurchaseOrderTotalAmount : function(id, totalAmount)
   {
       PurchaseOrders.update({_id: id}, {$set: {totalAmount: totalAmount}});

       let record = {
                       modifiedByUserId: Meteor.userId(),
                       modifiedOn: new Date(),
                       totalAmount: totalAmount
                   };

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'PurchaseOrder',
               type: 'RECORD_UPDATED',
               properties: record
           });
   }
  });
