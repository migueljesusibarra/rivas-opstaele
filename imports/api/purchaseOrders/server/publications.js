import { Meteor } from 'meteor/meteor';
import { PurchaseOrders } from '../collection.js';
//GET LIST
Meteor.publish("purchaseOrders", function(parameters)
{
  return PurchaseOrders.find(parameters.condition || {});
})
//GET EDIT
Meteor.publish("purchaseOrder", function(id)
{
	return PurchaseOrders.find({_id:id});
})
