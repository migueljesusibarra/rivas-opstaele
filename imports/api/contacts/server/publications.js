import { Meteor } from 'meteor/meteor';
import { Contacts } from '../collection.js';

Meteor.publish("contacts", function(condition) {
	return Contacts.find(condition || {});
})
