import { Mongo } from 'meteor/mongo';
import { Contacts } from './collection';

Meteor.methods({
    addContact: function(properties)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = Object.assign({
                                        _id: id,
                                        createdByUserId: Meteor.userId(),
                                        createdOn: new Date(),
                                        modifiedByUserId: Meteor.userId(),
                                        modifiedOn: new Date()
                                    }, properties);

        Contacts.insert(record);

        Meteor.call("createHistory", {
             foreignId: id,
             module: 'Contact',
             channel: "PC",
             type: 'RECORD_CREATED',
             properties: properties
         });

        return id;
    },
    updateContact: function(id, properties)
    {
        let user = Meteor.user();
        
        let record = Object.assign(properties,
        {
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        });

        Contacts.update({_id: id}, {$set: properties});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Contact',
                channel: "PC",
                type: 'RECORD_UPDATED',
                properties: properties
            });
    },
    removeContact: function(id)
    {
        Contacts.remove({_id: id});
    }
});
