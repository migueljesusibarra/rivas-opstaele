export const Templates = {
    getBasicTemplateEmail: function(to, from, subject, text)
    {
        let html = `<p style="color:#004990;font-size:20px;">Rivas Opstaele</p>
                    <p style="color:#004990;font-size:13px;"><a href="http://rivasopstaele.com/" style="padding-left:5px;color:red;">here</a></p>
                    <div>
                        <span style="color:#004990;font-size:13px;">To: </span>
                        <span style="color:#004990;font-size:13px;">${to}</span>
                    </div>
                    <div>
                        <span style="color:#004990;font-size:13px;">From: </span>
                        <span style="color:#004990;font-size:13px;">${from}</span>
                    </div>
                    <img style="padding-top:50px;width:100px;" alt="logo" src="/img/logo2.png"/>
                    <div>
                        <span style="color:gray;font-size:11px;padding-top:30px;padding-bottom:10px;">© 2009 - 2016 AmeriFinance, Inc. | All Rights Reserved</span>
                    </div>`;

        return {
            to: to,
            from: from,
            subject: subject,
            html: html
        };
    }
};
