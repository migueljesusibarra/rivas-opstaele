import { Mongo } from 'meteor/mongo';
import { CustomerTypes } from './collection';

Meteor.methods({
  addCustomerType: function(properties)
 {
    let user = Meteor.user();
    let id = Random.id();

    let record = Object.assign({
          _id: id,
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
    }, properties);


    CustomerTypes.insert(record);

    return id;
 },
 updateCustomerType: function(id, properties)
 {
    let user = Meteor.user();
    let record = Object.assign(properties,
    {
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
    });

    CustomerTypes.update({_id: id}, {$set: properties});
 },
 removeCustomerType: function(id)
 {
    CustomerTypes.remove({_id: id});
 }

});
