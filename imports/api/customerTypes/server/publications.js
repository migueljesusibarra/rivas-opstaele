import { Meteor } from 'meteor/meteor';
import { CustomerTypes } from '../collection.js';

Meteor.publish("customerTypes", function() {
	return CustomerTypes.find({});
});
