import { Meteor } from 'meteor/meteor';
import { Equipments } from '../collection.js';

Meteor.publish("equipments", function() {
	return Equipments.find({});
});
