import { Mongo } from 'meteor/mongo';
import { Equipments } from './collection';

export const getNextEquipmentId = function()
{
    let record = Equipments.findOne({}, {sort:{index: -1}});
    return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

Meteor.methods({
  addEquipment: function(properties)
   {
       let id = getNextEquipmentId();
       let user = Meteor.user();

       let record = Object.assign({
           _id: id,
           index: parseInt(id),
           createdByUserId: Meteor.userId(),
           createdOn: new Date(),
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
       }, properties);

       Equipments.insert(record);

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'Equipment',
               channel: "PC",
               type: 'RECORD_CREATED',
               properties: properties
           });


       return id;
   },
   updateEquipment: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
         modifiedByUserId: Meteor.userId(),
         modifiedOn: new Date()
       });

       Equipments.update({_id: id}, {$set: record});

       Meteor.call("createHistory", {
               foreignId: id,
               module: 'Equipment',
               channel: "PC",
               type: 'RECORD_UPDATED',
               properties: properties
           });
   },
   setOutOfService: function(id, outOfService)
   {
      properties = {
                        outOfService: outOfService,
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date()
                    }

      Equipments.update({_id: id}, {$set: properties });

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'Equipment',
              channel: "PC",
              type: 'RECORD_UPDATED',
              properties: properties
          });
   },

   setInContract: function(id, inContract, contractId, contractNumber)
   {
      properties = {
                        inContract: inContract,
                        contractId: contractId,
                        contractNumber: contractNumber,
                        modifiedByUserId: Meteor.userId(),
                        modifiedOn: new Date()
                    }

      Equipments.update({_id: id}, {$set: properties });

      Meteor.call("createHistory", {
              foreignId: id,
              module: 'Equipment',
              channel: "PC",
              type: 'RECORD_UPDATED',
              properties: properties
          });
   }
});
