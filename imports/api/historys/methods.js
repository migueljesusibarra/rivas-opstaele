import { Mongo } from 'meteor/mongo';
import { Historys } from './collection';

Meteor.methods({
    createHistory: function(properties)
    {
        let user = Meteor.user();
        let userDetail = {
            createdByUserId: user._id,
            createByUsername: user.username,
            createdBy: user.profile.name
        };

        let record = Object.assign(properties, {
            _id: Random.id(),
            userDetail,
            createdOn: new Date()
        });

        Historys.insert(record);
    }
});
