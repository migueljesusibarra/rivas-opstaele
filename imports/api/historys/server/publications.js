import { Meteor } from 'meteor/meteor';
import { Historys } from '../collection.js';

Meteor.publish("historys", function(condition)
{
	return Historys.find(condition || {});
});
