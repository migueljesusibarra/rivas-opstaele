import { Mongo } from 'meteor/mongo';
import { Brands } from './collection';

export const getNextIndex = function()
{
    let record = Brands.findOne({}, {sort:{index: -1}});

    if(record)
    {
        let newIndex = parseInt(record.index || 0) + 1;

        return newIndex;
    }
    else
        return 1;

};

Meteor.methods({
    addBrand: function(description)
    {
        let user = Meteor.user();
        let id = Random.id();

        let record = {
            _id: id,
            index: getNextIndex(),
            description: description,
            createdByUserId: Meteor.userId(),
            createdOn: new Date(),
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        };

        Brands.insert(record);

        Meteor.call("createHistory", {
             foreignId: id,
             module: 'Brand',
             type: 'RECORD_CREATED',
             properties: record
         });

        return id;
    },
    updateBrand: function(id, description)
    {
        let user = Meteor.user();
        let record = {
            description: description,
            modifiedByUserId: Meteor.userId(),
            modifiedOn: new Date()
        };

        Brands.update({_id: id}, {$set: record});

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'Brand',
                type: 'RECORD_UPDATED',
                properties: record
            });
    },
    removeBrand: function(id)
    {
        Brands.remove({_id: id});
    }
});
