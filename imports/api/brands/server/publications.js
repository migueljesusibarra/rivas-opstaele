import { Meteor } from 'meteor/meteor';
import { Brands } from '../collection.js';

Meteor.publish("brands", function(condition) {
	return Brands.find(condition || {});
})
