import { Meteor } from 'meteor/meteor';
import { PreOrders } from '../collection.js';

Meteor.publish("preOrders", function(parameters)
{
  return PreOrders.find(parameters.condition || {});
})

Meteor.publish("calendars", function(parameters)
{
  return PreOrders.find(parameters.condition || {}, parameters.fields);
})
