import { Mongo } from 'meteor/mongo';
import { PreOrders } from './collection';
import { Util } from '/lib/common.js';

export const getNextPreOrdenId = function()
{
     let record = PreOrders.findOne({}, {sort:{index: -1}});
     return ((record != null ? parseInt(record._id) || 0 : 0) + 1).toString();
};

export const getNextPreOrdenPosId = function(receptionEquipmentId)
{
     let record = PreOrders.findOne({receptionEquipmentId: receptionEquipmentId}, {sort:{index: -1}});
     return ((record != null ? parseInt(record.pos) || 0 : 0) + 1).toString();
};

Meteor.methods({
    addPreOrder: function(properties)
    {
        let user = Meteor.user();
        let id = getNextPreOrdenId();
        let pos = getNextPreOrdenPosId(properties.receptionEquipmentId)

        let record = Object.assign({
                            _id: id,
                            index: parseInt(id),
                            pos: parseInt(pos),
                            createdByUserId: Meteor.userId(),
                            createdOn: new Date(),
                            modifiedByUserId: Meteor.userId(),
                            modifiedOn: new Date()
                        }, properties);


        PreOrders.insert(record);

        Meteor.call("createHistory", {
                foreignId: id,
                module: 'PreOrder',
                channel: "PC",
                type: 'RECORD_CREATED',
                properties: properties
            });

        return id;
   },
   addPreOrders: function(warranty, frequency, date, properties)
   {
      let x = warranty/frequency;

       for(i=0; i < x; i++)
       {
           date = Util.sumMonthDate(date, frequency);
           let day = date.getDay();
           //los dias que necesito aumentar son 1 o2 si es sabado 2 domingo 1
           let maxDay = (day == 0 ? 1 : 2);

           let user = Meteor.user();
           let id = getNextPreOrdenId();
           let pos = getNextPreOrdenPosId(properties.receptionEquipmentId)

           let record = Object.assign({
                  _id: id,
                  index: parseInt(id),
                  pos: parseInt(pos),
                  date: day == 6 || day == 0 ? Util.sumDayDate(date, maxDay) : date,
                  createdByUserId: Meteor.userId(),
                  createdOn: new Date(),
                  modifiedByUserId: Meteor.userId(),
                  modifiedOn: new Date()
              }, properties);

           PreOrders.insert(record);

           Meteor.call("createHistory", {
                   foreignId: id,
                   module: 'PreOrder',
                   channel: "PC",
                   type: 'RECORD_CREATED',
                   properties: properties
               });
      }

  },
  updatePreOrder: function(id, properties)
   {
       let user = Meteor.user();
       let record = Object.assign(properties,
       {
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
       });

       PreOrders.update({_id: id}, {$set: properties});

       Meteor.call("createHistory", {
                       foreignId: id,
                       module: 'PreOrder',
                       channel: "PC",
                       type: 'RECORD_UPDATED',
                       properties: properties
                   });
   },

   updatePreOrderCalendar(id, date)
   {
      properties = {
           date: date,
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
       }

      PreOrders.update({_id: id}, {$set: properties});

      Meteor.call("createHistory", {
                      foreignId: id,
                      module: 'PreOrder',
                      channel: "PC",
                      type: 'RECORD_UPDATED',
                      properties: properties
                  });
   },

   updatePreOrderisService(id, isService)
   {
       properties = {
           isService: isService,
           modifiedByUserId: Meteor.userId(),
           modifiedOn: new Date()
       }

      PreOrders.update({_id: id}, {$set: properties});

      Meteor.call("createHistory", {
                      foreignId: id,
                      module: 'PreOrder',
                      channel: "PC",
                      type: 'RECORD_UPDATED',
                      properties: properties
                  });
   },

   removePreOrder: function(id)
   {
      PreOrders.remove({_id: id});
   }

});
