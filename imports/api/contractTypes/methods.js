import { Mongo } from 'meteor/mongo';
import { ContractTypes } from './collection';

Meteor.methods({
 addContractType: function(properties)
   {
      let user = Meteor.user();
      let id = Random.id();

      let record = Object.assign({
          _id: id,
          createdByUserId: Meteor.userId(),
          createdOn: new Date(),
          modifiedByUserId: Meteor.userId(),
          modifiedOn: new Date()
      }, properties);

      ContractTypes.insert(record);

      return id;
   },
 updateContractType: function(id, properties)
   {
      let user = Meteor.user();
      let record = Object.assign(properties,
      {
        modifiedByUserId: Meteor.userId(),
        modifiedOn: new Date()

      });

       ContractTypes.update({_id: id}, {$set: properties});
   },
  removeContractType: function(id)
   {
      ContractTypes.remove({_id: id});
   }
});
