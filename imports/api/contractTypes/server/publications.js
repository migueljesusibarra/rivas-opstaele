import { Meteor } from 'meteor/meteor';
import { ContractTypes } from '../collection.js';

Meteor.publish("contractTypes", function() {
	return ContractTypes.find({});
})
