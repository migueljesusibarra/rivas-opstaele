import { Mongo } from 'meteor/mongo';
import { Accounts } from 'meteor/accounts-base';
import { Email } from 'meteor/email';
import { Templates } from '/imports/api/templates';

export const insertStatus = function(id,active)
{
    Meteor.users.update({_id: id}, {$set: {active: active}});
};

export const validateUserLogin = function(id)
{
    let user = Meteor.users.find({_id:id, active:true}).fetch();
    let status = user.active === null ? false:true;

    return status
};


Meteor.methods({
    addUser: function(properties)
    {
        // At this point first name and last name must not be empty
        let firstName = properties.firstName.trim();
        let lastName = properties.lastName.trim();

        let user = {
            username: properties.username,
            email: properties.email,
            password: properties.password,
            profile:
            {
                name: firstName + ' ' + lastName,
                firstName: firstName,
                lastName: lastName,
                phone: properties.phone,
                email: properties.email,
                notes: properties.notes,
                type: properties.type
            }};

        let userId = Accounts.createUser(user);

        insertStatus(userId,true);

        return userId;
    },
    validateUser:function(id)
    {
        return validateUserLogin(id);
    },
    updateUser: function(id, properties)
    {
        let firstName = properties.firstName.trim();
        let lastName = properties.lastName.trim();
        let profile = Meteor.users.findOne({_id: id}).profile;

        let user = {
            username: properties.username,
            email: properties.email,
            profile:
            {
                firstName: firstName,
                lastName: lastName,
                name: firstName + ' ' + lastName,
                phone: properties.phone,
                email: properties.email,
                notes: properties.notes,
                type: properties.type
            }};

        Meteor.users.update({_id: id}, {$set: user});

        if(Meteor.isServer)
        {
            if(properties.password != null && properties.password.length > 0)
                Accounts.setPassword(id, properties.password);
        }
    },
    setUserStatus: function(id, active)
    {
        Meteor.users.update({_id: id}, {$set: {active: active}});
    },
    sendEmail: function(to, subject, text)
    {
    // Let other method calls from the same client start running, without
    // waiting for the email sending to complete.
    this.unblock();

    if(Meteor.isServer)
        Email.send(Templates.getBasicTemplateEmail(to, "no-reply@kodein.io", subject, text));
    }
});
