import React from 'react';
import { Meteor } from 'meteor/meteor';
import { mount } from 'react-mounter';
import { Accounts } from 'meteor/accounts-base';
import { Login } from '../../ui/layouts/login';
import { AppContainer } from '../../ui/containers/App.js';
import { AppReportContainer } from '../../ui/containers/AppReport.js';
import { NavigationContainer } from '../../ui/containers/NavigationContainer.js';
import { CustomerContainerList } from '../../ui/containers/CustomerContainerList.js';
import { CustomerContainerForm } from '../../ui/containers/CustomerContainerForm.js';
import { EquipmentContainerList } from '../../ui/containers/EquipmentContainerList.js';
import { EquipmentContainerForm } from '../../ui/containers/EquipmentContainerForm.js';
import { InstallEquipmentContainerList } from '../../ui/containers/InstallEquipmentContainerList.js';
import { Aside } from '../../ui/layouts/Aside';
import { BrandContainer } from '../../ui/containers/BrandContainer.js';
import { ModelContainer } from '../../ui/containers/ModelContainer.js';
import { LocationContainer } from '../../ui/containers/LocationContainer.js';
import { SystemContainer } from '../../ui/containers/SystemContainer.js';
import { VerificationContainer } from '../../ui/containers/VerificationContainer.js';
import { PartContainer } from '../../ui/containers/PartContainer.js';
import { VendorContainerList } from '../../ui/containers/VendorContainerList.js';
import { VendorContainerForm } from '../../ui/containers/VendorContainerForm.js';
import { CustomerTypeContainer } from '../../ui/containers/CustomerTypeContainer.js';
import { TechnicianContainerList } from '../../ui/containers/TechnicianContainerList.js';
import { TechnicianContainerForm } from '../../ui/containers/TechnicianContainerForm.js';
import { PeriodContainer } from '../../ui/containers/PeriodContainer.js';
import { ReceptionEquipmentContainerList } from '../../ui/containers/ReceptionEquipmentContainerList.js';
import { ReceptionEquipmentContainerForm } from '../../ui/containers/ReceptionEquipmentContainerForm.js';
import { RReceptionEquipmentContainer } from '../../ui/containers/ReceptionEquipmentContainerForm.js';
import { ContractContainerList } from '../../ui/containers/ContractContainerList.js';
import { ContractContainerForm } from '../../ui/containers/ContractContainerForm.js';
import { ContractTypeContainer } from '../../ui/containers/ContractTypeContainer.js';
import { MissingContainer } from '../../ui/containers/MissingContainer.js';
import { TestPerformedContainer } from '../../ui/containers/TestPerformedContainer.js';
import { RoleContainer } from '../../ui/containers/RoleContainer.js';
import { UserContainerList } from '../../ui/containers/UserContainerList.js';
import { UserContainerForm } from '../../ui/containers/UserContainerForm.js';
import { PurchaseOrderSystemContainerList } from '../../ui/containers/PurchaseOrderSystemContainerList.js';
import { PurchseOrderSystemContainerForm } from '../../ui/containers/PurchseOrderSystemContainerForm.js';
import { PurchaseOrderPartContainerList } from '../../ui/containers/PurchaseOrderPartContainerList.js';
import { PurchseOrderPartContainerForm } from '../../ui/containers/PurchseOrderPartContainerForm.js';
import { ServiceOrderContainerList } from '../../ui/containers/ServiceOrderContainerList.js';
import { ServiceOrderContainerForm } from '../../ui/containers/ServiceOrderContainerForm.js';
import { RServiceOrderContainer } from '../../ui/containers/ServiceOrderContainerForm.js';
import { PreOrderListContainer } from '/imports/ui/containers/PreOrderContainer.js';
import { OrderTypeContainer } from '/imports/ui/containers/OrderTypeContainer.js';
import { ActivityContainer } from '/imports/ui/containers/ActivityContainer.js';
import { ProtocolContainerList } from '/imports/ui/containers/ProtocolContainerList.js';
import { CalendarContainer } from '/imports/ui/containers/CalendarContainer.js';
import { RCalendarContainer } from '/imports/ui/containers/CalendarContainer.js';
import { ReportContainer } from '/imports/ui/containers/ReportContainer.js';
//import { RReceptionEquipmentContainer } from '/imports/ui/containers/ReportContainer.js';
import { EmailContainer } from '/imports/ui/containers/EmailContainer.js';
import { ServiceProgramContainer } from '/imports/ui/containers/ServiceProgramContainer.js';
import { RServiceProgramContainer } from '/imports/ui/containers/ServiceProgramContainer.js';
import { PieWithLegendContainer } from '/imports/ui/containers/ChartContainer.js';


    Accounts.config({
        sendVerificationEmail: true,
        forbidClientAccountCreation: false
    });

    Accounts.onLogin(function() {

      let router = FlowRouter.current() || {};
      let path = router.path;

      // we only do it if the user is in the login page
      if(path === "/login"){
        FlowRouter.go("/");
    }
    });

    FlowRouter.triggers.enter([function(context,redirect){
      if(!Meteor.userId()) {
          FlowRouter.go('login');
      }
    }]);

    FlowRouter.route('/login', {
        name: 'login',
        action: function(params, query){
          Accounts._loginButtonsSession.set('dropdownVisible', true);
          mount(AppContainer, {
            content: <Login />
        });
      }
    });

    FlowRouter.route('/', {
        name: 'default',
        action: function(params, query){
            FlowRouter.go('/customers');
        }
    });

    FlowRouter.route('/customers', {
        name: 'customers',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <CustomerContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/customer/:id', {
        name: 'customer.detail',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer />,
                content: <CustomerContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/equipments', {
        name: 'equipments.detail',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <EquipmentContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/installequipments', {
        name: 'installequipments.detail',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <InstallEquipmentContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/preordens', {
        name: 'preordens',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PreOrderListContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/vendors', {
        name: 'vendors',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <VendorContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/vendor/:id', {
        name: 'vendor',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <VendorContainerForm  id={params.id}/>,
                aside: <Aside />
            });
        }
    });



    FlowRouter.route('/equipment/:id', {
        name: 'equipment',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <EquipmentContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/brands', {
        name: 'brands',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <BrandContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/models', {
        name: 'models',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ModelContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/locations', {
       name: 'locations',
       action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <LocationContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/verifications', {
        name: 'verifications',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <VerificationContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/systems', {
        name: 'systems',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <SystemContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/purchaseordersystems', {
        name: 'purchaseordersystem',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PurchaseOrderSystemContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/purchaseorderparts', {
        name: 'purchaseorderpart',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PurchaseOrderPartContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/purchaseordersystem/:id', {
        name: 'purchaseorder.system',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer />,
                content: <PurchseOrderSystemContainerForm  id={params.id} />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/purchaseorderpart/:id', {
        name: 'purchaseorder.part',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer />,
                content: <PurchseOrderPartContainerForm  id={params.id} />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/parts', {
        name: 'parts',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PartContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/customertypes', {
        name: 'customertypes',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <CustomerTypeContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/technicians', {
        name: 'technicians',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <TechnicianContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/technician/:id', {
        name: 'technician',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <TechnicianContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });


    FlowRouter.route('/periods', {
        name: 'period',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PeriodContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/receptionequipments', {
        name: 'receptionequipments',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ReceptionEquipmentContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/serviceorders', {
        name: 'serviceorders',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ServiceOrderContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/serviceorder/:id', {
        name: 'serviceorder',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ServiceOrderContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/protocols', {
        name: 'protocols',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ProtocolContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/calendar', {
        name: 'calendar',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <CalendarContainer startDate={query.start} endDate={query.end} customerId={query.customerId} technicianId={query.technicianId} />,
                aside: <Aside />
            });
        }
    });



    FlowRouter.route('/receptionequipment/:id', {
        name: 'receptionequipment',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ReceptionEquipmentContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/contract/:id', {
        name: 'contract',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ContractContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/contracts', {
        name: 'contracts',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ContractContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/contracttypes', {
        name: 'contracttypes',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ContractTypeContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/missings', {
        name: 'missings',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <MissingContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/testsPerformed', {
        name: 'testsPerformed',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <TestPerformedContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/orderTypes', {
        name: 'orderTypes',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <OrderTypeContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/activities', {
        name: 'activities',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ActivityContainer />,
                aside: <Aside />
            });
        }
    });


    FlowRouter.route('/roles', {
        name: 'roles',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <RoleContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/reportreceptionequipment/:id', {
        name: 'reportreceptionequipment',
        action: function(params, query)
        {
            mount(AppReportContainer, {
                    content: <RReceptionEquipmentContainer id={params.id}/>
            });
        }
    });

    FlowRouter.route('/reportserviceorder/:id', {
        name: 'reportserviceorder',
        action: function(params, query)
        {
            mount(AppReportContainer, {
                    content: <RServiceOrderContainer id={params.id}/>
            });
        }
    });

    FlowRouter.route('/reportserviceprogram/:id', {
        name: 'reportserviceprogram',
        action: function(params, query)
        {
            mount(AppReportContainer, {
                    content: <RServiceProgramContainer id={params.id}/>
            });
        }
    });

    FlowRouter.route('/reportcalendar', {
        name: 'reportcalendar',
        action: function(params, query)
        {
            mount(AppReportContainer, {
                    content: <RCalendarContainer />
            });
        }
    });

    FlowRouter.route('/report', {
        name: 'report',
        action: function(params, query)
        {
            mount(AppContainer, {
                   navigation: <NavigationContainer/>,
                   content: <ReportContainer id={query.id} module={query.module} />,
                   aside: <Aside />

            });
        }
    });

    FlowRouter.route('/users', {
        name: 'users',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <UserContainerList />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/user/:id', {
        name: 'user.detail',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer  />,
                content: <UserContainerForm id={params.id}/>,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/emails', {
        name: 'emails',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <EmailContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/chartequipment', {
        name: 'chartequipment',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <PieWithLegendContainer />,
                aside: <Aside />
            });
        }
    });

    FlowRouter.route('/serviceprograms/:id', {
        name: 'serviceprograms',
        action: function(params, query)
        {
            mount(AppContainer, {
                navigation: <NavigationContainer/>,
                content: <ServiceProgramContainer id={params.id}/>,
                aside: <Aside />
            });
        }
    });
