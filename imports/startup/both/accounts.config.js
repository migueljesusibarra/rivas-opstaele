import { Accounts } from 'meteor/accounts-base';

if(Accounts.ui != null)
{
    Accounts.ui.config({
        passwordSignupFields: 'USERNAME_ONLY'
    });
}
