// Register your apis here

import '../../api/links/methods.js';
import '../../api/links/server/publications.js';

import '../../api/equipments/methods.js';
import '../../api/equipments/server/publications.js';

import '../../api/customers/methods.js';
import '../../api/customers/server/publications.js';

import '../../api/brands/methods.js';
import '../../api/brands/server/publications.js';

import '../../api/locations/methods.js';
import '../../api/locations/server/publications.js';

import '../../api/models/methods.js';
import '../../api/models/server/publications.js';

import '../../api/systems/methods.js';
import '../../api/systems/server/publications.js';

import '../../api/verifications/methods.js';
import '../../api/verifications/server/publications.js';

import '../../api/contacts/methods.js';
import '../../api/contacts/server/publications.js';

import '../../api/purchaseOrders/methods.js';
import '../../api/purchaseOrders/server/publications.js';

import '../../api/purchaseOrderDetails/methods.js';
import '../../api/purchaseOrderDetails/server/publications.js';

import '../../api/purchaseOrderParts/methods.js';
import '../../api/purchaseOrderParts/server/publications.js';

import '../../api/parts/methods.js';
import '../../api/parts/server/publications.js';

import '../../api/vendors/methods.js';
import '../../api/vendors/server/publications.js';

import '../../api/customerTypes/methods.js';
import '../../api/customerTypes/server/publications.js';

import '../../api/technicians/methods.js';
import '../../api/technicians/server/publications.js';

import '../../api/periods/methods.js';
import '../../api/periods/server/publications.js';

import '../../api/receptionEquipments/methods.js';
import '../../api/receptionEquipments/server/publications.js';

import '../../api/configurations/methods.js';
import '../../api/configurations/server/publications.js';

import '../../api/contractTypes/methods.js';
import '../../api/contractTypes/server/publications.js';

import '../../api/contracts/methods.js';
import '../../api/contracts/server/publications.js';

import '../../api/missings/methods.js';
import '../../api/missings/server/publications.js';

import '../../api/testsPerformed/methods.js';
import '../../api/testsPerformed/server/publications.js';

import '../../api/receptionEquipmentTestPerformeds/methods.js';
import '../../api/receptionEquipmentTestPerformeds/server/publications.js';

import '../../api/roles/methods.js';
import '../../api/roles/server/publications.js';

import '../../api/users/methods.js';
import '../../api/users/server/publications.js';

import '../../api/comments/methods.js';
import '../../api/comments/server/publications.js';

import '../../api/preOrders/methods.js';
import '../../api/preOrders/server/publications.js';

import '../../api/serviceOrders/methods.js';
import '../../api/serviceOrders/server/publications.js';

import '../../api/activities/methods.js';
import '../../api/activities/server/publications.js';

import '../../api/activitiesService/methods.js';
import '../../api/activitiesService/server/publications.js';

import '../../api/serviceOrderParts/methods.js';
import '../../api/serviceOrderParts/server/publications.js';

import '../../api/contractDetails/methods.js';
import '../../api/contractDetails/server/publications.js';

import '../../api/technicianReceptionEquipments/methods.js';
import '../../api/technicianReceptionEquipments/server/publications.js';

import '../../api/partsRequest/methods.js';
import '../../api/partsRequest/server/publications.js';

import '../../api/historyReleaseSw/methods.js';
import '../../api/historyReleaseSw/server/publications.js';

import '../../api/orderTypes/methods.js';
import '../../api/orderTypes/server/publications.js';

import '../../api/protocols/methods.js';
import '../../api/protocols/server/publications.js';

import '../../api/emails/methods.js';
import '../../api/emails/server/publications.js';

import '../../api/servicePrograms/methods.js';
import '../../api/servicePrograms/server/publications.js';

import '../../api/historys/methods.js';
import '../../api/historys/server/publications.js';
