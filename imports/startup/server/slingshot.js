Slingshot.fileRestrictions("rivas-opstaele-attachments", {
    allowedFileTypes: ["image/png", "image/jpeg", "image/gif", "image/jpg", "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],
    maxSize: 2 * 1024 * 1024,
});

Slingshot.createDirective("rivas-opstaele-attachments", Slingshot.S3Storage, {
    bucket: "rivas-opstaele",
    acl: "public-read",
    region: "us-west-1",
    expire: 24 * 60 * 60 * 1000,
    authorize: function () {
        if (!this.userId) {
            var message = "Inicia sesión antes de publicar imágenes";
            throw new Meteor.Error("Es necesario iniciar sesión", message);
        }

        return true;
    },
    key: function (file, context) {
        let name = file.name;

        return "attachments/" + context.recordId + "/" + (new Meteor.Collection.ObjectID()._str + (name.substring(name.lastIndexOf("."), name.length))).toLowerCase();
    }
});