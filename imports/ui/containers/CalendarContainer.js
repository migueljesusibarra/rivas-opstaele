import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PreOrder } from '/imports/ui/views/src/receptionEquipment/PreOrder';
import { FullCalendar } from '/imports/ui/views/src/calendar/FullCalendar';
import { PreOrders } from '../../api/preOrders/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';
import { Systems } from '/imports/api/systems/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Models } from '/imports/api/models/collection.js';
import moment from 'moment';
import { Util } from '/lib/common.js';
import { RCalendar } from '/imports/ui/views/src/reports/RCalendar';

const composerCalendar = (params, onData) => {
    let dateRange = getDateRange(params.startDate, params.endDate);
    let condition =  {date: {$gte: new Date(dateRange.start), $lte: new Date(dateRange.end)}}

    if(params.technicianId != null)
    {
        condition =  {technicianId: params.technicianId , date: {$gte: new Date(dateRange.start), $lte: new Date(dateRange.end)}}

    }else if(params.customerId != null)
    {
        condition =  {customerId: params.customerId , date: {$gte: new Date(dateRange.start), $lte: new Date(dateRange.end)}}
    }

    let parameters = {
        fields: {fields: { customerId: 1, technicianId: 1, date: 1, serialNumber: 1, systemId: 1, brandId: 1, modelId: 1}},
        condition: condition
    };

    let preOrderHandle = Meteor.subscribe('calendars', parameters);
    let customerHandle = Meteor.subscribe('customers');
    let technicianHandle = Meteor.subscribe('technicians');
    let systemsHandle = Meteor.subscribe('systems');
    let brandsHandle = Meteor.subscribe('brands');
    let modelsHandle = Meteor.subscribe('models');

    let events = [];

    if(preOrderHandle.ready() && customerHandle.ready() &&
        technicianHandle.ready() && systemsHandle.ready() &&
        brandsHandle.ready() && modelsHandle.ready())
    {
        let customers = Customers.find({}).fetch();
        let technicians = Technicians.find({}).fetch();
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let models = Models.find({}).fetch();

        let records = PreOrders.find(parameters.condition, {transform: function(record){
            eventItem = {};
            let name = params.customerId != null ? "FCG. " + getFieldCollection(record.technicianId, technicians) : (params.technicianId != null ?  getFieldCollection(record.customerId, customers) : "");

            let equipment = getFieldCollection(record.systemId, systems) + " " + getFieldCollection(record.brandId, brands) + " " + getFieldCollection(record.modelId, models);
            eventItem.id = record._id;
            eventItem.title =  equipment + "\n" + "NS. " +record.serialNumber + "\n" + name;
            eventItem.start = moment(record.date).format('YYYY-MM-DD');
            events.push(eventItem);

            return record;

        }}).fetch();

        onData(null, {
                events: events,
                customers: customers,
                technicians: technicians,
                customerId: params.customerId,
                technicianId: params.technicianId
            });
    }
};

export const CalendarContainer = composeDefault(composerCalendar, FullCalendar);

const composerReport = (params, onData) => {
    onData(null, {
                records: []
        });
};

export const RCalendarContainer = composeDefault(composerReport, RCalendar);

const getDateRange = (start, end) => {
	return {start: getStartDate(start), end: getEndDate(end)};
};

const getStartDate = (date) => {
	// If the start date is not provided set it to the first of the month
	if(date == null)
	{
		let currentDate = new Date();

		return new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	}

	return date;
};

const getEndDate = (date) => {
	if(date == null)
    {
        let currentEndDate = new Date();
		return new Date(currentEndDate.getFullYear(), currentEndDate.getMonth(), 0);
    }

    return date;
};

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
