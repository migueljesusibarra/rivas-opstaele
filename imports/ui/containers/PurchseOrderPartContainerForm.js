import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PurchaseOrders } from '../../api/purchaseOrders/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Equipments } from '../../api/equipments/collection';
import { Systems } from '../../api/systems/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Vendors } from '../../api/vendors/collection.js';
import { Models } from '../../api/models/collection.js';
import { Periods } from '../../api/periods/collection.js';
import { CustomerTypes } from '../../api/customerTypes/collection.js';
import { PurchaseOrderParts } from '../../api/purchaseOrderParts/collection.js';
import { PurchaseOrderFormPart } from '../views/src/purchaseOrder/PurchaseOrderFormPart.jsx';


const composer = (params, onData) => {

     let purchaseOrderHandle = Meteor.subscribe('purchaseOrders', params.id);
     let customerHandle = Meteor.subscribe('customers');
     let customerTypeHandle = Meteor.subscribe('customerTypes');
     let systemHandle = Meteor.subscribe('systems');
     let modelHandle = Meteor.subscribe('models');
     let brandHandle = Meteor.subscribe('brands');
     let vendorHandle = Meteor.subscribe('vendors');
     let periodHandle = Meteor.subscribe('periods');
     let equipmentHandle = Meteor.subscribe('equipments');
     let purchaseOrderDetailHandle = Meteor.subscribe('purchaseOrderDetails');

     if(purchaseOrderHandle.ready() && customerHandle.ready() && systemHandle.ready() && modelHandle.ready() && brandHandle.ready()
      && vendorHandle.ready() && periodHandle.ready() && purchaseOrderDetailHandle.ready() && equipmentHandle.ready() && customerTypeHandle.ready())
      {
          let isNew = !(parseInt(params.id) > 0);

            if(!isNew)
                editComposer(params, onData);
            else
                createComposer(params, onData);

    }
  };

  const editComposer = (params, onData) =>
  {
      let condition = {_id: params.id};
      let totalDetail = 0;

      let data = PurchaseOrders.findOne(condition, {transform: function(record){
          record.equipment = PurchaseOrders.findOne({serialNumber: record.serialNumber}).equipment;
          return  record;
      }});

      let dataDetail = PurchaseOrderParts.find({purchaseOrderId: params.id}).fetch();
      let customers = Customers.find({}).fetch();
      let systems = Systems.find({}).fetch();
      let brands = Brands.find({}).fetch();
      let models = Models.find({brandId: data.brandId}).fetch();
      let vendors = Vendors.find({}).fetch();
      let periods = Periods.find({}).fetch();
      let customerTypes = CustomerTypes.find({}).fetch();
      let customerType = getCustomerType(getCustomerTypeId(data.customerId, customers), customerTypes);

      totalDetail = dataDetail.length;

    onData(null, {
      isNew: false,
      data: data,
      customers: customers,
      systems: systems,
      models: models,
      brands: brands,
      vendors: vendors,
      periods: periods,
      dataDetail: dataDetail,
      customerTypes: customerTypes,
      customerType: customerType,
      totalDetail: totalDetail
    });

  };

  const createComposer = (params, onData) =>
  {
    let customers = Customers.find({}).fetch();
    let systems = Systems.find({}).fetch();
    let brands = Brands.find({}).fetch();
    let vendors = Vendors.find({}).fetch();
    let periods = Periods.find({}).fetch();
    let customerTypes = CustomerTypes.find({}).fetch();
    let date = new Date();
    let data = {date: new Date(date.setDate(date.getDate())), status: "Nuevo", equipment:{} };

    onData(null, {
      isNew: true,
      data: data,
      customers: customers,
      systems: systems,
      brands: brands,
      vendors: vendors,
      periods: periods,
      customerTypes: customerTypes

    });

  };

  export const PurchseOrderPartContainerForm = composeDefault(composer, PurchaseOrderFormPart);

  const getCustomerType =  (id, collection) =>
  {
   let i = 0;

   for( i; i < collection.length; i++)
   {
    if(collection[i]._id  == id)
    {
      return collection[i].description;
    }
  }
  }

  const getCustomerTypeId =  (id, collection) =>
  {
   let i = 0;

   for( i; i < collection.length; i++)
   {
    if(collection[i]._id  == id)
    {
      return collection[i].customerTypeId;
    }
  }
  }
