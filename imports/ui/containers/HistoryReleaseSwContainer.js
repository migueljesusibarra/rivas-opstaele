import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { HistoryReleaseSwList } from '/imports/ui/views/src/serviceOrder/HistoryReleaseSwList';
import { HistoryReleaseSw } from '../../api/historyReleaseSw/collection.js';

const composer = (props, onData) => {

    let historyReleaseSwHandler = Meteor.subscribe('historyReleaseSw');
    let members = Meteor.subscribe('members');

    if(members.ready() && historyReleaseSwHandler.ready())
    {
        let users =  Meteor.users.find({}).fetch();

        let historyReleaseSwRecords = HistoryReleaseSw.find({systemId: props.systemId, brandId: props.brandId, modelId: props.modelId}, { transform: function(record){
            record.createByUser = fieldCollection(record.createdByUserId, users);
            return record;
        }}).fetch();

        onData(null, {records: historyReleaseSwRecords});
    }
};

export const HistoryReleaseSwContainer = composeDefault(composer, HistoryReleaseSwList);

const fieldCollection =  (id, collection) =>
{
    let i = 0;

    for( i; i < collection.length; i++)
    {
        if(collection[i]._id == id)
            return  (collection[i].profile.firstName + ' ' + collection[i].profile.lastName) || collection[i].username  ;
    }
}
