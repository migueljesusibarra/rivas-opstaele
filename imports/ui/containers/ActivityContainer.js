import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Activity } from '/imports/ui/views/src/catalogs/Activity';
import { Activities } from '/imports/api/activities/collection.js';

const composer = (props, onData) => {
	console.log("testing");
	if(Meteor.subscribe('activities').ready()){
		console.log("testing");
		const records = Activities.find({}).fetch();
		onData(null, {records});
	}
};

export const ActivityContainer = composeDefault(composer, Activity);
