import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { TechnicianReceptionEquipment } from '/imports/ui/views/src/receptionEquipment/TechnicianReceptionEquipment';
import { TechnicianReceptionEquipments } from '/imports/api/technicianReceptionEquipments/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';

const composer = (props, onData) => {
    let condition = {receptionEquipmentId: props.receptionEquipmentId};
    let technicianReceptionEquipmentHandle = Meteor.subscribe('technicianReceptionEquipments', condition);
    let technicianHandle = Meteor.subscribe('technicians');

    if(technicianHandle.ready() && technicianReceptionEquipmentHandle.ready())
    {
        let technicianAll = Technicians.find({}).fetch();

        let technicianReceptionEquipments = TechnicianReceptionEquipments.find(condition, {transform: function(record)
        {
            record.technicianName = getFieldCollection(record.technicianId, technicianAll);

            return record;
        }}).fetch();

        let receptionEquipmentTechnicianIds = [];

        for (let technicianReceptionEquipment of technicianReceptionEquipments)
        {
		  receptionEquipmentTechnicianIds.push(technicianReceptionEquipment.technicianId);
        }

        let technicians = Technicians.find({ _id: { $nin: receptionEquipmentTechnicianIds }}).fetch();

        onData(null, {technicians: technicians, technicianReceptionEquipments: technicianReceptionEquipments});
    }
};

export const TechnicianReceptionEquipmentContainer = composeDefault(composer, TechnicianReceptionEquipment);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
