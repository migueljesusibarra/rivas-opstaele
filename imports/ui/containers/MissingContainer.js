import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Missing } from '/imports/ui/views/src/catalogs/Missing';
import { Missings } from '../../api/missings/collection.js';

const composer = (props, onData) => {

	if(Meteor.subscribe('missings').ready())
	{
		const records = Missings.find({receptionEquipmentId: props.receptionEquipmentId}).fetch();
		onData(null, {records});
	}
};

export const MissingContainer = composeDefault(composer, Missing);
