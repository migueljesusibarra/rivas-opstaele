import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Systems } from '/imports/api/systems/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';
import { Periods } from '/imports/api/periods/collection.js';
import { OrderTypes } from '/imports/api/orderTypes/collection.js';
import { ServiceOrders } from '/imports/api/serviceOrders/collection.js';
import { ServiceOrderForm } from '../views/src/serviceOrder/ServiceOrderForm.jsx';
import { ActivitiesService } from '/imports/api/activitiesService/collection';
import { ServiceOrderParts } from '/imports/api/serviceOrderParts/collection.js';
import { Contacts } from '/imports/api/contacts/collection.js';
import { RServiceOrder } from '/imports/ui/views/src/reports/RServiceOrder';
import { Activities } from '/imports/api/activities/collection';
import { Parts } from '/imports/api/parts/collection.js';

const composer = (params, onData) => {
    let condition = {_id: params.id};

    let serviceHandle = Meteor.subscribe('serviceOrders', condition);
    let customerHandle = Meteor.subscribe('customers');
    let systemHandle = Meteor.subscribe('systems');
    let brandHandle = Meteor.subscribe('brands');
    let technicianHandle = Meteor.subscribe('technicians');
    let orderTypeHandle = Meteor.subscribe('orderTypes');

    if(serviceHandle.ready() && customerHandle.ready() && systemHandle.ready()
      && brandHandle.ready() && technicianHandle.ready()
      && orderTypeHandle.ready())
     {
        let isNew = !(parseInt(params.id) > 0);

        if(!isNew)
            editComposer(params, onData);
        else
            createComposer(params, onData);
    }
};

const editComposer = (params, onData) => {
    let condition = {_id: params.id};

    let data = ServiceOrders.findOne(condition);
    let conditionPeriod = {modelId: data.modelId};
    let conditionModel = {brandId: data.brandId};

    let periodHandle = Meteor.subscribe('periods', conditionPeriod);
    let modelHandle = Meteor.subscribe('models', conditionModel);

    if(periodHandle.ready() && modelHandle.ready())
    {
        let customers = Customers.find({}).fetch();
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let models = Models.find(conditionModel).fetch();
        let technicians = Technicians.find({}).fetch();
        let orderTypes = OrderTypes.find({}).fetch();
        let periods = Periods.find(conditionPeriod).fetch();

        onData(null, {
            isNew: false,
            data: data,
            customers: customers,
            systems: systems,
            brands: brands,
            models: models,
            technicians: technicians,
            orderTypes: orderTypes,
            periods: periods
        });
    }

};

const createComposer = (params, onData) => {

    let periodHandle = Meteor.subscribe('periods');
    let modelHandle = Meteor.subscribe('models');

    if(periodHandle.ready() && modelHandle.ready())
    {
        let date = new Date();
        let data = {date: new Date(date.setDate(date.getDate())), status: "Nuevo" };
        let customers = Customers.find({}).fetch();
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let models = [];
        let technicians = Technicians.find({}).fetch();
        let orderTypes = OrderTypes.find({}).fetch();
        let periods = [];

    onData(null, {
        isNew: true,
        data: data,
        customers: customers,
        systems: systems,
        brands: brands,
        models: models,
        technicians: technicians,
        orderTypes: orderTypes,
        periods: periods

    });
}

};

export const ServiceOrderContainerForm = composeDefault(composer, ServiceOrderForm);

const composerReport = (params, onData) =>
{
    let condition = {_id: params.id};
    let serviceHandle = Meteor.subscribe('serviceOrders', condition);
    let serviceOrderPartHandle = Meteor.subscribe('serviceOrderParts');

    if(serviceHandle.ready())
    {
        let data = ServiceOrders.findOne(condition) || {};
        let conditionCusotmer = {_id: data.customerId};
        let conditionSystem = {_id: data.systemId};
        let conditionBrand = {_id: data.brandId};
        let conditionModel = {_id: data.modelId};
        let conditionTechnician = {_id: data.technicianId};
        let conditionOrderType = {_id: data.orderTypeId};
        let conditionDetail = {serviceOrderId: data._id};

        let customerHandle = Meteor.subscribe('customers', conditionCusotmer);
        let systemHandle = Meteor.subscribe('systems', conditionSystem);
        let modelHandle = Meteor.subscribe('models', conditionModel);
        let brandHandle = Meteor.subscribe('brands', conditionBrand);
        let activityHandle = Meteor.subscribe('activitiesService', conditionDetail);
        let serviceOrderPartHandle = Meteor.subscribe('serviceOrderParts', conditionDetail);
        let technicianHandle = Meteor.subscribe('technicians', conditionTechnician);
        let orderTypeHandle = Meteor.subscribe('orderTypes', conditionOrderType);
        //agregar filtro conforme a la collection ActivitiesService
        let activitiesHandle = Meteor.subscribe('activities');
        let partHandle = Meteor.subscribe('parts');


        if(customerHandle.ready() && systemHandle.ready() && modelHandle.ready()
           && brandHandle.ready() && activityHandle.ready() && serviceOrderPartHandle.ready()
           && partHandle.ready()  && technicianHandle.ready() && orderTypeHandle.ready()
           && activitiesHandle.ready() && partHandle.ready())
        {
            let customer = Customers.findOne(conditionCusotmer);
            let system = Systems.findOne(conditionSystem);
            let brand = Brands.findOne(conditionBrand);
            let model = Models.findOne(conditionModel);
            let technician = Technicians.findOne(conditionTechnician);
            let orderType = OrderTypes.findOne(conditionOrderType);
            let conditionContact = {foreignId: customer._id, main: true}
            let contactHandle = Meteor.subscribe('contacts', conditionContact);

            //agregar filtro conforme a la collection ActivitiesService
            let activities = Activities.find({}).fetch();
            let parts = Parts.find({}).fetch();

            let activitiesService = ActivitiesService.find(conditionDetail, {transform: function(record)
            {
                record.description = getFieldCollection(record.activityId, activities);
                return record;
            }}).fetch();

            let serviceOrderParts = ServiceOrderParts.find(conditionDetail, {transform: function(record)
            {
                record.description = getFieldCollection(record.partId, parts);
                return record;
            }}).fetch();

            if(contactHandle.ready() && partHandle.ready())
            {
                let contact =  Contacts.findOne(conditionContact);

                let record = {
                    Id: data._id,
                    beginDate: data.beginDate,
                    endDate: data.endDate,
                    note: data.note,
                    technician: technician.name,
                    orderType: orderType.description,
                    descriptionFailure: data.descriptionFailure,
                    system: system.description,
                    brand: brand.description,
                    model: model.description,
                    serialNumber: data.serialNumber,
                    releaseSw: data.releaseSw,
                    installationDate: data.installationDate,
                    warrantyExpires: data.warrantyExpires,
                    name: customer.name,
                    address: customer.address,
                    phone: customer.phone,
                    fax: "",
                    contact: contact !=  null ? contact.name : "",
                    email: customer.email
             }

            onData(null, {
                    record: record,
                    activitiesService: activitiesService,
                    serviceOrderParts: serviceOrderParts
                 });
            }
        }


    }
}
export const RServiceOrderContainer = composeDefault(composerReport, RServiceOrder);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description;
      }
   }
}
