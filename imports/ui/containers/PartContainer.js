import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Part } from '/imports/ui/views/src/catalogs/Part.jsx';
import { Parts } from '../../api/parts/collection.js';
import { Vendors } from '../../api/vendors/collection.js';

const composer = (props, onData) =>
{
	let vendorHandle = Meteor.subscribe('vendors');

	if(Meteor.subscribe('parts').ready() && vendorHandle.ready())
	{
		let data = Parts.find({}).fetch();
		let vendors = Vendors.find({}).fetch();
		onData(null, {data: data, vendors:vendors});
	}
};

export const PartContainer = composeDefault(composer, Part);
