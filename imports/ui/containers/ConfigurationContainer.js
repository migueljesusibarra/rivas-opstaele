import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Configuration } from '/imports/ui/views/src/catalogs/Configuration';
import { Configurations } from '../../api/configurations/collection.js';
import { ReceptionEquipments } from '../../api/receptionEquipments/collection.js';

const composer = (props, onData) => {

 let systemId = props.systemId;
 let brandId = props.brandId;
 let modelId = props.modelId;

 let configurationHandle = Meteor.subscribe('configurations')
 let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments');

 if(configurationHandle.ready() && receptionEquipmentHandle.ready())
 {
   let receptionData = ReceptionEquipments.findOne({systemId: systemId, brandId: brandId, modelId: modelId, status: "Aprobado"}, {sort: {index: -1}});

   let records = Configurations.find({receptionEquipmentId: props.receptionEquipmentId}).fetch();
   let previousAuxRecords = receptionData != null ? Configurations.find({receptionEquipmentId: receptionData._id}).fetch() : [];

   let previousRecords = getConfigurationAvailable(records, previousAuxRecords);

   onData(null, {records: records, previousRecords: previousRecords, receptionEquipmentId: props.receptionEquipmentId});
  }
};

export const ConfigurationContainer = composeDefault(composer, Configuration);


const getConfigurationAvailable =  (recordConfigurations, previousRecordConfigurations) =>
{
    let previousRecords = [];
    if(recordConfigurations.length > 0)
    {
        for (let previousRecordConfiguration of previousRecordConfigurations)
        {
            let auxExist = false;
            for(let recordConfiguration of recordConfigurations)
            {
                if(previousRecordConfiguration.quantity == recordConfiguration.quantity &&
                    previousRecordConfiguration.code == recordConfiguration.code &&
                    previousRecordConfiguration.description == recordConfiguration.description)
                    auxExist = true;

            }

            if(!auxExist)
            {
                if(previousRecords.length == 0 )
                     previousRecords.push(previousRecordConfiguration);
                else
                {
                    let aux = false;
                    for(let previousRecord of previousRecords)
                    {
                        if(previousRecordConfiguration.quantity == previousRecord.quantity &&
                            previousRecordConfiguration.code == previousRecord.code &&
                            previousRecordConfiguration.description == previousRecord.description)
                            aux = true;

                    }

                    if(!aux)
                      previousRecords.push(previousRecordConfiguration);
                }

            }
        }
    }
    else
        previousRecords = previousRecordConfigurations;

    return previousRecords;
}
