import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Navigation } from '../layouts/Navigation.jsx';

const composer = (props, onData) => {

     if(Meteor.subscribe('members').ready())
     {
         let user = Meteor.users.findOne({_id:Meteor.userId()});

        onData(null, {user:user});
     }
};

export const NavigationContainer = composeDefault(composer, Navigation);
