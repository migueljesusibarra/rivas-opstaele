import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Equipments } from '../../api/equipments/collection.js';
import { Systems } from '../../api/systems/collection.js';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Locations } from '../../api/locations/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Technicians } from '../../api/technicians/collection.js';
import { EquipmentForm } from '../views/src/equipment/EquipmentForm';
import { Periods } from '../../api/periods/collection.js';
import { Util } from '/lib/common.js';

const composer = (params, onData) => {
   var condition = {_id: params.id};
   let equipmentHandle = Meteor.subscribe('equipments', condition);
   let systemHandle = Meteor.subscribe('systems');
   let modelHandle = Meteor.subscribe('models');
   let brandsHandle = Meteor.subscribe('brands');
   let customerHandle = Meteor.subscribe('customers');
   let locationsHandle = Meteor.subscribe('locations');
   let technicianHandle = Meteor.subscribe('technicians');
   let periodHandle = Meteor.subscribe('periods');

   if(equipmentHandle.ready() && systemHandle.ready() && modelHandle.ready() && customerHandle.ready()
     && brandsHandle.ready() && locationsHandle.ready() && technicianHandle.ready() && periodHandle.ready())
   {
    let data = Equipments.findOne(condition) || {};
    let id = data._id ? data._id : null;
    let isNew = id == null;

    let systems = Systems.find({}).fetch();
    let brands = Brands.find({}).fetch();
    let models = isNew || data.brand == null  ? [] : Models.find({brand: data.brand}).fetch();
    let locations = Locations.find({}).fetch();
    let customers = Customers.find({}).fetch();
    let technicians = Technicians.find({}).fetch();
    let periods = Periods.find({}).fetch();

    onData(null, {isNew: isNew, id: id, data: data, systems: systems, models: models, customers:customers, brands:brands, locations:locations, technicians:technicians, periods:periods});
}
};

export const EquipmentContainerForm = composeDefault(composer, EquipmentForm);
