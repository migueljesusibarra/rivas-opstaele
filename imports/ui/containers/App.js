import { compose } from 'react-komposer';
import { App } from '../layouts/App2.jsx';
import { getTrackerLoader } from '../../../lib/common.js';

const composer = (props, onData) => {
    let user = Meteor.user();

    let result = {
        user: user
    };

    onData(null, result);
};

export const AppContainer = compose(getTrackerLoader(composer))(App);
