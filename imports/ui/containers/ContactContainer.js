import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Contact } from '/imports/ui/views/src/catalogs/Contact';
import { Contacts } from '../../api/contacts/collection.js';

const composer = (props, onData) =>
{
	let conditions = {module: props.module, foreignId: props.foreignId};
	let handle = Meteor.subscribe("contacts", conditions);
	
	if(handle){
		let data = Contacts.find(conditions).fetch();
		onData(null, {data: data, module: props.module, foreignId: props.foreignId});
	}
};

export const ContactContainer = composeDefault(composer, Contact);
