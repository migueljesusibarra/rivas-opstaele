import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Vendors } from '../../api/vendors/collection.js';
import { VendorForm } from '../views/src/vendor/VendorForm.jsx';

const composer = (params, onData) => {
     var condition = {_id: params.id};
     let vendorHandle = Meteor.subscribe('vendors', condition);

    if(vendorHandle.ready())
    {
       let data = Vendors.findOne(condition);
       let id = params.id ? params.id : null;
       let isNew = id == null || id == 'add';

       onData(null, {isNew: isNew, data: data });
    }

};

export const VendorContainerForm = composeDefault(composer, VendorForm);
