import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { CustomerType } from '/imports/ui/views/src/catalogs/CustomerType';
import { CustomerTypes } from '../../api/customerTypes/collection.js';

const composer = (props, onData) => {

	if(Meteor.subscribe('customerTypes').ready())
	{
		const records = CustomerTypes.find({}).fetch();
		onData(null, {records: records});
	}
};

export const CustomerTypeContainer = composeDefault(composer, CustomerType);
