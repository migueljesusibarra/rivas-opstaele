import { Meteor } from 'meteor/meteor';
//import { composeWithLoading } from '/lib/common';
import { composeDefault } from '/lib/common';
import { Historys } from '/imports/api/historys/collection.js';
import { History } from '/imports/ui/components/src/History';

const composer = (props, onData) =>
{

    let conditions = {module: props.module, foreignId: props.foreignId};

    let handle = Meteor.subscribe("historys", conditions);

    if(handle.ready())
    {
        let records = Historys.find(conditions, {sort: {modifiedOn: -1}, limit: 10}).fetch();

        props = Object.assign({}, props);
           console.log("llego");
        onData(null, Object.assign(props, {records: records}));
    }
};

export const HistoryContainer = composeDefault(composer, History);
