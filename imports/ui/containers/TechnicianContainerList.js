import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { TechnicianList } from '/imports/ui/views/src/technicians/TechnicianList.jsx';
import { TechnicianSendEmailList } from '/imports/ui/views/src/technicians/TechnicianSendEmailList.jsx';
import { Technicians } from '../../api/technicians/collection.js';

const composer = (props, onData) =>
{
  if(Meteor.subscribe('technicians').ready()){
      const data = Technicians.find({}).fetch();
      onData(null, {data});
    }
};

export const TechnicianContainerList = composeDefault(composer, TechnicianList);


const composerSendEmail = (props, onData) =>
{
  if(Meteor.subscribe('technicians').ready()){
      const data = Technicians.find({}).fetch();
      onData(null, {data});
    }
};

export const TechnicianListSendEmailContainer = composeDefault(composerSendEmail, TechnicianSendEmailList);
