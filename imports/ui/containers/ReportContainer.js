import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Report } from '/imports/ui/components/src/Report';

const composer = (params, onData) => {
	onData(null, {record: params.id, module: params.module});
};

export const ReportContainer = composeDefault(composer, Report);
