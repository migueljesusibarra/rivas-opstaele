import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Customers } from '/imports/api/customers/collection.js';
import { Contacts } from '/imports/api/contacts/collection.js';
import { CustomerForm } from '/imports/ui/views/src/customer/CustomerForm.jsx';
import { CustomerTypes } from '/imports/api/customerTypes/collection.js';

const composer = (params, onData) => {
    let condition = {_id: params.id};
    let cutomerHandle = Meteor.subscribe('customers', condition);
    let contactHandle = Meteor.subscribe('contacts', condition);
    let customerTypeHandle= Meteor.subscribe('customerTypes');

 if(cutomerHandle.ready() && contactHandle.ready() && customerTypeHandle.ready())
 {
     let data = Customers.findOne(condition) || {};
     let id = params.id ? params.id : null;
     let isNew = id == "add" ||  id == null;
     let contact = Contacts.find(condition).fetch();
     let customerTypes = CustomerTypes.find({}).fetch();

     onData(null, {isNew: isNew, data: data, contact: contact, customerTypes: customerTypes});
 }

};

export const CustomerContainerForm = composeDefault(composer, CustomerForm);
