import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PreOrder } from '/imports/ui/views/src/receptionEquipment/PreOrder';
import { PreOrderList } from '/imports/ui/views/src/catalogs/PreOrderList';
import { PreOrders } from '../../api/preOrders/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';
import { Models } from '/imports/api/models/collection.js';

const composer = (props, onData) => {
        if(Meteor.subscribe('preOrders', {}).ready()){
            let records = PreOrders.find({receptionEquipmentId: props.receptionEquipmentId}).fetch();
            onData(null, {records});
        }
};

export const PreOrderContainer = composeDefault(composer, PreOrder);


const composerPreOrderList = (props, onData) => {

    let fieldsReception = {
        fields: {fields: { systemId: 1, brandId: 1, releaseSw: 1, installationDate: 1, warranty: 1, warrantyExpires: 1}}
    };

     let parameters = {
         condition: {isService: false}
     }
    //esta subcribe se esta utilizando en el componente
    let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments', fieldsReception);

    let preOrderHandle = Meteor.subscribe('preOrders', parameters);
    let customerHandle = Meteor.subscribe('customers');
    let technicianHandle = Meteor.subscribe('technicians');
    let modelHandle = Meteor.subscribe('models');


    if(preOrderHandle.ready() && customerHandle.ready() &&
        technicianHandle.ready() && modelHandle.ready() &&
        receptionEquipmentHandle.ready())
    {
        let customers = Customers.find({}).fetch();
        let technicians = Technicians.find({}).fetch();
        let models = Models.find({}).fetch();

        let records = PreOrders.find(parameters.condition, {transform: function(record)
            {
                record.customer = getFieldCollection(record.customerId, customers);
                record.technician = getFieldCollection(record.technicianId, technicians);
                record.model = getFieldCollection(record.modelId, models);

            return record;
           }}).fetch();

        onData(null, {records: records, technicians: technicians});
    }
};

export const PreOrderListContainer = composeDefault(composerPreOrderList, PreOrderList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
