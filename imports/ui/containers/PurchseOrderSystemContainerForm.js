import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PurchaseOrders } from '../../api/purchaseOrders/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Equipments } from '../../api/equipments/collection';
import { Systems } from '../../api/systems/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Vendors } from '../../api/vendors/collection.js';
import { Models } from '../../api/models/collection.js';
import { Periods } from '../../api/periods/collection.js';
import { CustomerTypes } from '../../api/customerTypes/collection.js';
import { PurchaseOrderDetails } from '../../api/purchaseOrderDetails/collection.js';
import { PurchaseOrderFormSystem } from '../views/src/purchaseOrder/PurchaseOrderFormSystem.jsx';
import { ReceptionEquipments } from '../../api/receptionEquipments/collection.js';


const composer = (params, onData) => {

 let purchaseOrderHandle = Meteor.subscribe('purchaseOrders', params.id);
 let customerHandle = Meteor.subscribe('customers');
 let customerTypeHandle = Meteor.subscribe('customerTypes');
 let systemHandle = Meteor.subscribe('systems');
 let modelHandle = Meteor.subscribe('models');
 let brandHandle = Meteor.subscribe('brands');
 let vendorHandle = Meteor.subscribe('vendors');
 let periodHandle = Meteor.subscribe('periods');
 let equipmentHandle = Meteor.subscribe('equipments');
 let purchaseOrderDetailHandle = Meteor.subscribe('purchaseOrderDetails');
 let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments');

 if(purchaseOrderHandle.ready() && customerHandle.ready() && systemHandle.ready() &&
    modelHandle.ready() && brandHandle.ready() && vendorHandle.ready() &&
    periodHandle.ready() && purchaseOrderDetailHandle.ready() && equipmentHandle.ready() &&
    customerTypeHandle.ready() && receptionEquipmentHandle.ready())
 {
    let isNew = !(parseInt(params.id) > 0);

    if(!isNew)
      editComposer(params, onData);
    else
      createComposer(params, onData);

 }
};

const editComposer = (params, onData) =>
{
   let condition = {_id: params.id};

        let data = PurchaseOrders.findOne(condition);
        let totalAmount = 0;
        let totalDetail = 0;

        let dataDetail = PurchaseOrderDetails.find({purchaseOrderId: params.id}).fetch();
        let customers = Customers.find({}).fetch();
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let models = Models.find({brandId: data.brandId}).fetch();
        let vendors = Vendors.find({}).fetch();
        let periods = Periods.find({}).fetch();
        let customerTypes = CustomerTypes.find({}).fetch();
        let customerType = getCustomerType(getCustomerTypeId(data.customerId, customers), customerTypes);
        let receptionEquipmentId = 0;

        if(data.hasReceptionEquipment)
        {
            let receptionEquipment = ReceptionEquipments.findOne({foreignId: data._id});
            if(receptionEquipment)
                receptionEquipmentId = receptionEquipment._id;

        }

        totalDetail = dataDetail.length;

        dataDetail.forEach(function(item)
        {
         totalAmount += item.totalPrice;

        }, this);


        onData(null, {
                        isNew: false,
                        data: data,
                        customers: customers,
                        systems: systems,
                        models: models,
                        brands: brands,
                        vendors: vendors,
                        periods: periods,
                        dataDetail: dataDetail,
                        customerTypes: customerTypes,
                        customerType: customerType,
                        totalAmount: totalAmount,
                        totalDetail: totalDetail,
                        receptionEquipmentId: receptionEquipmentId
                    });

};

const createComposer = (params, onData) =>
{
    let customers = Customers.find({}).fetch();
    let systems = Systems.find({}).fetch();
    let brands = Brands.find({}).fetch();
    let vendors = Vendors.find({}).fetch();
    let periods = Periods.find({}).fetch();
    let customerTypes = CustomerTypes.find({}).fetch();
    let date = new Date();
    let data = {date: new Date(date.setDate(date.getDate())), status: "New" };

   onData(null, {
                  isNew: true,
                  data: data,
                  customers: customers,
                  systems: systems,
                  brands: brands,
                  vendors: vendors,
                  periods: periods,
                  customerTypes: customerTypes,

                  });

};

export const PurchseOrderSystemContainerForm = composeDefault(composer, PurchaseOrderFormSystem);

const getCustomerType =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description;
      }
   }
}

const getCustomerTypeId =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].customerTypeId;
      }
   }
}
