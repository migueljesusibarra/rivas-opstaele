import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Systems } from '/imports/api/systems/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Periods } from '/imports/api/periods/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';
import { ReceptionEquipments } from '/imports/api/receptionEquipments/collection.js';
import { ReceptionEquipmentForm } from '../views/src/receptionEquipment/ReceptionEquipmentForm.jsx';
import { RReceptionEquipment } from '/imports/ui/views/src/reports/RReceptionEquipment';
import { Contacts } from '/imports/api/contacts/collection.js';
import { Configurations } from '/imports/api/configurations/collection.js';
import { Verifications } from '/imports/api/verifications/collection.js';
import { PreOrders } from '/imports/api/preOrders/collection.js';
import { ReceptionEquipmentTestsPerformed } from '/imports/api/receptionEquipmentTestPerformeds/collection.js';

const composer = (params, onData) => {
  var condition = {_id: params.id};

  let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments', condition);
  let customerHandle = Meteor.subscribe('customers');
  let systemHandle = Meteor.subscribe('systems');
  let brandHandle = Meteor.subscribe('brands');
  let periodHandle = Meteor.subscribe('periods');
  let technicianHandle = Meteor.subscribe('technicians');

  if(receptionEquipmentHandle.ready() && periodHandle.ready() &&
      brandHandle.ready() && systemHandle.ready() && customerHandle.ready()
       && technicianHandle.ready())
    {
       let isNew = !(parseInt(params.id) > 0);

        if(!isNew)
          editComposer(params, onData);
        else
          createComposer(params, onData);
        }
 };

const editComposer = (params, onData) => {

       let condition = {_id: params.id};

       let data = ReceptionEquipments.findOne(condition) || {};
       let conditionModel = {brandId: data.brandId}
       let modelHandler = Meteor.subscribe('models', conditionModel);

       if(modelHandler.ready())
       {
           let systems = Systems.find({}).fetch();
           let brands = Brands.find({}).fetch();
           let customers = Customers.find({}).fetch();
           let models = Models.find(conditionModel).fetch();
           let periods = Periods.find({}).fetch();
           let technicians = Technicians.find({}).fetch();

           onData(null, {isNew: false, data: data, systems: systems, brands: brands, models:models, customers: customers, periods: periods, technicians: technicians});
      }
};

const createComposer = (params, onData) => {

    let modelHandler = Meteor.subscribe('models');

    if(modelHandler.ready())
    {
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let customers = Customers.find({}).fetch();
        let models = [];
        let periods = Periods.find({}).fetch();
        let technicians = Technicians.find({}).fetch();
        let data = { status: "New" };

        onData(null, {isNew: true, data: data, systems: systems, brands: brands, models:models, customers: customers, periods: periods, technicians: technicians});
     }
};

export const ReceptionEquipmentContainerForm = composeDefault(composer, ReceptionEquipmentForm);

const composerReport = (params, onData) => {

    let condition = {_id: params.id};
    let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments', condition);

    if(receptionEquipmentHandle.ready())
    {
        let data = ReceptionEquipments.findOne(condition) || {};

        let conditionCusotmer = {_id: data.customerId};
        let conditionSystem = {_id: data.systemId};
        let conditionBrand = {_id: data.brandId};
        let conditionModel = {_id: data.modelId};
        let conditionDetail = {receptionEquipmentId: params.id};

        let customerHandle = Meteor.subscribe('customers', conditionCusotmer);
        let systemHandle = Meteor.subscribe('systems', conditionSystem);
        let modelHandle = Meteor.subscribe('models', conditionModel);
        let brandHandle = Meteor.subscribe('brands', conditionBrand);
        let configurationHandle = Meteor.subscribe('configurations', conditionDetail);
        let verificationHandle = Meteor.subscribe('verifications', conditionDetail);
        let testsPerformedHandle = Meteor.subscribe('receptionEquimentTestsPerformed', conditionDetail);
        let preOrderHandle =  Meteor.subscribe('preOrders', conditionDetail);

        if(customerHandle.ready() && systemHandle.ready() && modelHandle.ready() && brandHandle.ready()
            && configurationHandle.ready() && verificationHandle.ready() && testsPerformedHandle.ready() && preOrderHandle.ready())
           {
               let customer = Customers.findOne(conditionCusotmer);
               let system = Systems.findOne(conditionSystem);
               let brand = Brands.findOne(conditionBrand);
               let model = Models.findOne(conditionModel);
               //detalles
               let configurations = Configurations.find(conditionDetail).fetch();
               let verifications = Verifications.find({receptionEquipmentId: params.id, isMissing: false}).fetch();
               let missings = Verifications.find({receptionEquipmentId: params.id, isMissing: true}).fetch();
               let testsPerformeds = ReceptionEquipmentTestsPerformed.find(conditionDetail).fetch();
               let preOrders  = PreOrders.find(conditionDetail).fetch();
               let conditionContact = {foreignId: customer._id, main: true}
               let contactHandle = Meteor.subscribe('contacts', conditionContact);

               if(contactHandle.ready())
               {
                 let contact =  Contacts.findOne(conditionContact);

                  let record = {
                       Id: data._id,
                       system: system.description,
                       brand: brand.description,
                       model: model.description,
                       serialNumber: data.serialNumber,
                       releaseSw: data.releaseSw,
                       installationDate: data.installationDate,
                       warrantyExpires: data.warrantyExpires,
                       name: customer.name,
                       address: customer.address,
                       phone: customer.phone,
                       fax: "",
                       contact: contact !=  null ? contact.name : "",
                       email: customer.email
                   }

                   onData(null, {
                         record: record,
                         configurations: configurations,
                         verifications: verifications,
                         missings: missings,
                         testsPerformeds: testsPerformeds,
                         preOrders: preOrders
                     });
              }



           }

	}
};

export const RReceptionEquipmentContainer = composeDefault(composerReport, RReceptionEquipment);
