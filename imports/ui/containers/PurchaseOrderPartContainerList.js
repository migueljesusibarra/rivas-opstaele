import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PurchaseOrders } from '../../api/purchaseOrders/collection.js';
import { Vendors } from '../../api/vendors/collection.js';
import { Systems } from '../../api/systems/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { PurchaseOrderPartList } from '../views/src/purchaseOrder/PurchaseOrderPartList.jsx';

const composer = (props, onData) => {

  let parameters = {
        condition: {"type": "2"}
    };

  let purchaseOrderHandle = Meteor.subscribe('purchaseOrders', parameters);
  let vendorHandle = Meteor.subscribe('vendors');
  let systemHandle = Meteor.subscribe('systems');
  let customerHandle = Meteor.subscribe('customers');
  let modelHandle = Meteor.subscribe('models');
  let brandHandle = Meteor.subscribe('brands');

  if(purchaseOrderHandle.ready() && vendorHandle.ready() && systemHandle.ready() && modelHandle.ready() && customerHandle.ready()
    && brandHandle.ready())
  {
    let customers = Customers.find({}).fetch();
    let vendors = Vendors.find({}).fetch();
    let systems = Systems.find({}).fetch();
    let brands = Brands.find({}).fetch();
    let models = Models.find({}).fetch();

    let data = PurchaseOrders.find(parameters.condition, {transform: function(record)
     {
         record.customer = getFieldCollection(record.customerId, customers);
         record.supplier = getFieldCollection(record.supplierId, vendors);
         record.equipment = getFieldCollection(record.systemId, systems) + " " + getFieldCollection(record.brandId, brands) + " " + getFieldCollection(record.modelId, models);
        //  record.brand = getFieldCollection(record.brandId, brands);
        //  record.model = getFieldCollection(record.modelId, models);
         record.typeDescription = "Parte";

      return record;

    }}).fetch();

    onData(null, {data: data, typeDescription: "Parte"});
  }

};

export const PurchaseOrderPartContainerList = composeDefault(composer, PurchaseOrderPartList);

const getFieldCollection =  (id, collection) =>
{
    let i = 0;

    for( i; i < collection.length; i++)
    {
        if(collection[i]._id  == id)
        {
            return collection[i].description || collection[i].name;
        }
    }
}
