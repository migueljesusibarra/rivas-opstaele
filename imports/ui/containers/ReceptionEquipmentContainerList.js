import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ReceptionEquipments } from '../../api/receptionEquipments/collection.js';
import { ReceptionEquipmentList } from '../views/src/receptionEquipment/ReceptionEquipmentList.jsx';
import { Systems } from '../../api/systems/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Technicians } from '../../api/technicians/collection.js';


const composer = (props, onData) => {

   let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments');
   let customerHandle = Meteor.subscribe('customers');
   let systemHandle = Meteor.subscribe('systems');
   let modelHandle = Meteor.subscribe('models');
   let brandHandle = Meteor.subscribe('brands');
   let technicianHandle = Meteor.subscribe('technicians');

    if(receptionEquipmentHandle.ready() && customerHandle.ready() && systemHandle.ready()
        && modelHandle.ready() && brandHandle.ready() && technicianHandle.ready())
    {
       let systems = Systems.find({}).fetch();
       let customers = Customers.find({}).fetch();
       let models = Models.find({}).fetch();
       let brands = Brands.find({}).fetch();
       let technicians = Technicians.find({}).fetch();

       let data = ReceptionEquipments.find({status:"Borrador"}, {transform: function(record){

          record.customer = getFieldCollection(record.customerId, customers);
          record.equipment = getFieldCollection(record.systemId, systems) + " " + getFieldCollection(record.brandId, brands) + " " + getFieldCollection(record.modelId, models);
          record.technician = getFieldCollection(record.technicianId, technicians)

          return record;

       }}).fetch();

       onData(null, { data: data });
    }

};

export const ReceptionEquipmentContainerList = composeDefault(composer, ReceptionEquipmentList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
