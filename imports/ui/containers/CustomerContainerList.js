import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Customers } from '/imports/api/customers/collection';
import { CustomerList } from '/imports/ui/views/src/customer/CustomerList';
import { CustomerTypes } from '/imports/api/customerTypes/collection.js';
import { Util } from '/lib/common.js';

const composer = (props, onData) => {

   let customerHandle = Meteor.subscribe('customers');
   let customerTypeHandle= Meteor.subscribe('customerTypes');

   if(customerHandle.ready() && customerTypeHandle.ready()){

      let customerTypes = CustomerTypes.find({}).fetch();

      let data = Customers.find({}, {transform: function(record)
         {
            record.customerType = getFieldCollection(record.customerTypeId, customerTypes);
            record.department = getDeparmentFieldCollection(record.departmentId, Util.getDepartments());

            return record;

         }}).fetch();

      onData(null, {data: data, customerTypes: customerTypes});
   }
};

export const CustomerContainerList = composeDefault(composer, CustomerList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}

const getDeparmentFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i].value  == id)
      {
         return collection[i].text;
      }
   }
}
