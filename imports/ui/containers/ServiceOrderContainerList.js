import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ServiceOrderList } from '/imports/ui/views/src/serviceOrder/ServiceOrderList';
import { ServiceOrders } from '/imports/api/serviceOrders/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Systems } from '/imports/api/systems/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { OrderTypes } from '/imports/api/orderTypes/collection.js';

const composer = (props, onData) => {

    let serviceOrderHandle = Meteor.subscribe('serviceOrders');
    let customerHandle = Meteor.subscribe('customers');
    let systemHandle = Meteor.subscribe('systems');
    let brandHandle = Meteor.subscribe('brands');
    let modelHandle = Meteor.subscribe('models');
    let orderTypeHandle = Meteor.subscribe('orderTypes');

    if(serviceOrderHandle.ready() && customerHandle.ready() &&
    systemHandle.ready() && brandHandle.ready() && modelHandle.ready()
    && orderTypeHandle.ready())
    {
        let customers = Customers.find({}).fetch();
        let systems = Systems.find({}).fetch();
        let brands = Brands.find({}).fetch();
        let models = Models.find({}).fetch();
        let orderTypes = OrderTypes.find({}).fetch();

        let data = ServiceOrders.find({}, {transform: function(record)
        {
            record.customer = getFieldCollection(record.customerId, customers);
            record.equipment = getFieldCollection(record.systemId, systems) + " " + getFieldCollection(record.brandId, brands) + " " + getFieldCollection(record.modelId, models);
            record.orderType = getFieldCollection(record.orderTypeId, orderTypes);

            return record;
        }}).fetch();

        onData(null, {data});
    }

};

export const ServiceOrderContainerList = composeDefault(composer, ServiceOrderList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}

// consta insertPreorders = (data) =>
// {
//     for( i; i < collection.length; i++)
//     {
//         let data = {
//             customerId: data.customerId,
//             systemId: data.systemId,
//             brandId: data.brandId,
//             modelId: data.modelId,
//             serialNumber: data.serialNumber,
//             releaseSw: data.releaseSw,
//             installationDate: data.installationDate,
//             warranty: data.warranty,
//             warrantyExpires: data.warrantyExpires,
//             technicianId: data.technicianId,
//             orderTypeId: "",
//             source: "PreOrden",
//             foreignId: data._id,
//             note: fields.note.getValue(),
//             beginDate: data.date,
//             endDate: ""
//         };
//
//     }
// }
