import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PurchaseOrderParts } from '../../api/purchaseOrderParts/collection.js';
import { PurchaseOrderPart } from '../views/src/purchaseOrder/PurchaseOrderPart.jsx';
import { Parts } from '/imports/api/parts/collection.js';
import { Vendors } from '../../api/vendors/collection.js';
import { Comments } from '../../api/comments/collection.js';

const composer = (props, onData) => {
    let purchaseOrderPartHandle = Meteor.subscribe('purchaseOrderParts');
    let vendorHandle = Meteor.subscribe('vendors');
    let commentHandle = Meteor.subscribe("comments");
    let partHandle = Meteor.subscribe('parts');

    if(purchaseOrderPartHandle.ready() && vendorHandle.ready() &&
        commentHandle.ready() && partHandle.ready())
        {
            let comments = Comments.find({}).fetch();
            let vendors = Vendors.find({}).fetch();

            let data = PurchaseOrderParts.find({purchaseOrderId: props.purchaseOrderId}, {transform: function(record)
                {
                    record.attachRMA = getTypeAttach(record._id, comments, "RMA");
                    record.attachPOD = getTypeAttach(record._id, comments, "POD");
                    record.attachReturnInvoice = getTypeAttach(record._id, comments, "ReturnInvoice");
                    return record;

                }}).fetch();

                let partIds = [];

                for (let dataItem of data)
                {
                    partIds.push(dataItem.partId);
		        }

                let partRecords = Parts.find({ _id: { $nin: partIds }, active: true}).fetch();

                props = Object.assign({}, props);

                onData(
                    null,
                    Object.assign(
                        props,
                        {
                            data: data,
                            purchaseOrderId: props.purchaseOrderId,
                            vendors: vendors,
                            status: props.status,
                            partRecords: partRecords
                        })
        );
    }

};

export const PurchaseOrderPartContainer = composeDefault(composer, PurchaseOrderPart);

const getTypeAttach =  (id, collection, fieldType) => {
    let i = 0;
    let count = 0;

    for( i; i < collection.length; i++)
    {
        if(collection[i].foreignId  == id && collection[i].fieldType == fieldType)
            count++;
    }
    return count;
};
