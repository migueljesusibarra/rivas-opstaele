import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { TestPerformed } from '/imports/ui/views/src/catalogs/TestPerformed';
import { TestsPerformed } from '../../api/testsPerformed/collection.js';
import { Systems } from '../../api/systems/collection.js';
import { Models } from '../../api/models/collection.js';

const composer = (props, onData) => {

   let systemHandler = Meteor.subscribe('systems');
   let modelHandler = Meteor.subscribe('models');

   if(Meteor.subscribe('testsPerformed').ready() && systemHandler.ready() && modelHandler.ready())
      {
      	let systems = Systems.find({}).fetch();
        let models = Models.find({}).fetch();

        let records = TestsPerformed.find({}, {transform: function(record){
                record.model = getFieldCollection(record.modelId, models);
                record.system = getFieldCollection(record.systemId, systems);

                return record;

         }}).fetch();

        onData(null, {records: records, systems: systems, models: models });
      }
};

export const TestPerformedContainer = composeDefault(composer, TestPerformed);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
