import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Equipments } from '../../api/equipments/collection';
import { EquipmentList } from '/imports/ui/components/lookup/equipment/EquipmentList.jsx';
import { Systems } from '../../api/systems/collection.js';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';

const listComposer = (props, onData) =>
{
  let conditions = {customerId: props.foreignId};

  let handle = Meteor.subscribe("equipments", conditions);
  let systemHandle = Meteor.subscribe('systems');
  let modelHandle = Meteor.subscribe('models');
  let brandsHandle = Meteor.subscribe('brands');

  if(handle.ready() && systemHandle.ready() && brandsHandle.ready() && modelHandle.ready())
  {
    let systems = Systems.find({}).fetch();
    let models = Models.find({}).fetch();
    let brands = Brands.find({}).fetch();

    let records = Equipments.find(conditions, {transform: function(record){

     record.system = getFieldCollection(record.systemId, systems);
     record.brand = getFieldCollection(record.brandId, brands);
     record.model = getFieldCollection(record.modelId, models);

     return record;

   }}).fetch();  

    props = Object.assign({}, props);

    onData(null, Object.assign(props, {records: records, systems: systems, models: models, brands: brands}));
  }
};

export const EquipmentLookupContainer = composeDefault(listComposer, EquipmentList);

const getFieldCollection =  (id, collection) =>
{
  let i = 0;
  for( i; i < collection.length; i++)
  {
   if(collection[i]._id == id)
   {
    return collection[i].description || collection[i].name;
  }
}
}