import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Contracts } from '../../api/contracts/collection';
import { ContractList } from '../views/src/contracts/ContractList.jsx';
import { Customers } from '../../api/customers/collection.js';
import { Periods } from '../../api/periods/collection.js';
import { Util } from '/lib/common.js';

const composer = (props, onData) => {

	let contractHandle = Meteor.subscribe('contracts');
	let customerHandle = Meteor.subscribe('customers');
	let periodHandle = Meteor.subscribe('periods');

	if(contractHandle.ready() && customerHandle.ready() && periodHandle.ready())
	{
		let customers = Customers.find({}).fetch();
		let periods = Periods.find({}).fetch();

		let data = Contracts.find({}, {transform: function(record){
			 record.customer = fieldCollection(record.customerId, customers);
			 record.frequency = fieldCollection(record.frequencyId, periods);
			 return record
		}}).fetch();

		onData(null, {data: data, customers: customers});
	}
};

export const ContractContainerList = composeDefault(composer, ContractList);

 const fieldCollection =  (id, collection) =>
      {
         let i = 0;

         for( i; i < collection.length; i++)
         {
            if(collection[i]._id == id)
                return collection[i].name || collection[i].description;
         }
      } 



