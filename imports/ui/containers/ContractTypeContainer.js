import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ContractType } from '/imports/ui/views/src/catalogs/ContractType';
import { ContractTypes } from '../../api/contractTypes/collection.js';
import { Customers } from '../../api/customers/collection.js';

const composer = (props, onData) => {
	let customerHandle = Meteor.subscribe('customers');
	let contractHandle =  Meteor.subscribe('contractTypes');

	if(Meteor.subscribe('contractTypes').ready()){
		let data = ContractTypes.find({}).fetch();
		onData(null, {data:data});
	}
};

export const ContractTypeContainer = composeDefault(composer, ContractType);
