import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Equipments } from '/imports/api/equipments/collection';
import { EquipmentList } from '../views/src/equipment/EquipmentList';
import { Customers } from '/imports/api/customers/collection.js';
import { Systems } from '/imports/api/systems/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { Technicians } from '/imports/api/technicians/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Periods } from '/imports/api/periods/collection.js';

const composer = (props, onData) => {

   let conditions = {customerId: props.foreignId};

   let equipmentHandle = Meteor.subscribe('equipments');
   let customerHandle = Meteor.subscribe('customers');
   let systemHandle = Meteor.subscribe('systems');
   let modelHandle = Meteor.subscribe('models');
   let technicianHandle = Meteor.subscribe('technicians');
   let brandsHandle = Meteor.subscribe('brands');
   let periodHandle = Meteor.subscribe('periods');

   if(equipmentHandle.ready() && systemHandle.ready() && modelHandle.ready() &&
      customerHandle.ready() && technicianHandle.ready() &&  brandsHandle.ready() && periodHandle.ready())
   {
      let systems = Systems.find({}).fetch();
      let brands = Brands.find({}).fetch();
      let models = Models.find({}).fetch();
      let technicians = Technicians.find({}).fetch();
      let periods = Periods.find({}).fetch();

      let data = Equipments.find(conditions, {transform: function(record){

         record.system = getFieldCollection(record.systemId, systems);
         record.brand = getFieldCollection(record.brandId, brands);
         record.model = getFieldCollection(record.modelId, models);
         record.technician = getFieldCollection(record.technicianId, technicians);
         record.frequency = getFieldCollection(record.frequencyId, periods);

         return record;

      }}).fetch();

      onData(null, {
         data: data,
         systems: systems,
         models: models,
         technicians: technicians,
         brands: brands,
         foreignId: props.foreignId,
         periods: periods});
   }
};

export const EquipmentContainerList = composeDefault(composer, EquipmentList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
