import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PreviousTestPerformed } from '/imports/ui/views/src/receptionEquipment/PreviousTestPerformed.jsx';
import { TestsPerformed } from '/imports/api/testsPerformed/collection.js';
import { ReceptionEquipmentTestsPerformed } from '/imports/api/receptionEquipmentTestPerformeds/collection.js';

const composer = (props, onData) => {

	let testsPerformedHandle = Meteor.subscribe('testsPerformed');
	let receptionEquimentTestsPerformedHandle = Meteor.subscribe('receptionEquimentTestsPerformed');

    if(testsPerformedHandle.ready() && receptionEquimentTestsPerformedHandle.ready())
	{

		const receptionEquipmentTestsPerformedRecords = ReceptionEquipmentTestsPerformed.find({ receptionEquipmentId: props.receptionEquipmentId}).fetch();
		let testsPerformedIds = [];

		for (let receptionEquipmentTestsPerformedRecord of receptionEquipmentTestsPerformedRecords) {
		  testsPerformedIds.push(receptionEquipmentTestsPerformedRecord.partId);
		}

		const records = TestsPerformed.find({ _id: { $nin: testsPerformedIds }}).fetch();
		const records = TestsPerformed.find({}).fetch();

	     onData(null, {data:records});
	}

};

export const PreviousTestPerformedContainer = composeDefault(composer, PreviousTestPerformed);
