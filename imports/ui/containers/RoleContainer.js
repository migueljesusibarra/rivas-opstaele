import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Role } from '/imports/ui/views/src/catalogs/Role';
import { Roles } from '../../api/roles/collection.js';

const composer = (props, onData) =>
{
	if(Meteor.subscribe('roles').ready())
	{
		const records = Roles.find({}).fetch();
		onData(null, {records});
	}
};

export const RoleContainer = composeDefault(composer, Role);
