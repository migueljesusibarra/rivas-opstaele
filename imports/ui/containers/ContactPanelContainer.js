import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ContactPanel } from '/imports/ui/views/src/catalogs/ContactPanel';
import { Contacts } from '../../api/contacts/collection.js';

const composer = (props, onData) =>
{
	let conditions = {module: props.module, foreignId: props.foreignId};
	let handle = Meteor.subscribe("contacts", conditions);

	if(handle){
		let data = Contacts.find(conditions, {sort: {modifiedOn: -1}, limit: 10}).fetch();
		onData(null, {data: data, module: props.module, foreignId: props.foreignId});
	}
};

export const ContactPanelContainer = composeDefault(composer, ContactPanel);
