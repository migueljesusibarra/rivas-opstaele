import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ReceptionEquimentVerification } from '/imports/ui/views/src/receptionEquipment/ReceptionEquimentVerification';
import { Verifications } from '../../api/verifications/collection.js';
import { ReceptionEquipments } from '../../api/receptionEquipments/collection.js';

const composer = (props, onData) => {

   let systemId = props.systemId;
   let brandId = props.brandId;
   let modelId = props.modelId;
   let verificationHandle = Meteor.subscribe('verifications');
   let receptionEquipmentHandle = Meteor.subscribe('receptionEquipments');

    if(verificationHandle.ready() && receptionEquipmentHandle)
     {
            let receptionData = ReceptionEquipments.findOne({systemId: systemId, brandId: brandId, modelId: modelId, status: "Aprobado"}, {sort: {index: -1}});
            let previousRecords = receptionData != null ? Verifications.find({receptionEquipmentId: receptionData._id}).fetch() : [];
            let records = Verifications.find({receptionEquipmentId: props.receptionEquipmentId}).fetch();
            console.log(records);
            onData(null, {records: records, previousRecords: previousRecords, receptionEquipmentId: props.receptionEquipmentId});
     }
};

export const VerificationContainer = composeDefault(composer, ReceptionEquimentVerification);
