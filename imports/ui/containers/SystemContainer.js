import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { System } from '/imports/ui/views/src/catalogs/System';
import { Systems } from '../../api/systems/collection.js';

const composer = (props, onData) => {
        if(Meteor.subscribe('systems').ready()){
            const records = Systems.find({}).fetch();
            onData(null, {records});
        }
};

export const SystemContainer = composeDefault(composer, System);
