import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ContractDetail } from '/imports/ui/views/src/contracts/ContractDetail';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Systems } from '../../api/systems/collection.js';
import { ContractDetails } from '../../api/contractDetails/collection.js';
import { Equipments } from '../../api/equipments/collection';
import { Util } from '/lib/common.js';

   const composer = (props, onData) => {

      let condition = {contractId: props.contractId};
      let conditionEquipment = {customerId: props.customerId, inContract: false};

      let systemHandled = Meteor.subscribe('systems');
      let branHandled = Meteor.subscribe('brands');
      let modelHandled = Meteor.subscribe('models');
      let contracDetailHandled = Meteor.subscribe('contractDetails', condition);
      let equipmentHandle = Meteor.subscribe('equipments', conditionEquipment);

      if(branHandled.ready() && modelHandled.ready() && systemHandled.ready() && equipmentHandle.ready() && contracDetailHandled.ready())
      {
         let systems = Systems.find({}).fetch();
         let brands = Brands.find({}).fetch();
         let models = Models.find({}).fetch();
         let periods =  Util.getPeriods();

         let equipments = Equipments.find(conditionEquipment, {transform: function(record){

               record.equipment = fieldCollection(record.systemId, systems) + " " +  fieldCollection(record.brandId, brands) + " " + fieldCollection(record.modelId, models);
               record.frequency = fieldPeriod(record.frequencyId, periods);

               return record;

         }}).fetch();

         let records = ContractDetails.find(condition, {transform: function(record){

            record.equipment = fieldCollection(record.systemId, systems) + " " +  fieldCollection(record.brandId, brands) + " " + fieldCollection(record.modelId, models);
            record.frequency = fieldPeriod(record.frequencyId, periods);

            return record;

         }}).fetch();

        onData(null, {
                        records: records,
                        brands: brands,
                        systems: systems,
                        models: models,
                        equipments: equipments,
                        contractId: props.contractId,
                        contractNumber: props.contractNumber
                  });

      }
   };

export const ContractDetailContainer = composeDefault(composer, ContractDetail);

   const fieldCollection =  (id, collection) =>
   {
      let i = 0;

      for( i; i < collection.length; i++)
      {
            if(collection[i]._id == id)
            {
               return collection[i].name || collection[i].description;
         }
     }
   }

  const fieldPeriod =  (id, collection) =>
  {
      let i = 0;

      for( i; i < collection.length; i++)
      {
            if(collection[i].value == id)
            {
               return collection[i].text;
         }
     }
   }
