import { compose } from 'react-komposer';
import { AppReport } from '../layouts/AppReport.jsx';
import { getTrackerLoader } from '../../../lib/common.js';

const composer = (props, onData) => {
    let user = Meteor.user();

    let result = {
        user: user
    };

    onData(null, result);
};

export const AppReportContainer = compose(getTrackerLoader(composer))(AppReport);
