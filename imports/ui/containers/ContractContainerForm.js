import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Contracts } from '../../api/contracts/collection.js';
import { ContractForm } from '../views/src/contracts/ContractForm.jsx';
import { ContractTypes } from '../../api/contractTypes/collection.js';
import { Customers } from '../../api/customers/collection.js';
import { Util } from '/lib/common.js';



const composer = (params, onData) => {
    let condition = {_id: params.id};
    let contractHandle = Meteor.subscribe('contracts', condition);
    let customerHandle = Meteor.subscribe('customers');
    let contractTypeHandle =  Meteor.subscribe('contractTypes');
    
    
 if(contractHandle.ready() && customerHandle.ready() && contractHandle.ready())
 {
    let isNew = !(parseInt(params.id) > 0);

        if(!isNew)
          editComposer(params, onData);
        else
          createComposer(params, onData)
 }
};

const editComposer = (params, onData) => {
   let condition = {_id: params.id}; 
   let data = Contracts.findOne(condition) || {};
   let customers = Customers.find({}).fetch();
   let contractTypes = ContractTypes.find({}).fetch();
   let periods =  Util.getPeriods();
        
   onData(null, {isNew: false, data: data, customers: customers, contractTypes: contractTypes, periods: periods});
}

const createComposer = (params, onData) => {
   let data = { status: "Vigente" };
   let customers = Customers.find({}).fetch();
   let contractTypes = ContractTypes.find({}).fetch();
   let periods =  Util.getPeriods();
   
   onData(null, {isNew: true, data: data, customers: customers, contractTypes: contractTypes, periods: periods});

}

export const ContractContainerForm = composeDefault(composer, ContractForm);