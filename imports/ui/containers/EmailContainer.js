import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Email } from '/imports/ui/views/src/catalogs/Email';
import { Emails } from '/imports/api/emails/collection.js';

const composer = (props, onData) => {

  let emailHandle =  Meteor.subscribe('emails');

  if(emailHandle.ready()){
		let data = Emails.find({}).fetch();
		onData(null, {data:data});
	}
};

export const EmailContainer = composeDefault(composer, Email);
