import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Location } from '/imports/ui/views/src/catalogs/Location';
import { Locations } from '../../api/locations/collection.js';

const composer = (props, onData) => {
	if(Meteor.subscribe('locations').ready()){
		const records = Locations.find({}).fetch();
		onData(null, {records});
	}
};

export const LocationContainer = composeDefault(composer, Location);
