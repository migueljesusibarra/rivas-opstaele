import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ReceptionEquimentTestPerformed } from '/imports/ui/views/src/receptionEquipment/ReceptionEquimentTestPerformed';
import { ReceptionEquipmentTestsPerformed } from '../../api/receptionEquipmentTestPerformeds/collection.js';
import { TestsPerformed } from '../../api/testsPerformed/collection.js';

const composer = (props, onData) => {

    let receptionEquipmentId = props.receptionEquipmentId
    let systemId = props.systemId;
    let modelId = props.modelId;
    let receptionEquimentTestsPerformedHandle = Meteor.subscribe('receptionEquimentTestsPerformed');
    let testsPerformedHandle = Meteor.subscribe('testsPerformed');

    if(receptionEquimentTestsPerformedHandle.ready() && testsPerformedHandle.ready())
    {
        let records = ReceptionEquipmentTestsPerformed.find({receptionEquipmentId: receptionEquipmentId}).fetch();
        let testsPerformedIds = [];

        for (let recordItem of records)
        {
            testsPerformedIds.push(recordItem.partId);
		}

        let recordTests = TestsPerformed.find({_id: { $nin: testsPerformedIds }, systemId: systemId, modelId: modelId }).fetch();

        onData(null, {records: records, recordTests: recordTests, receptionEquipmentId: receptionEquipmentId});
    }
};

export const ReceptionEquipmentTestPerformedContainer = composeDefault(composer, ReceptionEquimentTestPerformed);
