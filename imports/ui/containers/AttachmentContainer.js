import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { AttachmentPanel } from '/imports/ui/components/src/AttachmentPanel';
import { Comments } from '../../api/comments/collection.js';

const composer = (props, onData) => {

    if(Meteor.subscribe('comments').ready()){
        const records = Comments.find({foreignId: props.foreignId, collection: props.collection, fieldType: props.fieldType}, { sort: { createdOn: 1} }).fetch();
        onData(null, {records: records, deleteFile: props.deleteFile});
    }
};

export const AttachmentContainer = composeDefault(composer, AttachmentPanel);
