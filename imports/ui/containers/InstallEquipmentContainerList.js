import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Equipments } from '../../api/equipments/collection';
import { InstallEquipmentList } from '../views/src/equipment/InstallEquipmentList';
import { Customers } from '../../api/customers/collection.js';
import { Systems } from '../../api/systems/collection.js';
import { Models } from '../../api/models/collection.js';
import { Technicians } from '../../api/technicians/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Periods } from '../../api/periods/collection.js';

const composer = (props, onData) => {

   let equipmentHandle = Meteor.subscribe('equipments');
   let customerHandle = Meteor.subscribe('customers');
   let systemHandle = Meteor.subscribe('systems');
   let modelHandle = Meteor.subscribe('models');
   let technicianHandle = Meteor.subscribe('technicians');
   let brandsHandle = Meteor.subscribe('brands');
   let periodHandle = Meteor.subscribe('periods');

   if(equipmentHandle.ready() && systemHandle.ready() && modelHandle.ready() &&
      customerHandle.ready() && technicianHandle.ready() &&  brandsHandle.ready() && periodHandle.ready())
   {
      let systems = Systems.find({}).fetch();
      let brands = Brands.find({}).fetch();
      let models = Models.find({}).fetch();
      let technicians = Technicians.find({}).fetch();
      let periods = Periods.find({}).fetch();
      let customers = Customers.find({}).fetch();

      let data = Equipments.find({}, {transform: function(record){

         record.equipment = getFieldCollection(record.systemId, systems) + " " + getFieldCollection(record.brandId, brands) + " " + getFieldCollection(record.modelId, models)
         record.technician = getFieldCollection(record.technicianId, technicians);
         record.frequency = getFieldCollection(record.frequencyId, periods);;
         record.customer = getFieldCollection(record.customerId, customers);

         return record;

      }}).fetch();

      onData(null, {
         data: data,
         systems: systems,
         models: models,
         technicians: technicians,
         brands: brands,
         foreignId: props.foreignId,
         periods: periods
      });
   }
};

export const InstallEquipmentContainerList = composeDefault(composer, InstallEquipmentList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
