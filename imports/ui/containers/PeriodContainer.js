import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Period } from '/imports/ui/views/src/catalogs/Period';
import { Periods } from '/imports/api/periods/collection.js';
import { Models } from '/imports/api/models/collection.js';

const composer = (props, onData) => {

	let modelHandler = Meteor.subscribe('models');

	if(Meteor.subscribe('periods').ready() && modelHandler.ready()){
        let models = Models.find({}).fetch();
		let records = Periods.find({}, {transform: function(record){
			record.model = fieldCollection(record.modelId, models);

			return record;
		    }}).fetch();

		onData(null, {records: records, models: models});
	}
};

export const PeriodContainer = composeDefault(composer, Period);

const fieldCollection =  (id, collection) =>
{
      let i = 0;

      for( i; i < collection.length; i++)
      {
            if(collection[i]._id == id)
            {
                  return collection[i].name || collection[i].description;
            }
      }
}
