import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { OrderType } from '/imports/ui/views/src/catalogs/OrderType';
import { OrderTypes } from '../../api/orderTypes/collection.js';

const composer = (props, onData) => {

  let orderTypeHandle =  Meteor.subscribe('orderTypes');

  if(orderTypeHandle.ready()){
		let data = OrderTypes.find({}).fetch();
		onData(null, {data:data});
	}
};

export const OrderTypeContainer = composeDefault(composer, OrderType);