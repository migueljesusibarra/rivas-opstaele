import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ServiceProgramList } from '/imports/ui/views/src/serviceOrder/ServiceProgramList.jsx';
import { ServicePrograms } from '/imports/api/servicePrograms/collection.js';
import { ServiceOrders } from '/imports/api/serviceOrders/collection.js';
import { Protocols } from '/imports/api/protocols/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { Systems } from '/imports/api/systems/collection.js';
import { Brands } from '/imports/api/brands/collection.js';
import { Periods } from '/imports/api/periods/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { RServiceProgram } from '/imports/ui/views/src/reports/RServiceProgram';
import { Contacts } from '/imports/api/contacts/collection.js';
import { Util } from '/lib/common.js';

const composer = (params, onData) =>
{
	let condition = {_id: params.id}; //este parametro es la orden de servicio para los protocolos

	let serviceHandle = Meteor.subscribe('serviceOrder', condition);

	if(serviceHandle.ready())
	{
		let serviceData = ServiceOrders.findOne(condition);

		if(serviceData != null)
		{
		  	let conditionProtocol = {modelId: serviceData.modelId, "periods.value": serviceData.period};
			let conditonPrograms = {serviceOrderId: serviceData._id, period: serviceData.period};
			let conditonModel = {_id: serviceData.modelId};
			let conditonSystem = {_id: serviceData.systemId};
			let conditonBrand = {_id: serviceData.brandId};
			let conditionPeriod = {_id: serviceData.period};

		  	let progtocolsHandle = Meteor.subscribe('protocols', conditionProtocol);
			let programHandle = Meteor.subscribe('servicePrograms', conditonPrograms);
			let modelHandle = Meteor.subscribe('models', conditonModel);
			let systemHandle = Meteor.subscribe('systems', conditonSystem);
			let brandHandle = Meteor.subscribe('brands', conditonBrand);
			let periodHandle = Meteor.subscribe('periods', conditionPeriod);

			if(progtocolsHandle.ready() && programHandle.ready() && modelHandle.ready()
			   && systemHandle.ready() && brandHandle.ready() && periodHandle.ready())
			  {
				let programsData = ServicePrograms.find(conditonPrograms).fetch();
                let modelData = Models.findOne(conditonModel);
				let systemData = Systems.findOne(conditonSystem);
				let brandData = Brands.findOne(conditonBrand);
				let periodData = Periods.findOne(conditionPeriod);

		  		let  protocolData = Protocols.find(conditionProtocol, {transform: function(record){
                                   record.check = getFieldMaterial(record._id, programsData).ok;
								   record.material = getFieldMaterial(record._id, programsData).material;

								   return record;

							  }}).fetch();

				let dataOrder =
				{
                     serviceOrderId: serviceData._id,
					 periodId: periodData._id,
					 period: periodData.description,
					 equipment: systemData.description + " " + brandData.description + " " + modelData.description
				}

				onData(null, {data: protocolData, dataOrder: dataOrder });
			}

		}else
		 {
			throw new Meteor.Error("Record was not found.");
		}
	}
};

export const ServiceProgramContainer = composeDefault(composer, ServiceProgramList);

const composerReport = (params, onData) =>
{
	let condition = {_id: params.id};
	let serviceHandle = Meteor.subscribe('serviceOrders', condition);

	if(serviceHandle.ready())
	{
		let data = ServiceOrders.findOne(condition) || {};
		let conditionCusotmer = {_id: data.customerId};
        let conditionSystem = {_id: data.systemId};
        let conditionBrand = {_id: data.brandId};
        let conditionModel = {_id: data.modelId};
		let conditionPeriod = {_id: data.period};
		let conditionProtocol = {modelId: data.modelId, "periods.value": data.period};
		let conditonPrograms = {serviceOrderId: data._id, period: data.period};

		let customerHandle = Meteor.subscribe('customers', conditionCusotmer);
        let systemHandle = Meteor.subscribe('systems', conditionSystem);
        let modelHandle = Meteor.subscribe('models', conditionModel);
        let brandHandle = Meteor.subscribe('brands', conditionBrand);
		let periodHandle = Meteor.subscribe('periods', conditionPeriod);
		let progtocolsHandle = Meteor.subscribe('protocols', conditionProtocol);
		let programHandle = Meteor.subscribe('servicePrograms', conditonPrograms);

		if(customerHandle.ready() && systemHandle.ready() && modelHandle.ready()
           && brandHandle.ready() && progtocolsHandle.ready() && programHandle.ready())
		   {
			   let customer = Customers.findOne(conditionCusotmer);
               let system = Systems.findOne(conditionSystem);
               let brand = Brands.findOne(conditionBrand);
               let model = Models.findOne(conditionModel);
			   let periodData = Periods.findOne(conditionPeriod);
			   let conditionContact = {foreignId: customer._id, main: true}
               let contactHandle = Meteor.subscribe('contacts', conditionContact);

			   if(contactHandle.ready())
               {
                   let contact =  Contacts.findOne(conditionContact);
				   let programsData = ServicePrograms.find(conditonPrograms).fetch();

				   let  protocolData = Protocols.find(conditionProtocol, {transform: function(record){
                                      record.check = getFieldMaterial(record._id, programsData).ok;
   								      record.material = getFieldMaterial(record._id, programsData).material;

   								   return record;

   							  }}).fetch();

                   let record = {
                       Id: data._id,
					   period: periodData.description,
                       system: system.description,
                       brand: brand.description,
                       model: model.description,
                       serialNumber: data.serialNumber,
                       releaseSw: data.releaseSw,
                       installationDate: data.installationDate,
                       warrantyExpires: data.warrantyExpires,
                       name: customer.name,
                       address: customer.address,
                       phone: customer.phone,
                       fax: "",
                       contact: contact !=  null ? contact.name : "",
                       email: customer.email
                }

				console.log(protocolData);

				onData(null, {
	                    record: record,
						protocolData: protocolData
	                 });
		   }
	}
}
}

export const RServiceProgramContainer = composeDefault(composerReport, RServiceProgram);

const getFieldMaterial =  (id, collection) =>
{
	let i = 0;
	let data = {}
	let check = false;

	for( i; i < collection.length; i++)
   	{
		if(collection[i].protocolId  == id)
      	{
			data.ok = true;
			data.material = collection[i].material;
            check = true

          	return data;
	     }
	 }

	 if(!check)
	 {
     	data.ok = false;
		data.material = "";
		return data;
	 }
}
