import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ServiceOrderPartList } from '/imports/ui/views/src/serviceOrder/ServiceOrderPartList.jsx';
import { Parts } from '/imports/api/parts/collection.js';
import { ServiceOrderParts } from '/imports/api/serviceOrderParts/collection.js';


const composer = (props, onData) =>{

    let partHandle = Meteor.subscribe('parts');
    let serviceOrderPartHandle = Meteor.subscribe('serviceOrderParts');

    if(partHandle.ready() && serviceOrderPartHandle.ready())
    {
        let serviceOrderParts = ServiceOrderParts.find({serviceOrderId: props.serviceOrderId}).fetch();
        let partIds = [];

		for (let serviceOrderPart of serviceOrderParts)
        {
		  partIds.push(serviceOrderPart.partId);
        }

        let parts = Parts.find({ _id: { $nin: partIds }, active: true}).fetch();
        onData(null, {parts: parts, serviceOrderParts: serviceOrderParts});
    }
};

export const ServiceOrderPartContainerList = composeDefault(composer, ServiceOrderPartList);
