import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Roles } from '../../api/roles/collection.js';
import { UserList } from '/imports/ui/views/src/users/UserList.jsx';

const composer = (props, onData) => {

    let members = Meteor.subscribe('members');
    let rolesHandle = Meteor.subscribe('roles');

    if(members.ready() && rolesHandle.ready())
    {
        let roles = Roles.find({}).fetch();

        const records = Meteor.users.find({},{transform: function(record)
        {
            let typeName = fieldCollection(record.profile ? record.profile.type : '', roles);
            record.typeName = typeName;

			 return record;
		}}).fetch();

        onData(null, {records});
    }
};

export const UserContainerList = composeDefault(composer, UserList);

const fieldCollection =  (id, collection) =>
{
    let i = 0;

    for( i; i < collection.length; i++)
    {
        if(collection[i]._id == id)
            return collection[i].name || collection[i].description;
    }
}
