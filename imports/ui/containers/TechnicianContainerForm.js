import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Technicians } from '../../api/technicians/collection.js';
import { TechnicianForm } from '../views/src/technicians/TechnicianForm.jsx';

const composer = (params, onData) => {
     var condition = {_id: params.id};
     let technicianHandle = Meteor.subscribe('technicians', condition);

    if(technicianHandle.ready())
    {
       let data = Technicians.findOne(condition) || {};
       let id = params.id ? params.id : null;
       let isNew = id == "add" ||  id == null;

       onData(null, {isNew: isNew, data: data});
    }

};

export const TechnicianContainerForm = composeDefault(composer, TechnicianForm);
