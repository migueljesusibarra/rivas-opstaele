import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Model } from '/imports/ui/views/src/catalogs/Model';
import { Models } from '../../api/models/collection.js';
import { Brands } from '../../api/brands/collection.js';
import { Systems } from '../../api/systems/collection.js';

const composer = (props, onData) => {

      let systemHandler = Meteor.subscribe('systems');
      let branHandler = Meteor.subscribe('brands');
      let modelHandler = Meteor.subscribe('models');

      if(branHandler.ready() && modelHandler.ready() && systemHandler.ready())
      {
            let systems = Systems.find({}).fetch();
            let brands = Brands.find({}).fetch();

            let records = Models.find({}, {transform: function(record){

                  record.system = fieldCollection(record.systemId, systems);
                  record.brand = fieldCollection(record.brandId, brands);

                  return record;

            }}).fetch();

            onData(null, {records: records, brands: brands, systems: systems});
      }
};

export const ModelContainer = composeDefault(composer, Model);

const fieldCollection =  (id, collection) =>
{
      let i = 0;

      for( i; i < collection.length; i++)
      {
            if(collection[i]._id == id)
            {
                  return collection[i].name || collection[i].description;
            }
      }
}
