import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ProtocolList } from '/imports/ui/views/src/protocols/ProtocolList';
import { Protocols } from '/imports/api/protocols/collection.js';
import { Models } from '/imports/api/models/collection.js';

const composer = (props, onData) => {

	let periodsHandle = Meteor.subscribe('periods')

	if(Meteor.subscribe('protocols').ready() && Meteor.subscribe('models').ready() && periodsHandle.ready())
	{
        let models = Models.find({}).fetch();

        let records = Protocols.find({isParent: true}, {transform: function(record) {
             record.model = fieldCollection(record.modelId, models);

           return record;

        }}).fetch();

           
		onData(null, {records: records, models: models});
	}
};

export const ProtocolContainerList = composeDefault(composer, ProtocolList);

const fieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
