import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PieWithLegend } from '/imports/ui/views/src/charts/PieWithLegend';
import { Models } from '/imports/api/models/collection.js';
import { Equipments } from '/imports/api/equipments/collection.js';
import { Systems } from '/imports/api/systems/collection.js';

const composer = (props, onData) => {

     let equipmentHandle = Meteor.subscribe('equipments');
     let modelHandle = Meteor.subscribe('models');
     let systemHandle = Meteor.subscribe('systems');
     let dataProviders = [];

	if(equipmentHandle.ready() && modelHandle.ready() && systemHandle.ready())
    {
        let systems = Systems.find({}, {transform: function(record)
        {
             providerItem = {
                     country: record.description,
                     litres: Equipments.find({systemId: record._id}).count()
             };

             dataProviders.push(providerItem);

             return record;

        }}).fetch();

		onData(null, {dataProviders});
	}
};

export const PieWithLegendContainer = composeDefault(composer, PieWithLegend);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id == id)
      {
         return collection[i].description || collection[i].name;
      }
   }
}
