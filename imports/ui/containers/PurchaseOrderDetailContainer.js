import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { PurchaseOrderDetails } from '../../api/purchaseOrderDetails/collection.js';
import { PurchaseOrderDetail } from '../views/src/purchaseOrder/PurchaseOrderDetail.jsx';

const composer = (props, onData) => {
 let purchaseOrderDetailHandle = Meteor.subscribe('purchaseOrderDetails');

 if(purchaseOrderDetailHandle.ready())
 {
   let data = PurchaseOrderDetails.find({purchaseOrderId: props.purchaseOrderId}).fetch();
   let totalAmount = 0;
   let count = 0 ;
   
   data.forEach(function(item)
   {
     totalAmount += item.totalPrice;
     ++count;

   }, this);

   onData(null,{

     data: data,
     purchaseOrderId: props.purchaseOrderId,
     status: props.status,
     totalAmount: totalAmount,
     count:count
   })
 }
};

export const PurchaseOrderDetailContainer = composeDefault(composer, PurchaseOrderDetail);
