import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { UserForm } from '/imports/ui/views/src/users/UserForm.jsx';
import { Roles } from '../../api/roles/collection.js';

const composer = (params, onData) => {
     let condition = {_id: params.id, type: {$ne: 'Dealer'}};
     let membersHandle = Meteor.subscribe('members', condition);
     let rolesHandle = Meteor.subscribe('roles');
     //let rolesHandle = Meteor.subscribe('roles');

    if(membersHandle.ready()){
        let data = Meteor.users.findOne(condition) || {profile: {}};
        let roles = Roles.find({}).fetch();
        let id = data._id ? data._id : null;
        
        onData(null, Object.assign(data, {
            isNew: id == null,
            id: id,
            roles: roles
        }));
    }
};

export const UserContainerForm = composeDefault(composer, UserForm);
