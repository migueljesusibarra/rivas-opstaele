import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { VendorList } from '/imports/ui/views/src/vendor/VendorList';
import { Vendors } from '/imports/api/vendors/collection.js';

const composer = (props, onData) =>
{
  let vendorHandler = Meteor.subscribe('vendors')

    if(vendorHandler.ready())
    {
        let data = Vendors.find({}).fetch();
        onData(null, {data: data});
    }
};

export const VendorContainerList = composeDefault(composer, VendorList);
