import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ServiceOrderActivityList } from '/imports/ui/views/src/serviceOrder/ServiceOrderActivityList.jsx';
import { ActivitiesService } from '/imports/api/activitiesService/collection';
import { Activities } from '/imports/api/activities/collection';


const composer = (props, onData) =>{
    let condition = {serviceOrderId: props.serviceOrderId}
    let activitiesServiceHandle = Meteor.subscribe('activitiesService', condition);
    let activitiesHandle = Meteor.subscribe('activities');

    if(activitiesServiceHandle.ready() && activitiesHandle.ready())
    {      
        const activities = Activities.find({}).fetch();  
        
        const records = ActivitiesService.find(condition, {transform: function(record)
        {
        	record.description = getFieldCollection(record.activityId, activities);
        
        	return record;
        
        }}).fetch();
               
        onData(null, {records: records, activities: activities});
    }
};

export const ServiceOrderActivityContainerList = composeDefault(composer, ServiceOrderActivityList);

const getFieldCollection =  (id, collection) =>
{
   let i = 0;

   for( i; i < collection.length; i++)
   {
      if(collection[i]._id  == id)
      {
         return collection[i].description;
      }
   }
}
