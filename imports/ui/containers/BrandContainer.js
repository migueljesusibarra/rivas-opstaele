import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { Brand } from '/imports/ui/views/src/catalogs/Brand';
import { Brands } from '../../api/brands/collection.js';

const composer = (props, onData) => {
	if(Meteor.subscribe('brands').ready()){
		const records = Brands.find({}).fetch();
		onData(null, {records});
	}
};

export const BrandContainer = composeDefault(composer, Brand);
