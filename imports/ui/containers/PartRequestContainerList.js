import { Meteor } from 'meteor/meteor';
import { composeDefault } from '/lib/common';
import { ServicePartRequestList } from '/imports/ui/views/src/serviceOrder/ServicePartRequestList';
import { PartsRequest } from '../../api/partsRequest/collection.js';
import { Parts } from '/imports/api/parts/collection.js';


const composer = (props, onData) => {

	let condition = {serviceOrderId: props.serviceOrderId}
	let partHandle = Meteor.subscribe('parts');
	let partRequestHandle = Meteor.subscribe('partsRequest', condition);

	if(partRequestHandle.ready() && partHandle.ready())
	{
		const records = PartsRequest.find(condition).fetch();

		let partIds = [];

		for (let record of records)
	    {
		  partIds.push(record.partId);
	    }

		let parts = Parts.find({ _id: { $nin: partIds }, active: true}).fetch();

		onData(null, {records:records, parts: parts});
	}
 };

export const PartRequestContainerList = composeDefault(composer, ServicePartRequestList);
