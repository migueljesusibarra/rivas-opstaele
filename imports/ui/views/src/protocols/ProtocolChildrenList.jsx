import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel, Dropdown, NoteField} from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';

export class ProtocolChildrenList extends Component
{
  constructor(props)
  {
    super(props);

    this.state = {
        search: null,
        records: props.records
    };

    this.onSearch = this.onSearch.bind(this);
    this.openDialogChildren = this.openDialogChildren.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.saveChanges = this.saveChanges.bind(this);

  }

  onSearch(event)
  {
    this.setState({search: event.target.value});
  }

  getModels()
  {
      this.props.models.sort(Util.applySortObject);

      return this.props.models.map(function(model)
      {
          return {value: model._id, text: model.description};
      });
  }

  getColumns()
  {
      let self = this;

      let columns = [
      {
          key: 2,
          header: 'Pos',
          minWidth: 0,
          maxWidth: 20,
          dataIndex: 'position'
       },
      {
          key: 3,
          header: 'Actividad',
          minWidth: 0,
          maxWidth: 100,
          renderer: function(record)
          {
              if(record.activity.length > 72 || record.material.length > 72 )
                   return (<textarea id={record._id + "-id"} disabled rows={self.getRowTextArea(record)} style={{width: "100%", border: "none", backgroundColor: "#fff", resize: "none"}} defaultValue={record.activity}></textarea>)
               else
                  return record.activity

          }

      },
      {
          key: 4,
          header: 'P#',
          minWidth: 0,
          maxWidth: 10,
          dataIndex: 'period'
      },
      {
          key: 5,
          minWidth: 0,
          maxWidth: 100,
          header: 'Material',
          renderer: function(record)
          {
              if(record.activity.length > 72 || record.material.length > 72 )
                    return (<textarea id={record._id + "-id"} disabled rows={self.getRowTextArea(record)} style={{width: "100%", border: "none", backgroundColor: "#fff", resize: "none"}} defaultValue={record.material} ></textarea>)
              else
                   return record.material == null || record.material == "" ? "- - - - - " : record.material;
          }

      }];

      return columns;
  }

  getRowTextArea(record)
  {
      let value = record.material.length > record.activity.length ?  record.material.length : record.activity.length;

      let row = 5;

      if(value <= 60)
            row = 1
      else if(value > 60 && value <= 120)
            row = 2
      else if(value > 120 && value <= 180)
            row = 3
      else if(value < 300)
           row = 4

      return row;
  }

  getToolbar()
  {
      let self = this;

      let items = [
      {
          type: 'group',
          className: 'hbox-l',
          items: [{
                text: 'Agregar como Master',
                cls: 'button-add',
                handler: function()
                {
                    let record = self.refs.protocolChildrenGrid.getSelectedRecord();

                    if(record != null)
                        Meteor.call("updateIsParent", record._id, true, function(error, result){});
                    else
                        alert("Por favor seleccione un registro");
              }
          },
          {
              text: 'Editar',
              cls: 'button-edit',
              handler: function()
              {
                  let record = self.refs.protocolChildrenGrid.getSelectedRecord();

                  if(record != null)
                      self.openDialogChildren(record);
                  else
                      alert("Por favor seleccione un registro");
              }
          },
          {
              text: 'Eliminar',
              cls: 'button-delete',
              handler: function()
              {
                  //self.removeModel();
              }
          }]
      }];

      let toolbar = (
      <div>
          <Toolbar className="north hbox" id="main-toolbar" items={items} />
          <div className="hbox div-second-tool-bar">
              <div className="middle-width-quarter-column">
                  <SearchField
                  ref= "search"
                  key={"searchfield"}
                  className = "search-field-single-customer"
                  placeholder=" Buscar"
                  value={this.state.search}
                  onSearch={this.onSearch} />
              </div>
              <div className="middle-width-middle-column text-aling-second-tool-bar">
                  <span>{"Protocolos Detalle"}</span>
              </div>
          </div>
      </div>);

      return toolbar;
  }

  getDialogChildren()
  {
      let items = [
      {
          text: 'Guardar',
          cls: 'button-save',
          handler: this.saveChanges
      },
      {
          key: 'close',
          text: 'Cerrar',
          cls: 'button-close',
          handler: this.closeDialog
      }];

      let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

      return (
          <Dialog ref="dialogChildren" className="contact-dialog" title="Tipo de cliente" modal>
              {toolbar}
              <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                  <HiddenField ref="record" value="" />
                  <Dropdown ref="model" label="Modelo" items={this.getModels()}  emptyOption={true} required={true}/>
                  <TextField ref="position" label="Posición" required/>
                  <NoteField ref="activity" label="Actividad" required/>
                  <TextField ref="period" label="Periodo" required/>
                  <NoteField ref="material" label="Material" />
              </FormPanel>
          </Dialog>
      );
  }

  openDialogChildren(record)
  {
      let self = this;
      let fields = self.refs;

      if(record != null)
          {
              fields.record.setValue(record._id);
              fields.model.setValue(record.modelId);
              fields.position.setValue(record.position);
              fields.activity.setValue(record.activity);
              fields.period.setValue(record.period);
              fields.material.setValue(record.material);
              fields.dialogChildren.setTitle("Editar Protocolo");
              fields.dialogChildren.open();
          }
          else
              alert("Por favor seleccione un registro.");
      }

  saveChanges()
  {
      let self = this;
      let fields = self.refs;

      if(fields.form.isValid(fields))
      {
          let recordId = fields.record.getValue();

          let dataUpdate = {
                            modelId: fields.model.getValue(),
                            activity: fields.activity.getValue(),
                            period: fields.period.getValue(),
                            material: fields.material.getValue(),
                       }

        Meteor.call("updateProtocol", recordId, dataUpdate, function(error, result){});
        fields.dialogChildren.close();
  }
  else
      alert("Por favor ingrese todos los campos requeridos.");
  }

  getRecords()
  {
    let search = this.state.search;

    if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
    else
        return this.props.records;
  }

  onFilter(record, search)
  {
    return (record.model.search(new RegExp(search, "i")) != -1 ||
            record.activity.search(new RegExp(search, "i")) != -1);
  }

  getChildrenGridPanel()
  {
      return (<GridPanel style={{height: '283px'}}
                id="protocolChildrenGrid"
                title="Protocol"
                ref="protocolChildrenGrid"
                columns={this.getColumns()}
                records={this.getRecords()}
                toolbar={this.getToolbar()} />);
  }

  closeDialog()
  {
      this.refs.dialogChildren.close();
  }

  render()
  {
    return (<div>
            {this.getChildrenGridPanel()}
            {this.getDialogChildren()}
        </div>)
  }
}
