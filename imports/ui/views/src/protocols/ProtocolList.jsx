import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel, Dropdown, NoteField, CheckBox, DialogPanel} from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';
import { ProtocolChildrenList } from './ProtocolChildrenList.jsx';
import { Protocols } from '/imports/api/protocols/collection.js';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Periods } from '/imports/api/periods/collection.js';

export class ProtocolList extends Component
{
  constructor(props)
  {
      super(props);

      this.state = {
          search: null,
          record: null,
          isChildren: true,
          recordChildrens: null,
          recordDialog: null,
          opened: false,
          title: "New"

    };

    this.onSearch = this.onSearch.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
    this.onSelectionChange = this.onSelectionChange.bind(this);
    this.onChangeModel = this.onChangeModel.bind(this);

  }

  componentWillReceiveProps(nextProps)
  {
       let record = this.refs.protocolGrid.getSelectedRecord();
       this.getUpdateSetState(record);
  }

  onRowDoubleClick(record)
  {
     this.openRecord(record);
  }

   getUpdateSetState(record)
   {

       if(record != null && record.isParent && record.parenttage != '' && record.parenttage != null)
       {
           let records = Protocols.find({parent: record.parenttage}).fetch();

           if(records.length > 0)
               this.setState({recordChildrens: records, record: record});
           else
              this.setState({recordChildrens: [], record: record});
      }
      else
      {
          this.setState({record: record});
      }
   }

  onSelectionChange(record)
  {
    this.getUpdateSetState(record);
  }

  openRecord(record)
  {
      recordDialog = {
              record: record._id,
              parent: record.parent,
              parenttage: record.parenttage,
              modelId: record.modelId,
              position: record.position,
              activity: record.activity,
              material: record.material,
              periods: record.periods
      }

      this.setState({recordDialog: recordDialog, opened: true, title: "Editar"});
  }

  getModels()
  {
      this.props.models.sort(Util.applySortObject);

      return this.props.models.map(function(model)
      {
          return {value: model._id, text: model.description};
      });
  }

  onChangeModel(value)
  {
      let fields = this.refs;

      recordDialog = {
              record: fields.record.getValue(),
              parent: fields.parent.getValue(),
              parenttage: fields.parenttage.getValue(),
              modelId: value,
              position: fields.position.getValue(),
              activity: fields.activity.getValue(),
              material: fields.material.getValue()
      }

     this.setState({recordDialog: recordDialog});
  }
  onChangeCheckbook(value)
  {
      console.log("check");
  }

  getPeriods(recordDialog)
  {
      let self = this;

     let periods = Periods.find({modelId: recordDialog != null ?  recordDialog.modelId : 0}).fetch();

     if(periods.length > 0)
     {
         let componentgrup = periods.map(function(period)
         {
            let periodCheck = self.onExistCheck(recordDialog.periods != null ?  recordDialog.periods : [], period._id);

            let id = "check-"+(new Date()).getMilliseconds()+Math.floor(Math.random()*1000);
             return (<th key={"row-P" + period._id}>
                        <input className="periodCheckbox" key={id} type="checkbox"  value={period._id} defaultChecked={periodCheck} onChange={this.onChangeCheckbook} />
                        <spam>{"P" + period.description}</spam>
                    </th>);
         });

          return componentgrup;
     }else {
         return (<th key={"row-P-0"}>
                   <spam>{"No hay periodos configurados para este modelo"}</spam>
                  </th>);
     }


  }

  onExistCheck(array, record)
  {
     let i = 0;
     let check = false;

     for( i; i < array.length; i++)
     {
        if(array[i].value  == record)
        {
           check = true;
           break;
        }
    }

    return check;
  }

  onSearch(event)
  {
    this.setState({search: event.target.value});
  }

  getColumns()
  {
      let self = this;
      
      let columns = [
      {
          key: 1,
          header: 'Id',
          minWidth: 0,
          maxWidth: 10,
          dataIndex: 'index'
        //   renderer: function(record)
        //   {
        //       return record.parenttage;
        //   }
      },
      {
          key: 2,
          header: 'Modelo',
          minWidth: 0,
          maxWidth: 30,
          dataIndex: 'model'
      },
      {
          key: 3,
          header: 'Pos',
          minWidth: 0,
          maxWidth: 20,
          dataIndex: 'parenttage'
      },
      {
          key: 4,
          header: 'Actividad',
          minWidth: 0,
          maxWidth: 100,
          renderer: function(record)
          {
              if(record.activity.length > 72 || record.material.length > 72 )
                   return (<textarea id={record._id + "-id"} disabled rows={self.getRowTextArea(record)} style={{width: "100%", border: "none", backgroundColor: "#fff", resize: "none"}} defaultValue={record.activity}></textarea>)
               else
                  return record.activity

          }

      },
      {
          key: 5,
          header: 'P#',
          minWidth: 0,
          maxWidth: 10,
          dataIndex: 'period'
      },
      {
          key: 6,
          minWidth: 0,
          maxWidth: 100,
          header: 'Material',
          renderer: function(record)
          {
              if(record.activity.length > 72 || record.material.length > 72 )
                    return (<textarea id={record._id + "-id"} disabled rows={self.getRowTextArea(record)} style={{width: "100%", border: "none", backgroundColor: "#fff", resize: "none"}} defaultValue={record.material} ></textarea>)
              else
                   return record.material == null || record.material == "" ? "- - - - - " : record.material;
          }

      }];

      return columns;
  }

  getRowTextArea(record)
  {
      let value = record.material.length > record.activity.length ?  record.material.length : record.activity.length;

      let row = 5;

      if(value <= 60)
            row = 1
      else if(value > 60 && value <= 120)
            row = 2
      else if(value > 120 && value <= 180)
            row = 3
      else if(value < 300)
           row = 4

      return row;
  }

  getToolbarItems()
  {
     let self = this;

     let items = {
                  new: {
                      text: 'Nuevo',
                      cls: 'button-new-dealer',
                      handler: function()
                      {
                          recordDialog = {
                                  record: "",
                                  parent: "",
                                  parenttage: "",
                                  modelId: "",
                                  position: "",
                                  activity: "",
                                  material: "",
                                  periods: ""
                          }

                          self.setState({recordDialog: recordDialog, opened: true, title: "New"});
                      }
                  },
                edit: {
                    text: 'Editar',
                    cls: 'button-edit',
                    handler: function()
                    {
                        let record = self.refs.protocolGrid.getSelectedRecord();
                        if(record != null)
                            self.openRecord(record);
                        else
                            self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                    }
                },
                addDetail: {
                    text: 'Agregar Detalle',
                    cls: 'button-add',
                    handler: function()
                    {
                        let record = self.refs.protocolGrid.getSelectedRecord();

                        if(record != null){

                          recordDialog = {
                                record: "",
                                parent: record.position, //se agrega la position del seleccinado poer que ese sera el padre del registro nuevo
                                parenttage: record.parenttage,
                                modelId: record.modelId,
                                position: "",
                                activity: "",
                                material: "",
                                periods: ""
                           }
                            self.setState({recordDialog: recordDialog, opened: true, title: "Detalle"});

                        }else
                            self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                 }
             },
             remove: {
                    text: 'Eliminar',
                    cls: 'button-delete',
                    handler: function()
                    {
                        //self.removeModel();
                    }
            },
            history: {
                text: 'Historial',
                cls: 'button-history',
                handler: function()
                {
                    let record = self.refs.protocolGrid.getSelectedRecord();

                    if(record != null)
                        self.refs.historyProtocol.onPenDialog();
                    else
                        self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                }
            }
      }
      return items;
  }

  getToolbar()
  {
      let self = this;
      let actions = self.getToolbarItems();
      leftGroup = [actions.new, actions.edit, actions.addDetail, actions.remove], rightGroup = [actions.history];

      let items = [{
          type: 'group',
          className: 'hbox-l',
          items: leftGroup
      },
      {
          type: 'group',
          className: 'hbox-r',
          items: rightGroup
      }];

      let toolbar = (
      <div>
          <Toolbar className="north hbox" id="main-toolbar" items={items} />
          <div className="hbox div-second-tool-bar">
              <div className="middle-width-quarter-column">
                  <SearchField
                  ref= "search"
                  key={"searchfield"}
                  className = "search-field-single-customer"
                  placeholder=" Buscar"
                  value={this.state.search}
                  onSearch={this.onSearch} />
              </div>
              <div className="middle-width-middle-column text-aling-second-tool-bar">
                  <span>{"Protocolos Master"}</span>
              </div>
          </div>
      </div>);

      return toolbar;
  }

  getDialog()
  {
      let self = this;
      let items = [
      {
          text: 'Guardar',
          cls: 'button-save',
          handler: function()
          {
              let fields = self.refs;

              if(fields.parent.getValue() != '' && fields.parent.getValue() != null
                 && fields.record.getValue() == '' || fields.record.getValue() == null)
                 self.saveChildrenChanges();
              else
                 self.saveChanges();

          }
      },
      {
          key: 'close',
          text: 'Cerrar',
          cls: 'button-close',
          handler: this.closeDialog
      }];

      let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);
      let recordDialog = this.state.recordDialog;
      let display = this.state.opened ? 'block' : 'none';

      return (<div className="overlay" onClick={this.onHide} style={{display: display}}>x

          <DialogPanel id={this.props.id}   className={this.props.className} style={{display: display}}>
                   {toolbar}
              <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                                  <HiddenField ref="record" value={recordDialog != null ? recordDialog.record : ""} />
                                  <HiddenField ref="parent" value={recordDialog != null ? recordDialog.parent : ""} />
                                  <HiddenField ref="parenttage" value={recordDialog != null ? recordDialog.parenttage : ""} />
                                  <Dropdown ref="model" label="Modelo" items={this.getModels()} value={recordDialog != null ? recordDialog.modelId : ""} onChange={this.onChangeModel.bind(this)} emptyOption={true} required={true} />
                                  <TextField ref="position" label="Posición" value={recordDialog != null ? recordDialog.position : ""} required/>
                                  <NoteField ref="activity" label="Actividad" value={recordDialog != null ? recordDialog.activity : ""} required/>
                                 <NoteField ref="material" label="Material"  value={recordDialog != null ? recordDialog.material : ""} />
                                <div>
                                    <spam>Periodos</spam>
                              <div style={{border: "2px solid #89bcf5"}}>
                            <table>
                              <thead className="table-protocol-Periods">
                                 <tr>
                                   {this.getPeriods(this.state.recordDialog)}
                                </tr>
                          </thead>
                      </table>
                  </div>
                      </div>
             </FormPanel>
          </DialogPanel>
      </div>);

  }

  openDialog(action, record)
  {
      let self = this;
      let fields = self.refs;

      if(action == 'edit')
      {
          if(record != null)
          {
              fields.record.setValue(record._id);
              fields.parent.setValue(record.parent);
              fields.parenttage.setValue(record.parenttage);
              fields.model.setValue(record.modelId);
              fields.position.setValue(record.position);
              fields.activity.setValue(record.activity);
              //fields.period.setValue(record.period);
              fields.material.setValue(record.material);
              fields.dialog.setTitle("Editar Protocolo");
              fields.dialog.open();
          }
          else
              alert("Por favor seleccione un registro.");
      }
      else if(action == 'add')
          {
              fields.record.setValue('');
              fields.parent.setValue('');
              fields.parenttage.setValue('');
              fields.model.setValue('');
              fields.position.setValue('');
              fields.activity.setValue('');
              //fields.period.setValue('');
              fields.material.setValue('');
              fields.dialog.setTitle("Nuevo Protocolo");

        fields.dialog.open();

      }
      else if(action == 'children')
      {
          fields.record.setValue('');
          fields.parent.setValue(record.position);
          fields.parenttage.setValue(record.parenttage);
          fields.model.setValue(record.modelId);
          fields.position.setValue('');
          fields.activity.setValue('');
          fields.period.setValue('');
          fields.material.setValue('');
          fields.dialog.setTitle("Nuevo Protocolo Hijo");
          fields.dialog.open();
      }
  }

  saveChildrenChanges()
  {
      let self = this;
      let fields = self.refs;

      if(fields.form.isValid(fields))
      {
          let recordId = fields.record.getValue();

          let data = {
                modelId: fields.model.getValue(),
                position: fields.position.getValue(),
                periods: this.getPropertiesPeriods(),
                activity: fields.activity.getValue(),
                material: fields.material.getValue(),
                parent: fields.parenttage.getValue(),
                parenttage: fields.parenttage.getValue() + "." + fields.position.getValue(),
                isParent: false,
                isChildren: true
          }

          Meteor.call("addProtocol", data, function(error, result){});
          self.setState({opened: false});
      }
      else
          alert("Por favor ingrese todos los campos requeridos.");
  }

   getPropertiesPeriods()
   {
       let checkedValue = [];

       let inputElements = document.getElementsByClassName('periodCheckbox');

       for(var i=0; inputElements[i]; ++i)
         {
             if(inputElements[i].checked)
             {
                 checkedValue.push({value :inputElements[i].value});
             }
         }

         return checkedValue;
   }

  saveChanges()
  {
      let self = this;
      let fields = self.refs;

      if(fields.form.isValid(fields))
      {
          let recordId = fields.record.getValue();

          let data = {
                        modelId: fields.model.getValue(),
                        position: fields.position.getValue(),
                        periods: this.getPropertiesPeriods(),
                        activity: fields.activity.getValue(),
                        material: fields.material.getValue(),
                        parent: "",
                        parenttage: fields.position.getValue(),
                        isParent: true,
                        isChildren: false
                     }

          if(recordId.length > 0)
          {
              let dataUpdate = {
                                    modelId: fields.model.getValue(),
                                    position: fields.position.getValue(),
                                    activity: fields.activity.getValue(),
                                    periods: this.getPropertiesPeriods(),
                                    material: fields.material.getValue(),
                               }

              Meteor.call("updateProtocol", recordId, dataUpdate, function(error, result){});
          }
          else
              Meteor.call("addProtocol", data, function(error, result){});

             self.setState({opened: false});
      }
      else
          alert("Por favor ingrese todos los campos requeridos.");
  }

  getRecords()
  {
    let search = this.state.search;

    if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
    else
        return this.props.records;
  }

  onFilter(record, search)
  {
    return (record.model.search(new RegExp(search, "i")) != -1 ||
            record.activity.search(new RegExp(search, "i")) != -1 ||
            record.parenttage.search(new RegExp(search, "i")) != -1);
  }

  getGridPanel()
  {
      let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyProtocol" module="Protocol" foreignId={this.state.record._id} title="Historial Protocolo"/>

  return (<div>
                  <ToastrMessage ref="toastrMsg" />
                  {historyContainer}
                  <GridPanel style={{height: '283px'}}
                      id="protocolGrid"
                      title="Protocol"
                      ref="protocolGrid"
                      columns={this.getColumns()}
                      records={this.getRecords()}
                      toolbar={this.getToolbar()}
                      onRowDoubleClick = {this.onRowDoubleClick}
                      onSelectionChange={this.onSelectionChange}   />
             </div>);
  }

  closeDialog()
  {
     this.setState({opened: false});
  }

  render()
  {
      let recordChildrens = this.state.recordChildrens != null ? this.state.recordChildrens : [];

    return (<div>
            <div  style={{height: '50%'}}>
            {this.getGridPanel()}
        </div>
        <div style={{height: '50%'}}>
            <ProtocolChildrenList  records = {recordChildrens} models ={this.props.models} />
            </div>
            {this.getDialog()}
        </div>)
  }
}
