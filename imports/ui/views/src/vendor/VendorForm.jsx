import React, { Component } from 'react';
import { Container, Toolbar, FormPanel, Fieldset, FormColumn, Dropdown, TextField, NoteField, container,
  Button, PhoneField, EmailField, DisplayField, GridPanel, Input } from 'react-ui';
  import { ContactContainer } from '../../../containers/ContactContainer'
  import { ContactPanelContainer } from '../../../containers/ContactPanelContainer'
  import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

  export class VendorForm extends Component
  {
    static get defaultProps()
    {
      return {
        ventorTypes: [
          {value: "Phillips", text: "Phillips"},
          {value: "Not Phillips", text: "Not Phillips"},
        ]
      };
    }

    constructor(props)
    {
      super(props);
      this.onVendorAdded = this.onVendorAdded.bind(this);
    }

    getVendorTypes()
    {
      return this.props.ventorTypes.map(function(type)
      {
        return { value:type.value, text:type.text };
      });
    }

    getToolbarItems()
    {
      let self = this;
      let items =
      {
        back:{
          text: '',
          cls: 'button-back',
          handler: function(){
            FlowRouter.go('/vendors');
          }
        },
        new:  {
          text: 'Nuevo',
          cls: 'button-new-dealer',
          handler: function(){
            self.setState({_action: "add"});
            FlowRouter.go('/vendor/add');
          }
        },
        save:{
          text: 'Guardar',
          cls: 'button-save',
          handler:function(){
            self.saveChanges("save");
          }
        },
        saveAndClose:{
          text: 'Guardar y Cerrar',
          cls: 'button-save-close',
          handler: function()
          {
            self.saveChanges("saveAndClose")
          }
        },
        print:  {
          text: 'Imprimir',
          cls: 'button-print',
          handler: function()
          {
            console.log('imprimir');
          }
        },
        toggle:{
          text: this.props.isNew || this.props.data.active !== false ? 'Desactivar' : 'Activar',
          cls: 'button-delete',
          handler: function(){
            self.toggleActive();
          }
        }
      }

      return items
    }

    getToolbar()
    {
      let self = this;
      let actions = self.getToolbarItems();
      let leftGroup = []

      if(this.props.isNew)
      {
        leftGroup.push(actions.back);
        leftGroup.push(actions.save);
      }
      else
      {
        leftGroup.push(actions.back);
        leftGroup.push(actions.new);
        leftGroup.push(actions.save);
        leftGroup.push(actions.saveAndClose);
        leftGroup.push(actions.toggle);
      }

      let items = [
        {
          type: 'group',
          className: 'hbox-l',
          items: leftGroup
        }];

        return <Toolbar className="north hbox" items={items} />;
      }

      toggleActive()
      {
        let message = this.props.data.active ? "Está seguro que desea activar este Proveedor?" :  "Está seguro que desea desactivar este Proveedor?"
        let confirmation = confirm(message);

        if( confirmation == true)
        Meteor.call("setStatusVendor", this.props.data._id, this.props.data.active ? false : true);
      }

      getProperties(fields)
      {
        let record = {
          name: fields.name.getValue(),
          address: fields.address.getValue(),
          phone: fields.phone.getValue(),
          type: fields.type.getValue(),
          note: fields.note.getValue(),
          active: this.props.isNew ? true : this.props.data.active
        }
        return record;
      }

      saveChanges(button)
      {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
          let properties = this.getProperties(fields);

          if (this.props.isNew)
          Meteor.call("addVendor", properties, this.onVendorAdded);
          else
          {
            Meteor.call("updateVendor", this.props.data._id, properties, function(error, result){
              if(!error)
              {
                if(button == "saveAndClose")
                FlowRouter.go('/vendors');
                else {
                  self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
                }

              }else {
                throw self.getToast().showMessage("error", error);
              }
            });
          }
        }
        else
        {
          self.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
        }
      }

      onVendorAdded(error, result)
      {
        if(!error)
        {
          this.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
          FlowRouter.go('/vendor/' + result);
        }
        else
        this.getToast().showMessage("error", error);
      }

      getToast()
      {
        return this.refs.toastrMsg;
      }

      render()
      {
        let data = this.props.data || {};

        let contactPanel = (<ContactPanelContainer foreignId = {data._id} module = "vendor"/>);

        let textContainer = (<p style = {{marginLeft: "10px"}}><strong>La sección de contacto aparecerán al guardar el registro</strong></p>);
        let componentContact = this.props.data == null ? textContainer : contactPanel;

        return <div>
          {this.getToolbar()}
          <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
            <ToastrMessage ref="toastrMsg" />
            <Fieldset className="hbox box-form" >
              <FormColumn className="hbox-r" style={{marginRight: "10px"}}>
                <Dropdown ref="type" label="Tipo" items={this.getVendorTypes()} value={data.type} onChange={this.onChoose} emptyOption={true} required={true}/>
                <TextField ref="name" label="Nombre" value = {data.name} required/>
                <TextField ref="address" label="Dirección"  value = {data.address} required={true} maxLength="100"/>
                <PhoneField ref="phone"  label="Teléfono" value = {data.phone} required={true} mask="(___) ____-____" />
                <DisplayField ref="active" label="Estado">{data.active !== false ? 'Activo' : 'Inactivo'}</DisplayField>
                <NoteField ref="note" value = {data.note} label="Notas" />
              </FormColumn>
              <FormColumn className="hbox-r">
                {componentContact}
              </FormColumn>
            </Fieldset>
          </FormPanel>
        </div>
      }

    }
