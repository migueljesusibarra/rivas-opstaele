import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel,
    PhoneField,NoteField } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';

export class VendorList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.records
        };

        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    removeVendor()
    {
        let record = this.refs.vendorGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar proveedor?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un proveedor.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.vendorGrid.getSelectedRecord();

        Meteor.call("removeVendor", record._id, function(error, result){});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        FlowRouter.go("/vendor/" + record._id);
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Id',
            dataIndex: 'index'
        },
        {
            key: 2,
            header: 'Nombre',
            dataIndex: 'name'
        },
        {
            key: 3,
            header: 'Tipo',
            dataIndex: 'type'
        },
        {
            key: 4,
            header: 'Dirección',
            dataIndex: 'address'
        },
        {
            key: 5,
            header: 'Teléfono',
            dataIndex: 'phone'
        },
        {
            key: 6,
            header: 'Estado',
            minWidth: 50,
            renderer: function(record)
            {
                return record.active !== false ? 'Activo' : 'Inactivo';
            }
        },
        {
            key: 6,
            header: 'Fecha Modificado',
            minWidth: 50,
            renderer: function(record)
            {
                return record.modifiedOn != "" ? Util.getDateISO(record.modifiedOn): "";

            }
        }];

        //let data = this.props.data;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function()
            {
                //self.openDialog("add")
                FlowRouter.go('/vendor/add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                let grid = ReactDOM.findDOMNode(self);
                let row = grid.querySelector("tr.x-selected");

                if(row != null)
                {
                    let record = self.props.data[row.dataset.index];
                    FlowRouter.go("/vendor/" + record._id);
                }
                else
                    alert("Por favor seleccione un registro");
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeVendor();
            }
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Lista de Proveedores"}</span>
                </div>
            </div>
        </div>);

        return (<GridPanel
        id="vendorGrid"
        ref="vendorGrid"
        title="Cliente"
        columns={columns}
        records={this.getRecords()}
        toolbar={toolbar}
        onRowDoubleClick={this.onRowDoubleClick} />)
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }
    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.data.filter(c => this.onFilter(c, search));
      else
        return this.props.data;
    }

    onFilter(record, search)
    {
      return (record.name.search(new RegExp(search, "i")) != -1 ||
      record.type.search(new RegExp(search, "i")) != -1 ||
      record.address.search(new RegExp(search, "i")) != -1 ||
      record.phone.search(new RegExp(search, "i")) != -1);
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
            <div>
                {this.getGridPanel()}
                {this.getConfirmDialog()}
            </div>
        );
    }
}
