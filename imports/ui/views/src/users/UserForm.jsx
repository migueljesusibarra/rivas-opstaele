import React, {Component} from 'react';
import { Toolbar, FormPanel, Fieldset, FormColumn,
        Dropdown, FormTextArea, FormInput, TextField,
        NoteField, Button, Field, PhoneField,
        EmailField, PasswordField, DisplayField } from 'react-ui';
import '/imports/api/users/collection.js';
import { Util } from '/lib/common.js';

export class UserForm extends Component
{
    constructor(props)
    {
        super(props);
    }

    saveChanges()
    {
        let form = this.refs.form;
        let fields = this.refs;

        if(form.isValid(fields))
        {
            if(!this.checkPassword())
            {
                alert("The password and the confirmation doesn't match.");
                //this.refs.toastrMsg.showMessage("error", "The password and the confirmation doesn't match.");
                return;
            }

            let record = {
                firstName: fields.firstName.getValue(),
                lastName: fields.lastName.getValue(),
                email: fields.email.getValue(),
                phone: fields.phone.getValue(),
                username: fields.username.getValue(),
                password: fields.password.getValue(),
                type: fields.userType.getValue(),
                notes: fields.notes.getValue()
            };

            let usernameValid = this.isValidUsername(record);
            let emailValid = this.isValidEmail(record);

            if(usernameValid && emailValid)
            {
                if (this.props.isNew)
                {
                    let self = this;

                    Meteor.call("addUser", record, function(error, recordId)
                    {
                        if(!error)
                        {
                            alert("El usuario fue creado");
                            //self.refs.toastrMsg.showMessage("succes", "The user was created.");
                            FlowRouter.go('/users');
                        }
                        else
                            alert("Ha ocurrido un error");
                            //self.refs.toastrMsg.showMessage("error", "An error has happened.");
                    });
                }
                else
                {
                    let self = this;

                    Meteor.call("updateUser", this.props.id, record, function(error, result)
                    {
                        if(!error)
                        {
                            alert("El usuario ha sido actualizado.");
                            //self.refs.toastrMsg.showMessage("succes", "The user has been updated.");
                            FlowRouter.go('/users');
                        }
                        else
                            alert("Ha ocurrido un error.");
                            //self.refs.toastrMsg.showMessage("error", "An error has happened.");
                    });
                }
            }
            else
            {
                let message = null;

                if(!usernameValid)
                    message = "El nombre de usuario ya existe.";
                else
                    if(!emailValid)
                        message = "Se encontró el correo electrónico asignado a otro usuario.";
                    else
                        throw new Meteor.Error("La condición no es válida.");

                alert(message);
                //this.refs.toastrMsg.showMessage("warning", message);
            }
        }
        else
        {
            alert("Por favor ingrese todos los campos requeridos.");
            //this.refs.toastrMsg.showMessage("warning", "Please enter all required fields.");
        }
    }

    isValidUsername(record)
    {
        let user = Meteor.users.findOne({username: record.username});

        return user == null || (user._id == this.props.id);
    }

    isValidEmail(record)
    {
        let user = Meteor.users.findOne({"profile.email": record.email});

        return user == null || (user._id == this.props.id);
    }

    checkPassword()
    {
        let password = this.refs.password.getValue();
        let confirm = this.refs.confirm.getValue();

        return ((password != null && password.length > 0 && password == confirm) || !password);
    }

    toggleActive()
    {
        Meteor.call("setUserStatus", this.props.id, this.props.active === false ? true : false);
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: '',
            cls: 'button-back',
            handler: function()
            {
                FlowRouter.go('/users');
            }
        }];

        if(!this.props.isNew)
        {
            items.push({
            text: 'Nuevo',
            cls: 'button-new-dealer',
            handler: function()
            {
                FlowRouter.go('/user/add');
            }});
        }

        items.push({
        text: 'Guardar',
        cls: 'button-save',
        handler: function()
        {
            self.saveChanges()
        }});

        if(this.props.id != null)
        {
            items.push(
            {
                text: this.props.active !== false ? 'Desactivar' : 'Activar',
                cls: 'button-delete',
                handler: function(){
                    self.toggleActive();
            }});
        }

        return (<Toolbar className="north hbox" items={items} />);
    }

    getRoles()
    {
        this.props.roles.sort(Util.applySortObject);

        return this.props.roles.map(function(role)
        {
            return {value: role._id, text: role.description};
        });
    }

    render()
    {
        let self = this;
        let toolbar =  this.getToolbar();
        let roles = this.getRoles();

        return (
        <div>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox">
                    <FormColumn style={{padding: "10px"}} className="border-picturesagent">
                        <img src="../../../../images/IconUser.png" alt="Contract" height="180"/>
                    </FormColumn>
                    <FormColumn  title="Detalle" className="hbox-l">
                        <TextField ref="firstName" label="Primer Nombre" value={this.props.profile ? this.props.profile.firstName : ''} required={true} maxLength="50"/>
                        <TextField ref="lastName" label="Priemer Apellido" value={this.props.profile ? this.props.profile.lastName : ''} required={true} maxLength="50"/>
                        <EmailField ref="email" label="Correo Electrónico" value={this.props.profile ? this.props.profile.email : ''} required={true} />
                        <PhoneField ref="phone" label="Teléfono" value={this.props.profile ? this.props.profile.phone : ''} required={true} mask="(___) ___-____" />
                        <DisplayField label="Estado">{this.props.active !== false ? 'Activo' : 'Inactivo'}</DisplayField>
                    </FormColumn>
                    <FormColumn title="Credenciales" className="hbox-r">
                        <Dropdown ref="userType" label="Tipo de Usuario" items={roles} required={true} value={this.props.profile.type} emptyOption={true}/>
                        <TextField ref="username" label="Nombre de Usuario" value={this.props.username} required={true} maxLength="50"/>
                        <PasswordField ref="password" label="Contraseña" required={this.props.isNew} maxLength="50" />
                        <PasswordField ref="confirm" label="Confirmar" required={this.props.isNew} maxLength="50" />
                        <NoteField ref="notes" label="Nota" value={this.props.profile ? this.props.profile.notes : ''} />
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </div>);
    }
}
