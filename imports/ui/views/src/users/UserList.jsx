import React, { Component } from 'react';
import { GridPanel,Toolbar, Button } from 'react-ui';
import ReactDOM from 'react-dom';
import { CardList } from '/imports/ui/components/src/CardList.jsx';

export class UserList extends Component
{
    constructor(props)
    {
        super(props);
    }

    getToolBar()
    {
        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: [
            {
                text: 'Nuevo',
                cls: 'button-new-dealer',
                handler: function(){
                FlowRouter.go('/user/add');
                }
            }]
        }];

        return (<Toolbar className="north hbox" id="main-toolbar" items={items} />);
    }

    getCardUsers()
    {
        let records = this.props.records;

        return (<div>{this.getToolBar()}<CardList records={records}></CardList></div>);
        }

    render()
    {
        return this.getCardUsers();
    }
}
