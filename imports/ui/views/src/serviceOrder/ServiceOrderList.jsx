import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button } from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';
import moment from 'moment';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class ServiceOrderList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        FlowRouter.go("/serviceorder/" + record._id);
    }

    onSearch(event)
    {
        this.setState({search: event.target.value});
    }

    getColumns()
    {
        let columns = [
         {
            key: 1,
            header: 'Id',
            minWidth: 50,
            dataIndex: '_id'
        },
        {
              key: 2,
              header: 'T. Orden',
              minWidth: 60,
              dataIndex: 'orderType'
        },
        {
            key: 3,
            header: 'Cliente',
            minWidth: 50,
            dataIndex: 'customer'
        },

        {
            key: 3,
            header: 'Equipo',
            minWidth: 80,
            dataIndex: 'equipment'
        },
        {
            key: 4,
            header: 'Serie',
            minWidth: 50,
            dataIndex: 'serialNumber'
        },
        {
            key: 5,
            header: 'R. SW',
            minWidth: 50,
            dataIndex: 'releaseSw'

        },

        {
            key: 6,
            header: 'Status',
            minWidth: 50,
            renderer: function(record){
                 return (record.status ? record.status : 'Borrador');
            }
        }];

         return columns;
    }

    getToolbarItems()
    {
        let self = this;

        let items = {
                       new: {
                           text: 'Nuevo',
                           cls: 'button-new',
                           handler: function()
                           {
                               FlowRouter.go('/serviceorder/add');
                           }
                       },
                       edit: {
                           text: 'Editar',
                           cls: 'button-edit',
                           handler: function()
                           {
                               let record = self.refs.serviceOrderGrid.getSelectedRecord();

                               if(record != null)
                                    FlowRouter.go("/serviceorder/" + record._id);
                               else
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                            }
                        },
                        history: {
                           text: 'Historial',
                           cls: 'button-history',
                           handler: function()
                           {
                             let record = self.refs.serviceOrderGrid.getSelectedRecord();

                             if(record != null)
                               self.refs.historyServiceOrder.onPenDialog();
                            else
                               self.getToast().showMessage("warning", "Por favor seleccione un registro");
                            }
                       },
                    }

                    return items;
    }

    getToast()
    {
        return this.refs.toastrMsg;
    }

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.new, actions.edit], rightGroup = [actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Ordenes de Servicios"}</span>
                </div>
            </div>
        </div>);
        return toolbar;
    }

    getRecords()
    {
        let search = this.state.search;

        if(search)
            return this.props.data.filter(c => this.onFilter(c, search));
        else
            return this.props.data;
    }

    onFilter(record, search)
    {
        return (
            record.customer.search(new RegExp(search, "i")) != -1 ||
            record.equipment.search(new RegExp(search, "i")) != -1
        );
    }

    getGridPanel()
    {
         let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyServiceOrder" module="ServiceOrder" foreignId={this.state.record._id} title="Historial O. Servicio" />

        return (<div>
                  <ToastrMessage ref="toastrMsg" />
                  {historyContainer}
                  <GridPanel
                      id="serviceOrderGridId"
                      ref="serviceOrderGrid"
                      title="PurchaseOrder"
                      columns={this.getColumns()}
                      records={this.getRecords()}
                      toolbar={this.getToolbar()}
                      onRowDoubleClick={this.onRowDoubleClick}
                      onSelectionChange={this.onSelectionChange} />
              </div>);
    }

    render()
    {
        return (<div>{this.getGridPanel()}</div>);
    }

}
