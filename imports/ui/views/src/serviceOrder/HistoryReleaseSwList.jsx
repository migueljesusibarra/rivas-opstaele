import React,{ Component } from 'react';
import { GridPanel } from 'react-ui';
import { Util } from '/lib/common.js';
import moment from 'moment';

export class HistoryReleaseSwList extends Component
{
    constructor(props)
    {
        super(props);
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Actualización SW',
            minWidth: 20,
            renderer: function(record)
            {
                return <a><b> Release SW: </b>{record.releaseSw} </a>;
            }
        },
        {
            key: 2,
            header: '',
            dataIndex: 'releaseSw',
            maxWidth: 200,
            renderer: function(record)
            {
                if(record.index == 1)
                   return (<a><b> Creado por: </b>{record.createByUser} </a>);
                else
                   return (<a><b> Actualizado por: </b>{record.createByUser} </a>);
                    
            }
        },
        {
            key: 3,
            header: '',
            dataIndex: 'createdOn',
            maxWidth: 20,
            renderer: function(record)
            {
                let date = moment(record.createdOn).format('MM/DD/YYYY h:m:ss A');
                return (<span>{date}</span>)
            }
        }
        ];

        return columns;
    }

    getGridPanel()
    {
        let data = this.props.records;

        return  (<GridPanel
        id="historyReleaseSwGridId"
        ref="historyReleaseSwGridId"
        title="Historial Release Sw List"
        columns={this.getColumns()}
        records={data} />)
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
        </div>
        )
    }
}
