import React, {Component} from 'react';
import { Toolbar, GridPanel, TextField, FormPanel, EmailField, DialogPanel } from 'react-ui';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx'
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { TechnicianListSendEmailContainer } from '/imports/ui/containers/TechnicianContainerList.js';


export class ServicePartRequestList extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            defaultChecked: false,
            opened: false
         };

        this.notSelectedParts = [];
        this.selectRecords = 0;
        this.openDialogPart = this.openDialogPart.bind(this);
        this.closeDialogParts = this.closeDialogParts.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onGenerarPurchaseOrder = this.onGenerarPurchaseOrder.bind(this);
        this.closeDialogTechnicianSendEmails = this.closeDialogTechnicianSendEmails.bind(this);
  }

  onRowDoubleClick(record)
  {
      this.openRecord(record);
  }

  closeDialogTechnicianSendEmails()
  {
      this.refs.technicianLisSendEmailDialog.close();
  }

  openRecord(record)
  {
      this.openDialog('edit', record);
  }

  openDialogPart()
  {
      this.setState({opened: true});
  }

  closeDialogParts()
  {
      this.setInitializeValue();
  }

  setInitializeValue()
  {
      let table = document.getElementById('partsRequestedGridId');
      let rowCheck = table.querySelectorAll('input[type=checkbox]');
      let rowQuantity = table.querySelectorAll('input[name=numberQuantity]');

      for (let i = 0; i < rowCheck.length; ++i)
      {
          rowCheck[i].checked = false;
      }

      for (let i = 0; i < rowQuantity.length; i++)
      {
          rowQuantity[i].value = 1;
      }

      this.refs.toolbar.unChecked();
      this.selectRecords = 0;

      this.setState({opened: false});
  }

  onClickItem(event)
  {
      let checked = event.target.checked;
      let totalRecords = this.notSelectedParts.length;

      if (!checked)
      {
          this.selectRecords--;
          this.refs.toolbar.unChecked();
      }
      else
      {
          this.selectRecords++;
          if(this.selectRecords == totalRecords)
              this.refs.toolbar.checked();
      }
  }

  SaveCheckAll()
  {
      let self = this;
      let table = document.getElementById('partsRequestedGridId');
      let rowCheck = table.querySelectorAll('input[name=checkbox]');
      let rowQuantity = table.querySelectorAll('input[name=numberQuantity]');

      let checked = false;

      let requestedParts = [];

      for (var i = 0; i < rowCheck.length; i++)
      {
          if(rowCheck[i].checked)
          {
              checked = true;

              let data = {
                  serviceOrderId: self.props.serviceOrderId,
                  partId: rowCheck[i].dataset.partid,
                  quantity: rowQuantity[i].value,
                  description: rowCheck[i].dataset.description,
                  number: rowCheck[i].dataset.number

              };

              requestedParts.push(data);
          }
      }

      if(!checked)
         alert("Por favor seleccione un registro");
      else
         this.saveRequestedParts(requestedParts);


  }

  saveRequestedParts(data)
  {
      let self = this;

      Meteor.call("addPartsRequest", data, function(error, result)
      {
          if(!error)
              self.closeDialogParts();
          else
              alert(error);
      });
  }

  onCheckAllUnCheckAll(value)
  {
      this.refs.toolbar.setChecked(value);
      let table = document.getElementById('partsRequestedGridId');
      let rowCheck = table.querySelectorAll('input[type=checkbox]');

      for (var i = 0; i < rowCheck.length; i++)
      {
          rowCheck[i].checked = value;
      }

      if(value)
          this.selectRecords = this.notSelectedParts.length;
      else
          this.selectRecords = 0;
  }

  removeModel()
  {
      let record = this.refs.partRequestGrid.getSelectedRecord();

      if(record != null)
      {
          this.refs.confirmDialog.setTitle('Está seguro que desea eliminar el modelo?"');
          this.refs.confirmDialog.onOpen();
      }
    else
        alert("Por favor seleccione un modelo.");
  }

   onGenerarPurchaseOrder()
   {
       let self = this;
       let properties = this.getPurchaseProperties(this.props.dataDetail);
       let collection = this.props.records;

       if(collection.length > 0)
       {
           if(self.props.hasPurchase)
           {
               self.OnAddPurchaseOrderParts(collection, self.props.purchaseOrderId, self.props.hasPurchase, self.props.serviceOrderId);
           }
           else{

               Meteor.call("addPurchaseOrder", properties, function(error, result)
               {
                   //result is the purchase generate
                   if(!error)
                   {
                       self.OnAddPurchaseOrderParts(collection, result, self.props.hasPurchase, self.props.serviceOrderId);
                   }
                   else
                    alert(error);
               });
          }
        }
        else
        {
          alert("No Hay parte para solicitar");
        }
  }

   OnAddPurchaseOrderParts(collection, purchaseOrderId, hasPurchase, serviceOrderId)
   {
       let requestedParts = [];
       let self = this;

       for (var i = 0; i < collection.length; i++)
       {
           let data = self.getPropertiesRequestPart(collection[i], purchaseOrderId)
           requestedParts.push(data);
       }

       Meteor.call("addPurchaseOrderParts", requestedParts, hasPurchase, function(error, result)
       {
           if(!error)
           {
               if(!hasPurchase)
               {
                   Meteor.call("updateServiceHasPurchase", serviceOrderId, purchaseOrderId, true, function(error, result)
                   {
                       if(!error)
                       {
                           self.refs.technicianLisSendEmailDialog.open();
                           alert("La compra de tipo parte se genero correctmente");
                       }
                  });
              }else
              {
                self.refs.technicianLisSendEmailDialog.open();
                alert("La compra de tipo parte se genero correctmente");
              }
           }else {
               alert(error);
           }
       });

   }

   getPurchaseProperties(data)
    {
       let record =
       {
          type: "2",
          orderClass: "",
          supplierId: "",
          date: new Date(),
          serviceOrderNr: data.serviceOrderId,
          customerId: data.customerId,
          systemId: data.systemId,
          brandId: data.brandId,
          modelId: data.modelId,
          serialNumber: data.serialNumber,
          equipmentId:  data.serialNumber,
          inboundSAPOrderNr: "",
          incoterms: "",
          inFreightForwarder: "",
          inAwb: "",
          inInhDate: "",
          note: "",
          source: "serviceOrder",
          foreignId: data.serviceOrderId,
          equipment: {},
          hasReceptionEquipment: false

        };

      return record;
    }

    getPropertiesRequestPart(data, purchaseOrderId)
    {
        let record =
        {
            purchaseOrderId: purchaseOrderId,
            code: data.number,
            description: data.description,
            quantity:  data.quantity,
            returnable: false,
            outboundRMANr: "",
            outDate: "",
            outFreightForwarder: "",
            outAwb: "",
            outPOD: "",
            outReceiptDate: "",
            foreignId: data._id,
            source: "RequestedPart"

        };

        return record;
    }

  onOkConfirmDialog()
    {
        let record = this.refs.partRequestGrid.getSelectedRecord();

        Meteor.call("removePartsRequest", record._id, function(error, result){});
    }

  getColumns()
    {
        let columns = [
        {
            key: 1,
            header: 'Ctd',
            minWidth: 0,
            maxWidth: 10,
            dataIndex: 'quantity'
        },
        {
            key: 2,
            header: 'Número',
            minWidth: 0,
            maxWidth: 30,
            dataIndex: 'number',
            align: 'center'
        },
        {
            key: 3,
            minWidth: 0,
            maxWidth: 100,
            header: 'Descripción',
            dataIndex: 'description'
        }];

        return columns;
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: [{
                        text: 'Partes Existentes',
                        cls: 'button-tools',
                        handler: function()
                        {
                            self.openDialogPart();
                        }
                },
                {
                    text: 'Eliminar',
                    visible: this.props.status != 'Closed',
                    cls: 'button-delete',
                    handler: function()
                    {
                        self.removeModel();
                    }
            },
             {
                text: 'Enviar Correo',
                visible: this.props.status != 'Closed',
                cls: 'button-email-technician',
                handler: function()
                {
                    self.refs.technicianLisSendEmailDialog.open();
                    // let record = self.refs.partRequestGrid.getSelectedRecord();
                    //
                    // if(record != null)
                    //     self.sendPartRequestEmail(record._id, record.emailSent);
                    //  else
                    //     alert("Por favor seleccione un registro");
                }
            },
            {
                text: 'Generar Compra',
                cls: 'button-generatePurchase',
                handler: function()
                {
                   self.onGenerarPurchaseOrder();
                }
            }]
        }];

         let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return toolbar;
    }

    getGridPanel()
    {
        let records = this.props.records;

        return (<GridPanel
        id="partRequestGridId"
        title="Part Request"
        ref="partRequestGrid"
        columns={this.getColumns()}
        records={records}
        toolbar={this.getToolbar()}
        onRowDoubleClick={this.onRowDoubleClick} />);
    }

    getDialog()
    {
        let self = this;
        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: function(event )
            {
                self.SaveCheckAll();
            }
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: function()
            {
                self.closeDialogParts();
            }
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        let columns = [
        {
            key: 1,
            minWidth: 0,
            maxWidth: 20,
            header:'Check',
            renderer: function(record)
            {
                return(<input
                    type="checkbox"
                    name="checkbox"
                    key={'checkR-'+record._id}
                    data-partid = {record._id}
                    data-code = {record.code}
                    data-number = {record.number}
                    data-description = {record.description}
                    defaultChecked = {self.state.defaultChecked}
                    onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            minWidth: 0,
            maxWidth: 50,
            header: 'Ctd',
            renderer: function(record)
            {
                return(<input className="service-inputGrid"
                    key={'number-quantity-'+record._id}
                    defaultValue = {1}
                    type="number"
                    name="numberQuantity" />);
            }
        },
        {
            key: 3,
            header: 'Número',
            minWidth: 0,
            maxWidth: 30,
            dataIndex: 'number',
            align: 'center'
        },
        {
            key: 4,
            header: 'Descripción',
            minWidth: 0,
            maxWidth: 100,
            dataIndex: 'description'
        }];

        this.notSelectedParts = this.props.parts;

        let display = this.state.opened ? 'block' : 'none';

        return (
        <div className="overlay" style={{display: display}}>
            <DialogPanel ref="dialogPartRequest" width="850"
                className="vbox center"
                title="Seleccione Partes"
                modal>
                <GridPanel
                    id="partsRequestedGridId"
                    ref="partsRequestedGrid"
                    className="center"
                    columns={columns}
                    records={this.props.parts}
                    toolbar={toolbar}  />
            </DialogPanel>
        </div>
        );
    }

    openDialog(action, record)
    {
        let self = this;
        let fields = self.refs;
        fields.dialogPartRequest.open();
    }

    closeDialog()
    {
      let self = this;
      self.refs.dialogPartRequest.close();
    }

    sendPartRequestEmail(id, email)
    {
       let self = this;

       Meteor.call("sendPartsRequestEmail", id, email, function(error, result)
        {
           if(!error)
             {
               self.refs.dialogPartRequest.close();
               alert("Correo de la solicitud enviado");
             }
             else
               alert(error);
        });
    }

   getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

  render()
  {
      let technicianListSendEmailContainer = (
      <Dialog
          ref="technicianLisSendEmailDialog"
          title="Ingenieros"
          modal>
          <TechnicianListSendEmailContainer recordId={this.props.purchaseOrderId} module = "ServiceRequestedPart" close={this.closeDialogTechnicianSendEmails}/>
      </Dialog>);

     return (<div>
             {technicianListSendEmailContainer}
     	     {this.getGridPanel()}
             {this.getDialog()}
             {this.getConfirmDialog()}
     	    </div>);
  }
}
