import React, { Component } from 'react';
import { GridPanel, Container, FormPanel, TextField, FormColumn, Fieldset } from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';

export class ServiceProgramList extends Component
{
    constructor(props)
    {
      super(props)
      this.onClickSave = this.onClickSave.bind(this);
      this.onClickItem = this.onClickItem.bind(this);
    }

    onClickClose(event)
    {
       FlowRouter.go('/serviceorder/' + this.props.dataOrder.serviceOrderId);
    }



    onClickItem(event)
    {
        let checked = event.target.checked;
        let table = document.getElementById('programGridId');
        //let rowtextarea = table.querySelectorAll('textarea[name=textAnotation]');
        miguel = event;
        //alert(event.target.index);
    }

    onClickSave(event)
    {
        let table = document.getElementById('programGridId');
        let rowCheck = table.querySelectorAll('input[name=check]');
        let rowtextarea = table.querySelectorAll('textarea[name=textAnotation]');
        let arrayData = [];

        for (var i = 0; i < rowCheck.length; i++)
        {
                  data = {
                      serviceOrderId: this.props.dataOrder.serviceOrderId,
                      period: this.props.dataOrder.periodId,
                      protocolId: rowCheck[i].dataset.protocolid,
                      check: rowCheck[i].checked,
                      material: rowtextarea[i].value
                  }

                  arrayData.push(data);

        }

        this.saveChekOk(arrayData);
    }

    saveChekOk(protocols)
    {
        Meteor.call("addServicePrograms", protocols, function(error, result)
        {
            if(!error)
                alert("se chequiaron correctamente")
            else
                throw alert(error);
        });
    }

    getColumns()
      {
          self = this;

          let columns = [
          {
              key: 1,
              header: 'Pos',
              minWidth: 0,
              maxWidth: 10,
              renderer: function(record)
              {
                  return record.parenttage;
              }
          },
          {
              key: 2,
              header: 'Contenido',
              minWidth: 0,
              maxWidth: 72,
             // align: 'center',
              renderer: function(record)
              {
                  if(record.activity.length > 72 || record.material.length > 72 )
                       return (<textarea id={record._id + "-id"} name="textarea" disabled rows={self.getRowTextArea(record)} style={{width: "100%", border: "none", backgroundColor: "#fff", resize: "none"}} defaultValue={record.activity}></textarea>)
                   else
                      return record.activity

              }

          },
          {
              key: 3,
              minWidth: 0,
              maxWidth: 10,
              header: 'OK',
              align:"center",
              dataIndex: 'check',
              renderer: function(record, index, data)
              {
                         return(<input
                                type="checkbox"
                                name="check"
                                key={'check-'+record._id}
                                data-protocolid = {record._id}
                                defaultChecked={record.check}
                                onClick={self.onClickItem}>
                            </input>);

             }
          },
          {
              key: 4,
              minWidth: 0,
              maxWidth: 100,
              header: 'Material Anotaciones',
              align: "center",
              renderer: function(record)
              {
                  let id = "textarea-"+(new Date()).getMilliseconds()+Math.floor(Math.random()*1000);
                  let lengthRow = self.getRowTextArea(record)
                    return (<textarea id={record._id} key={id} name="textAnotation" rows={lengthRow == 1 ? 2 : lengthRow} style={{width: "100%", border: "none",  resize: "none"}} defaultValue={record.material} ></textarea>)
              }

          }];

          return columns;
      }

      getRowTextArea(record)
      {
          let value = record.material.length > record.activity.length ?  record.material.length : record.activity.length;

          let row = 5;

          if(value <= 60)
                row = 1
          else if(value > 60 && value <= 120)
                row = 2
          else if(value > 120 && value <= 180)
                row = 3
          else if(value < 300)
                row = 4

          return row;
      }

      getGridPanel()
      {
          let records = this.props.data;
          let toolbar = [];

            return (<GridPanel
                id="programGridId"
                title="Program"
                ref="programGrid"
                columns={this.getColumns()}
                records={records}
                toolbar={toolbar}
          />);
      }

    render()
    {
       return <div>
              	<Container className="vbox center">
                    	<FormPanel ref="form">
                            <Fieldset className="hbox box-form" style={{marginRight: '5px'}}>
                            <FormColumn className="hbox-service-order-column service-order-width-column-1" >
                                <TextField ref="serviceOrderId"  label="Orden Servicio Nr" value={this.props.dataOrder.serviceOrderId}  disabled />
                            </FormColumn>
                            <FormColumn className="hbox-service-order-column service-order-width-column-2" >
                                <TextField ref="period"  label="Periodo #"  value={this.props.dataOrder.period} disabled />
                                <HiddenField ref="periodId" value={this.props.dataOrder.periodId} />
                            </FormColumn>
                            <FormColumn className="hbox-service-order-column service-order-width-column-3" style={{marginRight: "165px",  marginLeft: "-137px"}}>
                                <TextField ref="modelId"  label="Equipo"  value={this.props.dataOrder.equipment} disabled />
                            </FormColumn>
                            </Fieldset>
                        </FormPanel>
                </Container>
               <div className="x-toolbar north hbox">
                   <div className="hbox-l">
                        <a href="javascript:void(0);" className="button-save" onClick={this.onClickSave.bind(this)}>Guardar</a>
                        <a href="javascript:void(0);" className="button-close" onClick={this.onClickClose.bind(this)}>Cerrar</a>
               </div>
           </div>
           {this.getGridPanel()}
           </div>
    }
}
