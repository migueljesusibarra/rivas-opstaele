import React, { Component } from 'react';
import { Toolbar, GridPanel, FormPanel, Fieldset,
    FormColumn, DialogPanel } from 'react-ui';

export class ServiceOrderPartList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            defaultChecked: false,
            opened: false
         };

        this.notSelectedParts = [];
        this.selectRecords = 0;
        this.openDialogPart = this.openDialogPart.bind(this);
        this.closeDialogParts = this.closeDialogParts.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.onDeletePart = this.onDeletePart.bind(this);
    }

    onDeletePart()
    {
        let record = this.refs.partGrid.getSelectedRecord();

        if(record != null)
        {
           let confirmation = confirm("Está seguro de que desea eliminar este registro?");

            if(confirmation == true)
                Meteor.call("removeServiceOrderPart", record._id, function(error, result){});
        }
        else
            alert("Por favor seleccione una actividad.");

    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[name=checkbox]');
        let rowQuantity = table.querySelectorAll('input[name=numberQuantity]');
        let rowSerialNumberOld = table.querySelectorAll('input[name=serialNumberOld]');
        let rowSerialNumberNew = table.querySelectorAll('input[name=serialNumberNew]');

        let checked = false;

        let serviceOrderParts = [];

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;

                let data = {
                    serviceOrderId: self.props.serviceOrderId,
                    partId: rowCheck[i].dataset.partid,
                    quantity: rowQuantity[i].value,
                    serialNumberOld: rowSerialNumberOld[i].value,
                    serialNumberNew: rowSerialNumberNew[i].value,
                    description: rowCheck[i].dataset.description,
                    number: rowCheck[i].dataset.number

                };

                serviceOrderParts.push(data);
            }
        }

        if(!checked)
            alert("Por favor seleccione un registro");
        else
           this.saveServiceOrderParts(serviceOrderParts);

    }

    saveServiceOrderParts(data)
    {
        let self = this;

        Meteor.call("addServiceOrderParts", data, function(error, result)
        {
            if(!error)
                self.closeDialogParts();
            else
                throw alert(error);
        });
    }

    openDialogPart()
    {
        this.setState({opened: true});
    }

    closeDialogParts()
    {
        this.setInitializeValue();
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.notSelectedParts.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.notSelectedParts.length;
        else
            this.selectRecords = 0;
    }

    setInitializeValue()
    {
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');
        let rowQuantity = table.querySelectorAll('input[name=numberQuantity]');
        let rowSerialNumberOld = table.querySelectorAll('input[name=serialNumberOld]');
        let rowSerialNumberNew = table.querySelectorAll('input[name=serialNumberNew]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        for (let i = 0; i < rowQuantity.length; i++)
        {
            rowQuantity[i].value = 1;
        }

        // for (let i = 0; i < rowSerialNumberOld.length; i++)
        // {
        //     rowSerialNumberOld[i].value = 1;
        // }
        //
        // for (let i = 0; i < rowSerialNumberNew.length; i++)
        // {
        //     rowSerialNumberNew[i].value = 1;
        // }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;

        this.setState({opened: false});
    }

    getDialog()
    {
        let self = this;
        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: function(event )
            {
                self.SaveCheckAll();
            }
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: function()
            {
                self.closeDialogParts();
            }
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        let columns = [
        {
            key: 1,
            minWidth: 0,
            maxWidth: 20,
            header:'Check',
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                name="checkbox"
                key={'check-'+record._id}
                data-partid = {record._id}
                data-code = {record.code}
                data-number = {record.number}
                data-description = {record.description}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            minWidth: 0,
            maxWidth: 50,
            header: 'Ctd',
            renderer: function(record)
            {
                return(<input className="service-inputGrid"
                key={'number-quantity-'+record._id}
                defaultValue = {1}
                type="number"
                name="numberQuantity" />);
            }
        },
        {
            key: 3,
            header: 'Número',
            minWidth: 0,
            maxWidth: 30,
            dataIndex: 'number',
            align: 'center'
        },
        {
            key: 4,
            header: 'Descripción',
            minWidth: 0,
            maxWidth: 100,
            dataIndex: 'description'
        },
        {
            key: 5,
            header: 'Old NS',
            minWidth: 0,
            maxWidth: 80,
            dataIndex: 'serialNumberOld',
            renderer: function(record)
            {
                return(<input
                key={'serie-number-old-'+record._id}
                type="number"
                name="serialNumberOld" />);
            }
        },
        {
            key: 6,
            header: 'New NS',
            minWidth: 0,
            maxWidth: 80,
            dataIndex: 'serialNumberNew',
            renderer: function(record)
            {
                return(<input
                key={'serie-number-new-'+record._id}
                type="number"
                name="serialNumberNew" />);
            }
        }];

        this.notSelectedParts = this.props.parts;

        let display = this.state.opened ? 'block' : 'none';

        return (
        <div className="overlay" style={{display: display}}>
            <DialogPanel width="850"
                className="vbox center"
                title="Seleccione Partes"
                modal>
                <GridPanel
                id="partsGridId"
                ref="partsGrid"
                className="center"
                columns={columns}
                records={this.props.parts}
                toolbar={toolbar}  />
            </DialogPanel>
        </div>
        );
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: [
            {
                text: 'Partes Existentes',
                cls: 'button-tools',
                handler: function()
                {
                    self.openDialogPart();
                }
            },
            {
                text: 'Eliminar',
                cls: 'button-delete',
                handler: function()
                {
                    self.onDeletePart();
                }
            }]
        }];

        return (this.props.disabled ? '' : <Toolbar className="north hbox" id="main-toolbar" items={items} />);
    }

    getColumns()
    {
        let columns = [
         {
            key: 1,
            header: 'Ctd',
            minWidth: 0,
            maxWidth: 20,
            dataIndex: 'quantity'
        },
        {
           key: 3,
           header: 'Número',
           minWidth: 0,
           maxWidth: 30,
           dataIndex: 'number',
           align: 'center'
       },
        {
            key: 2,
            header: 'Descripción',
            minWidth: 0,
            maxWidth: 100,
            dataIndex: 'description'
        },
        {
            key: 4,
            header: 'Old NS',
            minWidth: 0,
            maxWidth: 50,
            dataIndex: 'serialNumberOld'
        },
        {
            key: 5,
            header: 'New NS',
            minWidth: 0,
            maxWidth: 50,
            dataIndex: 'serialNumberNew'
        }];

        return columns;
    }

    getGridPanel()
    {
        return  (
        <GridPanel
        id="partGridId"
        ref="partGrid"
        title="part"
        columns={this.getColumns()}
        toolbar={this.getToolbar()}
        records={this.props.serviceOrderParts}/>);
    }

    render()
    {
        return(
            <div>
                {this.getGridPanel()}
                {this.getDialog()}
            </div>
        );
    }
}
