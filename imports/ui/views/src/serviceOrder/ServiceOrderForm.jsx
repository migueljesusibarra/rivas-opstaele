import React, { Component } from 'react';
import { FormPanel, DisplayField, Container, EmailField,
	PhoneField, Fieldset, FormColumn, Toolbar,
	GridPanel, TextField, Dropdown, DateField,
	NoteField} from 'react-ui';
import { Models } from '/imports/api/models/collection.js';
import { Periods } from '/imports/api/periods/collection.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Util } from '/lib/common.js';
import { ServiceOrderActivityContainerList } from '/imports/ui/containers/ServiceOrderActivityContainerList';
import { ServiceOrderPartContainerList } from '/imports/ui/containers/ServiceOrderPartContainerList';
import { PartRequestContainerList } from '/imports/ui/containers/PartRequestContainerList';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HistoryReleaseSwContainer } from '/imports/ui/containers/HistoryReleaseSwContainer';


export class ServiceOrderForm extends Component
{
	constructor(props)
	{
		super(props);

		this.state = {
		    data: this.getDataStateProps(props.data, props.models, props.periods),
		    disabledDetail: (props.isNew || props.data.status == "Closed"),
		    disabledField: props.data.status == "Closed",
			disabledProtocol: true
		};

		this.onServiceOrderAdded = this.onServiceOrderAdded.bind(this);
		this.onChangeBrand = this.onChangeBrand.bind(this);
		this.toggleAprove = this.toggleAprove.bind(this);
		this.closeDialogHistoryReleaseSW = this.closeDialogHistoryReleaseSW.bind(this);
		this.openDialogHistoryReleaseSW = this.openDialogHistoryReleaseSW.bind(this);

	}

	componentWillReceiveProps(nextProps)
	{
		this.setState({
			data: this.getDataStateProps(nextProps.data, nextProps.models, nextProps.periods),
			disabledDetail: (nextProps.isNew || nextProps.data.status == "Closed"),
			disabledField: nextProps.data.status == "Closed"
		});
	}

	toggleAprove(status)
	{
		let serviceOrderId = this.props.id;
		let self = this;

		Meteor.call("onApproveServiceOrder", serviceOrderId, status, function(error, result)
		{
			if(!error)
				self.getToast().showMessage("succes", "Cambio de estado exitoso!");
			else
				self.getToast().showMessage("error", error);
		});
	}

	getDataStateProps(record, models, periods)
	{
		let data = Object.assign(
		{
			models: this.getModels(models || []),
			periods: this.getPeriods(periods || [])
		}, record);

		return data;
	}

	getSystems()
	{
		this.props.systems.sort(Util.applySortObject);

		return this.props.systems.map(function(system)
		{
			return {value: system._id, text: system.description};
		});
	}

	getBrands()
	{
		this.props.brands.sort(Util.applySortObject);

		return this.props.brands.map(function(brand)
		{
			return {value: brand._id, text: brand.description};
		});
	}

	getCustomers()
	{
		this.props.customers.sort(Util.applySortObject);

		return this.props.customers.map(function(customer)
		{
			return {value: customer._id, text: customer.name};
		});
	}

	getModels(models)
	{
	   models.sort(Util.applySortObject);

		return models.map(function(model)
		{
			return {value: model._id || model.value, text: model.description || model.text};
		});
	}

	getTechnicians()
	{
		this.props.technicians.sort(Util.applySortObject);

		return this.props.technicians.map(function(technician)
		{
			return {value: technician._id, text: technician.name};
		});
	}

	getPeriods(periods)
	{
		periods.sort(Util.applySortObject);

	    return periods.map(function(item)
		{
			return {value: item._id, text: item.description};
		});
	}

	getOrderTypes()
	{
		return this.props.orderTypes.map(function(orderType)
		{
			return {value: orderType._id, text: orderType.description};
		});
	}

	onChangeBrand(value)
	{
		let fields = this.refs;
		let models = Models.find({brandId: value}).fetch() || [];

		this.setState({
			data: this.setDataState(fields, models,this.state.data.periods, value, fields.model.getValue())
		});
	}

	onChangeModel(value)
	{
		let fields = this.refs;
		let periods = Periods.find({modelId: value}).fetch() || [];

		this.setState({
			data: this.setDataState(fields, this.state.data.models, periods, fields.brand.getValue(), value)
		});
	}

	setDataState(fields, models, periods, brandId, modelId)
	{
		let installationDate = fields.installationDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.installationDate.getValue()) : null;
		let warrantyDate = fields.warrantyExpires.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.warrantyExpires.getValue()) : null;
		let warrantyExpires = fields.warrantyExpires.getValue().toString() != "Invalid Date" ? Util.sumMonthDate(fields.installationDate.getValue(), fields.warranty.getValue()) : null;
		let beginDate = fields.beginDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.beginDate.getValue()) : null;
		let endDate = fields.endDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.endDate.getValue()) : null;

		let data =
		{
			brandId: brandId,
			systemId: fields.system.getValue(),
			models: this.getModels(models),
			periods: this.getPeriods(periods),
			modelId: modelId,
			customerId: fields.customer.getValue(),
			serialNumber: fields.serialNumber.getValue(),
			period: fields.period.getValue(),
			releaseSw: fields.releaseSw.getValue(),
			installationDate: installationDate,
			warranty: warrantyDate,
			warrantyExpires: warrantyExpires,
			technicianId: fields.technician.getValue(),
			orderTypeId: fields.orderType.getValue(),
			note: fields.note.getValue(),
			beginDate: beginDate,
			endDate: endDate
		};

        return data;
	}

	getToolbarItems()
	{
		let self = this;

		let items = {
			        	new:
						{
							text: 'Nuevo',
							cls: 'button-new',
							handler: function()
							{
								FlowRouter.go('/serviceorder/add');
							}
					},
					save:
					{
						text: 'Guardar',
						cls: 'button-save',
						visible: this.props.data.status != 'Closed',
						handler: function()
						{
							self.saveChanges("save");
						}
					},
					close:
					{
						key: 'close',
						text: 'Cerrar',
						cls: 'button-close',
						handler: function()
						{
							FlowRouter.go('/serviceorders');
						}
					},
					history:
					{
						key: 'history',
						text: 'H. Release SW',
						cls: 'button-history',
						handler: function()
						{
							self.openDialogHistoryReleaseSW();
						}
					},
					procol:
					{
						text: 'Protocolo',
						cls: 'button-history',
						visible: self.props.data.orderTypeId == 2,
						handler: function()
						{
							if(self.refs.period.getValue() != "")
							  {
								  let id = self.props.data._id;
								  FlowRouter.go('/serviceprograms/' + id);
							  }
							  else{
								  self.getToast().showMessage("warning", "Para generar los protocolos de esta orden necesita el periodo");
							  }
						}
					},
					toogle:
					{
						text: this.props.data.status == 'Waiting for parts' ? 'Ready Parts' : 'Close Order',
						cls: 'button-approval',
						visible: this.props.data.status != 'Closed',
						handler: function()
						{
							if(self.props.data.status === "In-Process" || self.props.data.status== 'Delivered parts' || self.props.data.status == null)
                               {
								   self.toggleAprove('Closed');
							   }
							else if(self.props.data.status === "Waiting for parts" || self.props.data.status == null)
							{
                                self.toggleAprove('Delivered parts');
							}
					    }
					},
					report: {
			            text: 'Reporte',
			            cls: 'button-report',
			            handler: function()
			            {
			                FlowRouter.go("/report");
			                FlowRouter.setQueryParams({id: self.props.data._id, module: "reportserviceorder"});
			            }
			        },
					reportProtocol: {
						text: 'R. Protocol',
						cls: 'button-report',
						handler: function()
						{
							FlowRouter.go("/report");
							FlowRouter.setQueryParams({id: self.props.data._id, module: "reportserviceprogram"});
						}
					}};

					return items;
	}

	getPropertiesHistory(fields, serviceOrderId)
	{
		let record = {
			serviceOrderId: serviceOrderId,
			systemId: fields.system.getValue(),
			brandId: fields.brand.getValue(),
			modelId: fields.model.getValue(),
			releaseSw: fields.releaseSw.getValue()
		}

		return record;
	}

	saveChanges(button)
	{
		let self = this;
		let fields = self.refs;

		if(fields.form.isValid(fields))
		{
			let properties = self.getProperties(fields);
			let propertyHistoryReleaseSw = self.getPropertiesHistory(fields, self.props.data._id)

			if(self.props.isNew)
				Meteor.call("addServiceOrder", properties, self.onServiceOrderAdded);
			else
			{
				Meteor.call("updateServiceOrder", self.props.data._id, properties, function(error, result)
				{
					if(!error)
					{
						if(self.refs.releaseSw.getValue().trim() != "")
			            {
			                Meteor.call("addHistoryReleaseSw", propertyHistoryReleaseSw, function(error, result){});
			            }

						self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");

						if(button == "saveAndClose")
							FlowRouter.go('/serviceorders');
					}
					else
						self.getToast().showMessage("error", error);

				});
			}
		}
		else
			self.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
	}

	onServiceOrderAdded(error, result)
	{
        let self = this;
		let fields = self.refs;

		if(!error)
		{
			if(this.refs.releaseSw.getValue().trim() != "")
            {
                let propertyHistoryReleaseSw = self.getPropertiesHistory(fields, result)

                Meteor.call("addHistoryReleaseSw", propertyHistoryReleaseSw, function(error, result){});
            }

			this.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
			FlowRouter.go('/serviceorder/' + result);
		}
		else
			this.getToast().showMessage("error", error);
	}

	getProperties(fields)
	{
		let data = {
			customerId: fields.customer.getValue(),
			systemId: fields.system.getValue(),
			brandId: fields.brand.getValue(),
			modelId:fields.model.getValue(),
			serialNumber: fields.serialNumber.getValue(),
			period: fields.period.getValue(),
			releaseSw: fields.releaseSw.getValue(),
			installationDate: fields.installationDate.getValue(),
			warranty: fields.warranty.getValue(),
			warrantyExpires: Util.sumMonthDate(fields.installationDate.getValue(), fields.warranty.getValue()),
			technicianId: fields.technician.getValue(),
			descriptionFailure: fields.descriptionFailure.getValue(),
			orderTypeId: fields.orderType.getValue(),
			source: "",
			foreignId: "",
			hasPurchase: false,
			purchaseOrderId: "",
			note: fields.note.getValue(),
			beginDate: fields.beginDate.getValue(),
			endDate: fields.endDate.getValue()
		};

		return data;
	}

	getToast()
	{
		return this.refs.toastrMsg;
	}

	getToolbar()
	{
		let self = this;
		let actions = self.getToolbarItems();
		leftGroup = [], rightGroup = [];

		if(this.props.isNew)
			leftGroup.push(actions.save);
		else
		{
			leftGroup.push(actions.new);
			leftGroup.push(actions.save);
			leftGroup.push(actions.procol);
			rightGroup.push(actions.toogle);
			rightGroup.push(actions.report);
			rightGroup.push(actions.reportProtocol);
			rightGroup.push(actions.history);
		}

		leftGroup.push(actions.close);

		let items = [{
			type: 'group',
			className: 'hbox-l',
			items: leftGroup
		},
		{
			type: 'group',
			className: 'hbox-r',
			items: rightGroup
		}];

		return (<Toolbar className="north hbox" items={items} />);
	}

	getRenderDetail()
	{
		let disabledDetail = this.state.disabledDetail;

		let dataDetail = {
            serviceOrderId: this.props.id,
			customerId: this.props.data.customerId,
			systemId: this.props.data.systemId,
			brandId: this.props.data.brandId,
			modelId: this.props.data.modelId,
			serialNumber: this.props.data.serialNumber
		}

		let componentActivity = (
			<Fieldset className="hbox box-form">
				<FormColumn  title="Actividades" className="hbox-l border-contract-activity">
					<ServiceOrderActivityContainerList systemId={this.props.data.systemId} brandId={this.props.data.brandId} modelId={this.props.data.modelId} serviceOrderId={this.props.id} disabled = {disabledDetail}/>
				</FormColumn>
			</Fieldset>
		);

		let componentPartRequest = (
	    	<Fieldset className="hbox box-form">
				<FormColumn  title="Solicitud de Partes" className="hbox-l border-contract-part">
                	<PartRequestContainerList dataDetail={dataDetail} serviceOrderId={this.props.id} hasPurchase={this.props.data.hasPurchase != null ? this.props.data.hasPurchase : false} purchaseOrderId={this.props.data.purchaseOrderId} disabled = {disabledDetail} />
                </FormColumn>
             </Fieldset>
			);

		let componentPart = (
			<Fieldset className="hbox box-form" style={{marginRight: '1px'}}>
				<FormColumn  title="Partes" className="hbox-l border-contract-part">
				<ServiceOrderPartContainerList serviceOrderId={this.props.id} disabled = {disabledDetail}/>
				</FormColumn>
			</Fieldset>
		);

		return (
		<div>
			{componentActivity}
			{componentPartRequest}
			{componentPart}
		</div>
		);
	}

	closeDialogHistoryReleaseSW()
	{
	  this.refs.dialog.close();
	}

	openDialogHistoryReleaseSW()
	{
		this.refs.dialog.setTitle("Historial Release SW");
		this.refs.dialog.open();
	}

	getDialogHistoryReleaseSW()
	{

		let items = [
		{
			key: 'close',
			text: 'Cerrar',
			cls: 'button-close',
			handler: this.closeDialogHistoryReleaseSW
		}];

		let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

		let systemId = this.props.data.systemId;
		let brandId = this.props.data.brandId;
		let modelId = this.props.data.modelId;

		return (
		<Dialog ref="dialog"  modal>
			{toolbar}
			<HistoryReleaseSwContainer systemId={systemId} brandId={brandId} modelId={modelId} />
		</Dialog>);
	}

	render()
	{
        let data = this.props.data;
		let dataState = this.state.data;
		console.log(dataState.periods);
		let disabledField = this.state.disabledField;
		let componentsDetail = this.getRenderDetail();
		let dateNow = new Date();
		let beginDate = dataState.beginDate != null && dataState.beginDate != "" ?  Util.getDateISO(dataState.beginDate) : Util.getDateISO(dateNow);
	    let endDate =  dataState.endDate != null && dataState.endDate != ""? Util.getDateISO(dataState.endDate) : Util.getDateISO(dateNow);
    	return (<Container className="vbox center">
			<div style={{textAlign: "center", color: "#004990"}}>
            <span style={{fontSize: "18px"}}>Orden de Servicio - </span>
			<span style={{fontSize: "18px", color: "#565656"}}>{this.props.data.status} </span>
           </div>
			{this.getToolbar()}
				<FormPanel ref="form">
					<ToastrMessage ref="toastrMsg" />
					<Fieldset className="hbox box-form" style={{marginRight: '5px'}}>
						<FormColumn className="hbox-service-order-column service-order-width-column-1" >
						 <TextField ref="serviceOrderId"  label="Orden Servicio Nr" value={data._id}  disabled />
						  <Dropdown ref="orderType" label="Tipo de Orden" items={this.getOrderTypes()}  value = {dataState.orderTypeId} emptyOption={true} required={true} disabled = {disabledField}/>
						 <DateField ref="beginDate" label="Fecha Apertura" className = "service-order-input-date"  maxDate={beginDate} value = {dataState.beginDate != null ? Util.getDateISO(dataState.beginDate) : null} disabled = {disabledField}/>
						 <DateField ref="endDate" label="Fecha Cierre" className = "service-order-input-date" minDate={endDate} value = {dataState.endDate != null && dataState.endDate != "" ? Util.getDateISO(dataState.endDate) : null} disabled = {disabledField}/>
						 <Dropdown ref="period" label="Periodo" items={dataState.periods} value = {dataState.period} emptyOption={true} required={true} disabled = {disabledField}/>
					</FormColumn>
						<FormColumn  className="hbox-service-order-column service-order-width-column-2" >
							<Dropdown className="hbox-l" ref="customer" label="Cliente" items={this.getCustomers()} value={dataState.customerId} required={true} emptyOption={true} disabled = {disabledField}/>
							<Dropdown className="hbox-l" ref="system" label="Sistema" items={this.getSystems()} value={dataState.systemId} required={true} emptyOption={true} disabled = {disabledField}/>
							<Dropdown className="hbox-l" ref="brand" label="Marca" value ={dataState.brandId} items={this.getBrands()} onChange={this.onChangeBrand.bind(this)} required={true} emptyOption={true} disabled = {disabledField}/>
							<Dropdown className="hbox-l" ref="model" label="Modelo" value= {dataState.modelId} items = {dataState.models} onChange={this.onChangeModel.bind(this)} emptyOption={true} disabled = {disabledField}/>
                            <TextField ref="serialNumber" label="Serie" value = {dataState.serialNumber}  disabled = {disabledField}/>
						</FormColumn>
						<FormColumn  className="hbox-service-order-column service-order-width-column-3" >
							<TextField ref="releaseSw" label="Release SW" value = {dataState.releaseSw}  maxLength="50" required={true} disabled = {disabledField}/>
							<DateField ref="installationDate" value= {dataState.installationDate != null ? Util.getDateISO(dataState.installationDate) : null} className = "service-order-input-date" label="Fecha de Instalación"  required={true} disabled = {disabledField}/>
                            <TextField ref="warranty" label="Garantía (m)" value= {dataState.warranty}  required={true} disabled = {disabledField}/>
							<DateField ref="warrantyExpires" className = "service-order-input-date" label="Garantía Vence" value = {dataState.warrantyExpires != null ? Util.getDateISO(dataState.warrantyExpires) : null}  disabled = {true} />
							<Dropdown ref="technician" label="Ing. Responsable" items={this.getTechnicians()} value = {dataState.technicianId} emptyOption={true} required={true} disabled = {disabledField}/>
                        </FormColumn>
					</Fieldset>
					<Fieldset className="hbox box-form" style={{marginTop: '6px'}}>
						<FormColumn className="hbox-l">
							<NoteField ref="descriptionFailure" label="Descripción de Falla"  value={dataState.descriptionFailure} disabled = {disabledField}/>
						</FormColumn>
					</Fieldset>
					{componentsDetail}
					<Fieldset className="hbox box-form" style={{marginTop: '6px'}}>
						<FormColumn className="hbox-l">
							<NoteField ref="note" label="Diagnóstico / Observaciones"  value={dataState.note} disabled = {disabledField}/>
						</FormColumn>
					</Fieldset>
				</FormPanel>
			{this.getDialogHistoryReleaseSW()}
			</Container>
		)
	}
}
