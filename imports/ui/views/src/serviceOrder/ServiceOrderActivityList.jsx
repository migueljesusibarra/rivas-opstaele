import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FormPanel, Dropdown, Toolbar, GridPanel, Fieldset,
    FormColumn, TextField, DateField, NoteField } from 'react-ui';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';

export class ServiceOrderActivityList extends Component
{
    constructor(props)
    {
        super(props);

        this.openDialogActivity = this.openDialogActivity.bind(this);
        this.closeDialogActivity = this.closeDialogActivity.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.deleteActivity = this.deleteActivity.bind(this);
    }

    openDialogActivity(action)
    {
         let self = this;
         let fields = self.refs;

        if(action == 'edit')
        {
             let record = self.refs.activityGrid.getSelectedRecord();

             if(record != null)
             {
                 fields.record.setValue(record._id);
                 fields.activity.setValue(record.activityId);
                 fields.date.setValue(record.date != null ? Util.getDateISO(record.date) : null);
                 fields.beginHour.setValue(record.beginHour);
                 fields.endHour.setValue(record.endHour);
                 fields.note.setValue(record.note);
                 fields.dialog.setTitle("Editar Actividad");
                 fields.dialog.open();
             }
             else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                 fields.record.setValue('');
                 fields.activity.setValue('');
                 fields.date.setValue('');
                 fields.beginHour.setValue('');
                 fields.endHour.setValue('');
                 fields.note.setValue('');
                 fields.dialog.setTitle("Nueva Actividad");
                 fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                serviceOrderId: self.props.serviceOrderId,
                activityId: fields.activity.getValue(),
                date: fields.date.getValue(),
                beginHour: fields.beginHour.getValue(),
                endHour: fields.endHour.getValue(),
                note: fields.note.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateActivitieService", recordId, data, function(error, result){});
            else
                Meteor.call("addActivitieService", data, function(error, result){});

            if(fields.updateSw.getValue().trim() != "")
            {
                Meteor.call("updateReleaseSw", this.props.serviceOrderId, fields.updateSw.getValue(), function(error, result){});

                let recordHistoryReleaseSw = {
                    serviceOrderId: this.props.serviceOrderId,
                    systemId: this.props.systemId,
                    brandId: this.props.brandId,
                    modelId: this.props.modelId,
                    releaseSw: fields.updateSw.getValue()
                };

                Meteor.call("addHistoryReleaseSw", recordHistoryReleaseSw, function(error, result){});
            }

            fields.dialog.close();
        }
        else
        {
            alert("Por favor ingrese todos los campos requeridos.");
        }
    }

    deleteActivity()
    {
        let record = this.refs.activityGrid.getSelectedRecord();

        if(record != null)
        {
           let confirmation = confirm("Está seguro de que desea eliminar este registro?");

            if(confirmation == true)
                Meteor.call("removeActivitieService", record._id, function(error, result){});
        }
        else
            alert("Por favor seleccione una actividad.");
    }

    getActivities()
    {
        this.props.activities.sort(Util.applySortObject);

        return this.props.activities.map(function(activity)
        {
            return {value: activity._id, text: activity.description};
        });
    }

    getDialog()
    {
        let items = [{
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogActivity
        }];

        let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        //ToDo remove class contact-dialog
        return (
            <Dialog ref="dialog" className="contact-dialog" title="Actividad" modal>
                {toolbar}
                <FormPanel ref="form">
                    <Fieldset className="hbox box-form">
                        <FormColumn className="hbox-l " style={{marginRight: "10px"}}>
                             <HiddenField ref="record" value="" />
                            <Dropdown
                                ref="activity"
                                label="Descripción"
                                items={this.getActivities()}
                                emptyOption={true}
                                required={true}/>
                            <DateField
                                ref="date"
                                label="Fecha"
                                required={true}/>
                            <TextField
                                ref="beginHour"
                                label="Hora Inicio"
                                required={true}
                                type="time"/>
                            <TextField
                                ref="endHour"
                                label="Hora Fin"
                                required={true}
                                type="time"/>
                            <NoteField ref="note"
                                label="Nota"/>
                            <TextField
                                ref="updateSw"
                                label="Actualización SW"/>
                            </FormColumn>
                    </Fieldset>
                </FormPanel>
            </Dialog>
        );
    }

    closeDialogActivity()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getColumns()
    {
        let columns = [
        {
            key: 1,
            header: 'Descripción',
            dataIndex: 'description'
        },
        {
            key: 2,
            header: 'Fecha',
            dataIndex: 'date',
            renderer: function(record)
            {
                return (record.date != "" ? Util.getDateISO(record.date): "");
            }
        },
        {
            key: 3,
            header: 'Hora Inicio',
            dataIndex: 'beginHour'
        },
        {
            key: 4,
            header: 'Hora Fin',
            dataIndex: 'endHour'
        },
        {
            key: 5,
            header: 'Nota',
            dataIndex: 'note'
        }];

        return columns;
    }

    getToolBar()
    {
        let self = this;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function()
            {
                self.openDialogActivity('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                self.openDialogActivity('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.deleteActivity();
            }
        }];

        return (this.props.disabled ? '' : <Toolbar items={items}/>);
    }

    getGridPanel()
    {

        return  (
        <GridPanel
        id="activityGridId"
        ref="activityGrid"
        title="Actividad"
        columns={this.getColumns()}
        records={this.props.records}
        toolbar={this.getToolBar()} />)
    }

    render()
    {
        return(
            <div>
                {this.getGridPanel()}
                {this.getDialog()}
            </div>
        );
    }
}
