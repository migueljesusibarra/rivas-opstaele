import React, { Component } from 'react';
import { FormPanel, DisplayField, Container, EmailField,
    PhoneField, Fieldset, FormColumn, Toolbar,
    GridPanel, TextField, Dropdown, DateField,
    NoteField} from 'react-ui';
import { Util } from '/lib/common.js';
import { VerificationContainer } from '../../../containers/VerificationContainer'
import { ConfigurationContainer } from '../../../containers/ConfigurationContainer'
import { MissingContainer } from '../../../containers/MissingContainer'
import { ReceptionEquipmentTestPerformedContainer } from '../../../containers/ReceptionEquipmentTestPerformedContainer'
import { Models } from '/imports/api/models/collection.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { TechnicianPanel } from '../technicians/TechnicianPanel.jsx'
import { PreOrderContainer } from '/imports/ui/containers/PreOrderContainer';
import { TechnicianReceptionEquipmentContainer } from '/imports/ui/containers/TechnicianReceptionEquipmentContainer.js';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class ReceptionEquipmentForm extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data: this.getDataStateProps(props.data, props.models),
            disabledModel: (props.isNew || props.data.status == "Aprobado"),
            disabledField: props.data.status == "Aprobado"
        };

        this.onChangeBrand = this.onChangeBrand.bind(this);
        this.onReceptionEquipmentAdded = this.onReceptionEquipmentAdded.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    componentWillReceiveProps(nextProps){

        let models = Models.find({brandId: nextProps.data.brandId}).fetch() || [];
        this.setState({
            data: this.getDataStateProps(nextProps.data, models),
            disabledModel: (nextProps.isNew || nextProps.data.status == "Aprobado"),
            disabledField: nextProps.data.status == "Aprobado" }
        );
    }

    onOkConfirmDialog()
    {
        let self = this;
        self.saveEquipment();
    }

    getDataStateProps(record, models)
    {
        let data = Object.assign(
        {
            models: this.getModels(models || [])
        }, record);

        return data;
    }

    getSystems()
    {
        return this.props.systems.map(function(system)
        {
            return {value: system._id, text: system.description};
        });
    }

    getBrands()
    {
        return this.props.brands.map(function(brand)
        {
            return {value: brand._id, text: brand.description};
        });
    }

    getCustomers()
    {
        return this.props.customers.map(function(customer)
        {
        return {value: customer._id, text: customer.name};
        });
    }

    getModels(models)
    {
        return models.map(function(model)
        {
            return {value: model._id, text: model.description};
        });
    }

    onChangeBrand(value)
    {
        let fields = this.refs;
        let models = Models.find({brandId: value}).fetch() || [];
        this.setState({
            data: this.setDataState(fields, models, value),
            disabledModel: (value == "" ? true : false)
        });
    }

    setDataState(fields, models, dropdownValue)
    {
        let installationDate = fields.installationDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.installationDate.getValue()) : null;
        let warrantyDate = fields.warrantyExpires.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.warrantyExpires.getValue()) : null;

        let data =  {
            brandId: dropdownValue,
            systemId: fields.system.getValue(),
            models: this.getModels(models),
            model: fields.model.getValue(),
            customerId: fields.customer.getValue(),
            serialNumber: fields.serialNumber.getValue(),
            releaseSw: fields.releaseSw.getValue(),
            installationDate: installationDate,
            warranty: fields.warranty.getValue(),
            warrantyExpires: warrantyDate,
            frequency: fields.frequency.getValue()
        };

        return data;
    }

    saveEquipment()
    {
        let fields = this.refs;
        let self = this;
        let data = self.props.data;

        if(this.refs.form.isValid(fields))
        {
            let data = {
                customerId: fields.customer.getValue(),
                systemId: fields.system.getValue(),
                modelId: fields.model.getValue(),
                brandId: fields.brand.getValue(),
                serialNumber: fields.serialNumber.getValue(),
                releaseSw: fields.releaseSw.getValue(),
                installationDate: fields.installationDate.getValue(),
                warranty: fields.warranty.getValue(),
                outOfService: false,
                frequencyId: fields.frequency.getValue(),
                source: "receptionEquipment",
                foreignId: this.props.data._id,
                isReceptionEquipment: true,
                purchaseOrderSystemId: this.props.data.foreignId,
                isPurchaseOrder: this.props.data.foreignId == "" ? false : true,
                technicianId: this.props.data.technicianId,
                contractId: "",
                contractNumber: "",
                inContract: false
            };

            Meteor.call("addEquipment", data, function(error, result)
            {
                if(!error)
                    self.generatePreOrden(data.warranty, data.frequencyId, result);
                else
                    self.getToast().showMessage("error", error);
            });
        }
        else
            self.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
    }

    getProperties(fields)
    {
        let data = {
            customerId: fields.customer.getValue(),
            systemId: fields.system.getValue(),
            brandId: fields.brand.getValue(),
            modelId:fields.model.getValue(),
            serialNumber: fields.serialNumber.getValue(),
            releaseSw: fields.releaseSw.getValue(),
            installationDate: fields.installationDate.getValue(),
            warranty: fields.warranty.getValue(),
            warrantyExpires: Util.sumMonthDate(fields.installationDate.getValue(), fields.warranty.getValue()),
            frequency: fields.frequency.getValue()
        };

        if(this.props.isNew)
        {
            data.source = "",
            data.foreignId = ""
        }

        return data;
    }

    generatePreOrden(warranty, frequency, result)
    {
        let date = this.refs.installationDate.getValue();
        let self = this;

        let properties = {
            receptionEquipmentId: this.props.data._id,
            systemId: this.props.data.systemId,
            brandId: this.props.data.brandId,
            modelId: this.props.data.modelId,
            serialNumber: this.props.data.serialNumber,
            technicianId: this.props.data.technicianId,
            customerId: this.props.data.customerId,
            releaseSw: this.props.data.releaseSw,
            installationDate: this.props.data.installationDate,
            warrantyExpires: this.props.data.warrantyExpires,
            warranty: this.props.data.warranty,
            isService: false,
            serviceOrderId: ""
         }

        Meteor.call("addPreOrders", warranty, frequency, date, properties, function(error, result)
        {
            if(!error)
            {
                self.OnApproval();
            }
            else
            {
                self.getToast().showMessage("error", error);
            }
        });
   }

    OnApproval()
    {
        let self = this;

        Meteor.call("setReceptionEquipmentStatus", this.props.data._id, "Aprobado", function(error, result)
        {
            if(!error)
            {
                self.getToast().showMessage("info", "La recepción de equipo se ha aprobado correctamente.");
            }
            else
                self.getToast().showMessage("error", error);
        });
    }

    saveChanges(button)
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let properties = self.getProperties(fields);

            if(self.props.isNew)
                Meteor.call("addReceptionEquipment", properties, self.onReceptionEquipmentAdded);
            else
            {
                Meteor.call("updateReceptionEquipment", self.props.data._id, properties, function(error, result)
                {
                    if(!error)
                    {
                        self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");

                        if(button == "saveAndClose")
                            FlowRouter.go('/receptionequipments');

                    }
                    else
                      self.getToast().showMessage("error", error);

                });
            }
        }
        else
            this.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
    }

    onReceptionEquipmentAdded(error, result)
    {
        if(!error)
        {
            this.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
            FlowRouter.go('/receptionequipment/' + result);
        }
        else
            this.getToast().showMessage("error", error);
    }

    getToolbarItems()
    {
        let self = this;

        let items = {
        new:
        {
            text: 'Nuevo',
            cls: 'button-new',
            handler: function()
                {
                FlowRouter.go('/receptionequipment/add');
            }
        },
        save:
        {
            text: 'Guardar',
            cls: 'button-save',
            visible: this.props.data.status != 'Aprobado',
            handler: function(){
                self.saveChanges("save");
            }
        },
        close:
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: function(){
                FlowRouter.go('/receptionequipments');
            }
        },
        report: {
            text: 'Reporte',
            cls: 'button-report',
            handler: function()
            {
                FlowRouter.go("/report");
                FlowRouter.setQueryParams({id: self.props.data._id, module: "reportreceptionequipment"});
            }
        },
        toogle:
        {
            text: 'Aprobar',
            cls: 'button-approval',
            visible: self.props.data.status != 'Aprobado',
            handler: function()
            {
                if(self.props.data.status === "Borrador")
                    self.toggleAprove();
            }
        }};

        return items;
    }

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [], rightGroup = [];

        if(this.props.isNew)
            leftGroup.push(actions.save);
        else
        {
            leftGroup.push(actions.new);
            leftGroup.push(actions.save);
            rightGroup.push(actions.toogle);
            rightGroup.push(actions.report);
        }

        leftGroup.push(actions.close);

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        return (<Toolbar className="north hbox" items={items} />);
    }

    toggleAprove()
    {
        if(!this.isEmptyField())
        {
            if(this.props.data.technicianId && this.props.data.technicianId != "")
            {
                this.refs.confirmDialog.setTitle("Está seguro que desea aprobar la recepción del equipo?");
                this.refs.confirmDialog.onOpen();
            }
            else
                alert("Debe seleccionar Ing. Responsable");
        }
        else
            alert("Ingrese campos requeridos");
    }

    isEmptyField()
    {
        let isEmpty = false;

        if(this.refs.releaseSw.getValue().trim() == "" || this.refs.frequency.getValue().trim() == "")
            isEmpty = true;

        return isEmpty;
    }

    onChangeFrequency(value)
    {
        if(!this.props.isNew)
            Meteor.call("updateReceptionEquipment", this.props.data._id, {frequency: value}, function(error, result){});
    }

    onChangeReleaseSw(value)
    {
        if(!this.props.isNew)
            Meteor.call("updateReceptionEquipment", this.props.data._id, {releaseSw: value}, function(error, result){});
    }

    getRenderDetail(id, systemId, brandId, modelId)
    {
        let componentConfiguration = (<Fieldset className="hbox box-form">
        <FormColumn  title=" Configuración" className="hbox-l border-reception-equipment">
            <ConfigurationContainer receptionEquipmentId = {id} systemId = {systemId} brandId = {brandId} modelId = {modelId} />
        </FormColumn>
        </Fieldset>);

        let componentVerification = (<Fieldset className="hbox box-form">
        <FormColumn  title="Verificación" className="hbox-l border-reception-equipment">
            <VerificationContainer receptionEquipmentId = {id} systemId = {systemId} brandId = {brandId} modelId = {modelId} />
        </FormColumn>
        </Fieldset>);

        let componentTestPerformed = (<Container className="vbox">
                                        <Fieldset className="hbox box-form">
                                            <FormColumn  title=" Pruebas Realizadas" className="hbox-l border-reception-equipment">
                                                <ReceptionEquipmentTestPerformedContainer receptionEquipmentId = {id} systemId = {systemId} modelId = {modelId} />
                                            </FormColumn>
                                        </Fieldset></Container>);

        let componentPreOrden = (<Fieldset className="hbox box-form">
        <FormColumn  title="Visitas de Mantenimiento" className="hbox-l border-reception-equipment">
            <PreOrderContainer receptionEquipmentId = {id} />
        </FormColumn>
        </Fieldset>);

        return (<div>
        {componentConfiguration}
        {componentVerification}
        {componentTestPerformed}
        {componentPreOrden}
        </div>);
    }

    getToast()
    {
      return this.refs.toastrMsg;
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        okLabel="Si"
        noLabel="No"
        />);
    }

    render()
    {
        let data = this.props.data;
        let dataState = this.state.data;
        let disableModel = this.state.disabledModel;
        let disabledField = this.state.disabledField;
        let componentsDetail = "";

        if(!this.props.isNew )
            componentsDetail = this.getRenderDetail(this.props.data._id, this.props.data.systemId, this.props.data.brandId, this.props.data.modelId);
        else
            componentsDetail = "Cuando guardar un registro se mostrara la sección de detalles";

        return(<Container className="vbox center">
            <div style={{textAlign: "center", color: "#004990"}}>
            <span style={{fontSize: "18px"}}>Recepción de Equipo</span>
           </div>
        {this.getToolbar()}
            <FormPanel ref="form">
                <ToastrMessage ref="toastrMsg" />
                <Fieldset className="hbox box-form" style={{marginRight: '5px'}}>
                    <FormColumn className="hbox-reception-equipment-column reception-equipment-width-column-1" >
                        <Dropdown className="hbox-l" ref="customer" label="Cliente" items={this.getCustomers()} value={dataState.customerId} required={true} emptyOption={true} disabled = {disabledField}/>
                        <Dropdown className="hbox-l" ref="system" label="Sistema" items={this.getSystems()} value={dataState.systemId} required={true} emptyOption={true} disabled = {disabledField}/>
                        <Dropdown className="hbox-l" ref="brand" label="Marca" items={this.getBrands()} value={dataState.brandId} onChange={this.onChangeBrand.bind(this)} required={true} emptyOption={true} disabled = {disabledField}/>
                        <Dropdown className="hbox-l" ref="model" label="Modelo" items={dataState.models} value={dataState.modelId} required={true} emptyOption={true} disabled = {disableModel} />
                        <TextField ref="serialNumber" label="Serie" value={dataState.serialNumber} disabled = {disabledField} required={true} />
                    </FormColumn>
                    <FormColumn  className="hbox-reception-equipment-column reception-equipment-width-column-2" >
                    <FormColumn>
                        <TextField ref="releaseSw" onChange={this.onChangeReleaseSw.bind(this)} label="Release SW" value={dataState.releaseSw} maxLength="50" disabled = {disabledField} required={true}/>
                        <DateField ref="installationDate"  className = "reception-equipment-input-date" label="Fecha de Instalación" value={dataState.installationDate != null ? Util.getDateISO(dataState.installationDate) : null} disabled = {disabledField} required={true}/>
                        <TextField ref="warranty" label="Garantía (m)" value={dataState.warranty} disabled = {disabledField} required={true}/>
                        <DateField ref="warrantyExpires" label="Garantía Vence" className = "reception-equipment-input-date" value={dataState.warrantyExpires != null ? Util.getDateISO(dataState.warrantyExpires) : null} disabled = {true}/>
                        <Dropdown ref="frequency" onChange={this.onChangeFrequency.bind(this)} label="Frecuencia" value={dataState.frequency} items={Util.getPeriods()} emptyOption={true} required={true} disabled = {disabledField}/>
                    </FormColumn>
                    </FormColumn>
                    <FormColumn  className="hbox-reception-equipment-column reception-equipment-width-column-3" >
                        <TechnicianReceptionEquipmentContainer receptionEquipmentId= {data._id} receptionEquipmentTechnicianId = {data.technicianId} disabled = {this.props.isNew}/>
                    </FormColumn>
                </Fieldset>
                {componentsDetail}
            </FormPanel>
        {this.getConfirmDialog()}
        </Container>)
    }
}
