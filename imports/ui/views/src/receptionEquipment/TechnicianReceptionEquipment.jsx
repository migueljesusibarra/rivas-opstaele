import React, { Component } from 'react';
//import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Toolbar, GridPanel, DialogPanel } from 'react-ui';

export class TechnicianReceptionEquipment extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            defaultChecked: false,
            opened: false,
            receptionEquipmentId: props.receptionEquipmentId
        };

        this.notSelectedTechnicians = [];
        this.selectRecords = 0;
        this.onClickItem = this.onClickItem.bind(this);
        this.onShowDialogTechnicians = this.onShowDialogTechnicians.bind(this);
        this.onCloseDialogTechnicians = this.onCloseDialogTechnicians.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);

        console.log();
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState({
            receptionEquipmentId: nextProps.receptionEquipmentId
        })
    }

    onDeleteTechnician(value, isTechnician, receptionEquipmentId, event)
    {
        Meteor.call("removeTechnicianReceptionEquipments", value, function(error, result){});

        if(isTechnician)
            Meteor.call("removeTechnicianFromReceptionEquipment", receptionEquipmentId,function(error, result){});
    }

    assignTechnician(technicianId, receptionEquipmentId, event)
    {
        Meteor.call("assignTechnician", receptionEquipmentId, technicianId,function(error, result){});
    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('techniciansGridId');
        let rowCheck = table.querySelectorAll('input[name=checkbox]');

        let checked = false;

        let technicianReceptionEquipments = [];

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;

                let data = {
                receptionEquipmentId: self.state.receptionEquipmentId,
                technicianId: rowCheck[i].dataset.technicianid,
                };

                technicianReceptionEquipments.push(data);
            }
        }

        if(!checked)
            alert("Por favor seleccione un registro");
        else
            this.saveTechnicianReceptionEquipments(technicianReceptionEquipments);

    }

    saveTechnicianReceptionEquipments(data)
    {
        let self = this;

        Meteor.call("addTechnicianReceptionEquipments", data, function(error, result)
        {
            if(!error)
                self.onCloseDialogTechnicians();
            else
                throw alert(error);
        });
    }

    setInitializeValue()
    {
        let table = document.getElementById('techniciansGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;

        this.setState({opened: false});
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.notSelectedTechnicians.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('techniciansGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.notSelectedTechnicians.length;
        else
            this.selectRecords = 0;
    }

    onShowDialogTechnicians()
    {
        this.setState({opened: true});
    }

    onCloseDialogTechnicians()
    {
        this.setInitializeValue();
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.SaveCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.onCloseDialogTechnicians
        }];

        return (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'Check',
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                name="checkbox"
                data-technicianid = {record._id}
                data-technicianname = {record.name}
                key={'check-'+record._id}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            header: 'Nombre',
            dataIndex: 'name'
        },
        {
            key: 3,
            header: 'Corre Electrónico',
            dataIndex: 'email'
        },
        {
            key: 4,
            header: 'Celular',
            dataIndex: 'cellPhone'
        }];

        return columns;
    }

    createDialogCustomer()
    {
        this.notSelectedTechnicians = this.props.technicians;

        let display = this.state.opened ? 'block' : 'none';

        return(
        <div className="overlay" style={{display: display}}>
            <DialogPanel
                className="vbox center"
                title="Ingenieros"
                modal>
                <GridPanel
                id="techniciansGridId"
                ref="techniciansGrid"
                className="center"
                columns={this.getColumns()}
                records={this.props.technicians}
                toolbar={this.getToolbar()} />
            </DialogPanel>
        </div>
        );
    }

    getTechnicianReceptionEquipmentList()
    {
        let self = this;

        let items = this.props.technicianReceptionEquipments.map(function(record, index)
        {
            let isTechnician = self.props.receptionEquipmentTechnicianId == record.technicianId;
            let color =   isTechnician ? "technician-reception-equipment-color-red" : "technician-reception-equipment-color-DarkGray";
            let circle = (<div onClick={self.assignTechnician.bind(this, record.technicianId, record.receptionEquipmentId)} className={'technician-reception-equipment-circle '+color} />);

            return (
                <div key={"contact" + index} className="technician-item">
                    <div>
                        {circle}
                        <div className = "technician-reception-equipment-text">
                            {record.technicianName}
                        </div>
                    </div>
                    <div className="contact-toolbar">
                        <button onClick={self.onDeleteTechnician.bind(this, record._id, isTechnician, record.receptionEquipmentId)}>
                            <i className="delete-icon-contact"></i>
                        </button>
                    </div>
                </div>);
        });

        return items;
    }

    render()
    {
        let header = this.props.disabled ? "" : (<a href="#" className="technician-reception-equipment" onClick={this.onShowDialogTechnicians}></a>);

        return (
        <div>
            <div key={"contact-" + 0} className="technician-reception-equipment-item-title">
                 <i className="technician-reception-equipment-icon-contact"></i><span className="technician-reception-equipment-title">Ing. Responsable</span>
                 <div className="technician-reception-equipment-toolbar-new">
                   {header}
                 </div>
            </div>
            {this.getTechnicianReceptionEquipmentList()}
            {this.createDialogCustomer()}
        </div>);
    }
}
