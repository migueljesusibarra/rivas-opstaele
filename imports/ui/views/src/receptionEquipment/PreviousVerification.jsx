import React, { Component } from 'react';Fieldset
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel, Fieldset, FormColumn,
    CheckBox, NumberField, DialogPanel } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class PreviousVerification extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            defaultChecked: false,
            opened: false
         };

        this.openDialogPrevious = this.openDialogPrevious.bind(this);
        this.closeDialogPrevious = this.closeDialogPrevious.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.records = [];
        this.selectRecords = 0;
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.records.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;

            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('previousVerificationGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.records.length;
        else
            this.selectRecords = 0;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'Check',
            maxWidth: 20,
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                key={'check-'+record._id}
                name="checkVerification"
                data-code = {record.code}
                data-description = {record.description}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            header: 'Cantidad',
            maxWidth: 60,
            renderer: function(record)
            {
                return(<input key={'number-'+record._id}
                className = "width-quantity-previous"
                defaultValue={record.quantity}
                type="number" >
                </input>);
            }
        },
        {
            key: 3,
            header: 'Código',
            dataIndex: 'code',
            maxWidth: 20
        },
        {
            key: 4,
            header: 'Descripción',
            dataIndex: 'description',
            minWidth: 200
        },
        {
            key: 5,
            header: 'Número de Serie',
            minWidth: 50,
            renderer: function(record)
            {
                return(<input
                key={'text-'+record._id}
                className = "width-serialNumber-previous"
                defaultValue={record.serialNumber}
                type="text" >
                </input>);
            }
        }];

        return columns;
    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('previousVerificationGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox][name=checkVerification]');
        let rowQuantity = table.querySelectorAll('input[type=number]');
        let rowSerialNumber = table.querySelectorAll('input[type=text]');
        let checked = false;

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
                {
                    checked = true;

                    let data = {
                        receptionEquipmentId: self.props.receptionEquipmentId,
                        code: rowCheck[i].dataset.code,
                        quantity: rowQuantity[i] != null ? rowQuantity[i].value : "",
                        description: rowCheck[i].dataset.description,
                        serialNumber: rowSerialNumber[i] != null ? rowSerialNumber[i].value : "",
                        isMissing: false
                    };

                    this.saveVerification(data);
                }
        }

        if(!checked)
            alert("Por favor seleccione un registro");

    }

    saveVerification(data)
    {
        let self = this;

        Meteor.call("addVerification", data, function(error, result)
        {
            if(!error)
                self.closeDialogPrevious();
            else
                throw alert(error);
        });
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.SaveCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogPrevious
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        return toolbar;
    }

    getGridPanel()
    {
        let data = this.props.data;
        this.records = data;

        return(
        <DialogPanel
            className="vbox center"
            title="Verification anterior del equipo"
            modal>
            <GridPanel
            id="previousVerificationGridId"
            ref="previousVerificationGrid"
            className="center"
            columns={this.getColumns()}
            records={data}
            toolbar={this.getToolbar()} />
        </DialogPanel>
        );
    }

    getDialog()
    {
        let display = this.state.opened ? 'block' : 'none';

        return (
        <div className="overlay" style={{display: display}}>
            {this.getGridPanel()}
        </div>);
    }

    openDialogPrevious()
    {
        let self = this;
        let fields = self.refs;
        this.setState({
            defaultChecked: false,
            opened: true
        });
    }

    closeDialogPrevious()
    {
        this.setState({opened: false});
    }

    render()
    {
        return (<div>{this.getDialog()}</div>);
    }
}
