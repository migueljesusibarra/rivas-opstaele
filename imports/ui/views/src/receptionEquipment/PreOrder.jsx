import React, { Component } from 'react';
import { Toolbar, DateField, GridPanel, TextField, EmailField, PhoneField, FormPanel,Fieldset,FormColumn,NumberField } from 'react-ui';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';
import { SearchField } from '/imports/ui/components/src/SearchField';


export class PreOrder extends Component
{
   constructor(props) {

       super(props);

       this.openDialogPreOrder = this.openDialogPreOrder.bind(this);
       this.closeDialogPreOrder = this.closeDialogPreOrder.bind(this);
       this.updatePreOrder = this.updatePreOrder.bind(this);
   }

   getColumns()
   {
     let columns = [
        {
            key: 1,
            header: '#',
            minWidth: 30,
            renderer: function(record){
                return  record.pos;
            }
         },
         {
          key: 2,
          header: 'Visita',
          minWidth: 50,
          renderer: function(record)
          {
            return record.date != "" ? Util.spanishDate(record.date): "";
          }
    }];

      return columns;
   }

   getToolbar()
   {
      let self = this;
      let items = [
      {
            text: 'Editar',
            cls: 'button-edit',
            handler: function(){
                self.openDialogPreOrder('edit');
            }
       }
      ];

     let toolbar = (<div>
                        <Toolbar className="north hbox" id="main-toolbar" items={items} />
                  </div>);

     return toolbar;
   }

    getGridPanel()
    {
        let data = this.props.records;

        return  <GridPanel
        id="preOrdenGridId"
        ref="preOrdenGrid"
        title="Pre-Orden"
        columns={this.getColumns()}
        records={data}
        toolbar={this.getToolbar()} />
     }

  getDialog()
    {

            let items = [{
                text: 'Guardar',
                cls: 'button-save',
                handler: this.updatePreOrder
            },
            {
                key: 'close',
                text: 'Cerrar',
                cls: 'button-close',
                handler: this.closeDialogPreOrder
            }];

            let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;


            return (<Dialog ref="dialog"  modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <DateField ref="date" label="Fecha" />
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>);
    }

    openDialogPreOrder(action)
    {
        let self = this;
        let fields = self.refs;
        let record = self.refs.preOrdenGrid.getSelectedRecord();

        if(record != null)
        {
            fields.record.setValue(record._id);
            fields.date.setValue(record.date != null ? Util.getDateISO(record.date) : null);
            fields.dialog.setTitle("Editar Visita");
            fields.dialog.open();
        }
        else
        {
            alert("Por favor seleccione un registro.");
        }
    }

   updatePreOrder()
    {
      let self = this;
      let recordId = this.refs.record.getValue();
      let properties = {
        date: self.refs.date.getValue()
      }

      Meteor.call("updatePreOrder", recordId, properties, function(error, result)
      {
          if(!error)
             alert("La fecha fue Actualizada");
          else
             alert(error);
      });

      this.closeDialogPreOrder();
    }

  closeDialogPreOrder()
  {
    this.refs.dialog.close();
  }

   render()
   {
     return (<div>
            {this.getGridPanel()}
            {this.getDialog()}
             </div>)
   }
}
