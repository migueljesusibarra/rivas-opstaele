import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField,NoteField, FormPanel,Fieldset,
    FormColumn,NumberField } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { RadioButton } from '/imports/ui/components/src/RadioButton.jsx';
import { PreviousTestPerformed } from '/imports/ui/views/src/receptionEquipment/PreviousTestPerformed.jsx';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class ReceptionEquimentTestPerformed extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {opened: this.props.opened || false}

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    removeTestPerformed()
    {
        let record = this.refs.testPerformedGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar la prueba realizada?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una prueba realizada.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.testPerformedGrid.getSelectedRecord();

        Meteor.call("removeReceptionEquipmentTestsPerformed", record._id, function(error, result){});
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                description: fields.description.getValue(),
                passFail: fields.passFail.getValue(),
                observation: fields.observation.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateReceptionEquipmentTestsPerformed", recordId, data, function(error, result){});
            else
                Meteor.call("addReceptionEquipmentTestsPerformed", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Descripcion',
            maxWidth: 60,
            renderer: function(record)
            {
                return record.description;
            }
        },
        {
            key: 2,
            header: 'Pass / Fail',
            minWidth: 10,
            renderer: function(record)
            {
                return  record.passFail == true ? "Pass" : "Fail";
            }
        },
        {
            key: 3,
            header: 'Observación',
            minWidth: 300,
            renderer: function(record)
            {
                return record.observation;
            }
        }];

        let data = this.props.records;

        let items = [
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                self.openDialog('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeTestPerformed();
            }
        },
        {
            text: 'Inv. Pruebas',
            cls: 'button-test',
            handler: function()
            {
                self.setState({opened: true});
            }
        }];

        let toolbar = <Toolbar items={items}/>;

        return (<GridPanel
                    id="testPerformedGridId"
                    key="testPerformedGridId"
                    ref="testPerformedGrid"
                    title="Test Performed"
                    columns={columns}
                    records={data}
                    toolbar={toolbar} />);
    }

    getDialog()
    {
        let data = {};

        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog"  modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="description" label="Descripción" required disabled />
                        <CheckboxToggle ref="passFail" lable = "Pass / Fail"  labelYes="Pass" labelNo="Fail"/>
                        <NoteField ref="observation" label="Observación" />
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let record = self.refs.testPerformedGrid.getSelectedRecord();

            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.description.setValue(record.description);
                fields.passFail.setValue(record.passFail);
                fields.observation.setValue(record.observation);
                fields.dialog.setTitle("Editar Prueba");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.description.setValue('');
                fields.passFail.setValue(true);
                fields.dialog.setTitle("Nueva Prueba");
                fields.dialog.open();
            }
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }
    closeDialogPreviousTestPerformeds()
    {
        this.setState({opened: false});
    }

    getPreviousTestPerformed()
    {
        return (
            <PreviousTestPerformed
                opened={this.state.opened}
                data={this.props.recordTests}
                close={this.closeDialogPreviousTestPerformeds.bind(this)}
                receptionEquipmentId={this.props.receptionEquipmentId}/>);
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getPreviousTestPerformed()}
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>);
    }
}
