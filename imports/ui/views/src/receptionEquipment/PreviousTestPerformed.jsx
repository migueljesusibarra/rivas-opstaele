import React, { Component } from 'react';Fieldset
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel, Fieldset, FormColumn,
    CheckBox, NumberField, DialogPanel } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class PreviousTestPerformed extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            opened: this.props.opened || false,
            defaultChecked: false
        };

        this.records = [];
        this.selectRecords = 0;
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.closeDialogPreviousTestPerformed = this.closeDialogPreviousTestPerformed.bind(this);
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState({opened: nextProps.opened});
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.records.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('previousTestGridId');
        let rowCheck = table.querySelectorAll('input[name=check]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.records.length;
        else
            this.selectRecords = 0;
    }

    setInitializeValue()
    {
        let table = document.getElementById('previousTestGridId');
        let rowCheck = table.querySelectorAll('input[name=check]');
        let rowQuantity = table.querySelectorAll('input[name=number]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        for (let i = 0; i < rowQuantity.length; i++)
        {
            rowQuantity[i].value = 1;
        }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'Check',
            maxWidth: 20,
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                key={'check-'+record._id}
                data-partid = {record._id}
                name="check"
                data-description = {record.description}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}></input>);
            }
        },
        {
            key: 2,
            header: 'Prueba',
            dataIndex: 'description',
            minWidth: 200
        },
        {
            key: 3,
            header: "Pass / Fail",
            renderer: function(record)
            {
                return(<div className="form-field-value hbox-r compose-fields slider-boxs">
                    <label className="switch-grid">
                        <input type="checkbox" key={'check-'+record._id} name = "toogle" defaultChecked={true} ></input>
                        <div className="slider round">
                            <span className="on">Pass</span><span className="off">Fail</span>
                        </div>
                    </label>
                </div>);
            }

        }];

        return columns;
    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('previousTestGridId');
        let rowToogle = table.querySelectorAll('input[name=toogle]');
        let rowCheck = table.querySelectorAll('input[name=check]');
        let rowQuantity = table.querySelectorAll('input[type=number]');
        let checked = false;

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;

                let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                partId: rowCheck[i].dataset.partid,
                description: rowCheck[i].dataset.description,
                passFail: rowToogle[i].checked,
                observation: ""
                }

                self.saveTestsPerformed(data);
            }
        }

        if(!checked)
            alert("Por favor seleccione un registro");
    }

    saveTestsPerformed(data)
    {
        let self = this;

        Meteor.call("addReceptionEquipmentTestsPerformed", data, function(error, result)
        {
            if(!error)
                self.closeDialogPreviousTestPerformed();
            else
                throw alert(error);
        });
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.SaveCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogPreviousTestPerformed
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        return toolbar;
    }

    getGridPanel()
    {
        let data = this.props.data;
        this.records = data;

        let display = this.state.opened ? 'block' : 'none';
        
        return(
        <div className="overlay" style={{display: display}}>
            <DialogPanel
                className="vbox center"
                title="Inv. Pruebas"
                modal>
                <GridPanel
                id="previousTestGridId"
                ref="previousTestGrid"
                className="center"
                columns={this.getColumns()}
                records={data}
                toolbar={this.getToolbar()} />
            </DialogPanel>
        </div>);
    }

    closeDialogPreviousTestPerformed()
    {
        this.setInitializeValue();
        this.props.close();
    }

    render()
    {
        return(<div ref="previousTestPerformed">{this.getGridPanel()}</div>);
    }
}
