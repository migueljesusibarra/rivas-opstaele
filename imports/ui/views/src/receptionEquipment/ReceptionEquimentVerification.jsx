import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel,Fieldset,FormColumn,
    NumberField, CheckBox } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { PreviousVerification } from '/imports/ui/views/src/receptionEquipment/PreviousVerification.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class ReceptionEquimentVerification extends Component
{
    constructor(props)
    {
        super(props);
        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.handlepaste = this.handlepaste.bind(this);
        this.OnClickMissing = this.OnClickMissing.bind(this);
    }

    componentDidMount()
    {
      document.getElementById("pasteId").addEventListener('paste', this.handlepaste);
      document.getElementById("pasteId").setAttribute("contentEditable", true);
    }

    componentWillUnmount()
    {
      document.getElementById("pasteId").removeEventListener('paste', this.handlepaste);
      document.getElementById("pasteId").setAttribute("contentEditable", false);
    }

    handlepaste(e)
    {
        let self = this;
        let id = e.currentTarget.getAttribute("id");

        if (e && e.clipboardData && e.clipboardData.types && e.clipboardData.getData && id == "pasteId")
        {
            // Check for 'text/html' in types list. See abligh's answer below for deatils on
            // why the DOMStringList bit is needed
            types = e.clipboardData.types;

            if (((types instanceof DOMStringList) && types.contains("text/html")) ||
            (types.indexOf && types.indexOf('text/html') !== -1))
            {
                // Extract data and pass it to callback
                arrayData = self.getRecordPaste(e.clipboardData.getData('text'));
                this.savePastedExcel(this.getDataCollection(arrayData));

                e.stopPropagation();
                e.preventDefault();
                return false;
            }
        }
    }

    getDataCollection(arrayData)
    {
        let data = [];

        if(arrayData.length > 0)
        {
            for(var i=0; i<arrayData.length; i++)
            {
                let quantity = arrayData[i][1];
                let code = arrayData[i][2];
                let description = arrayData[i][3];
                let serialNumber = arrayData[i][4];

                data.push({
                    receptionEquipmentId: this.props.receptionEquipmentId,
                    code: code,
                    quantity:quantity ,
                    description: description,
                    serialNumber: serialNumber,
                    isMissing: false
                });
            }
        }

        return data;
    }

    getRecordPaste(pastedData)
    {
        pastedData = pastedData.split("\n");

        for (var i = 0; i < pastedData.length; i++)
        {
            pastedData[i] = pastedData[i].split("\t");

            // Check if last row is a dummy row
            if (pastedData[pastedData.length - 1].length == 1 && pastedData[pastedData.length - 1][0] == "")
                pastedData.pop();
            //remove empty data
            if (pastedData.length == 1 && pastedData[0].length == 1 && (pastedData[0][0] == "" || pastedData[0][0] == "\r"))
                pastedData.pop();
        }

        return pastedData;
    }

    removeVerification()
    {
        let record = this.refs.verificationGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar verificación?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una verificación.");

    }

    onOkConfirmDialog()
    {
        let record = this.refs.verificationGrid.getSelectedRecord();

        Meteor.call("removeVerification", record._id, function(error, result){});
    }

    savePastedExcel(array)
    {
        Meteor.call("addBacthVerificationDetail", array, function(error, result)
        {
            if(!error)
                console.log("succes")
            else
                throw alert(error);
        });
     }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                code: fields.code.getValue(),
                quantity: fields.quantity.getValue(),
                description: fields.description.getValue(),
                serialNumber: fields.serialNumber.getValue(),
                isMissing: fields.isMissing.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateVerification", recordId, data, function(error, result){});
            else
                Meteor.call("addVerification", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    OnClickMissing(recordId, event)
    {
       Meteor.call("updateVerificationMissing", recordId, event.target.checked, function(error, result){});
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function()
            {
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                self.openDialog('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeVerification();
            }
        },
        {
            text: 'Verifi. Anteriores ',
            cls: 'button-previous',
            handler: function()
            {
                self.refs.previousVerification.openDialogPrevious();
            }
        }];

        let toolbar = <Toolbar items={items}/>;

        return toolbar;

    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: '#',
            minWidth: 30,
            renderer: function(record)
            {
                return  record.pos;
            }
        },
        {
            key: 2,
            header: 'Ctd',
            minWidth: 30,
            renderer: function(record)
            {
                return  record.quantity;
            }
        },
        {
            key: 3,
            header: 'Código',
            minWidth: 30,
            renderer: function(record)
            {
                return record.code;
            }
        },
        {
            key: 4,
            header: 'Descripción',
            minWidth: 120,
            renderer: function(record)
            {
                return record.description;
            }
        },
        {
            key: 5,
            header: 'Número de Serie',
            minWidth: 60,
            renderer: function(record)
            {
                return record.serialNumber
            }
        },
        {
            key: 6,
            header: 'Faltante',
            minWidth: 20,
            align: "center",
            renderer: function(record)
            {
                //#d26363
                return(<div className="form-field-value hbox-r compose-fields slider-boxs">
                    <label className="switch-grid">
                        <input type="checkbox" onClick={self.OnClickMissing.bind(self, record._id)} key={'check-'+record._id} name = "toogle" defaultChecked={record.isMissing} ></input>
                        <div className="slider round">
                            <span className="on">Si</span><span className="off">No</span>
                        </div>
                    </label>
                </div>);
            }
            // renderer: function(record)
            // {
            //     let classColor =  record.isMissing === false ? "check-checkmark-background-green" : "check-checkmark-background-red"
            //     let text =  record.isMissing === false ? "No" : "Si"
            //
            //   return(<a><span className="check-checkmark">
            //             <div className={ "check-checkmark-stem "  + classColor}></div>
            //             <div className={"check-checkmark-kick " + classColor}></div>
            //             </span>
            //             {text}
            //       </a>);
            // }
        }];

        let data = this.props.records;
        let toolbar = null;

        return  <GridPanel
            id="verificationGridId"
            ref="verificationGrid"
            title="verification"
            columns={columns}
            records={data}
            toolbar={toolbar} />
    }

    getDialog()
    {
        let data = {};
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Nuevo Verificación" modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="code" label="Código" required/>
                        <NumberField ref="quantity" label="Cantidad"  align = "left"  decimals={0}  required />
                        <TextField ref="description" label="Descripción" required />
                        <TextField ref="serialNumber" label="Número de Serie" />
                        <CheckBox ref="isMissing"  label="Faltante" value={data.isMissing === undefined ? false : data.isMissing}/>
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>
        );
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let record = self.refs.verificationGrid.getSelectedRecord();

            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.code.setValue(record.code);
                fields.quantity.setValue(record.quantity);
                fields.serialNumber.setValue(record.serialNumber);
                fields.description.setValue(record.description);
                fields.isMissing.setValue(record.isMissing);
                fields.dialog.setTitle("Editar Verificación");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.code.setValue('');
                fields.quantity.setValue('');
                fields.serialNumber.setValue('');
                fields.description.setValue('');
                fields.isMissing.setValue(false);
                fields.dialog.setTitle("Nueva Verificación");
                fields.dialog.open();
            }
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
            <Confirm
                ref="confirmDialog"
                onOk={this.onOkConfirmDialog}
             />);
    }

    render()
    {
        return (
        <div>
            <PreviousVerification
                ref="previousVerification"
                key="verifiId"
                receptionEquipmentId={this.props.receptionEquipmentId}
                data={this.props.previousRecords} />
               <div>
                {this.getToolbar()}
                <div id = "pasteId">
                    {this.getGridPanel()}
                </div>
                </div>
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>
        );
    }
}
