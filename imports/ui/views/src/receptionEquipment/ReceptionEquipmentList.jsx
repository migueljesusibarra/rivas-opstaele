import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button,FormPanel,Fieldset,FormColumn,Dropdown, TextField, DateField} from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';


export class ReceptionEquipmentList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    onRowDoubleClick(record)
    {
      this.openRecord(record);
    }

    openRecord(record)
    {
      FlowRouter.go("/receptionequipment/" + record._id);
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    movePurchaseOrder(record)
    {
        FlowRouter.go('/purchaseordersystem/'+ record.foreignId);
    }

    getColumns()
    {
        let self = this;

        let columns = [
            {
                  key: 1,
                  header: 'Id',
                  minWidth: 50,
                  dataIndex: '_id'
              },
              {
                  key: 2,
                  header: 'Cliente',
                  minWidth: 50,
                  dataIndex: 'customer'
              },

              {
                  key: 3,
                  header: 'Equipo',
                  minWidth: 50,
                  dataIndex: 'equipment'
              },
              {
                  key: 4,
                  header: 'Serie',
                  minWidth: 50,
                  dataIndex: 'serialNumber'
              },
              {
                  key: 6,
                  header: 'F. Instalación',
                  minWidth: 50,
                  align: "center",
                  dataIndex: 'installationDate',
                  renderer: function(record)
                  {
                      return record.installationDate != "" ? Util.getDateISO(record.installationDate): "";
                  }
              },
              {
                  key: 5,
                  header: 'R. SW',
                  minWidth: 50,
                  dataIndex: 'releaseSw'
              },
              {
                  key: 3,
                  header: 'P. Order',
                  minWidth: 50,
                  renderer: function(record)
                  {
                      let element = "";

                      if(record.source == "purchaseOrder")
                          element = (
                          <a
                          href=""
                          onClick={self.movePurchaseOrder.bind(self, record)}
                          style={{color: 'blue', textDecoration: 'underline'}}>
                          {"P. O  "+record.foreignId}
                          </a>);
                      else
                          element = 'No Aplica';

                      return element;
                  }
              },
              {
                  key: 7,
                  header: 'Estado',
                  minWidth: 50,
                  dataIndex: 'status',
                  renderer: function(record)
                  {
                      return (record.status == "Borrador" ? "Borrado" : record.status);
                  }
              }];

            return columns;
     }

     getToolbarItems()
     {
         let self = this;

         let items = {
                     new: {
                         text: 'Nuevo',
                         cls: 'button-new',
                         handler: function()
                         {
                             FlowRouter.go('/receptionequipment/add');
                         }
                    },
                    edit: {
                         text: 'Editar',
                         cls: 'button-edit',
                         handler: function()
                          {
                             let record = self.refs.receptionEquipmentGrid.getSelectedRecord();

                             if(record != null)
                             {
                                FlowRouter.go("/receptionequipment/" + record._id);

                              }
                              else
                              {
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                              }
                          }
                     },
                     report: {
                         text: 'Reporte',
                         cls: 'button-report',
                         handler: function()
                         {
                             let record = self.refs.receptionEquipmentGrid.getSelectedRecord();

                             if(record != null)
                             {
                                FlowRouter.go("/report");
                                FlowRouter.setQueryParams({id: record._id, module: "reportreceptionequipment"});
                              }
                              else
                              {
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                              }
                         }
                     },
                     history: {
                        text: 'Historial',
                        cls: 'button-history',
                        handler: function()
                        {
                          let record = self.refs.receptionEquipmentGrid.getSelectedRecord();

                          if(record != null)
                            self.refs.historyReceptionEquipment.onPenDialog();
                         else
                            self.getToast().showMessage("warning", "Por favor seleccione un registro");
                         }
                    },
                 }

                 return items;
     }

     getToast()
     {
         return this.refs.toastrMsg;
     }

     getToolbar()
     {
         let self = this;
         let actions = self.getToolbarItems();
         leftGroup = [actions.new, actions.edit], rightGroup = [actions.report, actions.history];

         let items = [{
             type: 'group',
             className: 'hbox-l',
             items: leftGroup
         },
         {
             type: 'group',
             className: 'hbox-r',
             items: rightGroup
         }];

        let toolbar = (
                        <div>
                            <Toolbar className="north hbox" id="main-toolbar" items={items} />
                            <div className="hbox div-second-tool-bar">
                                <div className="middle-width-quarter-column">
                                    <SearchField
                                        ref= "search"
                                        key={"searchfield"}
                                        className = "search-field-single-customer"
                                        placeholder=" Buscar"
                                        value={this.state.search}
                                        onSearch={this.onSearch} />
                                </div>
                                <div className="middle-width-middle-column text-aling-second-tool-bar">
                                    <span>{"Recepción de Equipos"}</span>
                                </div>
                            </div>
                        </div>);

        return toolbar;
     }

  getRecords()
  {
    let search = this.state.search;

    if(search)
        return this.props.data.filter(c => this.onFilter(c, search));
    else
        return this.props.data;
  }

  onFilter(record, search)
  {
      return (record.customer.search(new RegExp(search, "i")) != -1 ||
             record.system.search(new RegExp(search, "i")) != -1 ||
             record.brand.search(new RegExp(search, "i")) != -1 ||
             record.model.search(new RegExp(search, "i")) != -1);
  }

 getGridPanel()
  {
    let records = this.props.data;
    let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyReceptionEquipment" module="ReceptionEquipment" foreignId={this.state.record._id} title="Historial Recepción Equipos" />

    return (<div>
                    <ToastrMessage ref="toastrMsg" />
                    {historyContainer}
                    <GridPanel
                        id="receptionEquipmentGrid"
                        title="ReceptionEquipments"
                        ref="receptionEquipmentGrid"
                        columns={this.getColumns()}
                        records={this.getRecords()}
                        toolbar={this.getToolbar()}
                        onRowDoubleClick={this.onRowDoubleClick}
                        onSelectionChange={this.onSelectionChange} />
                </div>);
          }

    render()
    {
       return(<div>{this.getGridPanel()}</div>);
    }
}
