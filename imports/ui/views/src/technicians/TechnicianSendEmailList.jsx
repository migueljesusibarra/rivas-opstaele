import React, { Component } from 'react';
import { Toolbar, GridPanel} from 'react-ui';

export class TechnicianSendEmailList extends Component
{
    constructor(props)
    {
        super(props);
        this.records = [];
        this.selectRecords = 0;
        this.SendEmailCheckAll = this.SendEmailCheckAll.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.closeDialogTechnicianSendEmails = this.closeDialogTechnicianSendEmails.bind(this);
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.records.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('technicianSendEmailGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.records.length;
        else
            this.selectRecords = 0;
    }

    SendEmailCheckAll()
    {
        let self = this;
        let table = document.getElementById('technicianSendEmailGridId');
        let rowCheck = table.querySelectorAll('input[name=checkbox]');
        let checked = false;

        let technicianIds = [];

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;
                technicianIds.push(rowCheck[i].dataset.technicianid);
            }
        }

        if(!checked)
        {
           alert("Por favor seleccione un registro");
        }
        else
        {
            this.closeDialogTechnicianSendEmails();
            this.sendTechnicianEmail(technicianIds);
        }
    }

    sendTechnicianEmail(technicianIds)
    {
        let self = this;

        Meteor.call("sendTechnicianEmail", technicianIds, function(error, result)
        {
            if(!error)
            {
                if(self.props.module == "PurchaseOrderTypePart")
                {
                   console.log("send: " + self.props.recordId + " module: " + self.props.module);
                  self.OnInsertReceptionEquipmentTechnician(self.props.recordId, technicianIds);

                }else
                {
                    alert("Correo enviado");
                }
            }
            else
                alert(error);
        });
    }

    OnInsertReceptionEquipmentTechnician(receptionEquipmentId, technicianIds)
    {
        let technicianReceptionEquipment = [];

        for(let technicianId of technicianIds)
        {
            technicianReceptionEquipment.push({
                receptionEquipmentId: receptionEquipmentId,
                technicianId: technicianId,
            });
        }

        Meteor.call("addTechnicianReceptionEquipments", technicianReceptionEquipment, function(error, result)
        {
            if(!error)
              alert("Correo enviado");
            else
              alert(error);
        });
    }

    setInitializeValue()
    {
        let table = document.getElementById('technicianSendEmailGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;
    }

    closeDialogTechnicianSendEmails()
    {
        this.setInitializeValue();
        this.props.close();
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Enviar Correo',
            cls: 'button-email-technician',
            handler: this.SendEmailCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: self.closeDialogTechnicianSendEmails
        }];

        return (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'Check',
            maxWidth: 20,
            renderer: function(record)
            {
                return(<input
                           type="checkbox"
                           name="checkbox"
                           data-technicianid={record._id}
                           key={'check-'+record._id}
                           defaultChecked = {false}
                           onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            header: 'Nombre',
            dataIndex: 'name',
            maxWidth: 60
        },
        {
            key: 3,
            header: 'Corre Electrónico',
            dataIndex: 'email',
            minWidth: 60
        }];

        return columns;
    }

    getGridPanel()
    {
        let data = this.props.data;
        this.records = data;

        return (<GridPanel
                    id="technicianSendEmailGridId"
                    ref="technicianSendEmailGrid"
                    className="center"
                    columns={this.getColumns()}
                    records={data}
                    toolbar={this.getToolbar()} />);
    }

    render()
    {
        return(<div ref="technicianSendEmailList">{this.getGridPanel()}</div>)
    }
}
