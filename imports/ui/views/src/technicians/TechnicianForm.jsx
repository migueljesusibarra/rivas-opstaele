import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container, Toolbar, FormPanel, Fieldset,
    FormColumn, Dropdown, TextField, NoteField,
    container, Button, PhoneField, EmailField,
    DisplayField, GridPanel, Input } from 'react-ui';
import { Util } from '/lib/common.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { PieWithLegend } from '/imports/ui/views/src/charts/PieWithLegend.jsx'


export class TechnicianForm extends Component
{
    constructor(props)
    {
        super(props);

        this.saveChanges = this.saveChanges.bind(this);
        this.onTechnicianAdded = this.onTechnicianAdded.bind(this);
    }

    getProperties(fields)
    {
        let record = {
            name: fields.name.getValue(),
            address: fields.address.getValue(),
            cellPhone: fields.cellphone.getValue(),
            email: fields.email.getValue(),
            active: this.props.data.active === undefined ? true : this.props.data.active,
        };

        return record;
    }

    getToast()
    {
        return this.refs.toastrMsg;
    }

    saveChanges(button)
    {
        let fields = this.refs;
        let self = this;


        if(fields.form.isValid(fields))
        {
            let properties = this.getProperties(fields);

            if (this.props.isNew)
                Meteor.call("addTechnician", properties, this.onTechnicianAdded);
            else
            {
                Meteor.call("updateTechnician", this.props.data._id, properties, function(error, result)
                {
                    if(!error)
                    {
                        if(button == "saveAndClose")
                            FlowRouter.go('/technicians');
                        else
                            self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
                    }
                    else
                        throw self.getToast().showMessage("error", error);
                });
            }
        }
        else
            self.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
    }

    onTechnicianAdded(error, result)
    {
        if(!error)
        {
            this.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
            FlowRouter.go('/technician/' + result);
        }
        else
            this.getToast().showMessage("error", error);
    }

    getToolbarItems()
    {
        let self = this;

        let items = {
        back:
        {
            text: '',
            cls: 'button-back',
            handler: function()
            {
                FlowRouter.go('/technicians');
            }
        },
        new:
        {
            text: 'Nuevo',
            cls: 'button-new-dealer',
            handler: function()
            {
                FlowRouter.go('/technician/add');
            }
        },
        save:
        {
            text: 'Guardar',
            cls: 'button-save',
            handler:function()
            {
                self.saveChanges("save");
            }
        },
        print:
        {
            text: 'Imprimir',
            cls: 'button-print',
            handler: function()
            {
                conosole.log("In process");

            }
        },
        toggle:
        {
            text: this.props.data.active !== false ? 'Desactivar' : 'Activar',
            cls: 'button-delete',
            handler: function()
            {
                self.toggleActive();
            }
        }};

        return items;
    }

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        let leftGroup = []

        if(this.props.isNew)
        {
            leftGroup.push(actions.back);
            leftGroup.push(actions.save);
        }
        else
        {
            leftGroup.push(actions.back);
            leftGroup.push(actions.new);
            leftGroup.push(actions.save);
            leftGroup.push(actions.toggle);
            leftGroup.push(actions.print);
        }

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        }];

        return <Toolbar className="north hbox" items={items} />;
    }

    toggleActive()
    {
        let message = this.props.data.active ? "Está seguro que desea activar este Ingeniero?" :  "Está seguro que desea desactivar este Ingeniero?"
        let confirmation = confirm(message);

        if( confirmation == true)
            Meteor.call("setTechnicianActive", this.props.data._id, this.props.data.active ? false : true);
    }

    //   render()
    //   {
    //     return (<div><PieWithLegend /></div>)
    //   }
    render()
    {
        let data = this.props.data || {};

        return (
        <div>
            <div style={{textAlign: "center", color: "#004990"}}>
            <span style={{fontSize: "18px"}}>Ingeniero</span>
           </div>
            <Container className="vbox center">
                {this.getToolbar()}
                <FormPanel ref="form"  className="center" style={{"overflow": "auto"}}>
                    <ToastrMessage ref="toastrMsg" />
                    <Fieldset className="hbox box-form" >
                        <FormColumn className="hbox-r" style={{marginRight: "10px"}}>
                            <TextField ref="name" label="Nombre"  required={true} value ={data.name} maxLength="50"/>
                            <TextField ref="address" label="Dirección"  required={true} value ={data.address} maxLength="100"/>
                            <EmailField ref="email" label="Correo Electrónico" value ={data.email} required={true} />
                        </FormColumn>
                        <FormColumn className="hbox-l">
                            <PhoneField ref="cellphone" label="Teléfono / Cel" value ={data.cellPhone} mask="(___) ____-____" required={true} />
                            <DisplayField ref="active" label="Estado">{data.active !== false ? 'Activo' : 'Inactivo'}</DisplayField>
                        </FormColumn>
                    </Fieldset>
                </FormPanel>
            </Container>
        </div>
        )
    }
}
