import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, EmailField, Dropdown, PhoneField, FormPanel,Fieldset,FormColumn,CheckBox } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';

export class TechnicianPanel extends Component
{
  constructor(props)
  {
    super(props);
    this.saveChanges = this.saveChanges.bind(this);
    this.onNewContactClick = this.onNewContactClick.bind(this);
    this.onDeleteContactClick = this.onDeleteContactClick.bind(this);
  }

  onDeleteContactClick(event)
  {
    let recordId = event.target.closest("button").dataset.id;
    let confirmation = confirm("Está seguro de que desea eliminar este contacto?");

    if(confirmation == true)
    {
      Meteor.call("removeContact", recordId, function(error, result){});
    }
  }

  onNewContactClick(event)
  {
    console.log("event realizado");
  }

saveChanges()
{
  let self = this;

  if(self.refs.form.isValid(self.refs))
  {
    let recordId = self.refs.record.getValue();

    let data = {
      foreignId: self.props.foreignId,
      name: self.refs.name.getValue(),
      email: self.refs.email.getValue(),
      cellPhone: self.refs.cellPhone.getValue(),
      workPhone: self.refs.workPhone.getValue(),
      homePhone: self.refs.homePhone.getValue(),
      address: self.refs.address.getValue(),
      main:self.refs.main.getValue(),
      module: self.props.module,
    }

    if(recordId.length > 0)
    {
      Meteor.call("updateContact", recordId, data, function(error, result){});
    }
    else
    {
      Meteor.call("addContact", data, function(error, result){});
    }
    self.refs.dialog.close();
  }
  else
  {
    alert("Por favor ingrese todos los campos requeridos.");
  }
}

getList()
{
  let self = this;
  let data = [{
               id: "1", 
               name: "Miguel de Jesus Ibarra Sanabria",
               main: true
               },
               {
                 value: "2", 
                 name: "Pablo Ricardo Hernandez Perez",
                 main: false 
               }]

  let items = data.map(function(record, index)
  {
    let circle = record.main ? (<div><div className="circle-contact color-red-contact" ></div><div className = "contact-text">{record.name}</div></div>) : (<div><div className="circle-contact color-DarkGray-contact" ></div><div className = "contact-text">{record.name}</div>
       
      </div>)
     return <div key={"contact" + index} className="contact-item">
      {circle}
      <div className="contact-toolbar">
       <button onClick={self.onDeleteContactClick} data-id={record._id}><i className="delete-icon-contact"></i></button>
      </div>
    </div>;
  });

  return items;
}

render()
{
  return (<div>
    <div key={"contact-" + 0} className="contact-customer-item-title">
      <i className="customer-icon-contact"></i>
      <Dropdown ref="technician" label="Ing. Responsable" items={this.props.technicians} emptyOption={true}/>
      <div className="contact-toolbar-new">
       <a href="#" className="icon-contact-dealer" onClick={this.onNewContactClick}></a>
      </div>
    </div>
    {this.getList()}
   </div>)
}
}
