import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button } from 'react-ui';
import { Util } from '/lib/common.js';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class TechnicianList extends Component
{
  constructor(props)
  {
    super(props);

    this.state = {
      search: null,
      record: null
    };

    this.onSearch = this.onSearch.bind(this);
    this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
    this.onSelectionChange = this.onSelectionChange.bind(this);
  }

  onSelectionChange(record)
  {
    this.setState({record:record});
  }

  onRowDoubleClick(record)
  {
    this.openRecord(record);
  }
  openRecord(record)
  {
    FlowRouter.go("/technician/" + record._id);
  }

  onSearch(event)
  {
    this.setState({search: event.target.value});
  }

  getColumns()
  {
    let columns = [
     {
        key: 1,
        header: 'Id',
        dataIndex: '_id',
        minWidth: 50
      },
      {
        key: 2,
        header: 'Nombre',
        minWidth: 50,
        dataIndex: 'name'
      },
      {
        key: 3,
        header: 'Dirección',
        minWidth: 50,
        dataIndex: 'address'
      },
      {
        key: 4,
        header: 'Corre Electrónico',
        minWidth: 50,
        dataIndex: 'email'
      },
      {
        key: 5,
        header: 'Celular',
        minWidth: 50,
        dataIndex: 'cellPhone'
      },
      {
        key: 6,
        header: 'Estado',
        minWidth: 50,
        renderer: function(record){
          return record.active !== false ? 'Activo' : 'Inactivo';
        }
    }];

      return columns;
    }

    getToolbarItems()
    {
        let self = this;

       let items = {
                     new: {
                         text: 'Nuevo',
                         cls: 'button-new-dealer',
                         handler: function(){
                             FlowRouter.go('/technician/add');
                         }
                     },
                     edit: {
                         text: 'Editar',
                         cls: 'button-edit',
                         handler: function()
                         {
                             let record = self.refs.technicianGrid.getSelectedRecord();

                             if(record != null)
                             {
                                 FlowRouter.go("/technician/" + record._id);

                             }else
                             {
                                 self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                             }
                         }
                     },
                     print: {
                         text: 'Reporte',
                         cls: 'button-report',
                         handler: function()
                         {
                             FlowRouter.go("/report");
                         }
                     },
                     history: {
                         text: 'Historial',
                         cls: 'button-history',
                         handler: function()
                         {
                             let record = self.refs.technicianGrid.getSelectedRecord();

                             if(record != null)
                                self.refs.historyTechnician.onPenDialog();

                                else
                                    self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                        }
                    }
               }

            return items;
    }

    getToast()
	{
		return this.refs.toastrMsg;
	}

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.new, actions.edit], rightGroup = [actions.print, actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

      let toolbar = (
      <div>
          <Toolbar className="north hbox" id="main-toolbar" items={items} />
          <div className="hbox div-second-tool-bar">
              <div className="middle-width-quarter-column">
                  <SearchField
                  ref= "search"
                  key={"searchfield"}
                  className = "search-field-single-customer"
                  placeholder=" Buscar"
                  value={this.state.search}
                  onSearch={this.onSearch} />
              </div>
              <div className="middle-width-middle-column text-aling-second-tool-bar">
                  <span>{"Lista de Ingenieros"}</span>
              </div>
          </div>
      </div>);

      return toolbar;
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
      return this.props.data.filter(c => this.onFilter(c, search));
      else
      return this.props.data;
    }

    onFilter(record, search)
    {
      return (record.name.search(new RegExp(search, "i")) != -1 ||
      record.address.search(new RegExp(search, "i")) != -1 ||
      record.email.search(new RegExp(search, "i")) != -1 ||
      record.cellPhone.search(new RegExp(search, "i")) != -1);
    }

    render()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyTechnician" module="Technician" foreignId={this.state.record._id} title="Historial Ingeniero"/>

    return(<div>
             <ToastrMessage ref="toastrMsg" />
             {historyContainer}
            <GridPanel
            id="technician-grid"
            title="Technian"
            ref="technicianGrid"
            columns={this.getColumns()}
            records={this.getRecords()}
            toolbar={this.getToolbar()}
            onRowDoubleClick={this.onRowDoubleClick}
            onSelectionChange={this.onSelectionChange} />
            </div>);
    }

  }
