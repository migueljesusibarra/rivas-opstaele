import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button, Fieldset } from 'react-ui';
import { Util } from '/lib/common.js';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';


export class CustomerList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        FlowRouter.go("/customer/" + record._id);
    }

    onSearch(event)
    {
        this.setState({search: event.target.value});
    }

    onCloseDialogHistory()
    {
        this.refs.historiDialog.close();
    }

    getColumns()
    {
        let self = this;

        return columns = [
        {
            key: 1,
            header: 'Id',
            dataIndex: '_id',
            minWidth: 50
        },
        {
            key: 2,
            header: 'Nombre',
            dataIndex: 'name',
            minWidth: 50
        },
        {
            key: 3,
            header: 'Tipo',
            minWidth: 50,
            dataIndex: 'customerType'
        },
        {
            key: 4,
            header: 'Teléfono',
            dataIndex: 'phone',
            minWidth: 50
        },
        {
            key: 5,
            header: 'Correo Electrónico',
            dataIndex: 'email',
            minWidth: 50
        },
        {
            key: 6,
            header: 'Departamento',
            dataIndex: 'department',
            minWidth: 50
        },
        {
            key: 7,
            header: 'Estado',
            minWidth: 50,
            renderer: function(record)
            {
                return record.active !== false ? 'Activo' : 'Inactivo';
            }
        }];
    }

    getToolbarItems()
    {
        let self = this;

       let items = {
                new: {
                        text: 'Nuevo',
                        cls: 'button-new-dealer',
                        handler: function()
                        {
                            FlowRouter.go('/customer/add');
                        }
                    },
                edit: {
                    text: 'Editar',
                    cls: 'button-edit',
                    handler: function()
                    {
                        let record = self.refs.customerGrid.getSelectedRecord();

                        if(record != null)
                            FlowRouter.go("/customer/" + record._id);
                        else
                            self.getToast().showMessage("warning", "Por favor seleccione un registro");
                    }
                },
                history: {
                    text: 'Historial',
                    cls: 'button-history',
                    handler: function()
                    {
                        let record = self.refs.customerGrid.getSelectedRecord();

                        if(record != null)
                            self.refs.historyCustomer.onPenDialog();
                        else
                            self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                    }
                }
            }

            return items;
    }

    getToast()
	{
		return this.refs.toastrMsg;
	}

    getToolbar()
    {
        let self = this;
		let actions = self.getToolbarItems();
		leftGroup = [actions.new, actions.edit], rightGroup = [actions.history];

        let items = [{
			type: 'group',
			className: 'hbox-l',
			items: leftGroup
		},
		{
			type: 'group',
			className: 'hbox-r',
			items: rightGroup
		}];

        return (
            <div>
                <Toolbar
                    className="north hbox"
                    id="main-toolbar"
                    items={items} />
                    <div className="hbox div-second-tool-bar">
                        <div className="middle-width-quarter-column">
                            <SearchField
                                ref= "search"
                                key={"searchfield"}
                                className = "search-field-single-customer"
                                placeholder=" Buscar"
                                value={this.state.search}
                                onSearch={this.onSearch} />
                        </div>
                        <div className="middle-width-middle-column text-aling-second-tool-bar">
                            <span>{"Lista de Clientes"}</span>
                        </div>
                    </div>
                </div>);
    }

    getRecords()
    {
        let search = this.state.search;

        if(search)
            return this.props.data.filter(c => this.onFilter(c, search));
        else
            return this.props.data;
    }

    onFilter(record, search)
    {
        return (record.name.search(new RegExp(search, "i")) != -1 ||
            record.department.search(new RegExp(search, "i")) != -1 ||
            record.phone.search(new RegExp(search, "i")) != -1 ||
            record.customerType.search(new RegExp(search, "i")) != -1);
    }

    render()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyCustomer" module="Customer" foreignId={this.state.record._id} title="Historial Cliente"/>

        return (<div>
              <ToastrMessage ref="toastrMsg" />
              {historyContainer}
              <GridPanel
                    id="customerGrid"
                    ref="customerGrid"
                    title="Cliente"
                    columns={this.getColumns()}
                    records={this.getRecords()}
                    toolbar={this.getToolbar()}
                    onRowDoubleClick={this.onRowDoubleClick}
                    onSelectionChange={this.onSelectionChange} />
            </div>);
    }
}
