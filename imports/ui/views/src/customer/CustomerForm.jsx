import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container, FormPanel, Fieldset, FormColumn, Dropdown, TextField, NoteField, container,
  Button, PhoneField, EmailField, DisplayField, GridPanel, Input } from 'react-ui';
import { Util } from '/lib/common.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Field } from '/imports/ui/components/src/Field.jsx';
import { ContactPanelContainer } from '/imports/ui/containers/ContactPanelContainer'
import { EquipmentContainerList } from '/imports/ui/containers/EquipmentContainerList'


export class CustomerForm extends Component
{
    constructor(props)
    {
        super(props);
        this.saveChanges = this.saveChanges.bind(this);
        this.toggleActive  = this.toggleActive.bind(this);
        this.onChoose = this.onChoose.bind(this);
        this.onCustomerAdded = this.onCustomerAdded.bind(this);
        this.state =
        {
            type: this.getType(this.props.data.customerTypeId, this.props.customerTypes),
            data: this.props.data

        }
    }

    componentWillReceiveProps(props)
    {
        this.setState({data: props.data});
    }

    getCustomerTypes()
    {
        this.props.customerTypes.sort(Util.applySortObject);

        return this.props.customerTypes.map(function(type)
        {
            return { value:type._id, text:type.description };
        });
    }

    getType(id, collection)
    {
        let i = 0;

        for( i; i < collection.length; i++)
        {
            if(collection[i]._id == id)
            {
                return collection[i].type;
            }
        }
    }
    onChoose(value)
    {
        let self = this;

        this.setState({
            type : this.getType(value, this.props.customerTypes),
            data: this.setUpdateState(this.refs, value)
        });
    }

    onClickNew(event)
    {
        this.setState({
            data: this.setStateValue(null,null, true)
        });

        FlowRouter.go('/customer/add');
    }

    onClickSave(event)
    {
        this.saveChanges("save");
    }

    onClickActive(event)
    {
       this.toggleActive();
    }

    onClickClose(event)
    {
       FlowRouter.go('/customers');
    }

    setUpdateState(fields, value)
    {
        let data =
        {
            city: fields.city.getValue(),
            clinic: fields.clinic.getValue(),
            customerTypeId: value,
            departmentId: fields.department.getValue(),
            email: fields.email.getValue(),
            identificationCed: "",
            identificationRuc: "",
            name: fields.name.getValue(),
            note: fields.note.getValue(),
            phone: fields.phone.getValue(),
            address: fields.address.getValue()
        }

        return data;
    }

    setStateValue(fields, value, isNew)
    {
        let data = {
            city: isNew ? "" : fields.city,
            clinic: isNew ? "" : fields.clinic,
            customerTypeId: isNew ? "" : value,
            departmentId: isNew ? "" : fields.department,
            email: isNew ? "" : fields.email,
            identificationCed: isNew ? "" : fields.identificationCed,
            identificationRuc: isNew ? "" : fields.identificationRuc,
            name: isNew ? "" : fields.name,
            note: isNew ? "" : fields.note,
            phone: isNew ? "" : fields.phone,
            address: isNew ? "" : fields.address
        };

        return data;
    }

    getProperties(fields)
    {
        let record = {
            customerTypeId: fields.customerType.getValue(),
            name: fields.name.getValue(),
            clinic: fields.clinic.getValue(),
            identificationCed: this.state.type == 2 ? fields.identificationCed.getValue() : "",
            identificationRuc: this.state.type == 1 ? fields.identificationRuc.getValue(): "",
            phone: fields.phone.getValue(),
            email: fields.email.getValue(),
            city: fields.city.getValue(),
            departmentId: fields.department.getValue(),
            address: fields.address.getValue(),
            note: fields.note.getValue(),
            active: this.props.data.active === undefined ? true : this.props.data.active,
        };

        return record;

    }
    getToast()
    {
        return this.refs.toastrMsg;
    }

    saveChanges(button)
    {
        let fields = this.refs;
        let self = this;

        if(fields.form.isValid(fields))
        {
            let properties = this.getProperties(fields);

            if(this.props.isNew)
                Meteor.call("addCustomer", properties, this.onCustomerAdded);
            else
            {
                Meteor.call("updateCustomer", this.props.data._id, properties, function(error, result){
                    if(!error)
                    {
                        self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");

                        if(button == 'saveAndClose')
                        {
                            FlowRouter.go('/customers');
                        }
                    }
                    else
                    throw self.getToast().showMessage("error", error);
                });
            }
        }
        else
        {
            this.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
        }
    }

    onCustomerAdded(error, result)
    {
        let self = this;

        if(!error)
        {
            self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
            FlowRouter.go('/customer/' + result);
        }
        else
            self.getToast().showMessage("error", error);
    }

    toggleActive()
    {
        let message = this.props.data.active ? "Está seguro que desea activar este cliente?" :  "Está seguro que desea desactivar este cliente?"
        let confirmation = confirm(message);

        if(confirmation == true)
        {
            Meteor.call("setActive", this.props.data._id, this.props.data.active ? false : true);
        }
    }

      render()
      {
          let dataState = this.state.data;
          let data = this.props.data;

          let textfield1 = (<TextField ref="identificationCed"  label="Cédula #"  required={true} value ={dataState.identificationCed} maxLength="100"/>);
          let textfield2 = (<TextField ref="identificationRuc" label="RUC  #"  required={true} value ={dataState.identificationRuc} maxLength="100"/>);
          let identification = this.state.type == 2 ? textfield1 : textfield2;

          let contactPanel = (<ContactPanelContainer foreignId = {data._id} module = "customer"/>);

          let containerComponent = (<Container className="vbox contact-title center">
                                        <Fieldset className="hbox" style={{margin: '0px'}}>
                                            <FormColumn title="Equipos" className="hbox-r">
                                                <EquipmentContainerList foreignId={data._id} model="customer" />
                                            </FormColumn>
                                        </Fieldset>
                                    </Container>);

         let textContainer = (<p style = {{marginLeft: "10px"}}><strong>La sección de equipo aparecerá al guardar el registro</strong></p>);
         let textContact = (<p style = {{marginLeft: "10px"}}><strong>La sección de contacto aparecerá al guardar el registro</strong></p>);

         let componentEquipment = Util.isEmpty(data._id) ?  textContainer : containerComponent;
         let componentContact = Util.isEmpty(data._id) ?  textContact : contactPanel;

         let buuttonNew = (<div className="x-toolbar north hbox">
                             <div className="hbox-l">
                                <a href="javascript:void(0);" className="button-save" onClick={this.onClickSave.bind(this)}>Guardar</a>
                                <a href="javascript:void(0);" className="button-close" onClick={this.onClickClose.bind(this)}>Cerrar</a>
                                <span className="header-ttitel-edit" style={{paddingLeft: "417px"}}>Nuevo Cliente</span>
                            </div>
                         </div>);

         let componetButton = (<div className="x-toolbar north hbox">
                                <div className="hbox-l">
                                    <a href="javascript:void(0);" className="button-new-dealer" onClick={this.onClickNew.bind(this)} >Nuevo</a>
                                    <a href="javascript:void(0);" className="button-save" onClick={this.onClickSave.bind(this)}>Guardar</a>
                                    <a href="javascript:void(0);" className="button-delete" onClick={this.onClickActive.bind(this)} > {this.props.data.active !== false ? 'Desactivar' : 'Activar'}</a>
                                    <a href="javascript:void(0);" className="button-close" onClick={this.onClickClose.bind(this)}>Cerrar</a>
                                    <span className="header-ttitel-edit">Cliente</span>
                                    </div>
                                </div>);

         let toolbar = this.props.isNew ? buuttonNew : componetButton

         return(<Container className="vbox center" >
                     {toolbar}
                    <FormPanel ref="form" className="customer" >
                        <ToastrMessage ref="toastrMsg" />
                        <Fieldset className="hbox box-form" >
                            <FormColumn className="hbox-r" style={{marginRight: "10px"}}>
                                <Dropdown ref="customerType" label="Tipo" items={this.getCustomerTypes()} value={dataState.customerTypeId} onChange={this.onChoose} emptyOption={true} required={true}/>
                                <TextField ref="name" label="Nombre"  required={true} value ={dataState.name} maxLength="50"/>
                                <TextField ref="clinic" label="Clínica"  value ={dataState.clinic} maxLength="50"/>
                                <PhoneField ref="phone" label="Teléfono / Cel" value ={dataState.phone} required={true} mask="(___) ____-____" />
                                <EmailField ref="email" label="Correo Electrónico" value ={dataState.email}/>
                            </FormColumn>
                            <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                                {identification}
                                <Dropdown ref="department" label="Departamento" items={Util.getDepartments()} value = {dataState.departmentId} emptyOption={true} required={true}/>
                                <TextField ref="city" label="Ciudad"  value = {dataState.city} required={true} maxLength="50"/>
                                 <TextField ref="address" label="Dirección" value = {dataState.address} required={true} maxLength="50" />
                                <DisplayField ref="active" label="Estado">{data.active !== false ? 'Activo' : 'Inactivo'}</DisplayField>
                                <NoteField ref="note" label="Notas" value={dataState.note} />
                            </FormColumn>
                            <FormColumn className="hbox-l">
                                {componentContact}
                            </FormColumn>
                        </Fieldset>
                    <div>
                   {componentEquipment}
                  </div>
                  </FormPanel>
                   </Container>);
      }
    }
