import React, { Component } from 'react';Fieldset
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel, Fieldset, FormColumn,
    CheckBox, NumberField, DialogPanel } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';


export class EquipmentSingleList extends Component
{
    constructor(props)
    {
        super(props);
        this.state = { defaultChecked: false, opened: false };
        this.openDialogEquipmentSingle = this.openDialogEquipmentSingle.bind(this);
        this.closeDialogEquipmentSingle = this.closeDialogEquipmentSingle.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.onClickItem = this.onClickItem.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.records = [];
        this.selectRecords = 0;

    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.records.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('equipmentSingleGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.records.length;
        else
            this.selectRecords = 0;
    }

    setInitializeValue()
    {
        let table = document.getElementById('equipmentSingleGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');
        let rowQuantity = table.querySelectorAll('input[name=number]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        for (let i = 0; i < rowQuantity.length; i++)
        {
            rowQuantity[i].value = rowQuantity[i].dataset.initialquantity;
        }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'Check',
            maxWidth: 20,
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                name="checkbox"
                key={'check-'+record._id}
                data-equipmentId = {record._id}
                data-systemId = {record.systemId}
                data-brandId = {record.brandId}
                data-modelId = {record.modelId}
                data-serialNumber = {record.serialNumber}
                data-frequencyId = {record.frequencyId}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            header: 'Equipo',
            dataIndex: 'equipment',
            maxWidth: 60

        },
        {
            key: 3,
            header: 'Serial Number',
            dataIndex: 'serialNumber',
            minWidth: 200
        },
        {
            key: 4,
            header: 'Frecuencia',
            minWidth: 50,
            dataIndex: 'frequency'
        }];

        return columns;
    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('equipmentSingleGridId');
        let rowCheck = table.querySelectorAll('input[name=checkbox]');
        let checked = false;

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;

                let properties = {
                  contractId: self.props.contractId,
                  contractNumber: self.props.contractNumber,
                  equipmentId: rowCheck[i].dataset.equipmentid,
                  systemId: rowCheck[i].dataset.systemid,
                  brandId: rowCheck[i].dataset.brandid,
                  modelId: rowCheck[i].dataset.modelid,
                  serialNumber: rowCheck[i].dataset.serialnumber,
                  frequencyId: rowCheck[i].dataset.frequencyid
                };

                this.saveContractDetail(properties);
            }
        }

        if(!checked)
            alert("Por favor seleccione un registro")

    }

    saveContractDetail(properties)
    {
        let self = this;
        console.log(properties);
        Meteor.call("addContractDetail", properties, function(error, result)
        {
            if(!error)
                self.closeDialogEquipmentSingle();
            else
                throw alert(error);
        });
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.SaveCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogEquipmentSingle
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        return toolbar;
    }

    getGridPanelSingle()
    {
        let data = this.props.data;
        this.records = data;

        return (<GridPanel
            id="equipmentSingleGridId"
            ref="equipmentSingleGrid"
            className="center"
            columns={this.getColumns()}
            records={data}
            toolbar={this.getToolbar()} />);
    }

    getPanelDialog()
    {
        return (<DialogPanel ref="dialog" className="vbox center" title="Equipos" modal>
                {this.getGridPanelSingle()}
        </DialogPanel>);
    }

    getDialog()
    {
        let display = this.state.opened ? 'block' : 'none';

        return (
        <div className="overlay" style={{display: display}}>
            {this.getPanelDialog()}
        </div>);
    }

    openDialogEquipmentSingle()
    {
        this.setState({defaultChecked: false, opened: true});
    }

    closeDialogEquipmentSingle()
    {
        this.setInitializeValue();
        this.setState({opened: false});
    }

    render()
    {
        return (<div>{this.getDialog()}</div>);
    }
}
