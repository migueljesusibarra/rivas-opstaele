import React, { Component } from 'react';
import {GridPanel, Toolbar} from 'react-ui';
import { EquipmentSingleList } from '/imports/ui/views/src/contracts/EquipmentSingleList.jsx';


export class ContractDetail extends Component
{
   constructor(props) {
   	   super(props);
   }

  getColumns()
  {
    let columns = [
    {
      key: 1,
      header: '#',
      minWidth: 30,
      renderer: function(record){
          return  record.pos;
      }
    },
    {
        key: 2,
        header: 'Equipo',
        minWidth: 50,
        dataIndex: 'equipment'
    },
    {
        key: 3,
        header: 'Serie',
        minWidth: 50,
        dataIndex: 'serialNumber'
    },
    {
        key: 4,
        header: 'Frecuencia',
        minWidth: 50,
        dataIndex: 'frequency'
    }];

    return columns;

  }

  getToolbar()
  {
    let self = this;

    let items = [
      {
        type: 'group',
        className: 'hbox-l',
        items: [
          {
            text: 'Agregar',
            cls: 'button-add',
            handler: function(){
              self.refs.equipmentSingle.openDialogEquipmentSingle();
            }
          },
          {
          text: 'Eliminar',
          visible: this.props.status != 'Aprobado',
          cls: 'button-delete',
          handler: function()
          {
             let record = self.refs.contractDetailGrid.getSelectedRecord();

              if(record != null)
              {
                   let confirmation = confirm("Está seguro de que desea eliminar este registro?");

                    if(confirmation == true)
                    {
                       Meteor.call("removeContractDetail", record._id, record.equipmentId, function(error, result){});
                    }
              }
              else
              {
                  alert("Por favor seleccione un contacto.");
              }
          }
      }],
      },
    ];

      let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

      return toolbar;
  }

  getGridPanel()
  {
    let records = this.props.records;

    return <GridPanel
      id="contractDetailGridId"
      title="Contract-Detail"
      ref="contractDetailGrid"
      columns={this.getColumns()}
      records={records}
      toolbar={this.getToolbar()} />;
  }

  render()
  {
  	return (<div>
           <EquipmentSingleList ref="equipmentSingle" contractId={this.props.contractId} contractNumber = {this.props.contractNumber} data = {this.props.equipments}/>
           {this.getGridPanel()}
           </div>)
  }
}
