import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button } from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

  export class ContractList extends Component
  {
    constructor(props)
    {
       super(props);
       this.state = {
        search: null,
        record: null
      };

      this.onSearch = this.onSearch.bind(this);
      this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
      this.onSelectionChange = this.onSelectionChange.bind(this);
    }
    onSelectionChange(record)
    {
      this.setState({record:record});
    }
    onRowDoubleClick(record)
    {
      this.openRecord(record);
    }
    openRecord(record)
    {
      FlowRouter.go("/contract/" + record._id);
    }

    onSearch(event)
    {
        this.setState({search: event.target.value});
    }

    getColumns()
    {
        let columns = [
          {
              key: 1,
              header: 'Id',
              dataIndex: '_id'

          },
          {
              key: 2,
              header: 'Cliente',
              dataIndex: 'customer'

          },
           {
              key: 3,
              header: 'Código',
              dataIndex: 'contractNumber'

          },
          {
              key: 4,
              header: 'Descripción',
              dataIndex: 'description'
          },
          {
              key: 5,
              header: 'F. Inicio',
              align: 'center',
              renderer: function(record){
                  return record.startDate.toLocaleDateString();
              }

          },
          {
              key: 6,
              header: 'F. Finaliza',
              align: 'center',
              renderer: function(record){
                  return record.endingDate.toLocaleDateString();
              }

          },
          {
              key: 7,
              header: 'F. Firma',
              dataIndex: 'signatureDate',
              align: 'center',
              renderer: function(record){
                  return record.signatureDate.toLocaleDateString();
              }

          },
          {
              key: 8,
              header: 'Monto',
              dataIndex: 'amount',
              align: 'right',
              format: 'money'

          },
          {
              key: 9,
              header: 'Estado',
              renderer: function(record){
                  return record.valid == true ? "Vigente" : "Vencido";
              }
          }];

          return columns;
    }

    getToolbarItems()
    {
        let self = this;

        let items = {
                    new: {
                        text: 'Nuevo',
                        cls: 'button-add',
                        handler: function()
                        {
                            FlowRouter.go('/contract/add');
                        }
                    },
                    edit: {
                        text: 'Editar',
                        cls: 'button-edit',
                        handler: function()
                        {
                            let record = self.refs.contractGrid.getSelectedRecord();

                            if(record != null)
                            {
                                FlowRouter.go("/contract/" + record._id);
                            }
                            else
                            {
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                            }
                        }
                   },
                   history: {
                      text: 'Historial',
                      cls: 'button-history',
                      handler: function()
                      {
                        let record = self.refs.contractGrid.getSelectedRecord();

                        if(record != null)
                          self.refs.historyContract.onPenDialog();
                       else
                          self.getToast().showMessage("warning", "Por favor seleccione un registro");
                       }
                  }
              }

              return items;
    }

   getToast()
   {
      return this.refs.toastrMsg;
   }

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.new, actions.edit], rightGroup = [actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

                  let toolbar = (
                  <div>
                      <Toolbar className="north hbox" id="main-toolbar" items={items} />
                      <div className="hbox div-second-tool-bar">
                          <div className="middle-width-quarter-column">
                              <SearchField
                              ref= "search"
                              key={"searchfield"}
                              className = "search-field-single-customer"
                              placeholder=" Buscar"
                              value={this.state.search}
                              onSearch={this.onSearch} />
                          </div>
                          <div className="middle-width-middle-column text-aling-second-tool-bar">
                              <span>{"Lista de Contratos"}</span>
                          </div>
                      </div>
                  </div>);

                  return toolbar;

            return toolbar;
    }

  getRecords()
  {
    let search = this.state.search;

    if(search)
    return this.props.data.filter(c => this.onFilter(c, search));
    else
    return this.props.data;
  }

  onFilter(record, search)
  {
    return (record.contractNumber.search(new RegExp(search, "i")) != -1 ||
    record.customer.search(new RegExp(search, "i")) != -1 ||
    record.description.search(new RegExp(search, "i")) != -1 );
  }

  getGridPanel()
   {
       let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyContract" module="Contract" foreignId={this.state.record._id} title="Historial Contrato" />
   return (<div>
                       <ToastrMessage ref="toastrMsg" />
                       {historyContainer}
                        <GridPanel
                            id="contractGridId"
                            ref="contractGrid"
                            title="Contract"
                            columns={this.getColumns()}
                            records={this.getRecords()}
                            toolbar={this.getToolbar()}
                            onRowDoubleClick={this.onRowDoubleClick}
                            onSelectionChange={this.onSelectionChange} />
                    </div>);
   }

    render()
    {
       return <div>{this.getGridPanel()}</div>;
    }
  }
