import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, Dropdown, Fieldset, FormPanel, FormColumn, Container, TextField, DateField, DisplayField, NumberField, CheckBox } from 'react-ui';
import { Util } from '/lib/common.js';
import { ContractDetailContainer } from '../../../containers/ContractDetailContainer'

export class  ContractForm extends Component
{
    static get defaultProps()
    {
        return {
             ContractStates: [
                {value: "valid", text: "Vigente"},
                {value: "pendingApproval", text: "Pendiente Aprobación"},
                {value: "approvedOffer", text: "Oferta Aprobada"},
                {value: "pendingSigning", text: "Pendiente Firmar"},
                {value: "Signed", text: "Firmado"},
                {value: "defeated", text: "Vencido"}
            ]
        };
    }

  constructor(props)
  {
     super(props);
  }

  getContractStates()
  {
     this.props.ContractStates.sort(Util.applySortObject);

     return this.props.ContractStates.map(function(state)
        {
            return {value: state.value, text: state.text};
        });
  }

  getCustomers()
  {
      this.props.customers.sort(Util.applySortObject);

      return this.props.customers.map(function(customer)
      {
          return {value: customer._id, text: customer.name};
      });
  }

  saveChanges(button)
   {
       let form = this.refs.form;
       let fields = this.refs;

       if(form.isValid(fields))
       {
           let record = {
               customerId: fields.customer.getValue(),
               contractNumber: fields.contractNumber.getValue(),
               description:fields.description.getValue(),
               startDate: fields.startDate.getValue(),
               endingDate: fields.endingDate.getValue(),
               frequencyId: fields.frequency.getValue(),
               signatureDate: fields.signatureDate.getValue(),
               amount: fields.amount.getValue(),
               status: fields.status.getValue()
              };

           if (this.props.data._id == null)
           {
               Meteor.call("addContract", record, function(error, result)
               {
                 if(button == "saveAndClose")
                 {
                     FlowRouter.go('/contracts');
                 }
                 else
                 {
                    alert("Tus datos se han guardado correctamente!");
                    FlowRouter.go('/contract/' + result);
                 }
               });
           }
           else
           {
               alert("Tus datos se han guardado correctamente!");
               //alert("Your data has been saved correctly!!!");
               Meteor.call("updateContract", this.props.data._id, record, function(error, result){
                 if(button == "saveAndClose")
                 {
                     FlowRouter.go('/contracts');
                 }
               });
           }
       }
       else
       {
          alert("Por favor ingrese todos los campos requeridos.");
       }
     }

  getToolbarItems()
  {
        let self = this;

        let items = {
        new:
        {
            text: 'Nuevo',
            cls: 'button-new',
            handler: function()
                {
                FlowRouter.go('/contract/add');
            }
        },
        save:
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: function(){
                self.saveChanges("save");
            }
        },
        close:
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: function(){
                FlowRouter.go('/contracts');
            }
        },
        print:
        {
            text: 'Imprimir',
            cls: 'button-print',
            handler: function()
            {
                console.log('imprimir');
            }
        },
      };

       return items;
  }

  getToolbar()
    {
      let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [], rightGroup = [];

        if(self.props.isNew)
            leftGroup.push(actions.save);
        else
        {
            leftGroup.push(actions.new);
            leftGroup.push(actions.save);
            rightGroup.push(actions.print);
        }

        leftGroup.push(actions.close);

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        return <Toolbar className="north hbox" items={items} />;
    }

  render()
  {
      let data = this.props.data;

      return (<Container className="vbox center">
                <div style={{textAlign: "center", color: "#004990"}}>
                    <span style={{fontSize: "18px"}}>Contrato</span>
                </div>
                {this.getToolbar()}
                <FormPanel ref="form" className="center" style={{"overflow": "auto"}}>
                    <Fieldset className="hbox">
                        <FormColumn bodyCls="hbox" style={{flex: 1, marginTop: "10px"}}>
                            <FormColumn className="hbox-r">
                                <Dropdown ref="customer" className="contract-field" label="Cliente" items={this.getCustomers()} value={data.customerId} required={true} emptyOption={true}/>
                                <TextField ref="contractNumber" className="contract-field" label="Número de Contrato" value={data.contractNumber} required={true} maxLength="50"/>
                                <TextField ref="description" className="contract-field" label="Descripción" value={data.description} required={true} maxLength="50"/>
                            </FormColumn>
                        <FormColumn  style={{flex: "10%"}}>
                            <DateField ref="startDate" className="contract-date" label="Fecha de Inicio" value={data.startDate != null ? Util.getDateISO(data.startDate) : null}/>
                            <DateField ref="endingDate" className="contract-date" label="Fecha Final" value={data.endingDate != null ? Util.getDateISO(data.endingDate) : null}/>
                            <DateField ref="signatureDate" className="contract-date" label="Fecha de Firma" value={data.signatureDate != null ? Util.getDateISO(data.signatureDate) : null}/>
                        </FormColumn>
                        <FormColumn  style={{flex: 1}}>
                            <NumberField ref="amount" className="contract-field-column-three" label="Monto" value ={data.amount}  currency="USD"/>
                            <Dropdown ref="frequency" className="contract-field-column-three" label="Intervalo Facturación" items={Util.getPeriods()} value={data.frequencyId} required={true} emptyOption={true}/>
                            <Dropdown ref="status" className="contract-field-column-three" label="Estado" items={this.getContractStates()} value={data.status === undefined ? 'Conditioned' : data.status} required={true} onChange={this.onChangeStatus}/>
                        </FormColumn>
                    </FormColumn>
                </Fieldset>
                <Fieldset>
                    <ContractDetailContainer contractId={data._id} customerId={data.customerId} contractNumber={data.contractNumber}/>
                </Fieldset>
            </FormPanel>
     </Container>);
  }
}
