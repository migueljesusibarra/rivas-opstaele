import React, { Component } from 'react';
import "/public/amcharts/amcharts.js";
import "/public/amcharts/pie.js";
import "/public/amcharts/plugins/export/libs/pdfmake/pdfmake.min.js";
import "/public/amcharts/plugins/export/libs/fabric.js/fabric.min.js";
import "/public/amcharts/plugins/export/libs/jszip/jszip.min.js";
import "/public/amcharts/plugins/export/export.min.js";
import "/public/amcharts/plugins/export/export.css";
import "/public/amcharts/themes/light.js";

export class PieWithLegend extends Component {
    constructor(props)
    {
        super(props);
    }

    componentDidMount()
    {
        let self = this;

        let chart = AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "titles": [ {
                        "text": "Base Instalada",
                        "size": 16
            } ],
            "startDuration": 0,
            "theme": "light",
            "addClassNames": true,
            "legend":{
   	            "position":"right",
                //"marginRight":100,
                "autoMargins": false,
                 //"equalWidths": false,
                 "horizontalGap": 50,
                 "markerSize": 12,
                 "x":"20%",
                 "y":"25%"
                 //"valueAlign": "left",
                 //"valueWidth": 0
                 //"useGraphSettings": true,
                //"generateFromData": true
            },
            //"innerRadius": 0.5,
            //"labelRadius": -35,
            "angle": 10,
            "depth3D": 10,
            "defs": {
                "filter": [{
                    "id": "shadow",
                    "width": "100%",
                    "height": "100%",
                    "feOffset": {
                        "result": "offOut",
                        "in": "SourceAlpha",
                        "dx": 0,
                        "dy": 0
                    },
                    "feGaussianBlur": {
                        "result": "blurOut",
                        "in": "offOut",
                        "stdDeviation": 5
                    },
                    "feBlend": {
                        "in": "SourceGraphic",
                        "in2": "blurOut",
                        "mode": "normal"
                    }
                }]
            },
            "dataProvider": this.props.dataProviders,
            "valueField": "litres",
            "titleField": "country",
            "export": {
                "enabled": true
            }
        });

        chart.addListener("init", self.handleInit);
        chart.addListener("rollOverSlice", function(e) {
            self.handleRollOver(e);
        });

    }

    handleInit()
    {
       chart.legend.addListener("rollOverItem", handleRollOver);
    }

   handleRollOver(e){
      let wedge = e.dataItem.wedge.node;
      wedge.parentNode.appendChild(wedge);
   }

  render()
    {
       return (<div><div id="chartdiv"></div> </div>);
    }
}
