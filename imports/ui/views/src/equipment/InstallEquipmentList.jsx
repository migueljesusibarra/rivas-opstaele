import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button,
    FormPanel,Fieldset,FormColumn,Dropdown,
    TextField, DateField} from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx'
import { Models } from '../../../../api/models/collection.js';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { EquipmentAdvanceSearch } from '/imports/ui/views/src/equipment/EquipmentAdvanceSearch.jsx';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class InstallEquipmentList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.data,
            filterRecords: props.data,
            advanceSearch: false,
            record: null
        };

        this.onAdvanceSearch = this.onAdvanceSearch.bind(this);
        this.onClearFilter = this.onClearFilter.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    moveReceptionEquipment(record)
    {
        FlowRouter.go('/receptionequipment/'+ record.foreignId);
    }

    movePurchaseOrder(record)
    {
        FlowRouter.go('/purchaseordersystem/'+ record.purchaseOrderSystemId);
    }

    moveTechnician(record)
    {
        FlowRouter.go('/technician/'+ record.technicianId);
    }

    moveCustomer(record)
    {
        FlowRouter.go('/customer/'+ record.customerId);
    }

    getSystems()
    {
        return this.props.systems.map(function(system)
        {
            return {value: system._id, text: system.description};
        });
    }

    getBrands()
    {
        return this.props.brands.map(function(brand)
        {
            return {value: brand._id, text: brand.description};
        });
    }

    getModels(models)
    {
        return models.map(function(model)
        {
            return {value: model._id, text: model.description};
        });
    }

    getTechnicians()
    {
        return this.props.technicians.map(function(technician)
        {
            return {value: technician._id, text: technician.name};
        });
    }

    getPeriods()
    {
        return this.props.periods.map(function(period)
        {
            return {value: period._id, text: period.description};
        });
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Id',
            minWidth: 40,
            dataIndex: '_id'
        },
        {
            key: 2,
            header: 'R. Equipment',
            minWidth: 50,
            renderer: function(record)
            {
                let element = "";

                if(record.isReceptionEquipment)
                    element = (
                    <a
                    href=""
                    onClick={self.moveReceptionEquipment.bind(this, record)}
                    style={{color: 'blue', textDecoration: 'underline'}}>
                    {"R. E  "+record.foreignId}
                    </a>);
                else
                    element = 'No Aplica';

                return element;
            }
        },
        {
            key: 3,
            header: 'P. Order',
            minWidth: 50,
            renderer: function(record)
            {
                let element = "";

                if(record.isPurchaseOrder)
                    element = (
                    <a
                    href=""
                    onClick={self.movePurchaseOrder.bind(this, record)}
                    style={{color: 'blue', textDecoration: 'underline'}}>
                    {"P. O  "+record.purchaseOrderSystemId}
                    </a>);
                else
                    element = 'No Aplica';

                return element;
            }
        },
        {
            key: 4,
            header: 'Cliente',
            minWidth: 50,
            renderer: function(record)
            {

                let element = (
                <a
                    href=""
                    onClick={self.moveCustomer.bind(this, record)}
                    style={{color: 'blue', textDecoration: 'underline'}}>
                    {record.customer}
                </a>);

                return element;
            }
        },
        {
            key: 5,
            header: 'Equipo',
            minWidth: 40,
            dataIndex: 'equipment'
        },
        {
            key: 6,
            header: 'Serie',
            align: "center",
            minWidth: 40,
            dataIndex: 'serialNumber'
        },
        {
            key: 7,
            header: 'F. Instalación',
            minWidth: 50,
            align: "center",
            dataIndex: 'installationDate',
            renderer: function(record)
            {
                return record.installationDate != "" ? Util.getDateISO(record.installationDate): "";
            }
        },
        {
            key: 8,
            header: 'Ingeniero Responsable',
            minWidth: 50,
            dataIndex: 'technician',
            renderer: function(record)
            {
                let isTechnician = (record.technicianId && record.technicianId != "");
                let element = "";

                if(isTechnician)
                    element = (
                    <a
                    href=""
                    onClick={self.moveTechnician.bind(this, record)}
                    style={{color: 'blue', textDecoration: 'underline'}}>
                    {record.technician}
                    </a>);
                else
                    element = 'No Aplica';

                return element;
            }
        }];

        return columns;

    }

    getToolbarItems()
    {
        let self = this;

       let items = {
                    searchAdvance : {
                        text: 'Búsqueda avanzada',
                        cls: 'button-search',
                        handler: function()
                        {
                            self.showAdvanceSearch();
                        }
                    },
                    clear: {
                       text: 'Clear Filter',
                       cls: 'button-clear-filter',
                       handler: function()
                       {
                           self.onClearFilter();
                       }
                   },
                   history: {
                       text: 'Historial',
                       cls: 'button-history',
                       handler: function()
                       {
                           let record = self.refs.equipmentGrid.getSelectedRecord();

                           if(record != null)
                               self.refs.historyEquipment.onPenDialog();
                           else
                               self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                       }
                   }
               }

               return items;
   }

   getToast()
   {
       return this.refs.toastrMsg;
   }

    getToolBar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.searchAdvance, actions.clear], rightGroup = [actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        return (
            <div>
                <Toolbar className="north hbox" id="main-toolbar" items={items} />
                <div className="hbox div-second-tool-bar">
                    <div className="middle-width-quarter-column">
                        <SearchField
                        ref= "search"
                        key={"searchfield"}
                        className = "search-field-single-customer"
                        placeholder=" Buscar"
                        value={this.state.search}
                        onSearch={this.onSearch} />
                    </div>
                    <div className="middle-width-middle-column text-aling-second-tool-bar">
                        <span>{"Base Instalada"}</span>
                    </div>
                </div>
            </div>);
    }

    onSearch(event)
    {
        this.setState({search: event.target.value, advanceSearch: false});
    }

    onClearFilter()
    {
        this.setState({filterRecords: this.state.records});
    }

    onAdvanceSearch(searchValue)
    {
        this.setState({filterRecords: searchValue, advanceSearch: false});
    }

    showAdvanceSearch()
    {
        this.setState({advanceSearch: true});
    }

    getRecords()
    {
        let search = this.state.search;

        if(search)
            return this.state.filterRecords.filter(c => this.onFilter(c, search));
        else
            return this.state.filterRecords;
    }

    getGridPanel()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyEquipment" module="Equipment" foreignId={this.state.record._id} title="Historial Equipo"/>

        return (<div>
                <ToastrMessage ref="toastrMsg" />
                {historyContainer}
                <GridPanel
                    id="equipment-Grid"
                    ref="equipmentGrid"
                    title="Equipment"
                    columns={this.getColumns()}
                    records={this.getRecords()}
                    toolbar={this.getToolBar()}
                    onSelectionChange={this.onSelectionChange} />
              </div>);
    }

    onFilter(record, search)
    {
        return (
            (record.system && record.system.search(new RegExp(search, "i")) != -1) ||
            (record.brand && record.brand.search(new RegExp(search, "i")) != -1) ||
            (record.model && record.model.search(new RegExp(search, "i")) != -1) ||
            (record.serialNumber && record.serialNumber.search(new RegExp(search, "i")) != -1) ||
            (record.technician && record.technician.search(new RegExp(search, "i")) != -1) ||
            (record.foreignId && record.foreignId.search(new RegExp(search, "i")) != -1) ||
            (record.purchaseOrderSystemId && record.purchaseOrderSystemId.search(new RegExp(search, "i")) != -1)
        )
    }

    getEquipmentAdvanceSearch()
    {
        return (
            <EquipmentAdvanceSearch
                opened={this.state.advanceSearch}
                systems={this.props.systems}
                brands={this.props.brands}
                models={this.props.models}
                technicians={this.props.technicians}
                onAdvanceSearch={this.onAdvanceSearch}
                records={this.props.data}
            />);
    }

    render()
    {
        return(
        <div>
        {this.getGridPanel()}
        {this.getEquipmentAdvanceSearch()}
        </div>);
    }
}
