import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { GridPanel,Toolbar, Button,FormPanel,Fieldset,FormColumn,Dropdown, TextField, DateField} from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx'
import { Models } from '../../../../api/models/collection.js';
import { Equipments } from '../../../../api/models/collection.js';
import { SearchField } from '/imports/ui/components/src/SearchField';

export class EquipmentList extends Component
{
  constructor(props)
  {
      super(props);
      this.state = {
          search: null,
          data: this.getDataStateProps(),
          record: null,
          view: false
      };

      this.onSearch = this.onSearch.bind(this);
      this.onChangeBrand = this.onChangeBrand.bind(this);
      this.onSelectionChange = this.onSelectionChange.bind(this);
      this.openDialog = this.openDialog.bind(this);
      this.closeDialog = this.closeDialog.bind(this);
      this.saveChanges = this.saveChanges.bind(this);
      this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
  }

  onRowDoubleClick(record)
  {
    this.openRecord(record);
  }
  openRecord(record)
  {
      if(record != null && record.isReceptionEquipment)
        this.openDialog('view');
      else
        this.openDialog('edit');
  }

  onSelectionChange(record)
  {
      this.setState({record: record});
  }

  getDataStateProps()
  {
    let data = {
      recordId: 0,
      models: this.getModels(this.props.models),
      systemId: "",
      modelId: "",
      brandId: "",
      serialNumber: "",
      frequencyId: "",
      releaseSw: "",
      installationDate: null,
      warranty: "",
      technicianId: "",
      outOfService: "",
      purchaseOrderSystemId: "",
      foreignId: ""
    };

    return data;

  }

  onSearch(event)
  {
    this.setState({search: event.target.value});
  }

  moveReceptionEquipment(record)
  {
     FlowRouter.go('/receptionequipment/'+ record.foreignId);
  }

  movePurchaseOrder(record)
  {
     FlowRouter.go('/purchaseordersystem/'+ record.purchaseOrderSystemId);
  }

  moveContract(record)
  {
      FlowRouter.go('/contract/'+ record.contractId);
  }

  moveTechnician(record)
  {
     FlowRouter.go('/technician/'+ record.technicianId);
  }

  getColumns()
  {
    let self = this;

    let columns = [
    {
        key: 1,
        header: 'Id',
        minWidth: 20,
        dataIndex: '_id'
    },
    {
        key: 2,
        header: 'R. Equipo',
        minWidth: 20,
        renderer: function(record)
        {
          return (record.isReceptionEquipment ? <a href="" onClick={self.moveReceptionEquipment.bind(this, record)} style={{color: 'blue', textDecoration: 'underline'}}>{"R. E  "+record.foreignId}</a> : 'No Aplica');
        }
     },
    //  {
    //     key: 3,
    //     header: 'O. Compra',
    //     minWidth: 20,
    //     renderer: function(record)
    //     {
    //       return (record.isPurchaseOrder ? <a href="" onClick={self.movePurchaseOrder.bind(this, record)} style={{color: 'blue', textDecoration: 'underline'}}>{"O. C  "+record.purchaseOrderSystemId}</a> : 'No Aplica');
    //     }
    //   },
    {
       key: 3,
       header: 'N. Contrato',
       minWidth: 20,
       align: "center",
       dataIndex: 'contractNumber',
       renderer: function(record)
       {
          return (record.inContract ? <a href="" onClick={self.moveContract.bind(this, record)} style={{color: 'blue', textDecoration: 'underline'}}>{record.contractNumber}</a> : 'No Aplica');

       }
     },
      {
        key: 4,
        header: 'Equipo',
        //minWidth: 40,
        maxWidth: 100,
        renderer: function(record)
        {
           return record.system + "  " +  record.brand + "  " + record.model;
        }
      },
      {
        key: 5,
        header: 'Serie',
        minWidth: 40,
        dataIndex: 'serialNumber'
      },
       {
        key: 6,
        header: 'Fecha de Instalación',
        minWidth: 50,
        dataIndex: 'installationDate',
        renderer: function(record)
        {
            return record.installationDate != "" ? Util.getDateISO(record.installationDate): "";
        }
      },
      {
        key: 7,
        header: 'Ingeniero Responsable',
        minWidth: 50,
        dataIndex: 'technician',
        renderer: function(record)
        {
            let isTechnician = (record.technicianId && record.technicianId != "");
            return (isTechnician ? <a href="" onClick={self.moveTechnician.bind(this, record)} style={{color: 'blue', textDecoration: 'underline'}}>{record.technician}</a> : 'No Aplica');
        }

    },
    {
       key: 8,
       header: 'F. Servicio',
       minWidth: 20,
       align: "center",
       dataIndex: 'outOfService',
       renderer: function(record)
       {
         let classColor =  record.outOfService === false ? "check-checkmark-background-green" : "check-checkmark-background-red"
         return(<a><span className="check-checkmark">
            <div className={ "check-checkmark-stem "  + classColor}></div>
            <div className={"check-checkmark-kick " + classColor}></div>
            </span>
            </a>);
       }
   }];

     return columns;

  }

  getToolbar()
  {
      let leftGroup = [];
      let rightGroup = [];
      let self = this;
      let record = this.state.record;

       leftGroup = [{
          text: 'Nuevo',
          cls: 'button-add',
          handler: function()
          {
            self.openDialog('add');
          }
      }];

      if(record != null && record.isReceptionEquipment)
      {
          leftGroup.push(
              {
                text: 'Ver',
                cls: 'button-view',
                handler: function()
                {
                  self.openDialog('view');
                }
            },
          )

      }else
      {
          leftGroup.push({
             text: 'Editar',
             cls: 'button-edit',
             handler: function()
             {
               self.openDialog('edit');
             }
         })
      }
    //
    //   leftGroup.push({
    //       text: (record != null && !record.outOfService) ?  'En Servicio' : 'Fuera de Servicio',
    //       cls: 'button-delete',
    //       handler: function()
    //       {
    //           self.toggleOutOfService();
    //      }
    // });


    let items = [
       {
           type: 'group',
           className: 'hbox-l',
           items: leftGroup
       },
       {
           type: 'group',
           className: 'hbox-r',
           items: rightGroup
       }];

    let toolbar = <div>
      <Toolbar className="north hbox" id="main-toolbar" items={items} />
      <SearchField key={"searchfield"} className = "search-field-customer" placeholder=" Buscar" value={this.state.search} onSearch={this.onSearch} />
    </div>;

    return toolbar;
  }

  getGridPanel()
  {

    return <GridPanel
        id="equipment-Grid"
        ref="equipmentGrid"
        title="Equipment"
        columns={this.getColumns()}
        records={this.getRecords()}
        toolbar={this.getToolbar()}
       onSelectionChange={this.onSelectionChange}
       onRowDoubleClick={this.onRowDoubleClick} />;
  }


  toggleOutOfService()
  {
      if(this.state.record != null)
      {
          Meteor.call("setOutOfService", this.state.record._id, this.state.record.outOfService ? false : true);
      }
  }

  getSystems()
  {
    return this.props.systems.map(function(system)
    {
      return {value: system._id, text: system.description};
    });
  }

  getBrands()
  {
    return this.props.brands.map(function(brand)
    {
      return {value: brand._id, text: brand.description};
    });
  }

  getModels(models)
  {
    return models.map(function(model)
    {
      return {value: model._id, text: model.description};
    });
  }

  getTechnicians()
  {
    return this.props.technicians.map(function(technician)
    {
      return {value: technician._id, text: technician.name};
    });
  }

  onChangeBrand(value)
  {
    let fields = this.refs;
    let models = Models.find({brandId: value}).fetch() || [];
    this.setState({data: this.setUpdateState(fields, this.getModels(models), value)});
  }

  setUpdateState(fields, models, value)
  {
    let data = {
      recordId: fields.recordId.getValue(),
      models: models,
      systemId: fields.system.getValue(),
      modelId: fields.model.getValue(),
      brandId: value,
      serialNumber: fields.serialNumber.getValue(),
      frequencyId: fields.frequency.getValue(),
      releaseSw: fields.releaseSw.getValue(),
      installationDate: fields.installationDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.installationDate.getValue()) : null,
      warranty: fields.warranty.getValue(),
      technicianId: fields.technician.getValue(),
      outOfService: fields.outOfService.getValue()
    };

    return data;

  }

  setStateValue(fields, models, value, isNew)
  {
    let data = {
      recordId: isNew ? "" : fields._id,
      models: isNew ? [] : models,
      systemId: isNew ? "" : fields.systemId,
      modelId: isNew ? "" : fields.modelId,
      brandId: isNew ? "" : value,
      serialNumber: isNew ? "" : fields.serialNumber,
      frequencyId: isNew ? "" : fields.frequencyId,
      releaseSw: isNew ? "" : fields.releaseSw,
      installationDate: isNew ? null : fields.installationDate,
      warranty: isNew ? "" : fields.warranty,
      technicianId: isNew ? "" : fields.technicianId,
      outOfService: isNew ? "" : fields.outOfService,
      purchaseOrderSystemId: isNew ? "" : fields.purchaseOrderSystemId,
      foreignId: isNew ? "" : fields.foreignId,
      isReceptionEquipment: isNew ? "" : fields.isReceptionEquipment,
      isPurchaseOrder: isNew ? "" : fields.isPurchaseOrder,

    };

    return data;
  }

  getDialog()
  {
    let items = [{
        text: 'Guardar',
        cls: 'button-save',
        handler: this.saveChanges
    },{
      key: 'close',
      text: 'Cerrar',
      cls: 'button-close',
      handler: this.closeDialog
    }];

    let toolbar =  <Toolbar className="north hbox" id="main-toolbar" items={items} />;

    return (<Dialog ref="dialogEquipment" className="contact-dialog" key = "item1" title="Nuevo Equipo" modal>
    {toolbar}
    <FormPanel ref="form">
      <Fieldset className="hbox box-form">
        <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
          <HiddenField ref="recordId" value= {this.state.data.recordId} />
          <Dropdown ref="system" label="Sistema" items={this.getSystems()} value = {this.state.data.systemId} required={true} emptyOption={true} disabled={this.state.view}/>
          <Dropdown ref="brand" label="Marca" items={this.getBrands()} value = {this.state.data.brandId} onChange={this.onChangeBrand} required={true} emptyOption={true} disabled={this.state.view} />
          <Dropdown ref="model" label="Modelo" items={this.state.data.models} value = {this.state.data.modelId} required={true} emptyOption={true} disabled={this.state.view} />
          <TextField ref="serialNumber" label="Serie" value={this.state.data.serialNumber}  maxLength="50" required={true} disabled={this.state.view} />
          <Dropdown ref="frequency" label="Frecuencia" items={Util.getPeriods()} value={this.state.data.frequencyId}  required={true} emptyOption={true} disabled={this.state.view}/>
        </FormColumn>
        <FormColumn className="hbox-r">
          <TextField ref="releaseSw" label="Release SW" value={this.state.data.releaseSw} value={this.state.data.releaseSw} maxLength="50" disabled={this.state.view}/>
          <TextField ref="warranty" label="Garantía (Meses)" value={this.state.data.warranty} maxLength="50" disabled={this.state.view} />
          <DateField ref="installationDate" label="Fecha de Instalación" value={this.state.data.installationDate != null ? Util.getDateISO(this.state.data.installationDate) : null} required={true} disabled={this.state.view}/>
          <CheckboxToggle ref="outOfService" lable = "Fuera de Servicio" value = {this.state.data.outOfService} disabled={this.state.view} />
          <Dropdown ref="technician" label="Ingeniero Responsable" items={this.getTechnicians()} value={this.state.data.technicianId} required={true} emptyOption={true} disabled={this.state.view}/>
        </FormColumn>
      </Fieldset>
    </FormPanel>
  </Dialog>);
}

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let record = self.refs.equipmentGrid.getSelectedRecord();

            if(record != null)
            {
                let models = Models.find({brandId: record.brandId}).fetch() || [];
                this.setState({data: this.setStateValue(record, this.getModels(models), record.brandId, false), view: false});

                fields.dialogEquipment.setTitle("Editar Equipo");
                fields.dialogEquipment.open();
           }
           else
           {
               alert("Por favor seleccione un registro.");
           }
     }
     else if(action == 'add')
     {
         this.setState({data: this.setStateValue(null, null, null, true), view: false});
         fields.dialogEquipment.setTitle("Nuevo Equipo");
         fields.dialogEquipment.open();
     }
     else if(action == 'view')
     {
         let record = self.refs.equipmentGrid.getSelectedRecord();

         if(record != null)
         {
             let models = Models.find({brandId: record.brandId}).fetch() || [];
             this.setState({data: this.setStateValue(record, this.getModels(models), record.brandId, false), view: true});

             fields.dialogEquipment.setTitle("Ver Equipo");
             fields.dialogEquipment.open();
        }
        else
        {
            alert("Por favor seleccione un registro.");
        }
      }
  }

    closeDialog()
    {
        let self = this;
        self.refs.dialogEquipment.close();
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {

            let recordId = self.refs.recordId.getValue();


            let data = {
                customerId: self.props.foreignId,
                systemId: fields.system.getValue(),
                modelId: fields.model.getValue(),
                brandId: fields.brand.getValue(),
                serialNumber: fields.serialNumber.getValue(),
                releaseSw: fields.releaseSw.getValue(),
                installationDate: fields.installationDate.getValue(),
                warranty: fields.warranty.getValue(),
                outOfService: fields.outOfService.getValue(),
                technicianId: fields.technician.getValue(),
                frequencyId: fields.frequency.getValue(),
                isReceptionEquipment: this.state.data.isReceptionEquipment,
                isPurchaseOrder: this.state.data.isPurchaseOrder,
                purchaseOrderSystemId: this.state.data.purchaseOrderSystemId,
                foreignId: this.state.data.foreignId,
                contractId: "",
                contractNumber: "",
                inContract: false
        };

        if(recordId.length > 0)
        {
            Meteor.call("updateEquipment", recordId, data, function(error, result){});
        }
        else
        {
            Meteor.call("addEquipment", data, function(error, result){});
        }

        fields.dialogEquipment.close();
        }
        else
        {
            alert("Por favor ingrese todos los campos requeridos.");
        }
    }

    getRecords()
    {
        let search = this.state.search;

        if(search)
            return this.props.data.filter(c => this.onFilter(c, search));
        else
            return this.props.data;
    }

    onFilter(record, search)
    {
        return (record.serialNumber.search(new RegExp(search, "i")) != -1 ||
            record.system.search(new RegExp(search, "i")) != -1 ||
            record.brand.search(new RegExp(search, "i")) != -1 ||
            record.model.search(new RegExp(search, "i")) != -1 ||
            record.technician.search(new RegExp(search, "i")) != -1);
    }

    render()
    {
        return(<div>
            {this.getGridPanel()}
            {this.getDialog()}
        </div>);
    }
}
