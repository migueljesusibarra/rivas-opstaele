import React, { Component } from 'react';
import { DialogPanel, Dropdown, TextField, DateField,
Button } from 'react-ui';
import { Util } from '/lib/common.js';

export class EquipmentAdvanceSearch extends Component
{

    static get defaultProps(){
        return {
            modal: false
        };
    }

    constructor(props)
    {
        super(props);

        this.state = {
            opened: this.props.opened || false,
        };

        let systems = null;
        let brands = null;
        let models = null;
        let technicians = null;

        this.onClose = this.onClose.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.getFilter = this.getFilter.bind(this);
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState(
        {
            opened: nextProps.opened,
            function(){
                this.onSearch()
            }
        });
    }

    objectIsEmpty(obj)
    {
        return Object.keys(obj).length === 0;
    }

    validateString(value)
    {
        let isValid = true;

        if(value.trim().length <= 0)
            isValid = false

        return isValid;
    }

    getSearchValue()
    {

        let searchValue = {
            systemId: this.systems.getValue(),
            brandId: this.brands.getValue(),
            modelId: this.models.getValue(),
            technicianId: this.technicians.getValue()
        };

        return searchValue;
    }

    getFilterParams()
    {

        let objectValues = this.getSearchValue();

        let filters = {};

        for(let keyObjectValue in objectValues)
        {
            if(this.validateString(objectValues[keyObjectValue]))
                filters[keyObjectValue] = objectValues[keyObjectValue];
        }

        return filters;
    }

    getFilter(resourceRecords)
    {
        let records = resourceRecords || [];
        let filterParams = this.getFilterParams();
        let result = [];

        if (this.objectIsEmpty(filterParams))
            result = records;
        else
        {
            for(let keyFilterParams in filterParams)
            {
                for(let i = 0; i < records.length; i++)
                {
                    for(let keyRecord in records[i])
                    {
                        let record = records[i];

                        if(record[keyRecord] != null)
                        {
                            if(keyFilterParams == keyRecord)
                            {
                                if(record[keyRecord].toString() === filterParams[keyFilterParams] )
                                {
                                    //Get total element that exist in result object
                                    let totalResult = result.length;

                                    /*
                                        Ask if there is at least one record.
                                        If there is no record inserted in the array of objects (result)
                                        else, Searches the array of objects and asks if the record has already been inserted.
                                        If the record does not exist in the array is inserted otherwise nothing happens.
                                    */
                                    if(totalResult == 0)
                                    {
                                        result.push(record);
                                    }
                                    else
                                    {
                                        let valueExist = false;

                                        for(let j = 0; j < totalResult; j++)
                                        {
                                            if(result[j]._id == record._id)
                                            {
                                                valueExist = true;
                                            }
                                        }

                                        if(!valueExist)
                                        {
                                            result.push(record);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                records = result;
                result = [];
            }
        }

        return records;
    }

    getFooter()
    {
        let buttonClose = (
        <Button
        className="btn btn-primary"
        id={"dialog-close"}
        key={"dialog-close"}
        text={"Cerrar"}
        onClick={this.onClose.bind(this)} />
        );

        let buttonSearch = (
        <Button
        className="btn btn-danger"
        id={"dialog-search"}
        key={"dialog-search"}
        text={"Buscar"}
        onClick={this.onSearch.bind(this)} />
        );

        return (<div
        className="dialog-footer align-right">
        {buttonClose}
        {buttonSearch}
        </div>);
    }

    onSearch()
    {
        if(this.props.onAdvanceSearch)
            this.props.onAdvanceSearch(this.getFilter(this.props.records));
    }

    onClose(event)
    {
        this.close();
    }

    open()
    {
        this.setState({opened: true});
    }

    close()
    {
        this.setState({opened: false});
    }

    getSystem()
    {
        return this.props.systems.map(function(system)
        {
            return {value: system._id, text: system.description};
        });
    }

    getBrands()
    {
        return this.props.brands.map(function(grand)
        {
            return {value: grand._id, text: grand.description};
        });
    }

    getModels()
    {
        return this.props.models.map(function(model)
        {
            return {value: model._id, text: model.description};
        });
    }

    getTechnicians()
    {
        return this.props.technicians.map(function(technician)
        {
            return {value: technician._id, text: technician.name};
        });
    }

    render()
    {
        let display = this.state.opened ? 'block' : 'none';

        return(
            <div className="overlay" style={{display: display}}>
                <DialogPanel
                title="Búsqueda avanzada de equipos instalados"
                footer={this.getFooter()}
                className={this.props.className} style={{display: display}}>
                <Dropdown
                    ref={(systems) => {this.systems = systems;}}
                    label="Sistema"
                    items={this.getSystem()}
                    emptyOption={true}/>
                <Dropdown
                    ref={(brands) => {this.brands = brands;}}
                    label="Marca"
                    items={this.getBrands()}
                    emptyOption={true}/>
                <Dropdown
                    ref={(models) => {this.models = models;}}
                    label="Modelo"
                    items={this.getModels()}
                    emptyOption={true}/>
                <Dropdown
                    ref={(technicians) => {this.technicians = technicians;}}
                    label="Ing. Responsable"
                    items={this.getTechnicians()}
                    emptyOption={true}/>
                </DialogPanel>
            </div>
        );
    }
}
