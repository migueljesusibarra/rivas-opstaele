import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, Dropdown, Fieldset, FormPanel, FormColumn, TextField, DateField } from 'react-ui';
import { Equipments } from '../../../../api/equipments/collection';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { Util } from '/lib/common.js';
import { Models } from '../../../../api/models/collection.js';

export class EquipmentForm extends Component
{
  constructor(props)
  {
     super(props);
     this.state =
     {
       models: this.getModels(this.props.models),
       brand: this.props.data.brand
     }
      this.onChangeBrand = this.onChangeBrand.bind(this);
      this.saveChanges = this.saveChanges.bind(this);
  }

  saveChanges(button)
   {
       let form = this.refs.form;
       let fields = this.refs;
       if(form.isValid(fields))
       {
           let record = {
               customer: fields.customer.getValue(),
               system: fields.system.getValue(),
               model: fields.model.getValue(),
               brand:fields.brand.getValue(),
               serialNumber: fields.series.getValue(),
               location: fields.location.getValue(),
               releaseSw: fields.releaseSw.getValue(),
               openingDate: fields.openingDate.getValue(),
               instalationDate: fields.instalationDate.getValue(),
               warranty: fields.warranty.getValue(),
               outOfService: fields.outOfService.getValue(),
               technician: fields.technician.getValue(),
               frequency:fields.frequency.getValue(),
               source: "",
               foreignId: "", 
               isReceptionEquipment: false
           };

           if (this.props.data._id == null)
           {
               Meteor.call("addEquipment", record, function(error, result)
               {

                 if(button == "saveAndClose")
                 {
                     FlowRouter.go('/equipments');
                 }
                 else
                 {
                   fields.toastrMsg.showMessage("succes", "Tus datos se han guardado correctamente!");
                   FlowRouter.go('/equipment/' + result);
                 }
               });

           }
           else
           {
               fields.toastrMsg.showMessage("succes", "Tus datos se han guardado correctamente!");
               //alert("Your data has been saved correctly!!!");
               Meteor.call("updateEquipment", this.props.data._id, record, function(error, result){
                 if(button == "saveAndClose")
                 {
                     FlowRouter.go('/equipments');
                 }
               });
           }
       }
       else
       {
          fields.toastrMsg.showMessage("warning", "Por favor ingrese todos los campos requeridos.");
       }
   }

  getPeriods()
   {
       return this.props.periods.map(function(period)
       {
           return {value: period._id, text: period.description};
       });
   }

   getToolbar()
     {
         let self = this;

         let items = [
         {
             text: '',
             cls: 'button-back',
             handler: function(){
                 FlowRouter.go('/equipments');
             }
         },
         {
             text: 'Nuevo',
             cls: 'button-new-dealer',
             handler: function(){
                 FlowRouter.go('/equipment/add');
             }
         },
         {
             text: 'Guardar',
             cls: 'button-save',
             handler: function()
             {
                 self.saveChanges("save")
             }
         },
         {
             text: 'Guardar y Cerrar',
             cls: 'button-save-close',
             handler: function()
             {
                 self.saveChanges("saveAndClose")
              }
         },
         {
             text: 'Imprimir',
             cls: 'button-print',
             handler: function()
             {
                console.log('Esta es una prueba');
              }
         }];

         return <Toolbar className="north hbox" items={items} />;
     }

     getCustomers()
     {
         return this.props.customers.map(function(customer)
         {
             return {value: customer._id, text: customer.name};
         });
     }

     getSystems()
     {
         return this.props.systems.map(function(system)
         {
             return {value: system._id, text: system.description};
         });
     }

     getBrands()
     {
         return this.props.brands.map(function(brand)
         {
             return {value: brand._id, text: brand.description};
         });
     }

     getLocations()
     {
         return this.props.locations.map(function(location)
         {
             return {value: location._id, text: location.description};
         });
     }

     getTechnicians()
     {
         return this.props.technicians.map(function(technician)
         {
             return {value: technician._id, text: technician.name};
         });
     }

     getModels(models)
     {
        return models.map(function(model)
       {
           return {value: model._id, text: model.description};
       });
     }

     onChangeBrand(value)
     {
       let models = Models.find({brand: value}).fetch() || [];
       this.setState({models: this.getModels(models), brand: value});
     }

   render()
   {
      var data = this.props.data || {};
      let models = this.state.models;
      let brand = this.state.brand;

      return(<div>
                {this.getToolbar()}
                <FormPanel ref="form">
                     <ToastrMessage ref="toastrMsg" />
                    <Fieldset className="hbox box-form">
                        <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                            <Dropdown ref="customer" label="Cliente" items={this.getCustomers()} value={data.customer}  required={true} emptyOption={true}/>
                            <Dropdown ref="system" label="Sistema" items={this.getSystems()} value={data.system} required={true} emptyOption={true}/>
                            <Dropdown ref="brand" label="Marca" items={this.getBrands()} value={brand} onChange={this.onChangeBrand} required={true} emptyOption={true}/>
                            <Dropdown ref="model" label="Modelo" items={models} value={models} required={true} emptyOption={true}/>
                            <TextField ref="series" label="Serie" value={data.serialNumber} required={true} maxLength="50" required={true} />
                            <Dropdown ref="location" label="Ubicación" items={this.getLocations()} value={data.location} required={true} emptyOption={true}/>
                            <Dropdown ref="frequency" label="Frecuencia" value={data.frequency} items={this.getPeriods()} emptyOption={true}/>
                        </FormColumn>
                        <FormColumn className="hbox-r">
                            <TextField ref="releaseSw" label="Release SW" value={data.releaseSw} maxLength="50"/>
                            <DateField ref="openingDate" label="Fecha de Apertura" value={data.openingDate != null ? Util.getDateISO(data.openingDate) : null}/>
                            <DateField ref="instalationDate" label="Fecha de Instalación" value={data.instalationDate != null ? Util.getDateISO(data.instalationDate) : null}/>
                            <TextField ref="warranty" label="Garantía" value={data.warranty} maxLength="50"/>
                            <CheckboxToggle ref="outOfService" lable = "Fuera de Servicio" value = {data.outOfService} />
                            <Dropdown ref="technician" label="Ingeniero Responsable" items={this.getTechnicians()} value={data.technician} required={true} emptyOption={true}/>
                            </FormColumn>
                    </Fieldset>
                </FormPanel>
            </div>);
   }
}
