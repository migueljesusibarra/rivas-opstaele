import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { EditorGridPanel } from '/imports/ui/components/src/EditorGridPanel';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import { Toolbar, FormPanel } from 'react-ui';
import { Util } from '/lib/common.js';

export class Location extends Component
{
    constructor(props)
    {
        super(props);
        this.onEnter = this.onEnter.bind(this);
        this.onChange = this.onChange.bind(this);
        this.showEditor = this.showEditor.bind(this);
    }
    onEnter(event)
    {
        let target = event.target;
        let value = target.value;

        if(!Util.isEmpty(event.target.value))
         {
            Meteor.call("addLocation", value, function(error, result){});
         }
    }
    onChange(event)
    {
        let target = event.target;
        let recordId = target.dataset.id;

        if(!Util.isEmpty(event.target.value))
         {
             Meteor.call("updateLocation", recordId, target.value, function(error, result){});
         }
    }
    showEditor()
    {
        this.refs.locationGrid.showEditor();
    }
    getGridPanel()
    {
        let self = this;

        let columns = [{
            header: "Descripción",
            dataIndex: "name",
            renderer: function(record)
            {
                return <InlineEditor data-id={record._id} clicksToEdit="2" text={record.description} value={record.description} onChanged={self.onChange} placeholder="Please enter a text and press Enter"/>;
            }
        }];

        let records = this.props.records;

        let items = [{
            text: 'Nuevo',
            cls: 'button-add',
            handler: this.showEditor
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                let grid = ReactDOM.findDOMNode(self.refs.locationGrid);
                let row = grid.querySelector("tr.x-selected");

                if(row != null)
                {
                    let record = self.props.records[row.dataset.index];

                    let confirmation = confirm("Está seguro de que desea eliminar este registro?");
                      if( confirmation == true)
                      {
                        Meteor.call("removeLocation", record._id, function(error, result){
                            if(error)
                                alert(error.details);
                        });
                      }
                }
                else
                    alert('Por favor seleccione un registro.');
            }
        }];

        let toolbar = <Toolbar items={items}/>;
        return <EditorGridPanel
                    ref="locationGrid"
                    title="Location"
                    toolbar={toolbar}
                    columns={columns}
                    records={records}
                    onEnter={this.onEnter}
               />;
    }
    render()
    {
        return (<div>
                    {this.getGridPanel()}
                </div>);
    }
}
