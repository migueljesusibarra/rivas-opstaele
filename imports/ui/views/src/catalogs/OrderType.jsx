import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel,
    PhoneField, NoteField, NumberField } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';

export class OrderType extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.data
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    removeOrderType()
    {
        let record = this.refs.orderTypeGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar tipo de contrato?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un tipo de contrato.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.orderTypeGrid.getSelectedRecord();

        Meteor.call("removeOrderType", record._id, function(error, result){});
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Id',
            dataIndex: '_id'
        },
        {
            key: 2,
            header: 'Descripción',
            renderer: function(record)
            {
                return record.description;
            }
        }];

        //let data = this.props.data;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function(){
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function(){
                self.openDialog('edit');
            }
        }
        // {
        //     text: 'Eliminar',
        //     cls: 'button-delete',
        //     handler: function()
        //     {
        //         self.removeOrderType();
        //     }
        // }
    ];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Tipos de orden"}</span>
                </div>
            </div>
        </div>);

        return  <GridPanel
            id="orderTypeGrid"
            ref="orderTypeGrid"
            title="Tipo de Order"
            columns={columns}
            records={this.getRecords()}
            toolbar={toolbar} />
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.data.filter(c => this.onFilter(c, search));
      else
        return this.props.data;
    }

    onFilter(record, search)
    {
      return (record.description.search(new RegExp(search, "i")) != -1 );
    }

    getDialog()
    {
        let data = {};
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (<Dialog ref="dialog" className="contact-dialog" modal>
                    {toolbar}
                    <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="description" label="Descripción" required/>
                       </FormPanel>
                </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.data[row.dataset.index];

                fields.record.setValue(record._id);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar tipo de Order");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.description.setValue('');
                fields.dialog.setTitle("Nuevo tipo de Order");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                description: fields.description.getValue(),
            }

            if(recordId.length > 0)
                Meteor.call("updateOrderType", recordId, data, function(error, result){});
            else
                Meteor.call("addOrderType", data, function(error, result){});

             fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos..");
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>)
    }
}
