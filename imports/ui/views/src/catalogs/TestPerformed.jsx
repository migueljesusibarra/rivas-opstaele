import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, Dropdown, TextField,
    EmailField, PhoneField,NoteField, FormPanel,
    Fieldset, FormColumn, NumberField } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { RadioButton } from '/imports/ui/components/src/RadioButton.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';

export class TestPerformed extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.records
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    removeTestPerformed()
    {
        let record = this.refs.testPerformedGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar una prueba de equipo?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una prueba de equipo.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.testPerformedGrid.getSelectedRecord();

        Meteor.call("removeTestPerformed", record._id, function(error, result){});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit');
    }

    getSystems()
    {
        this.props.systems.sort(Util.applySortObject);

        return this.props.systems.map(function(system)
        {
            return { value: system._id, text: system.description};
        });
    }

    getModels()
    {
        this.props.models.sort(Util.applySortObject);

        return this.props.models.map(function(model)
        {
            return { value: model._id, text: model.description};
        });
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                description: fields.description.getValue(),
                systemId: fields.system.getValue(),
                modelId: fields.model.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateTestPerformed", recordId, data, function(error, result){});
            else
                Meteor.call("addTestPerformed", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Sistema',
            dataIndex: 'system'
        },
        {
            key: 2,
            header: 'Modelo',
            dataIndex: 'model'
        },
        {
            key: 3,
            header: 'Prueba',
            dataIndex: 'description'
        }];

        //let data = this.props.records;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function()
            {
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                self.openDialog('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeTestPerformed();
            }
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Pruebas de Equipos"}</span>
                </div>
            </div>
        </div>);

        return  (<GridPanel
            id="testPerformedGrid"
            ref="testPerformedGrid"
            title="Test Performed"
            columns={columns}
            records={this.getRecords()}
            toolbar={toolbar}
            onRowDoubleClick={this.onRowDoubleClick} />);
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
      else
        return this.props.records;
    }

    onFilter(record, search)
    {
      return (record.description.search(new RegExp(search, "i")) != -1 ||
              record.system.search(new RegExp(search, "i")) != -1 ||
              record.model.search(new RegExp(search, "i")) != -1);
    }

    getDialog()
    {
        let data = {};
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

            let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Nuevo Configuración" modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <Dropdown ref="system" label="Sistema" items={this.getSystems()}  emptyOption={true} required={true}/>
                        <Dropdown ref="model" label="Model" items={this.getModels()}  emptyOption={true} required={true}/>
                        <TextField ref="description" label="Prueba" required />
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.records[row.dataset.index];
                fields.record.setValue(record._id);
                fields.system.setValue(record.systemId);
                fields.model.setValue(record.modelId);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar Prueba");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.description.setValue('');
                fields.system.setValue('');
                fields.model.setValue('');
                fields.dialog.setTitle("Nueva Prueba");
                fields.dialog.open();
            }
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {

        return (<div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>);
    }
}
