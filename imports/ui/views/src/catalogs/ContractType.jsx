import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel,
    PhoneField, NoteField, NumberField } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class ContractType extends Component
{
    constructor(props)
    {
        super(props);
        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    removeContractType()
    {
        let record = this.refs.contractTypeGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar tipo de contrato?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un tipo de contrato.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.contractTypeGrid.getSelectedRecord();

        Meteor.call("removeContractType", record._id, function(error, result){});
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Descripción',
            renderer: function(record)
            {
                return record.description;
            }
        },
        {
            key: 2,
            header: 'Costo x Parte',
            renderer: function(record){
                return ("C$ " + record.costPart);
            }
        },
        {
            key: 3,
            header: 'Costo x Servicio',
            renderer: function(record){
                return ("C$ " + record.costService);
            }
        },
        {
            key: 4,
            header: 'Costo x Hora',
            renderer: function(record){
                return ("C$ " + record.costHour);
            }
        }];

        let data = this.props.data;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function(){
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function(){
                self.openDialog('edit');
            }
        },
        {
            text: 'Delete',
            cls: 'button-delete',
            handler: function()
            {
                self.removeContractType();
            }
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return  <GridPanel
            id="contractTypeGrid"
            ref="contractTypeGrid"
            title="Tipo de Contrato"
            columns={columns}
            records={data}
            toolbar={toolbar} />
    }
    getDialog()
    {
        let data = {};
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (<Dialog ref="dialog" className="contact-dialog" modal>
                    {toolbar}
                    <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="description" label="Descripción" required/>
                        <NumberField ref="costPart" label="Costo x Parte"  currency="USD" required/>
                        <NumberField ref="costService"  label="Costo x Servicio"  currency="USD" required />
                        <NumberField ref="costHour" label="Costo x Hora"   currency="USD" required/>
                     </FormPanel>
                </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.data[row.dataset.index];

                fields.record.setValue(record._id);
                fields.description.setValue(record.description);
                fields.costPart.setValue(record.costPart);
                fields.costService.setValue(record.costService);
                fields.costHour.setValue(record.costHour);
                fields.dialog.setTitle("Editar el Tipo de Contrato");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.description.setValue('');
                fields.costPart.setValue('');
                fields.costService.setValue('');
                fields.costHour.setValue('');
                fields.dialog.setTitle("Nuevo Tipo de Contrato");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
            description: fields.description.getValue(),
            costPart: fields.costPart.getValue(),
            costService: fields.costService.getValue(),
            costHour: fields.costHour.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateContractType", recordId, data, function(error, result){});
            else
                Meteor.call("addContractType", data, function(error, result){});

             fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos..");
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>)
    }
}
