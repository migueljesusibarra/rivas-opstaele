import React, { Component } from 'react';
import { GridPanel, Toolbar, FormPanel, Fieldset,
    FormColumn, DateField, Dropdown} from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { ReceptionEquipments } from '/imports/api/receptionEquipments/collection.js';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class PreOrderList extends Component {

    constructor(props)
    {
        super(props);

        this.state = {
          search: null,
          record: null
        };

        this.openDialogPreOrder = this.openDialogPreOrder.bind(this);
        this.closeDialogPreOrder = this.closeDialogPreOrder.bind(this);
        this.updatePreOrder = this.updatePreOrder.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    onSearch(event)
    {
        this.setState({search: event.target.value});
    }

    getEquipmentData(id)
    {
        let data = ReceptionEquipments.findOne({_id: id});
        return data == null ? {} : data;
    }

    openDialogPreOrder()
    {
        let self = this;
        let fields = self.refs;
        let record = self.refs.preOrdenGrid.getSelectedRecord();

        if(record != null)
        {
            fields.record.setValue(record._id);
            fields.date.setValue(record.date != null ? Util.getDateISO(record.date) : null);
            fields.technician.setValue(record.technicianId);
            fields.dialog.setTitle("Editar Visita");
            fields.dialog.open();
        }
        else
            self.getToast().showMessage("warning", "Por favor seleccione un registro");
    }

    updatePreOrder()
     {
         let self = this;
         let recordId = this.refs.record.getValue();
         let properties = {
             date: self.refs.date.getValue(),
             technicianId: self.refs.technician.getValue()
         }

       Meteor.call("updatePreOrder", recordId, properties, function(error, result)
       {
           if(!error)
              self.getToast().showMessage("succes", "La fecha fue Actualizada");
           else
              self.getToast().showMessage("error", error);
       });

       this.closeDialogPreOrder();
     }

    closeDialogPreOrder()
    {
      this.refs.dialog.close();
    }

    getTechnicians()
    {
      this.props.technicians.sort(Util.applySortObject);

      return this.props.technicians.map(function(technician)
      {
        return {value: technician._id, text: technician.name};
      });
    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.updatePreOrder
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogPreOrder
        }];

        let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (
        <Dialog ref="dialog"  modal>
        {toolbar}
        <FormPanel ref="form">
            <Fieldset className="hbox box-form">
                <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                <HiddenField ref="record" value="" />
                <DateField ref="date" label="Fecha" />
                <Dropdown ref="technician" label="Ing. Responsable" items={this.getTechnicians()}  required={true} emptyOption={true}/>
                </FormColumn>
            </Fieldset>
        </FormPanel>
        </Dialog>);
    }

    moveTechnician(record)
    {
       FlowRouter.go('/technician/'+ record.technicianId);
    }

    moveCustomer(record)
    {
       FlowRouter.go('/customer/'+ record.customerId);
    }

    moveReceptionEquipment(record)
    {
       FlowRouter.go('/receptionequipment/'+ record.receptionEquipmentId);
    }

    getToolbarItems()
    {
        let self = this;

        let items = {
                    edit: {
                        text: 'Editar',
                        cls: 'button-edit',
                        handler: function()
                        {
                            self.openDialogPreOrder();
                        }
                    },
                    generateOrder: {
                        text: 'Generar Ordenes',
                        cls: 'button-export',
                        handler: function()
                        {
                            self.generateOrders();
                        }
                    },
                    history: {
                       text: 'Historial',
                       cls: 'button-history',
                       handler: function()
                       {
                         let record = self.refs.preOrdenGrid.getSelectedRecord();

                         if(record != null)
                           self.refs.historyPreOrder.onPenDialog();
                        else
                           self.getToast().showMessage("warning", "Por favor seleccione un registro");
                        }
                   }
                }

                return items;
    }

    getToast()
    {
        return this.refs.toastrMsg;
    }

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.edit, actions.generateOrder], rightGroup = [actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

      let toolbar = (
      <div>
          <Toolbar className="north hbox" id="main-toolbar" items={items} />
          <div className="hbox div-second-tool-bar">
              <div className="middle-width-quarter-column">
                  <SearchField
                  ref= "search"
                  key={"searchfield"}
                  className = "search-field-single-customer"
                  placeholder=" Buscar"
                  value={this.state.search}
                  onSearch={this.onSearch} />
              </div>
              <div className="middle-width-middle-column text-aling-second-tool-bar">
                  <span>{"List de Pre-Orden"}</span>
              </div>
          </div>
      </div>);

      return  toolbar;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: '#',
            minWidth: 30,
            renderer: function(record)
            {
                return  record.pos;
            }
        },
        {
            key: 2,
            header: 'Visita',
            minWidth: 50,
            renderer: function(record)
            {
                return record.date != "" ? Util.spanishDate(record.date): "";
            }
        },
        {
            key: 3,
            header: 'Cliente',
            minWidth: 50,
            renderer: function(record)
            {
                return (
                <a
                href=""
                onClick={self.moveCustomer.bind(this, record)}
                style={{color: 'blue', textDecoration: 'underline'}}>
                {record.customer}
                </a> );
            }
        },
        {
            key: 4,
            header: 'Ing. Responsable',
            minWidth: 50,
            renderer: function(record)
            {
                return (
                <a
                href=""
                onClick={self.moveTechnician.bind(this, record)}
                style={{color: 'blue', textDecoration: 'underline'}}>
                {record.technician}
                </a> );
            }
        },
        {
            key: 5,
            header: 'R. Equipo',
            minWidth: 50,
            renderer: function(record)
            {
                return (
                <a
                href=""
                onClick={self.moveReceptionEquipment.bind(this, record)}
                style={{color: 'blue', textDecoration: 'underline'}}>
                {"R. E  "+record.receptionEquipmentId}
                </a>
                );
            }
        },
        {
            key: 6,
            header: 'Modelo',
            minWidth: 50,
            renderer: function(record)
            {
                return record.model;
            }
        },
        {
            key: 7,
            header: 'No. Serie',
            minWidth: 50,
            renderer: function(record)
            {
                return record.serialNumber;
            }
        }];

        return columns;
    }

    getRecords()
    {
        let search = this.state.search;

        if(search)
            return this.props.records.filter(c => this.onFilter(c, search));
        else
            return this.props.records;
    }

    onFilter(record, search)
    {
      return (record.customer.search(new RegExp(search, "i")) != -1 ||
      record.technician.search(new RegExp(search, "i")) != -1 ||
      record.serialNumber.search(new RegExp(search, "i")) != -1 ||
      record.model.search(new RegExp(search, "i")) != -1);
    }

    getGridPanel()
    {
        let data = this.props.records;
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyPreOrder" module="PreOrder" foreignId={this.state.record._id} title="Historial Pre-Orden" />

        return  (<div>
                        <ToastrMessage ref="toastrMsg" />
                        {historyContainer}
                        <GridPanel
                            id="preOrdenGridId"
                            ref="preOrdenGrid"
                            title="Pre-Orden"
                            toolbar={this.getToolbar()}
                            columns={this.getColumns()}
                            records={this.getRecords()}
                            onSelectionChange={this.onSelectionChange} />
                    </div>);
    }

   getPropertiesOrder(data)
   {
       let record = {
               customerId: data.customerId,
               systemId: data.systemId,
               brandId: data.brandId,
               modelId: data.modelId,
               serialNumber: data.serialNumber,
               releaseSw: data.releaseSw,
               installationDate: data.installationDate,
               warranty: data.warranty,
               warrantyExpires: data.warrantyExpires,
               technicianId: data.technicianId,
               orderTypeId: "2",
               source: "PreOrden",
               foreignId: data._id,
               note: "",
               beginDate: data.date,
               endDate: ""
           };

        return record;
   }

  generateOrders()
  {
        let self = this;
        let collection = this.props.records;
        let date = new Date();

          for(var i=0; i < collection.length; i++)
          {
              let data = collection[i];

              if(data.date < new Date())
              {
                  let properties = self.getPropertiesOrder(data);

                  Meteor.call("addOrderFromPreOrder", properties, function(error, result)
                  {
                    if(!error)
                      self.getToast().showMessage("succes", "Las ordenes se han generado correctamente!");
            		else
            		  self.getToast().showMessage("error", error);
                  });

             }
             else
              {
                  self.getToast().showMessage("info", "No hay Pre-ordenes para generar");
              }
          }
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
        </div>
        )
    }
}
