import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import { Toolbar, GridPanel, TextField, FormPanel, Dropdown} from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class Model extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    removeModel()
    {
        let record = this.refs.modelGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar el modelo?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un modelo.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.modelGrid.getSelectedRecord();

        Meteor.call("removeModel", record._id, function(error, result){});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit', record);
    }

    getColumns()
    {
        let columns = [
        {
            key: 1,
            header: 'Id',
            minWidth: 50,
            dataIndex: 'index'
        },
        {
            key: 2,
            header: 'Sistema',
            minWidth: 50,
            dataIndex: 'system'
        },
        {
            key: 3,
            header: 'Marca',
            minWidth: 50,
            dataIndex: 'brand'
        },
        {
            key: 4,
            header: 'Artículo',
            minWidth: 50,
            dataIndex: 'article'
        },
        {
            key: 5,
            header: 'Modelo',
            minWidth: 50,
            dataIndex: 'description'
        }];

        return columns;
    }

    getToolbarItems()
    {
       let self = this;

       let items = {
                   new: {
                       text: 'Nuevo',
                       cls: 'button-new-dealer',
                       handler: function()
                       {
                           self.openDialog('add');
                       }
                   },
                   edit: {
                       text: 'Editar',
                       cls: 'button-edit',
                       handler: function()
                       {
                           let record = self.refs.modelGrid.getSelectedRecord();

                           if(record != null)
                                self.openDialog('edit', record);
                          else
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                     }
                 },
                 remove: {
                     text: 'Eliminar',
                     cls: 'button-delete',
                     handler: function()
                     {
                         self.removeModel();
                     }
                 },
                 history: {
                     text: 'Historial',
                     cls: 'button-history',
                     handler: function()
                     {
                         let record = self.refs.modelGrid.getSelectedRecord();

                         if(record != null)
                             self.refs.historyModel.onPenDialog();
                         else
                             self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                     }
                 }
             }
             return items;
    }

    getToast()
	{
		return this.refs.toastrMsg;
	}

    getToolbar()
    {
        let self = this;
		let actions = self.getToolbarItems();
		leftGroup = [actions.new, actions.edit], rightGroup = [actions.history];

        let items = [{
			type: 'group',
			className: 'hbox-l',
			items: leftGroup
		},
		{
			type: 'group',
			className: 'hbox-r',
			items: rightGroup
		}];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Lista de Modelos"}</span>
                </div>
            </div>
        </div>);

        return toolbar;
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
      else
        return this.props.records;
    }

    onFilter(record, search)
    {
      return (record.article.search(new RegExp(search, "i")) != -1 ||
              record.system.search(new RegExp(search, "i")) != -1 ||
              record.brand.search(new RegExp(search, "i")) != -1 ||
              record.description.search(new RegExp(search, "i")) != -1);
    }

    getGridPanel()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyModel" module="Model" foreignId={this.state.record._id} title="Historial Model"/>

        return (<div>
                    <ToastrMessage ref="toastrMsg" />
                    {historyContainer}
                    <GridPanel
                        id="modelGrid"
                        title="Model"
                        ref="modelGrid"
                        columns={this.getColumns()}
                        records={this.getRecords()}
                        toolbar={this.getToolbar()}
                        onRowDoubleClick={this.onRowDoubleClick}
                        onSelectionChange={this.onSelectionChange} />
                </div>);
    }

    getSystems()
    {
        this.props.systems.sort(Util.applySortObject);

        return this.props.systems.map(function(system)
        {
            return { value: system._id, text: system.description};
        });
    }

    getModels()
    {
        this.props.brands.sort(Util.applySortObject);

        return this.props.brands.map(function(brand)
        {
            return { value: brand._id, text: brand.description};
        });
    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Tipo de cliente" modal>
            {toolbar}
            <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                <HiddenField ref="record" value="" />
                <Dropdown ref="system" label="Sistema" items={this.getSystems()}  emptyOption={true} required={true}/>
                <Dropdown ref="brand" label="Marca" items={this.getModels()}  emptyOption={true} required={true}/>
                <TextField ref="article" label="Articulo" required/>
                <TextField ref="description" label="Modelo" required/>
            </FormPanel>
        </Dialog>
        );
    }

    openDialog(action, record)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.system.setValue(record.systemId);
                fields.brand.setValue(record.brandId);
                fields.article.setValue(record.article);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar Modelo");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.system.setValue('');
                fields.brand.setValue('');
                fields.article.setValue('');
                fields.description.setValue('');
                fields.dialog.setTitle("Nuevo Modelo");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();
            let data = {
            description: fields.description.getValue(),
            systemId: fields.system.getValue(),
            brandId: fields.brand.getValue(),
            article: fields.article.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateModel", recordId, data, function(error, result){});
            else
                Meteor.call("addModel", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>);
    }
}
