import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel, Fieldset, FormColumn,
    NumberField } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { PreviousConfiguration } from '/imports/ui/views/src/receptionEquipment/PreviousConfiguration.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class Configuration extends Component
{
    constructor(props)
    {
        super(props);

        this.openDialogConfiguration = this.openDialogConfiguration.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialogConfiguration = this.closeDialogConfiguration.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    removeConfiguration()
    {
        let record = this.refs.configurationGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar configuración?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una configuración.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.configurationGrid.getSelectedRecord();
        Meteor.call("removeConfiguration", record._id, function(error, result){});
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                code: fields.code.getValue(),
                quantity: fields.quantity.getValue(),
                description: fields.description.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updateConfiguration", recordId, data, function(error, result){});
            else
                Meteor.call("addConfiguration", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: '#',
            //align:'center',
            //maxWidth: 10,
            renderer: function(record)
            {
                return  record.pos;
            }
        },
        {
            key: 2,
            header: 'Cantidad',
            align:'center',
            //maxWidth: 10,
            renderer: function(record)
            {
                return  record.quantity;
            }
        },
        {
            key: 3,
            header: 'Código',
            align:'center',
            //maxWidth: 20,
            renderer: function(record)
            {
                return record.code;
            }
        },
        {
            key: 4,
            header: 'Descripción',
            //minWidth: 120,
            renderer: function(record)
            {
                return record.description;
            }
        }];

        let data = this.props.records;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function()
            {
                self.openDialogConfiguration('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function()
            {
                self.openDialogConfiguration('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeConfiguration();
            }
        },
        {
            text: 'Config. Anteriores ',
            cls: 'button-previous',
            handler: function()
            {
                self.refs.previousConfiguration.openDialogPrevious();
            }
        }];

        let toolbar = <Toolbar items={items}/>;

        return (<GridPanel
            id="configurationGridId"
            ref="configurationGrid"
            title="configuration"
            columns={columns}
            records={data}
            toolbar={toolbar} />);
    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogConfiguration
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Nuevo Configuración" modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                <FormColumn className="hbox-l " style={{marginRight: "10px"}}>
                    <HiddenField ref="record" value="" />
                    <TextField ref="code" label="Código" required/>
                    <NumberField  ref="quantity"  label="Cantidad" decimals={0} align = "left" required />
                    <TextField ref="description" label="Descripción" required />
                </FormColumn>
            </Fieldset>
            </FormPanel>
        </Dialog>
        );
    }

    openDialogConfiguration(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let record = self.refs.configurationGrid.getSelectedRecord();

            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.code.setValue(record.code);
                fields.quantity.setValue(record.quantity);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar Configuración");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.code.setValue('');
                fields.quantity.setValue('');
                fields.description.setValue('');
                fields.dialog.setTitle("Nueva Configuración");
                fields.dialog.open();
            }
    }

    closeDialogConfiguration()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            <PreviousConfiguration
            ref="previousConfiguration"
            receptionEquipmentId={this.props.receptionEquipmentId}
            data={this.props.previousRecords} type = "configuration"/>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>);
    }
}
