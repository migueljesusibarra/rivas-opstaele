import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField, PhoneField, FormPanel,Fieldset,FormColumn,NumberField } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';

export class Verification extends Component
{
    constructor(props)
    {
        super(props);
        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
    }


    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                receptionEquipmentId: self.props.receptionEquipmentId,
                code: fields.code.getValue(),
                quantity: fields.quantity.getValue(),
                description: fields.description.getValue(),
                serialNumber: fields.serialNumber.getValue()
            }

            if(recordId.length > 0)
            {
                Meteor.call("updateVerification", recordId, data, function(error, result){});
            }
            else
            {
                Meteor.call("addVerification", data, function(error, result){});
            }

            fields.dialog.close();
        }
        else
        {
            alert("Por favor ingrese todos los campos requeridos.");
        }
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
            {
                key: 1,
                header: 'Cantidad',
                minWidth: 30,
                renderer: function(record){
                    return  record.quantity;
                }
            },
            {
                key: 2,
                header: 'Código',
                minWidth: 30,
                renderer: function(record){
                    return record.code;
                }
            },
            {
                key: 3,
                header: 'Descripción',
                minWidth: 120,
                renderer: function(record){
                    return record.description;
                }
            },
            {
                key: 4,
                header: 'Número de Serie',
                minWidth: 60,
                renderer: function(record){
                    return record.serialNumber
                }
            }];

            let data = this.props.records;

            let items = [{
                text: 'Nuevo',
                cls: 'button-add',
                handler: function(){
                    self.openDialog('add');
                }
            },
            {
                text: 'Editar',
                cls: 'button-edit',
                handler: function(){
                    self.openDialog('edit');
                }
            },
            {
                text: 'Delete',
                cls: 'button-delete',
                handler: function()
                {
                    let grid = ReactDOM.findDOMNode(self);
                    let row = grid.querySelector("tr.x-selected");

                    if(row != null)
                    {
                        let record = self.props.data[row.dataset.index];
                        let confirmation = confirm("Está seguro de que desea eliminar este registro?");

                        if(confirmation == true)
                        {
                            Meteor.call("removeVerification", record._id, function(error, result){});
                        }
                    }
                    else
                    {
                        alert("Por favor seleccione un contacto.");
                    }
                }
            }];

            let toolbar = <Toolbar items={items}/>;

            return  <GridPanel
                id="verificationGrid"
                title="verification"
                columns={columns}
                records={data}
                toolbar={toolbar} />
        }

        getDialog()
        {
            let data = {};
            let items = [{
                text: 'Guardar',
                cls: 'button-save',
                handler: this.saveChanges
            },
            {
                key: 'close',
                text: 'Cerrar',
                cls: 'button-close',
                handler: this.closeDialog
            }];

            let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

            return (<Dialog ref="dialog" className="contact-dialog" title="Nuevo Verificación" modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="code" label="Código" required/>
                        <NumberField ref="quantity" label="Cantidad"  align = "left"  decimals={0}  required />
                        <TextField ref="description" label="Descripción" required />
                        <TextField ref="serialNumber" label="Número de Serie" />
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.records[row.dataset.index];

                fields.record.setValue(record._id);
                fields.code.setValue(record.code);
                fields.quantity.setValue(record.quantity);
                fields.serialNumber.setValue(record.serialNumber);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar Verificación");
                fields.dialog.open();
            }
            else
            {
                alert("Por favor seleccione un registro.");
            }
        }
        else if(action == 'add')
        {
            fields.record.setValue('');
            fields.code.setValue('');
            fields.quantity.setValue('');
            fields.serialNumber.setValue('');
            fields.description.setValue('');
            fields.dialog.setTitle("Nueva Verificación");
            fields.dialog.open();
        }
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    render()
    {
        return (<div>
            {this.getGridPanel()}
              {this.getDialog()}
        </div>);
    }
}
