import React, { Component } from 'react';
import { Fieldset } from 'react-ui';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx'
import { HistoryContainer } from '/imports/ui/containers/HistoryContainer.js'

export class HistoryDialog extends Component
{
    constructor(props){
        super(props);

        this.onPenDialog = this.onPenDialog.bind(this);
    }

    onCloseDialogHistory()
    {
        this.refs.historiDialog.close();
    }

    onPenDialog()
    {
      this.refs.historiDialog.open();
    }

    render()
    {
       return (<Dialog ref="historiDialog"
           title={this.props.title}
           modal>
           <div className="hbox-l">
                  <button type="button" className="btn btn-primary" style={{marginBottom: "6px", padding: "1px 24px", borderColor:"#e46056", backgroundColor: "#e46056"}} onClick={this.onCloseDialogHistory.bind(this)}>Cerrar</button>
             </div>
           <Fieldset>
               <HistoryContainer module={this.props.module} foreignId={this.props.foreignId}/>
           </Fieldset>
       </Dialog>);
    }
}
