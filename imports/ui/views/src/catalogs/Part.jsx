import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, FormPanel,
    Dropdown, CheckBox, NoteField } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class Part extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    removePart()
    {
        let record = this.refs.partGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar esta parte?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una parte.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.partGrid.getSelectedRecord();

        Meteor.call("removePart", record._id, function(error, result){});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit');
    }

    getVendors()
    {
        this.props.vendors.sort(Util.applySortObject);

        return this.props.vendors.map(function(vendor)
        {
            return {value: vendor._id, text: vendor.name};
        });
    }

    getVendor(id, collection)
    {
        let i = 0;

        for( i; i < collection.length; i++)
        {
            if(collection[i]._id == id)
                return collection[i].name;
        }
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }
    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.data.filter(c => this.onFilter(c, search));
      else
        return this.props.data;
    }

    onFilter(record, search)
    {
      return (record.number.search(new RegExp(search, "i")) != -1 ||
      record.description.search(new RegExp(search, "i")) != -1 );
    }

    getToolbarItems()
    {
       let self = this;

       let items = {
                   new: {
                       text: 'Nuevo',
                       cls: 'button-new-dealer',
                       handler: function()
                       {
                           self.openDialog('add');
                       }
                   },
                   edit: {
                       text: 'Editar',
                       cls: 'button-edit',
                       handler: function()
                       {
                           self.openDialog('edit');
                       }
                  },
                 remove: {
                     text: 'Eliminar',
                     cls: 'button-delete',
                     handler: function()
                     {
                         self.removePart();
                     }
                 },
                 history: {
                     text: 'Historial',
                     cls: 'button-history',
                     handler: function()
                     {
                         let record = self.refs.partGrid.getSelectedRecord();

                         if(record != null)
                             self.refs.historyPart.onPenDialog();
                         else
                             self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                     }
                 }
             }
             return items;
    }

    getToast()
	{
		return this.refs.toastrMsg;
	}

    getToolbar()
    {
        let self = this;
        let actions = self.getToolbarItems();
        leftGroup = [actions.new, actions.edit, actions.remove], rightGroup = [actions.history];

        let items = [{
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Lista de Partes"}</span>
                </div>
            </div>
        </div>);

        return toolbar;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Número',
            renderer: function(record)
            {
                return record.number;
            }
        },
        {
            key: 2,
            header: 'Descripción',
            renderer: function(record)
            {
                return record.description;
            }
        },
        {
            key: 3,
            header: 'D. D',
            align:"center",
            renderer: function(record)
            {
                return record.defectiveReturn === false ? "No" : "Si";
            }
        },
        {
            key: 4,
            header: 'Proveedor',
            renderer: function(record)
            {
                return self.getVendor(record.vendor,self.props.vendors);
            }
        },
        {
            key: 5,
            header: 'Estado',
            renderer: function(record)
            {
                return record.active ? "Activo" : "Inactivo";
            }
        }];

        return columns;
    }

    getGridPanel()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyPart" module="Part" foreignId={this.state.record._id} title="Historial Parte"/>

        return (<div>
                    <ToastrMessage ref="toastrMsg" />
                    {historyContainer}
                    <GridPanel
                        id="partGridId"
                        title="Parts"
                        ref="partGrid"
                        columns={this.getColumns()}
                        records={this.getRecords()}
                        toolbar={this.getToolbar()}
                        onRowDoubleClick={this.onRowDoubleClick}
                        onSelectionChange={this.onSelectionChange} />
                </div>);

    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog" modal>
            {toolbar}
            <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                <HiddenField ref="record" value="" />
                <TextField ref="number" label="Número" required/>
                <TextField ref="description" label="Descripción" required/>
                <CheckBox ref="defectiveReturn"  label="Devolución Defectuosa" />
                <Dropdown  ref="vendor" label="Proveedor" items={this.getVendors()}  emptyOption={true}/>
                <NoteField ref="note" label="Notas" />
                <CheckBox ref="active"  label="Activo" />
            </FormPanel>
        </Dialog>);
    }

    openDialog(action)
    {
        let self = this;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");


            if(row != null)
            {
                let record = self.props.data[row.dataset.index];

                self.refs.record.setValue(record._id);
                self.refs.number.setValue(record.number);
                self.refs.description.setValue(record.description);
                self.refs.defectiveReturn.setValue(record.defectiveReturn);
                self.refs.vendor.setValue(record.vendor);
                self.refs.note.setValue(record.note);
                self.refs.active.setValue(record.active);
                self.refs.dialog.props.title === "Edit Part";
                self.refs.dialog.setTitle("Editar Parte");
                self.refs.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                self.refs.record.setValue('');
                self.refs.number.setValue('');
                self.refs.description.setValue('');
                self.refs.vendor.setValue('');
                self.refs.defectiveReturn.setValue(false);
                self.refs.note.setValue('');
                self.refs.active.setValue(true);
                self.refs.dialog.setTitle("Nueva Parte");
                self.refs.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;

        if(self.refs.form.isValid(self.refs))
        {
            let recordId = self.refs.record.getValue();

            let data = {
                description: self.refs.description.getValue(),
                number: self.refs.number.getValue(),
                vendor: self.refs.vendor.getValue(),
                defectiveReturn: self.refs.defectiveReturn.getValue(),
                note: self.refs.note.getValue(),
                active: self.refs.active.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updatePart", recordId, data, function(error, result){});
            else
                Meteor.call("addPart", data, function(error, result){});

            self.refs.dialog.close();
        }
        else
            self.refs.toastrMsg.showMessage("warning", "Por favor ingrese todos los campos requeridos.");
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>)
    }
}
