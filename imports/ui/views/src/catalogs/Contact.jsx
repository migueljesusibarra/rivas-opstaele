import React, { Component } from 'react';Fieldset
import { Toolbar, GridPanel, TextField, EmailField, PhoneField, FormPanel,Fieldset,FormColumn,CheckBox } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';

export class Contact extends Component
{
    constructor(props)
    {
        super(props);
        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Nombre',
            minWidth: 50,
            dataIndex: 'name'
        },
        {
            key: 2,
            header: 'Correo Electrónico',
            dataIndex: 'email',
            minWidth: 40
        },
        {
            key: 3,
            header: 'Celular',
            minWidth: 40,
            dataIndex: 'cellPhone'

        },
        {
            key: 4,
            header: 'Dirección',
            minWidth: 50,
            dataIndex: 'address'
        },{
            key: 5,
            header: 'Principal',
            align: 'center',
            minWidth: 10,
            renderer: function(record)
            {
              return record.main === true ? "Si":"No";
            }
        }
    ];

        let data = this.props.data;


        let items = [{
            text: 'Nuevo',
            cls: 'button-add',
            handler: function(){
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function(){
                self.openDialog('edit');
            }
        },
        {
            text: 'Delete',
            cls: 'button-delete',
            handler: function()
            {
                let grid = ReactDOM.findDOMNode(self);
                let row = grid.querySelector("tr.x-selected");

                if(row != null)
                {
                    let record = self.props.data[row.dataset.index];
                    let confirmation = confirm("Está seguro de que desea eliminar este registro?");

                      if(confirmation == true)
                      {
                        Meteor.call("removeContact", record._id, function(error, result){});
                      }
                }
                else
                {
                    alert("Por favor seleccione un contacto.");
                }
            }
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return  <GridPanel
            id="contactGrid"
            title="Customer"
            columns={columns}
            records={data}
            toolbar={toolbar} />
    }
    getDialog()
    {
        let data = {};
            let items = [{
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (<Dialog ref="dialog" className="contact-dialog" title="Nuevo Contacto" modal>
                    {toolbar}
                    <FormPanel ref="form">
                        <Fieldset className="hbox box-form">
                        <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="name" label="Nombre" required/>
                        <EmailField ref="email" label="Correo Electrónico" required />
                        <TextField ref="address" label="Dirección" required />
                        <PhoneField ref="cellPhone" label="Celular" mask="(___) ____-____" required />
                        <PhoneField ref="workPhone" label="Teléfono del trabajo" mask="(___) ____-____" />
                        <PhoneField ref="homePhone" label="Teléfono de la casa" mask="(___) ____-____"  />
                        <CheckBox ref="main"  label="Principal" value={data.main === undefined ? false : data.main}/>
                </FormColumn>
                </Fieldset>
                    </FormPanel>
                </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.data[row.dataset.index];

                fields.record.setValue(record._id);
                fields.name.setValue(record.name);
                fields.email.setValue(record.email);
                fields.cellPhone.setValue(record.cellPhone);
                fields.workPhone.setValue(record.workPhone);
                fields.homePhone.setValue(record.homePhone);
                fields.address.setValue(record.address);
                fields.main.setValue(record.main);
                fields.dialog.setTitle("Editar Contacto");
                fields.dialog.open();
            }
            else
            {
                alert("Por favor seleccione un registro.");
            }
        }
        else if(action == 'add')
        {
            fields.record.setValue('');
            fields.name.setValue('');
            fields.email.setValue('');
            fields.cellPhone.setValue('');
            fields.workPhone.setValue('');
            fields.homePhone.setValue('');
            fields.address.setValue('');
            fields.main.setValue(false);
            fields.dialog.setTitle("Nuevo Contacto");
            fields.dialog.open();
        }
    }

    saveChanges()
    {
        let self = this;

        if(self.refs.form.isValid(self.refs))
        {
            let recordId = self.refs.record.getValue();

            let data = {
                foreignId: self.props.foreignId,
                name: self.refs.name.getValue(),
                email: self.refs.email.getValue(),
                cellPhone: self.refs.cellPhone.getValue(),
                workPhone: self.refs.workPhone.getValue(),
                homePhone: self.refs.homePhone.getValue(),
                address: self.refs.address.getValue(),
                main:self.refs.main.getValue(),
                module: self.props.module,
            }

         if(recordId.length > 0)
            {
                Meteor.call("updateContact", recordId, data, function(error, result){});
            }
            else
            {
                Meteor.call("addContact", data, function(error, result){});
            }
            self.refs.dialog.close();
        }
        else
        {
            alert("Por favor ingrese todos los campos requeridos.");
        }
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    render()
    {
        return (<div>
            {this.getGridPanel()}
            {this.getDialog()}
        </div>)
    }
}
