import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import { Toolbar, GridPanel, TextField, FormPanel, Dropdown} from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { HistoryDialog } from '/imports/ui/views/src/catalogs/HistoryDialog.jsx'
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';

export class Period extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            record: null
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange(record)
    {
      this.setState({record:record});
    }

    removePeriod()
    {
        let record = this.refs.preriodGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar el periodo?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un modelo.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.periodGrid.getSelectedRecord();
        Meteor.call("removePeriod", record._id, function(error, result){});
    }

    onRowDoubleClick(record)
    {
        this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit', record);
    }

    getColumns()
    {
        let columns = [
        {
            key: 1,
            header: 'Id',
            dataIndex: 'index'
        },
        {
            key: 5,
            header: 'Modelo',
            dataIndex: 'model'
        },
        {
            key: 5,
            header: 'Periodo',
            dataIndex: 'description'
        }];

        return columns;
    }

    getToolbarItems()
    {
       let self = this;

       let items = {
                   new: {
                       text: 'Nuevo',
                       cls: 'button-new-dealer',
                       handler: function()
                       {
                           self.openDialog('add');
                       }
                   },
                   edit: {
                       text: 'Editar',
                       cls: 'button-edit',
                       handler: function()
                       {
                           let record = self.refs.preriodGrid.getSelectedRecord();

                           if(record != null)
                                self.openDialog('edit', record);
                          else
                                self.getToast().showMessage("warning", "Por favor seleccione un registro");
                     }
                 },
                 remove: {
                     text: 'Eliminar',
                     cls: 'button-delete',
                     handler: function()
                     {
                         self.removePeriod();
                     }
                 },
                 history: {
                     text: 'Historial',
                     cls: 'button-history',
                     handler: function()
                     {
                         let record = self.refs.preriodGrid.getSelectedRecord();

                         if(record != null)
                             self.refs.historyPeriod.onPenDialog();
                         else
                             self.getToast().showMessage("warning", "Por favor seleccione un registro!");
                     }
                 }
             }
             return items;
    }

    getToast()
	{
		return this.refs.toastrMsg;
	}

    getToolbar()
    {
        let self = this;
		let actions = self.getToolbarItems();
		leftGroup = [actions.new, actions.edit], rightGroup = [actions.history];

        let items = [{
			type: 'group',
			className: 'hbox-l',
			items: leftGroup
		},
		{
			type: 'group',
			className: 'hbox-r',
			items: rightGroup
		}];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Lista de Periodos"}</span>
                </div>
            </div>
        </div>);

        return toolbar;
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
      else
        return this.props.records;
    }

    onFilter(record, search)
    {
      return (record.model.search(new RegExp(search, "i")) != -1 ||
              record.description.search(new RegExp(search, "i")) != -1);
    }

    getGridPanel()
    {
        let historyContainer = this.state.record == null ? '' : <HistoryDialog ref="historyPeriod" module="Period" foreignId={this.state.record._id} title="Historial Periodo"/>

        return (<div>
                    <ToastrMessage ref="toastrMsg" />
                    {historyContainer}
                    <GridPanel
                        id="periodGrid"
                        title="periodo"
                        ref="preriodGrid"
                        columns={this.getColumns()}
                        records={this.getRecords()}
                        toolbar={this.getToolbar()}
                        onRowDoubleClick={this.onRowDoubleClick}
                        onSelectionChange={this.onSelectionChange} />
                </div>);
    }


    getModels()
    {
        this.props.models.sort(Util.applySortObject);

        return this.props.models.map(function(model)
        {
            return { value: model._id, text: model.description};
        });
    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} />);

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Tipo de cliente" modal>
            {toolbar}
            <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                <HiddenField ref="record" value="" />
                <Dropdown ref="model" label="Marca" items={this.getModels()}  emptyOption={true} required={true}/>
                <TextField ref="description" label="Numero" required/>
            </FormPanel>
        </Dialog>
        );
    }

    openDialog(action, record)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.model.setValue(record.modelId);
                fields.description.setValue(record.description);
                fields.dialog.setTitle("Editar Periodo");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.model.setValue('');
                fields.description.setValue('');
                fields.dialog.setTitle("Nuevo Modelo");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                            description: fields.description.getValue(),
                            modelId: fields.model.getValue()
                        }

            if(recordId.length > 0)
                Meteor.call("updatePeriod", recordId, data, function(error, result){});
            else
                Meteor.call("addPeriod", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>);
    }
}
