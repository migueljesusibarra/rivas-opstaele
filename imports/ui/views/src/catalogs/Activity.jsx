import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { EditorGridPanel } from '/imports/ui/components/src/EditorGridPanel';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import { Toolbar, FormPanel } from 'react-ui';
import { Util } from '/lib/common.js';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';

export class Activity extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.data
        };

        this.onEnter = this.onEnter.bind(this);
        this.onChange = this.onChange.bind(this);
        this.showEditor = this.showEditor.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    removeActivity()
    {
        let record = this.refs.activityGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar marca?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione una marca.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.activityGrid.getSelectedRecord();

        Meteor.call("removeActivity", record._id, function(error, result){});
    }

    onEnter(event)
    {
        let target = event.target;
        let value = target.value;

        if(!Util.isEmpty(event.target.value))
            Meteor.call("addActivity", value, function(error, result){});
    }

    onChange(event)
    {
        let target = event.target;
        let recordId = target.dataset.id;

        if(!Util.isEmpty(event.target.value))
            Meteor.call("updateActivity", recordId, target.value, function(error, result){});
    }

    showEditor()
    {
        this.refs.activityGrid.showEditor();
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.records.filter(c => this.onFilter(c, search));
      else
          return this.props.records;
    }

    onFilter(record, search)
    {
        return record.description.search(new RegExp(search, "i")) != -1;
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            header: "Descripción",
            dataIndex: "name",
            renderer: function(record)
            {
                return (<InlineEditor
                        data-id={record._id}
                        clicksToEdit="2"
                        text={record.description}
                        value={record.description}
                        onChanged={self.onChange}
                        placeholder="Introduzca un texto y pulse la tecla Intro"/>);
            }
        }];

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: this.showEditor
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeActivity();
            }
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Lista de Actividades"}</span>
                </div>
            </div>
        </div>);

        return (<EditorGridPanel
            ref="activityGrid"
            title="activity"
            toolbar={toolbar}
            columns={columns}
            records={this.getRecords()}
            onEnter={this.onEnter}
        />);
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
            <div>
                {this.getGridPanel()}
                {this.getConfirmDialog()}
            </div>
        );
    }
}
