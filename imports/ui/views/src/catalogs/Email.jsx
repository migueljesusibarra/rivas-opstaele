import React, { Component } from 'react';
import { Toolbar, GridPanel, EmailField, FormPanel} from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Util } from '/lib/common.js';

export class Email extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.data
        };

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    removeEmail()
    {
        let record = this.refs.emailGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar correo electrónico?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un correo electrónico.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.emailGrid.getSelectedRecord();

        Meteor.call("removeEmail", record._id, function(error, result){});
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Id',
            dataIndex: 'index'
        },
        {
            key: 2,
            header: 'Correo Electrónico',
            renderer: function(record)
            {
                return record.email;
            }
        }];

        //let data = this.props.data;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: function(){
                self.openDialog('add');
            }
        },
        {
            text: 'Editar',
            cls: 'button-edit',
            handler: function(){
                self.openDialog('edit');
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeEmail();
            }
        }];

        let toolbar = (
        <div>
            <Toolbar className="north hbox" id="main-toolbar" items={items} />
            <div className="hbox div-second-tool-bar">
                <div className="middle-width-quarter-column">
                    <SearchField
                    ref= "search"
                    key={"searchfield"}
                    className = "search-field-single-customer"
                    placeholder=" Buscar"
                    value={this.state.search}
                    onSearch={this.onSearch} />
                </div>
                <div className="middle-width-middle-column text-aling-second-tool-bar">
                    <span>{"Correos Electrónicos"}</span>
                </div>
            </div>
        </div>);

        return  <GridPanel
            id="emailGridId"
            ref="emailGrid"
            title="Correo Electronico"
            columns={columns}
            records={this.getRecords()}
            toolbar={toolbar} />
    }

    onSearch(event)
    {
      this.setState({search: event.target.value});
    }

    getRecords()
    {
      let search = this.state.search;

      if(search)
        return this.props.data.filter(c => this.onFilter(c, search));
      else
        return this.props.data;
    }

    onFilter(record, search)
    {
      return (record.description.search(new RegExp(search, "i")) != -1 );
    }

    getDialog()
    {
        let data = {};
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (<Dialog ref="dialog" className="contact-dialog" modal>
                    {toolbar}
                    <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
                        <HiddenField ref="record" value="" />
                        <EmailField ref="email" label="Correo Electrónico" />
                       </FormPanel>
                </Dialog>);
    }

    openDialog(action)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let grid = ReactDOM.findDOMNode(self);
            let row = grid.querySelector("tr.x-selected");

            if(row != null)
            {
                let record = self.props.data[row.dataset.index];

                fields.record.setValue(record._id);
                fields.email.setValue(record.email);
                fields.dialog.setTitle("Editar Correo Electrónico");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.email.setValue('');
                fields.dialog.setTitle("Nuevo Correo Electrónico");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                email: fields.email.getValue(),
            }

            if(recordId.length > 0)
                Meteor.call("updateEmail", recordId, data, function(error, result){});
            else
                Meteor.call("addEmail", data, function(error, result){});

             fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos..");
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>)
    }
}
