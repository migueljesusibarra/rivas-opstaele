import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import { Toolbar, GridPanel, TextField, FormPanel,Dropdown} from 'react-ui';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';

export class CustomerType extends Component
{
  static get defaultProps()
  {
    return {
      customerTypes: [
        {value: "1", text: "Persona Jurídica"},
        {value: "2", text: "Persona Natural"}]
      };
  }

    constructor(props)
    {
      super(props);
      this.openDialog = this.openDialog.bind(this);
      this.saveChanges = this.saveChanges.bind(this);
      this.closeDialog = this.closeDialog.bind(this);
      this.onRowDoubleClick = this.onRowDoubleClick.bind(this);

    }

    onRowDoubleClick(record)
    {
      this.openRecord(record);
    }
    openRecord(record)
    {
      this.openDialog('edit');
    }

    getType(id, collection)
    {
      let i = 0;

      for( i; i < collection.length; i++)
      {
        if(collection[i].value == id)
        {
          return collection[i].text;
        }
      }
    }

    getGridPanel()
    {
      let self = this;

      let columns = [
        {
          key: 1,
          header: 'Descripción',
          minWidth: 50,
          renderer: function(record)
          {
            return record.description
          }
        },
        {
          key: 2,
          header: 'Tipo',
          minWidth: 50,
          renderer: function(record)
          {
            return self.getType(record.type, self.props.customerTypes)
          }
        },
      ];

      let records = this.props.records;

      let items = [
        {
          type: 'group',
          className: 'hbox-l',
          items: [
            {
              text: 'Nuevo',
              cls: 'button-new-dealer',
              handler: function(){
                self.openDialog('add');
              }
            },
            {
              text: 'Editar',
              cls: 'button-edit',
              handler: function()
              {
                self.openDialog('edit');
              }
            },
            {
              text: 'Exportar',
              cls: 'button-export'
            },
            {
              text: 'Imprimir',
              cls: 'button-print'
            }
          ],
        },
      ];

      var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

      return <GridPanel
        id="customerTypeGrid"
        title="Tipo de Cliente"
        columns={columns}
        records={records}
        toolbar={toolbar}
        onRowDoubleClick={this.onRowDoubleClick} />;
    }

    getCustomerTypes()
    {
      return this.props.customerTypes.map(function(type)
      {
        return { value: type.value, text: type.text };
      });
    }

    getDialog()
    {
      let data = {};
      let items = [{
        text: 'Guardar',
        cls: 'button-save',
        handler: this.saveChanges
      },
      {
        key: 'close',
        text: 'Cerrar',
        cls: 'button-close',
        handler: this.closeDialog
      }];

      var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

      return (<Dialog ref="dialog" className="contact-dialog" title="Tipo de cliente" modal>
      {toolbar}
      <FormPanel ref="form" style={{border: '1px solid #ECECEC'}}>
        <HiddenField ref="record" value="" />
        <TextField ref="description" label="Description" required/>
        <Dropdown ref="type" label="Tipo" items={this.getCustomerTypes()}  emptyOption={true} required={true}/>
      </FormPanel>
    </Dialog>);
  }

  openDialog(action)
  {
    let self = this;
    let fields = self.refs;

    if(action == 'edit')
    {
      let grid = ReactDOM.findDOMNode(self);
      let row = grid.querySelector("tr.x-selected");

      if(row != null)
      {
        let record = self.props.records[row.dataset.index];
        fields.record.setValue(record._id);
        fields.description.setValue(record.description);
        fields.type.setValue(record.type);
        fields.dialog.open();
      }
      else
      {
        alert("Por favor seleccione un registro.");
      }
    }
    else if(action == 'add')
    {
      fields.record.setValue('');
      fields.description.setValue('');
      fields.type.setValue('');
      fields.dialog.open();
    }
  }

  saveChanges()
  {
    let self = this;
    let fields = self.refs;

    if(fields.form.isValid(fields))
    {
      let recordId = self.refs.record.getValue();
      let data = {
        type: fields.type.getValue(),
        description: fields.description.getValue()
      }

      if(recordId.length > 0)
      {
        Meteor.call("updateCustomerType", recordId, data, function(error, result){});

      }
      else
      {
        Meteor.call("addCustomerType", data, function(error, result){});

      }
      fields.dialog.close();
    }
    else
    {
      alert("Por favor ingrese todos los campos requeridos.");
    }
  }

  closeDialog()
  {
    let self = this;
    self.refs.dialog.close();
  }

  render()
  {
    return (<div>
      {this.getGridPanel()}
      {this.getDialog()}
    </div>);
  }
}
