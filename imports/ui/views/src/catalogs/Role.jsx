import React, { Component } from 'react';
import { EditorGridPanel } from '/imports/ui/components/src/EditorGridPanel';
import { Toolbar, FormPanel } from 'react-ui';
import { InlineEditor } from '/imports/ui/components/src/InlineEditor';
import ReactDOM from 'react-dom';
import { Dialog } from '/imports/ui/components/src/Dialog';
import { CheckBox } from 'react-ui';
import { Util } from '/lib/common.js';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class Role extends Component
{
    constructor(props)
    {
    super(props);

    this.onEnter = this.onEnter.bind(this);
    this.onChange = this.onChange.bind(this);
    this.showEditor = this.showEditor.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    removeRole()
    {
        let record = this.refs.roleGrid.getSelectedRecord();

        if(record != null)
        {
            this.refs.confirmDialog.setTitle('Está seguro que desea eliminar rol?"');
            this.refs.confirmDialog.onOpen();
        }
        else
            alert("Por favor seleccione un rol.");
    }

    onOkConfirmDialog()
    {
        let record = this.refs.roleGrid.getSelectedRecord();
        // Roles.remove({_id: record._id});
        Meteor.call("removeRole", record._id, function(error, result){});
    }

    onEnter(event)
    {
        let target = event.target;
        let value = target.value;

        if(!Util.isEmpty(event.target.value))
            Meteor.call("addRole", value, function(error, result){});
    }

    onChange(event)
    {
        let target = event.target;
        let recordId = target.dataset.id;

        if(!Util.isEmpty(event.target.value))
            Meteor.call("updateRole", recordId, target.value, function(error, result){});
    }

    showEditor()
    {
        this.refs.roleGrid.showEditor();
    }

    getGridPanel()
    {
        let self = this;

        let columns = [
        {
            header: "Descripción",
            dataIndex: "description",
            renderer: function(record)
            {
                return (<InlineEditor
                    data-id={record._id}
                    clicksToEdit="2"
                    text={record.description}
                    value={record.description}
                    onChanged={self.onChange}
                    placeholder="Introduzca un texto y pulse la tecla Intro"/>);
            }
        }];

        let records = this.props.records;

        let items = [
        {
            text: 'Nuevo',
            cls: 'button-add',
            handler: this.showEditor
        },
        {
            text: 'Permisos',
            cls: 'button-new',
            handler: function()
            {
                self.openDialog();
            }
        },
        {
            text: 'Eliminar',
            cls: 'button-delete',
            handler: function()
            {
                self.removeRole();
            }
        }];

        let toolbar = (<Toolbar items={items}/>);

        return (<EditorGridPanel
        ref="roleGrid"
        title="Roles"
        toolbar={toolbar}
        columns={columns}
        records={records}
        onEnter={this.onEnter}
        />);
    }

    getDialog()
    {
        let bbar = [
        {
            key: 'save',
            text: 'Save'
        },
        {
            key: 'close',
            text: 'Close'
        }];

        return (
        <Dialog ref="dialog" title="Permissions Form" bbar={bbar} modal>
            <FormPanel ref="form" className="test" style={{border: '1px solid #ECECEC'}}>
                <div id="permissions" className="permissions">
                    <div><label>Cliente:</label></div>
                    <CheckBox ref="addCustomer" className="check1" label="Add"/>
                    <CheckBox ref="editCustomer" className="check1" label="Edit"/>
                    <CheckBox ref="deleteCustomer" className="check1" label="Delete"/>
                    <CheckBox ref="viewCustomer" className="check1" label="View"/>
                </div>
            </FormPanel>
        </Dialog>);
    }

    openDialog()
    {
        this.refs.dialog.open();
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getGridPanel()}
            {this.getDialog()}
            {this.getConfirmDialog()}
            </div>
        );
    }
}
