import React, { Component } from 'react';
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel,Fieldset,FormColumn,CheckBox } from 'react-ui';
import ReactDOM from 'react-dom';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class ContactPanel extends Component
{
    constructor(props)
    {
        super(props);

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onNewContactClick = this.onNewContactClick.bind(this);
        this.onEditContactClick = this.onEditContactClick.bind(this);
        this.onDeleteContactClick = this.onDeleteContactClick.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);

    }

    onOkConfirmDialog()
    {
        let recordId = this.refs.confirmDialog.getValue();
        Meteor.call("removeContact", recordId, function(error, result){});
    }

    onChangeCheckBoxContactMain(value)
    {
        if(value)
        {
            let recordId = this.refs.record.getValue();
            let isMain = false;

            this.props.data.map(function(record, index){
                if(recordId != record._id)
                {
                    if(record.main)
                        isMain = true;
                }
            });

            if(isMain)
            {
                this.refs.main.setValue(false);
                alert("Ya existe un contacto principal");
            }
        }
    }

    onEditContactClick(event)
    {
        this.openDialog("edit",event.target.closest("button").dataset.id);
    }

    onDeleteContactClick(event)
    {
        let recordId = event.target.closest("button").dataset.id
        this.refs.confirmDialog.setTitle('Está seguro que desea eliminar el contacto?');
        this.refs.confirmDialog.onOpenAndValue(recordId);
    }

    onNewContactClick(event)
    {
        this.openDialog("add");
    }

    getDialog()
    {
        let data = {};
        let items = [{
        text: 'Guardar',
        cls: 'button-save',
        handler: this.saveChanges
        },
        {
        key: 'close',
        text: 'Cerrar',
        cls: 'button-close',
        handler: this.closeDialog
        }];

        let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (
        <Dialog ref="dialog" className="contact-dialog" title="Nuevo Contacto" modal>
            {toolbar}
            <FormPanel ref="form">
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-l" style={{marginRight: "10px"}}>
                        <HiddenField ref="record" value="" />
                        <TextField ref="name" label="Nombre" required/>
                        <EmailField ref="email" label="Correo Electrónico" required />
                        <PhoneField ref="cellPhone" label="Celular" mask="(___) ____-____" required />
                        <CheckBox ref="main"  label="Principal" value={data.main === undefined ? false : data.main} onChange={this.onChangeCheckBoxContactMain.bind(this)}/>
                    </FormColumn>
                </Fieldset>
            </FormPanel>
        </Dialog>);
    }

    openDialog(action, index)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            let record = self.props.data[index];
            fields.record.setValue(record._id);
            fields.name.setValue(record.name);
            fields.email.setValue(record.email);
            fields.cellPhone.setValue(record.cellPhone);
            fields.main.setValue(record.main);
            fields.dialog.setTitle("Editar Contacto");
            fields.dialog.open();

        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.name.setValue('');
                fields.email.setValue('');
                fields.cellPhone.setValue('');
                fields.main.setValue(false);
                fields.dialog.setTitle("Nuevo Contacto");
                fields.dialog.open();
            }
    }

    saveChanges()
    {
        let self = this;

        if(self.refs.form.isValid(self.refs))
        {
            let recordId = self.refs.record.getValue();

            let data = {
            foreignId: self.props.foreignId,
            name: self.refs.name.getValue(),
            email: self.refs.email.getValue(),
            cellPhone: self.refs.cellPhone.getValue(),
            main:self.refs.main.getValue(),
            module: self.props.module,
            }

            if(recordId.length > 0)
                Meteor.call("updateContact", recordId, data, function(error, result){});
            else
                Meteor.call("addContact", data, function(error, result){});

            self.refs.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    closeDialog()
    {
        let self = this;
        self.refs.dialog.close();
    }

    getList()
    {
        let self = this;

        let items = this.props.data.map(function(record, index)
        {

            let styleCircle = record.main ? "circle-contact color-red-contact" : "circle-contact color-DarkGray-contact";

            let circle = (<div><div className={styleCircle} ></div><div className = "contact-text">{record.name}</div></div>);

            return (
            <div key={"contact" + index} className="contact-item">
                {circle}
                <div className="contact-toolbar">
                    <button onClick={self.onEditContactClick} data-id={index}><i className="edit-icon-contact"></i></button>
                    <button onClick={self.onDeleteContactClick} data-id={record._id}><i className="delete-icon-contact"></i></button>
                </div>
            </div>);
        });

        return items;
    }

    getConfirmDialog()
    {
        return (<Confirm
            ref="confirmDialog"
            onOk={this.onOkConfirmDialog}
            okLabel="Si"
            noLabel="No"
        />);
    }

    render()
    {
        return (
        <div>
            <div key={"contact-" + 0} className="contact-customer-item-title">
                <i className="customer-icon-contact"></i><span className="contact-customer-title">Contactos</span>
                <div className="contact-toolbar-new">
                    <a href="#" className="icon-contact-dealer" onClick={this.onNewContactClick}></a>
                </div>
            </div>
            {this.getList()}
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>)
    }
}
