import React, { Component } from 'react';
import moment from 'moment';
import { Dropdown } from 'react-ui';
import $ from 'jquery';
import 'jquery/dist/jquery.min.js';
import 'jquery/dist/jquery-ui.min.js';
import 'moment/min/moment.min.js';
import 'fullcalendar/dist/fullcalendar.min.js';
import 'fullcalendar/dist/fullcalendar.css';

export class FullCalendar extends Component {

    constructor(props)
    {
        super(props);
    }

getOptionCustomer()
{
    let self = this;

   return this.props.customers.map(function(customer) {
       return (<option key={"option-" + customer._id} value={customer._id} >{customer.name}</option>)
      });
}

getOptiontechnicians()
{
    let self = this;

   return this.props.technicians.map(function(technician) {
      return (<option key={"option-" + technician._id} value={technician._id}  >{technician.name}</option>)
     });
}

onClickReport()
{
    FlowRouter.go("/report");
    FlowRouter.setQueryParams({id: "", module: "reportcalendar"});
}
onchangeCustomer(event)
{
    let view = $('#calendar').fullCalendar('getView');
    let intervalStart = new Date(view.intervalStart.format().toString());
    let endDate = new Date(view.intervalEnd.format().toString())
    let year = intervalStart.getFullYear();
    let month = intervalStart.getMonth();
    let startDate = new Date(year, month + 1, 1);

    if(event.target.value)
    {
        $(".fc-prev-button").removeClass('fc-state-disabled');
	    $(".fc-prev-button").prop('disabled', false);
        $(".fc-next-button").removeClass('fc-state-disabled');
	    $(".fc-next-button").prop('disabled', false);

        this.setSelectValue("technician-filter", "");
        FlowRouter.setQueryParams({start: startDate, end: endDate, customerId: event.target.value, technicianId: null});
    }else
    {
        $(".fc-prev-button").prop('disabled', true);
        $(".fc-prev-button").addClass('fc-state-disabled');

        $(".fc-next-button").prop('disabled', true);
        $(".fc-next-button").addClass('fc-state-disabled');
        FlowRouter.setQueryParams({start: null, end: null, customerId: null, technicianId: null});
    }
}
onchangeTechnician(event)
{
    let view = $('#calendar').fullCalendar('getView');
    let intervalStart = new Date(view.intervalStart.format().toString());
    let endDate = new Date(view.intervalEnd.format().toString())
    let year = intervalStart.getFullYear();
    let month = intervalStart.getMonth();
    let startDate = new Date(year, month + 1, 1);

    if(event.target.value)
    {
        $(".fc-prev-button").removeClass('fc-state-disabled');
        $(".fc-prev-button").prop('disabled', false);
        $(".fc-next-button").removeClass('fc-state-disabled');
        $(".fc-next-button").prop('disabled', false);
        this.setSelectValue("customer-filter", "");
        FlowRouter.setQueryParams({start: startDate, end: endDate, customerId: null, technicianId: event.target.value});
   }else{

       $(".fc-prev-button").prop('disabled', true);
       $(".fc-prev-button").addClass('fc-state-disabled');

       $(".fc-next-button").prop('disabled', true);
       $(".fc-next-button").addClass('fc-state-disabled');
      FlowRouter.setQueryParams({start: null, end: null, customerId: null, technicianId: null});
   }
}

setSelectValue(id, val)
{
    document.getElementById(id).value = val;
}

 getSelectValue(id)
 {
     return document.getElementById(id).value;
 }

  render()
  {
      $("#calendar").fullCalendar('removeEvents');
      $("#calendar").fullCalendar('addEventSource', this.props.events);

    return (<div>
            <div className="x-toolbar norte hbox">
                               <div className="hbox-l">
                                  <label style={{marginLeft: "10px"}}>Cliente: </label>
                                  <select id="customer-filter" defaultValue={this.props.customerId} style={{width: "200px", marginTop: "3px", border: "1px solid lightgray"}} onChange={this.onchangeCustomer.bind(this)}>
                                         <option value=""></option>
                                         {this.getOptionCustomer()}
                                     </select>
                                   <label style={{marginLeft: "10px"}}>Ingeniero: </label>
                                       <select id="technician-filter" defaultValue={this.props.technicianId} style={{width: "200px", marginTop: "3px", border: "1px solid lightgray"}} onChange={this.onchangeTechnician.bind(this)}>
                                              <option value=""></option>
                                             {this.getOptiontechnicians()}
                                      </select>
                               </div>
                               <div className="hbox-r">
                                    <a href="javascript:void(0);" className="button-report" onClick={this.onClickReport.bind(this)}>Reporte</a>
                               </div>
                               </div>

            <div className="divCalendar">
             <div id="calendar">
               </div>
           </div></div>);
  }

 getprintPreview(name)
 {
     let headerElements = document.getElementsByClassName('fc-header');//.style.display = 'none';
     for(var i = 0, length = headerElements.length; i < length; i++) {
         headerElements[i].style.display = 'none';
     }

     let toPrint = document.getElementsByClassName("divCalendar")[0].innerHTML;

     for(let i = 0, length = headerElements.length; i < length; i++) {
         headerElements[i].style.display = '';
     }

    let linkElements = document.getElementsByTagName('link');
    let link = '';
    for(let i = 0, length = linkElements.length; i < length; i++) {
        link = link + linkElements[i].outerHTML;
    }

    let styleElements = document.getElementsByTagName('style');
    let styles = '';
    for(let i = 0, length = styleElements.length; i < length; i++) {
        styles = styles + styleElements[i].innerHTML;
    }

    let frmhtml = document.getElementById("ifrm");

    if(frmhtml != null)
       frmhtml.parentNode.removeChild(frmhtml);

    let ifrm = document.createElement('iframe');
    ifrm.setAttribute('id', 'ifrm');
    ifrm.setAttribute('name', 'ifrm');
    document.body.appendChild(ifrm);

    let ifrm2 = window.frames["ifrm"];
    //ifrm2.document.write('<html><head><title></title>');
    ifrm2.document.write('<html><head><title>Calendario de Mantenimiento</title>'+link
     +'<style>'+styles+'</style>'+
     '<style type="text/css" media="print">' +
     '@page { size: auto; margin: 0;}'+
     'button { display: none; }'+
     ' </style>');
     ifrm2.document.write('<div style="width: 100%; margin-top: 20px">' +
           '<div style="width: 50%;  display: inline-block; float: initial;">'+
              '<a><img src="/img/logo2.png" style="margin-left: 50px;"></img></a>'+
              '<div style="margin-left: 102px; color: darkgray;"}}>DIVISION MEDICA</div>'+
              '<div style="margin-left: 10px;">Shell Plaza El Sol 2 C, Sur, 2 C. Este - Colonial los Robles</div>' +
              '<div style="margin-left: 25px;"}}>Telefono (505) 2252-48    width: 160px;00 - Fax (505) 2252-4811</div>'+
              '<div style="margin-left: 70px;">E-mail: <u style="color: darkgray;"">servicio@rivasopstaele.com</u></div>'+
           '</div>'+
            '<div style="width:50%; padding: 20px; display: inline-block; margin-left: -18px; float: initial;">'+
                '<u>Calendario de Mantenimientos</u>'+
                 '</div></div>');
    ifrm2.document.write('</head><body onload="window.print()">');
    // ifrm2.document.write('<div style="margin-top: 40px">asdfasdf<div>')

    ifrm2.document.write('<div style="margin-top: 40px" >miguel</div>');
    ifrm2.document.write(toPrint);
    ifrm2.document.write('</body></html>');
    ifrm2.document.close();

    // let popupWin = window.open('', '_blank');
    // popupWin.document.open();
    // popupWin.document.write('<html><head><title>Schedule Preview</title>'+link
    // +'<style>'+styles+'</style></head><body">')
    // popupWin.document.write(toPrint);
    // popupWin.document.write('</body></html>');
    // popupWin.document.close();
    // popupWin.print()

    // window.frames["ifrm"].focus();
    // window.frames["ifrm"].print();

}

  componentDidMount() {
     let self = this;
    $('#calendar').fullCalendar({

            events: this.props.events,
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNamesShort: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
            hiddenDays: [ 0, 6 ],
        //     customButtons: {
        //     myCustomButton: {
        //        text: 'Reporte',
        //        click: function() {
        //          self.getprintPreview();
          //
        //        }
        //       }
        //   },
            height: 650,
            //contentHeight: 100,
			header: {
				left: 'prev,next',
				center: 'title',
				right: ''
			},
            viewRender: function(view, element)
            {
                //$(".fc-row fc-week fc-widget-content").height(1);

                if(self.getSelectValue("customer-filter") != "")
                {
                    let intervalStart = new Date(view.intervalStart.format().toString());
                    let endDate = new Date(view.intervalEnd.format().toString())
                    let year = intervalStart.getFullYear();
                    let month = intervalStart.getMonth();
                    let startDate = new Date(year, month + 1, 1);
                    FlowRouter.setQueryParams({start: startDate, end: endDate, customerId: self.getSelectValue("customer-filter"), technicianId: null});

                }else if(self.getSelectValue("technician-filter") != "")
                {
                    let intervalStart = new Date(view.intervalStart.format().toString());
                    let endDate = new Date(view.intervalEnd.format().toString())
                    let year = intervalStart.getFullYear();
                    let month = intervalStart.getMonth();
                    let startDate = new Date(year, month + 1, 1);
                    FlowRouter.setQueryParams({start: startDate, end: endDate, customerId: null, technicianId: self.getSelectValue("technician-filter")});
                }
                else
                {
                    $(".fc-row").css("height", "20");
                    $(".fc-week").css("height", "20");
                    $(".fc-prev-button").prop('disabled', true);
			        $(".fc-prev-button").addClass('fc-state-disabled');
                    $(".fc-next-button").prop('disabled', true);
			        $(".fc-next-button").addClass('fc-state-disabled');
                }

            },
			editable: true,
			droppable: true, // this allows things to be dropped onto the calendar
            eventRender:function(event,element)
            {
              element.bind('dblclick',function(){
                 alert('double click event')
              });

            },
            eventDrop: function (event, delta, revertFunc)
            {
                  //var dropfromdate = event.start._i get date when initial the drap
               let droptodate = event.start.format('YYYY-MM-DD HH:mm');
               let date = new Date(droptodate);
               Meteor.call("updatePreOrderCalendar", event.id, date,  function(error, result){});
           },
       });
  }
}
