import React, { Component } from 'react';


let thStyle = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse"
};

let tableStyle = {
      width: "100%",
      border: "1px solid black",
      borderCollapse: "collapse",
      margin: "0 auto"
}

let divTdStyle = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",

}

export class ReportTable extends Component
 {
    constructor(props) {
        super(props);
    }
   //detail configurations
   getTableConfiguration(records)
   {

      let componentTable = records.map(function(item)
      {
           return (<tr key={"tr-" + item._id}>
                   <td style={thStyle}>
                    <div style={divTdStyle}>configuracion.quantity</div>
                   </td>
                   <td style={thStyle}>
                      <div style={divTdStyle}>configuracion.code</div>
                   </td>
                   <td style={thStyle}>
                     <div style={divTdStyle}>configuracion.description</div>
                   </td>
               </tr>)
       });

       return componentTable;
    }

    render()
    {
        return (<div>
                  <div style={{marginBottom: "5px", marginTop: "25px"}} >Lista de Configuracion</div>
                    <table style={tableStyle}>
                        <tbody>
                        <tr>
                            <th style={thStyle}>Cantidad</th>
                            <th style={thStyle}>Codigo</th>
                            <th style={thStyle}>Description</th>
                        </tr>
                          {this.getTableConfiguration(this.props.records)}
                        </tbody>
                    </table>
                </div>);
    }
}
