import React, { Component } from 'react';
import { Util } from '/lib/common.js';

let divTdStyle = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divTdLeft = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divheardleft = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    border: "1px solid black",
    marginLeft: "0px",
    float: "initial",
    marginTop: "13px"
};

let divheardleft2 = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    marginLeft: "-18px",
    float: "initial",
    marginTop: "2px"
};

let divheardright = {
    width: "45%",
	//padding: "20px",
	display: "inline-block",
    border: "1px solid black",
    marginRight: "0px",
    float: "right",
    marginTop: "11px"
};

let divheardright2 = {
    width: "43%",
    //padding: "20px",
    display: "inline-block",
    marginRight: "0px",
    float: "right",
    marginTop: "-11px"
};

let wrap = {
   width: "100%",
   borderCollapse: "collapse",
   marginTop: "16px"
 };

let titleHeader = {
   textAlign: "left",
   marginBottom: "9px",
   marginTop: "5px",
   marginLeft: "13px"
};

let tdStyleCode = {
   color: "#000000",
   cursor: "pointer",
   padding: ".5em",
   whiteSpace: "nowrap",
   fontSize:"12px",
   border: "1px solid black",
   borderCollapse: "collapse",
   width: "210px"
};

let tdStyleQuantity = {
color: "#000000",
cursor: "pointer",
padding: ".5em",
whiteSpace: "nowrap",
fontSize:"12px",
border: "1px solid black",
borderCollapse: "collapse",
width: "150px"
};

let tdStyleDescription = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "790px",
    textAlign: "left"
};

let tdStyleVerificationDescription = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "550px",
        textAlign: "left"
    };

    let tdStyleMissingDescription = {
            color: "#000000",
            cursor: "pointer",
            padding: ".5em",
            whiteSpace: "nowrap",
            fontSize:"12px",
            border: "1px solid black",
            borderCollapse: "collapse",
            width: "900px",
            textAlign: "left"
        };


let tdStyleSerie = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "250px",
        textAlign: "left"
    };

let tdStyleTestPerforme = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "360px",
    textAlign: "left"
};

let tdStylePassFail= {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "475px",
    textAlign: "center"
};

let tableStyle = {
  //width: "100%",
  border: "1px solid black",
  borderCollapse: "collapse",
  margin: "0 auto"
}

let divTdStyleList = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    textAlign: "center",
}

let flexContainer = {
   padding: "0",
   margin: "0",
   listStyle: "none",
   display: "-webkit-box",
   display: "-moz-box",
   display: "-ms-flexbox",
   display: "-webkit-flex",
   display: "flex",
   alignItems: "center",
   justifyContent: "center",
   marginTop: "-13px",
   height: "39px"
}

let rowPass = {
    width: "100%"
}

let textFonsizeLeft = {
    fontSize: "11px",
    marginLeft: "-50px",
    fontFamily: "Calibri (Body)"
}

let textFonsize = {
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
}

export class RReceptionEquipment extends Component
 {
     constructor(props)
     {
        super(props);
    }

      getTableConfiguration(records)
      {
          let tableCF;

          if(records.length > 0)
          {
              tableCF = records.map(function(item)
              {
                return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.quantity}</div></div>
                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.code}</div></div>
                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.description}</div></div>
                     </div>
                   );

            });
        }else
        {
           tableCF =  (<div key={"item-CF"} style={{width: "100%"}}>
                               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                       </div>);
        }

         return tableCF;
      }


      getTableVerification(records)
      {
          let tableVeifi = (<div>Not Data</div>);

          if(records.length > 0)
          {
               tableVeifi = records.map(function(item)
                {
                  return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.quantity}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.code}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "433px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.description}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "197px", display: "inline-block" }}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.serialNumber}</div></div>
                        </div>);
              });
          }else{
                 tableVeifi = (<div key={"item-ve"} style={{width: "100%"}}>
                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "433px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "197px", display: "inline-block" }}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                     </div>);
          }

         return tableVeifi;
      }

       getTableMissing(records)
       {
           let tableMS = (<div>Not Data</div>);

           if(records.length > 0)
           {
               tableMS = records.map(function(item)
               {
                   return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.quantity}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.code}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.description}</div></div>
                       </div>
                     );

                });
            }else
            {
                tableMS =  (<div key={"item-MS"} style={{width: "100%"}}>
                                    <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                                    <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "125px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                                    <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                                </div>
                            );

            }

           return tableMS;
        }

        getTableTestsPerformed(records)
        {
            let tableTS = (<div>Not Data</div>);

            if(records.length > 0)
            {
                tableTS = records.map(function(item)
                {
                    return (<div key={"item-"+ item._id} style={{width: "100%",  margin: "auto"}}>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "354px", display: "inline-block"}}><div style={{marginTop: "7px", fontSize: "11px", marginLeft: "5px",}}>{item.description}</div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "200px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px", marginLeft: "5px"}}>
                                <div style={flexContainer}>
                                    <div style={{width:"33%", border: "1px solid"}}>
                                        <span>{item.passFail == true ? "x" : "."}</span>
                                    </div>
                                    <div style={rowPass}>
                                        <span>Pass</span>
                                    </div>
                                    <div style={{width:"33%", border: "1px solid"}}>
                                        <span>{item.passFail == false ? "x" : "."}</span>
                                    </div>
                                    <div style={rowPass}>
                                        <span>Fail</span>
                                    </div>
                                </div></div></div>
                            <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "300px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px", marginLeft: "5px"}}>{item.observation != "" ? item.observation : "."}</div></div>
                       </div>
                     );
           });
       }else
       {
           tableTS = (<div key={"item-Test"} style={{width: "100%",  margin: "auto"}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "354px", display: "inline-block"}}><div style={{marginTop: "7px", fontSize: "11px", marginLeft: "5px",}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "200px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px", marginLeft: "5px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "300px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px", marginLeft: "5px"}}></div></div>
              </div>);
        }

           return tableTS;
     }


         getTablePreOrder(records)
         {
             let componentTable;
             let size = 6;
             let newRecords = new Array(Math.ceil(records.length / size)).fill("").map(function() {
                 return this.splice(0, size) }, records.slice());
                 let count = 0;

                 if(newRecords.length > 0)
                 {
                     componentTable = newRecords.map(function(item)
                     {
                         count = count + 1;
                         if(count != 1)
                         {
                             return (<div key={"item-"+ item[0]._id} style={{width: "100%"}}>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item[0] != null ? Util.getDateISO(item[0].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item[1] != null ? Util.getDateISO(item[1].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[2] != null ? Util.getDateISO(item[2].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[3] != null ? Util.getDateISO(item[3].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[4] != null ? Util.getDateISO(item[4].date) : "."}</div></div>
                                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[5] != null ? Util.getDateISO(item[5].date) : "."}</div></div>
                                     </div>
                                 );
                         }
                         else
                         {
                                return (<div key={"item-PO"+ item[0]._id} style={{width: "100%"}}>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item[0] != null ? Util.getDateISO(item[0].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px",  width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item[1] != null ? Util.getDateISO(item[1].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[2] != null ? Util.getDateISO(item[2].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[3] != null ? Util.getDateISO(item[3].date) : "."}</div></div>
                                          <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[4] != null ? Util.getDateISO(item[4].date) : "."}</div></div>
                                         <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item[5] != null ? Util.getDateISO(item[5].date) : "."}</div></div>
                                     </div>
                                 );
                         }

                     });

            }else
            {
                componentTable =  (<div key={"item-0"} style={{width: "100%"}}>
                                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px",  width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                                    <div style={{borderRight: "1px solid black", borderBottom: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "150px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                                </div>
                            );
                    }

             return componentTable;
         }

    render()
    {
        let  date = new Date();
        let monthname=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        let dateNow = date.getDate()+" días de "+monthname[date.getMonth()]+" del año "+date.getFullYear();

        let data = this.props.record;
        let dataConfig = this.props.configurations;
        let dataVerifi = this.props.verifications;
        let dataMissing = this.props.missings;
        let dataTestsPerformed = this.props.testsPerformeds;
        let dataPreOrden = this.props.preOrders;

        return <div>
            <div className="rr-header">
            <div className="divHeaderImage" style={wrap}>
              <div style={divheardleft2}>
                 <a><img src="/img/logo2.png" style={{marginLeft: "73px", width: "175px"}}></img></a>
                 <div style={{marginLeft: "114px", color: "darkgray", fontSize: "11px"}}>DIVISION MEDICA</div>
                 <div style={{marginLeft: "25px", fontSize: "12px"}}>Frente a Policlínica Nicaragüense Bolonia Managua, Nicaragua</div>
                 <div style={{marginLeft: "49px", fontSize: "12px"}}>Teléfono (505) 2268-0370 - Móvil (505) 8966-2946</div>
                 <div style={{marginLeft: "87px", fontSize: "12px"}}>E-mail: <u style={{color: "darkgray"}}>servicio@rivasopstaele.com</u></div>
              </div>
               <div style={divheardright2}>
                   <span style={{fontFamily: "Calibri (Body)"}}>Recepción de Equipo No {data.Id}</span>
                   <hr style={{backgroundColor: "black", height: "5px", width: "452px", marginLeft: "-5%"}}></hr>
               </div>
            </div>
            <div style={wrap}>
              <div style={divheardleft}>
              <div style={{marginTop: "-20px", fontFamily: "Calibri (Body)"}}><strong>Información General</strong></div>
              <h5 style={titleHeader}><u>Información de Equipo:</u></h5>
                  <table>
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Sistema:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.system.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                               <td>
                               <div style={divTdLeft}>Marca:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.brand.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Model:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.model.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Número de Serie:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.serialNumber.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Revisión SW:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.releaseSw.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Fecha de Instalación:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.installationDate)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Garantia Vence:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.warrantyExpires)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                  </div>
              <div style={divheardright}>
              <h5 style={titleHeader}><u>Información del Cliente:</u></h5>
                  <table >
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Nombre:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.name.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Dirección:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                           <td>
                               <div style={divTdStyle}></div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(47,47) != "" ? data.address.substr(47,47) : "."}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Teléfono :</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.phone}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Fax:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.fax}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Contacto:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.contact.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>E-Mail:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.email.substr(0,47)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                 </div>
          </div>
      </div>
          <div className="page-margin-report" >
              <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Lista de Configuración</strong></u></h4>
                  <div style={{width: "100%",}}>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Cantidad</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "125px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Código</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Descripción</div></div>
                  </div>
                  {this.getTableConfiguration(dataConfig)}
                <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Lista de Verificación</strong></u></h4>
                    <div style={{width: "100%",}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Cantidad</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "125px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Código</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "433px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Descripción</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "197px", display: "inline-block" }}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Número de Serie</div></div>
                    </div>
                        {this.getTableVerification(dataVerifi)}
                      <h4 style={{marginBottom: "5px", marginTop: "px"}} ><u><strong>Lista de Faltante</strong></u></h4>
                          <div style={{width: "100%",}}>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Cantidad</div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "125px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Código</div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "627px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Descripción</div></div>
                          </div>
                          {this.getTableMissing(dataMissing)}
                     <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Lista de Pruebas Realizadas</strong></u></h4>
                         <div style={{width: "100%",}}>
                             <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "354px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Descripción de Pruebas Realizadas</div></div>
                             <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "200px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Pass/Fail</div></div>
                             <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "300px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Observaciones</div></div>
                         </div>
                          {this.getTableTestsPerformed(dataTestsPerformed)}
                          <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Calendario de visitas de Mantenimiento Preventivo</strong></u></h4>
                          {this.getTablePreOrder(dataPreOrden)}
                          <div style={{fontSize: "11px", marginTop: "20px"}} >{"En fe de lo cual firmamos dos tantos en el departamento de Managua a los " + dateNow + "."}</div>
                          <div style={textFonsize}>En el abajo firmante, declara por la presente:</div>
                          <div style={textFonsize}>1. Haber recibido los items en la lista de Configuración.</div>
                          <div style={textFonsize}>2. Haber recibido los items en la lista de Verificación.</div>
                          <div style={textFonsize}>3. Dar fe de la aprobación de las pruebas realizadas en la lista de Pruebas Realizadas.</div>
                          <div style={textFonsize}>El cliente queda enterado de que los terminos de garantía se rigen a partir de la fecha indicada como la fecha de instalación.</div>
                          <div style={{marginBottom: "5px", marginTop: "100px"}}>
                          <div  style={{width: "100%", borderCollapse: "collapse"}}>
                            <div style={{width: "50%", display: "inline-block", marginLeft: "-18px",  marginTop: "20px"}}>
                              <hr style={{backgroundColor: "black", height: "0px", width: "390px", marginLeft: "4%"}} />
                              <span style= {{marginLeft: "127px"}}><strong>Nombre, Firma, Sello y Fecha </strong></span>
                              <span style= {{marginLeft: "180px"}}><strong>Cliente</strong></span>
                            </div>
                            <div style= {{width: "50%", display: "inline-block", marginRight: "0px",  marginTop: "20px"}}>
                                <hr style= {{backgroundColor: "black", height: "0px", width: "385px", marginLeft: "14%"}} />
                                <span style= {{marginLeft: "127px"}}><strong>Nombre, Firma, Sello y Fecha </strong></span>
                                <span style= {{marginLeft: "180px"}}><strong>FSE - Rivas Opstaele</strong></span>
                            </div>
                        </div>
                       </div>
          </div>
        </div>
    }
 }
