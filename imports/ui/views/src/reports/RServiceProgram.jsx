import React, { Component } from 'react';
import { Util } from '/lib/common.js';

let divTdStyle = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divTdLeft = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divheardleft = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    border: "1px solid black",
    marginLeft: "0px",
    float: "initial",
    marginTop: "13px"
};

let divheardleft2 = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    marginLeft: "-18px",
    float: "initial",
    marginTop: "2px"
};

let divheardright = {
    width: "45%",
	//padding: "20px",
	display: "inline-block",
    border: "1px solid black",
    marginRight: "0px",
    float: "right",
    marginTop: "11px"
};

let divheardright2 = {
    width: "43%",
    //padding: "20px",
    display: "inline-block",
    marginRight: "0px",
    float: "right",
    marginTop: "-11px"
};

let wrap = {
   width: "100%",
   borderCollapse: "collapse",
   marginTop: "16px"
 };

let titleHeader = {
   textAlign: "left",
   marginBottom: "9px",
   marginTop: "5px",
   marginLeft: "13px"
};

let tdStyleCode = {
   color: "#000000",
   cursor: "pointer",
   padding: ".5em",
   whiteSpace: "nowrap",
   fontSize:"12px",
   border: "1px solid black",
   borderCollapse: "collapse",
   width: "210px"
};

let tdStylePos = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "50px",
    textAlign: "left"
};

let tdStyleDescription = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "790px",
    textAlign: "left"
};

let tdStyleVerificationDescription = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "550px",
        textAlign: "left"
    };

    let tdStyleMissingDescription = {
            color: "#000000",
            cursor: "pointer",
            padding: ".5em",
            whiteSpace: "nowrap",
            fontSize:"12px",
            border: "1px solid black",
            borderCollapse: "collapse",
            width: "900px",
            textAlign: "left"
        };


let tdStyleSerie = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "250px",
        textAlign: "left"
    };

let tdStyleActivity = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "360px",
    textAlign: "center"
};

let thStylePos = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "246px",
    textAlign: "center"
};

let tdStyleContent = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "1182px",
    textAlign: "center"
};

let tdStyleleOk = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"10px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "10px",
    textAlign: "center"
};

let thStyleMaterial = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "899px",
    textAlign: "center"
};

let tableStyle = {
  //width: "100%",
  border: "1px solid black",
  borderCollapse: "collapse",
  margin: "0 auto"
}

let divTdStyleList = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    textAlign: "left",
}

let flexContainer = {
   padding: "0",
   margin: "0",
   listStyle: "none",
   display: "-webkit-box",
   display: "-moz-box",
   display: "-ms-flexbox",
   display: "-webkit-flex",
   display: "flex",
   alignItems: "center",
   justifyContent: "center"
}

let rowPass = {
    width: "100%"
}

let textFonsizeLeft = {
    fontSize: "11px",
    marginLeft: "-50px",
    fontFamily: "Calibri (Body)"
}

let textFonsize = {
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
}

export class RServiceProgram extends Component
 {
     constructor(props)
     {
        super(props);
     }

     getTableProgram(records)
     {
         let componentTable = records.map(function(item)
         {
             return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>{item.parenttage}</div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black",  height: "26px", width: "433px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>{item.activity}</div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black",  height: "26px", width: "125px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>.</div></div>
                              <div style={{borderRight: "1px solid black", borderBottom: "1px solid black",  height: "26px", width: "197px", display: "inline-block" }}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>{item.material != "" ? item.material : "." }</div></div>
                     </div>
                   );

         });

         return componentTable;
      }

    render()
    {
        let data = this.props.record;
        let dataProt = this.props.protocolData;

        return <div>
            <div className="rr-header">
            <div className="divHeaderImage" style={wrap}>
              <div style={divheardleft2}>
                 <a><img src="/img/logo2.png" style={{marginLeft: "73px", width: "175px"}}></img></a>
                 <div style={{marginLeft: "114px", color: "darkgray", fontSize: "11px"}}>DIVISION MEDICA</div>
                 <div style={{marginLeft: "25px", fontSize: "12px"}}>Frente a Policlínica Nicaraguense Bolonia Managua, Nicaragua</div>
                 <div style={{marginLeft: "49px", fontSize: "12px"}}>Teléfono (505) 2268-0370 - Móvil (505) 8966-2946</div>
                 <div style={{marginLeft: "87px", fontSize: "12px"}}>E-mail: <u style={{color: "darkgray"}}>servicio@rivasopstaele.com</u></div>
              </div>
               <div style={divheardright2}>
                   <span style={{fontFamily: "Calibri (Body)"}}>Protocolo de Mantenimiento Preventivo</span>
                   <hr style={{backgroundColor: "black", height: "5px", width: "452px", marginLeft: "-5%"}}></hr>
                       <table>
                           <tbody>
                               <tr>
                                   <td>
                                       <div style={{fontSize: "14px", marginLeft: "-9px"}}>Orden No:</div>
                                   </td>
                                   <td>
                                       <div style={{fontSize: "14px", marginLeft: "35px"}}>{data.Id}</div>
                                   </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <div style={{fontSize: "14px", marginLeft: "-9px"}}>Periodo No:</div>
                                      </td>
                                      <td>
                                          <div style={{fontSize: "14px", marginLeft: "35px"}}>{data.period}</div>
                                      </td>
                                     </tr>
                             </tbody>
                        </table>
             </div>
            </div>
            <div style={wrap}>
              <div style={divheardleft}>
              <div style={{marginTop: "-20px", fontFamily: "Calibri (Body)"}}><strong>Información General</strong></div>
              <h5 style={titleHeader}><u>Información de Equipo:</u></h5>
                  <table>
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Sistema:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.system.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                               <td>
                               <div style={divTdLeft}>Marca:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.brand.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Model:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.model.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Número de Serie:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.serialNumber.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Revisión SW:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.releaseSw.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Fecha de Instalación:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.installationDate)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Garantia Vence:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.warrantyExpires)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                  </div>
              <div style={divheardright}>
              <h5 style={titleHeader}><u>Información del Cliente:</u></h5>
                  <table >
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Nombre:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.name.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Dirección:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                           <td>
                               <div style={divTdStyle}></div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(47,47) != "" ? data.address.substr(47,47) : "."}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Teléfono :</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.phone}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Fax:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.fax}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Contacto:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.contact.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>E-Mail:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.email.substr(0,47)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                 </div>
          </div>
      </div>
          <div className="page-margin-report" >
              <div style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Programa:</strong></u></div>
                  <div style={{width: "100%",}}>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "103px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Pos</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "433px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Contenido</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "125px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>ok</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "197px", display: "inline-block" }}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Material Anotaciones</div></div>
                  </div>
                  {this.getTableProgram(dataProt)}
            </div>
        </div>
    }
 }
