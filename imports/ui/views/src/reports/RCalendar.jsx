import React, { Component } from 'react';

let wrap = {
   width: "100%",
   borderCollapse: "collapse",
   marginTop: "16px"
 };

 let divheardleft2 = {
     width: "45%",
     //padding: "20px",
     display: "inline-block",
     marginLeft: "-18px",
     float: "initial",
     marginTop: "2px"
 };

 let divheardright2 = {
     width: "43%",
     //padding: "20px",
     display: "inline-block",
     marginRight: "0px",
     float: "right",
     marginTop: "-11px"
 };

 let innerDiv =
 {
   position: "relative",
   transform: "translate(0px, -50%)",
   top: "14%"
 }

export class RCalendar extends Component
{
     constructor(props)
     {
       super(props);
     }

     render()
     {
        return (<div>
            <div className="rr-header">
            <div className="divHeaderImage" style={wrap}>
              <div style={divheardleft2}>
                 <a><img src="/img/logo2.png" style={{marginLeft: "73px", width: "175px"}}></img></a>
                 <div style={{marginLeft: "114px", color: "darkgray", fontSize: "11px"}}>DIVISION MEDICA</div>
                 <div style={{marginLeft: "25px", fontSize: "12px"}}>Frente a Policlínica Nicaraguense Bolonia Managua, Nicaragua</div>
                 <div style={{marginLeft: "49px", fontSize: "12px"}}>Teléfono (505) 2268-0370 - Móvil (505) 8966-2946</div>
                 <div style={{marginLeft: "87px", fontSize: "12px"}}>E-mail: <u style={{color: "darkgray"}}>servicio@rivasopstaele.com</u></div>
              </div>
               <div style={divheardright2}>
                   <span style={{fontFamily: "Calibri (Body)"}}>Calendario de Mantenimiento</span>
                   <hr style={{backgroundColor: "black", height: "5px", width: "452px", marginLeft: "-5%"}}></hr>
                </div>
                <div style={{textAlign: "center", fontSize: "18px"}} >Diciembre 2018</div>
                <div style={{marginTop: "15px"}}>Ingeniero: Walter Pineda</div>
                    <div style={{width: "100%"}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "19px", width: "170px", display: "inline-block", textAlign: "center", backgroundColor: "#eaeaea"}}><div style={{fontWeight: "bold", marginTop: "3px", fontSize: "11px" }}>Lunes</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "19px", width: "170px", display: "inline-block", textAlign: "center", backgroundColor: "#eaeaea"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "3px", fontSize: "11px"}}>Martes</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "19px", width: "170px", display: "inline-block", textAlign: "center", backgroundColor: "#eaeaea"}}><div style={{fontWeight: "bold", marginTop: "3px", fontSize: "11px"}}>Miercoles</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "19px", width: "170px", display: "inline-block", textAlign: "center", backgroundColor: "#eaeaea"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "3px", fontSize: "11px"}}>Jueves</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "19px", width: "170px", display: "inline-block", textAlign: "center", backgroundColor: "#eaeaea"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "3px", fontSize: "11px"}}>Viernes</div></div>
                    </div>
        </div>
        <div className="page-margin-report" >
            {this.displayCalendar()}
        </div>
            </div>
            </div>);
     }

    numAttrs(obj)
    {
       let count = 0;

       for(let key in obj)
        {
            if (obj.hasOwnProperty(key))
            {
                ++count;
            }
        }

      return count;
    }

   displayCalendar()
   {
         let htmlContent ="";
         let FebNumberOfDays ="";
         let  counter = 1;
         let dateNow =   new Date("April" +' 1 ,'+"2018");
         let month = dateNow.getMonth();

         let nextMonth = month+1; //+1; //Used to match up the current month with the correct start date.
         let prevMonth = month -1;
         let day = 1;
         let year = dateNow.getFullYear();

      //Determing if February (28,or 29)
      if (month == 1)
      {
         if ((year%100!=0) && (year%4==0) || (year%400==0))
         {
           FebNumberOfDays = 29;
         }else{
           FebNumberOfDays = 28;
         }
      }

      // names of months and week days.
      let monthNames = ["January","February","March","April","May","June","July","August","September","October","November", "December"];
      let dayNames = ["Sunday","Monday","Tuesday","Wednesday","Thrusday","Friday", "Saturday"];
      let dayPerMonth = ["31", ""+FebNumberOfDays+"","31","30","31","30","31","31","30","31","30","31"]

      // days in previous month and next one , and day of week.
      var nextDate = new Date(nextMonth +' 1 ,'+year);
      let weekdays= nextDate.getDay();
      let weekdays2 = weekdays;
      let numOfDays = dayPerMonth[month];

       let records = [];
       let objet = {};

       for(indice = 1; indice <= numOfDays; indice++)
           {
               let dateFor = new Date("April" + ' ' +  indice + ','+"2018");

               if(dateFor.getDay() == 1 &&  indice == 1)
               {
                    objet['' + dayNames[0]] = ".";
                    objet['' + dayNames[dateFor.getDay()]] = indice;

               }else if(dateFor.getDay() == 2 &&  indice == 1)
               {
                   objet['' + dayNames[0]] = ".";
                    objet['' + dayNames[1]] = ".";
                    objet['' + dayNames[dateFor.getDay()]] = indice;

               }else if(dateFor.getDay() == 3 &&  indice == 1)
               {
                    objet['' + dayNames[0]] = ".";
                    objet['' + dayNames[1]] = ".";
                    objet['' + dayNames[2]] = ".";
                    objet['' + dayNames[dateFor.getDay()]] = indice;

                }else if(dateFor.getDay() == 4 &&  indice == 1)
                 {
                      objet['' + dayNames[0]] = ".";
                      objet['' + dayNames[1]] = ".";
                      objet['' + dayNames[2]] = ".";
                      objet['' + dayNames[3]] = ".";
                      objet['' + dayNames[dateFor.getDay()]] = indice;

                 }else if(dateFor.getDay() == 5 &&  indice == 1)
                 {
                     objet['' + dayNames[0]] = ".";
                     objet['' + dayNames[1]] = ".";
                     objet['' + dayNames[2]] = ".";
                     objet['' + dayNames[3]] = ".";
                     objet['' + dayNames[4]] = ".";
                     objet['' + dayNames[dateFor.getDay()]] = indice;

                 }else if(dateFor.getDay() == 6 &&  indice == 1)
                 {
                     objet['' + dayNames[0]] = ".";
                     objet['' + dayNames[1]] = ".";
                     objet['' + dayNames[2]] = ".";
                     objet['' + dayNames[3]] = ".";
                     objet['' + dayNames[4]] = ".";
                     objet['' + dayNames[5]] = ".";
                     objet['' + dayNames[dateFor.getDay()]] = indice;
                 }
                 else
                 {
                    objet['' + dayNames[dateFor.getDay()]] = indice;
                 }

               if(this.numAttrs(objet) > 6)
               {
                   records.push(objet);
                   objet = {};
               }else if(indice == numOfDays)
               {
                   records.push(objet);
               }
           }

       componentTable = records.map(function(item)
       {
           return (<div key={"item" + item.Monday} style={{width: "100%"}}>
               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black",  width: "170px", display: "inline-block", textAlign: "left" }}><div style={{marginLeft: "5px", marginTop: "3px", fontSize: "11px" }}>{item.Monday != null ? item.Monday : "." }</div>
                <div style={innerDiv}>CT Ingenuity</div>
                <div style={innerDiv}>728323-320120</div>
                <div style={innerDiv}>Hospital Vivian Pellas</div>
                <div style={innerDiv}>----------------------</div>
                    <div style={innerDiv}>CT Ingenuity</div>
                    <div style={innerDiv}>728323-320120</div>
                    <div style={innerDiv}>Hospital Vivian Pellas</div>
                </div>
               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black",  width: "170px", display: "inline-block", textAlign: "left" }}><div style={{marginLeft: "5px", marginTop: "3px", fontSize: "11px"}}>{item.Tuesday != null ? item.Tuesday : "."}</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>----------------------</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
               </div>
               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black",  width: "170px", display: "inline-block", textAlign: "left" }}><div style={{marginLeft: "5px", marginTop: "3px", fontSize: "11px"}}>{item.Wednesday != null ? item.Wednesday: "." }</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>----------------------</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
               </div>
               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black",  width: "170px", display: "inline-block", textAlign: "left" }}><div style={{marginLeft: "5px", marginTop: "3px", fontSize: "11px"}}>{item.Thrusday != null ? item.Thrusday : "."}</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>.</div>
               <div style={innerDiv}>----------------------</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
                   <div style={innerDiv}>.</div>
               </div>
               <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black",  width: "170px", display: "inline-block", textAlign: "left" }}><div style={{marginLeft: "5px", marginTop: "3px", fontSize: "11px"}}>{item.Friday != null ? item.Friday : "."}</div>
                 <div style={innerDiv}>CT Ingenuity</div>
                 <div style={innerDiv}>.</div>
                 <div style={innerDiv}>.</div>
                 <div style={innerDiv}>----------------------</div>
                     <div style={innerDiv}>.</div>
                     <div style={innerDiv}>.</div>
                     <div style={innerDiv}>.</div>
                 </div>
           </div>);
       });

     return componentTable;
 }
}
