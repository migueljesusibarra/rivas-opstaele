import React, { Component } from 'react';
import { Util } from '/lib/common.js';

let divTdStyle = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divTdLeft = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    marginLeft: "10px",
    marginRight: "95px",
    padding: "0.2em",
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
};

let divheardleft = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    border: "1px solid black",
    marginLeft: "0px",
    float: "initial",
    marginTop: "13px"
};

let divheardleft2 = {
    width: "45%",
    //padding: "20px",
    display: "inline-block",
    marginLeft: "-18px",
    float: "initial",
    marginTop: "2px"
};

let divheardright = {
    width: "45%",
	//padding: "20px",
	display: "inline-block",
    border: "1px solid black",
    marginRight: "0px",
    float: "right",
    marginTop: "11px"
};

let divheardright2 = {
    width: "43%",
    //padding: "20px",
    display: "inline-block",
    marginRight: "0px",
    float: "right",
    marginTop: "-11px"
};

let wrap = {
   width: "100%",
   borderCollapse: "collapse",
   marginTop: "16px"
 };

let titleHeader = {
   textAlign: "left",
   marginBottom: "9px",
   marginTop: "5px",
   marginLeft: "13px"
};

let tdStyleCode = {
   color: "#000000",
   cursor: "pointer",
   padding: ".5em",
   whiteSpace: "nowrap",
   fontSize:"12px",
   border: "1px solid black",
   borderCollapse: "collapse",
   width: "210px"
};

let tdStyleQuantity = {
color: "#000000",
cursor: "pointer",
padding: ".5em",
whiteSpace: "nowrap",
fontSize:"12px",
border: "1px solid black",
borderCollapse: "collapse",
width: "150px"
};

let tdStyleDescription = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "790px",
    textAlign: "left"
};

let tdStyleVerificationDescription = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "550px",
        textAlign: "left"
    };

    let tdStyleMissingDescription = {
            color: "#000000",
            cursor: "pointer",
            padding: ".5em",
            whiteSpace: "nowrap",
            fontSize:"12px",
            border: "1px solid black",
            borderCollapse: "collapse",
            width: "900px",
            textAlign: "left"
        };


let tdStyleSerie = {
        color: "#000000",
        cursor: "pointer",
        padding: ".5em",
        whiteSpace: "nowrap",
        fontSize:"12px",
        border: "1px solid black",
        borderCollapse: "collapse",
        width: "250px",
        textAlign: "left"
    };

let tdStyleActivity = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "360px",
    textAlign: "center"
};

let tdStyleQuantityPart = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "96px",
    textAlign: "center"
};

let tdStyleDescriptionPart = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "468px",
    textAlign: "center"
};

let tdStyleNSPart = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "207px",
    textAlign: "center"
};

let tdStyleActivityDate = {
    color: "#000000",
    cursor: "pointer",
    padding: ".5em",
    whiteSpace: "nowrap",
    fontSize:"12px",
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "175px",
    textAlign: "center"
};

let tableStyle = {
  //width: "100%",
  border: "1px solid black",
  borderCollapse: "collapse",
  margin: "0 auto"
}

let divTdStyleList = {
    whitespace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    textAlign: "center",
}

let flexContainer = {
   padding: "0",
   margin: "0",
   listStyle: "none",
   display: "-webkit-box",
   display: "-moz-box",
   display: "-ms-flexbox",
   display: "-webkit-flex",
   display: "flex",
   alignItems: "center",
   justifyContent: "center"
}

let rowPass = {
    width: "100%"
}

let textFonsizeLeft = {
    fontSize: "11px",
    marginLeft: "-50px",
    fontFamily: "Calibri (Body)"
}

let textFonsize = {
    fontSize: "11px",
    fontFamily: "Calibri (Body)"
}


export class RServiceOrder extends Component
 {
     constructor(props)
     {
        super(props);
    }

    getTableActivity(records)
    {
        let tableAct = (<div>Not Data</div>);

        if(records.length > 0)
        {
          tableAct = records.map(function(item)
          {
            return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "325px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.description}</div></div>
                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "130px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{Util.getDateISO(item.date)}</div></div>
                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.beginHour}</div></div>
                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.endHour}</div></div>
                     <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "240px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.note != "" ? item.note : "."}</div></div>
                    </div>);
          });
      }else{
          tableAct =  (<div key={"item-ACT"} style={{width: "100%"}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "325px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "130px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "240px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                    </div>);
      }

          return tableAct;
    }

    getTablePart(records)
    {
        let tablePart = (<div>Not Data</div>);

        if(records.length)
        {
          tablePart = records.map(function(item)
          {
            return (<div key={"item-"+ item._id} style={{width: "100%"}}>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.quantity}</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "145px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.number}</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "334px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}>{item.description}</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.serialNumberOld != "" ? item.serialNumberOld : "."}</div></div>
                      <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}>{item.serialNumberNew != "" ? item.serialNumberNew : "."}</div></div>
                   </div>);
         });
      }else{
         tablePart = (<div key={"item-PRT"} style={{width: "100%"}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black", height: "26px", width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px", width: "145px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "334px", display: "inline-block"}}><div style={{marginLeft: "5px", marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", height: "26px",  width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginTop: "7px", fontSize: "11px"}}></div></div>
                    </div>);
     }

       return tablePart;
   }

    getLinesTextArea(str)
    {
        let text = str.split("\n");
        let count = str.split("\n").length;

        for(var i = 0;i < text.length;i++){
            if(text[i].length > 130)
                count += (text[i].match(/.{1,130}/g).length);
        }

        return count;
    }

   setTextArea(str, type)
   {
       let records = this.getTextOfLine(str);
       let count = 0;
       let textArea = records.map(function(item)
       {
          count = count + 1;
          return (<div key={type.toString() + count.toString() + item.id} style={{fontSize: "11px", marginTop: "3px"}}>{item.value}</div>)
       });

       return textArea;
   }

    getTextOfLine(str)
    {
        let text = str.split("\n");
        let arrayText = []

        for(var i = 0;i < text.length; i++)
        {
            if(text[i].length > 130)
            {
                let textM = text[i].match(/.{1,130}/g);

                for(var x = 0;x < textM.length; x++)
                {
                   arrayText.push({id: "0" + x + i,  value: textM[x]});
                }
            }
            else
            {
              arrayText.push({id: "0" + i, value: text[i]});
            }
        }

        return arrayText;
    }

    render()
    {
        let data = this.props.record;
        let lines = this.getLinesTextArea(data.note);
        let linesFail = this.getLinesTextArea(data.descriptionFailure);
        let dataAcv = this.props.activitiesService;
        let datapart = this.props.serviceOrderParts;

        return <div>
            <div className="rr-header">
            <div className="divHeaderImage" style={wrap}>
              <div style={divheardleft2}>
                 <a><img src="/img/logo2.png" style={{marginLeft: "73px", width: "175px"}}></img></a>
                 <div style={{marginLeft: "114px", color: "darkgray", fontSize: "11px"}}>DIVISION MEDICA</div>
                 <div style={{marginLeft: "25px", fontSize: "12px"}}>Frente a Policlínica Nicaragüense Bolonia Managua, Nicaragua</div>
                 <div style={{marginLeft: "49px", fontSize: "12px"}}>Teléfono (505) 2268-0370 - Móvil (505) 8966-2946</div>
                 <div style={{marginLeft: "87px", fontSize: "12px"}}>E-mail: <u style={{color: "darkgray"}}>servicio@rivasopstaele.com</u></div>
              </div>
               <div style={divheardright2}>
                   <span style={{fontFamily: "Calibri (Body)"}}>Orden de Servicio No {data.Id}</span>
                   <hr style={{backgroundColor: "black", height: "5px", width: "452px", marginLeft: "-5%"}}></hr>
                       <table>
                           <tbody>
                               <tr>
                                   <td>
                                       <div style={{fontSize: "11px", marginLeft: "-9px"}}>Fecha de Apertura:</div>
                                   </td>
                                   <td>
                                       <div style={{fontSize: "11px", marginLeft: "30px"}} >{Util.getDateISO(data.beginDate)}</div>
                                   </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style={{fontSize: "11px", marginLeft: "-9px"}}>Fecha de Cierre:</div>
                                    </td>
                                    <td>
                                        <div style={{fontSize: "11px", marginLeft: "30px"}}>{Util.getDateISO(data.endDate)}</div>
                                    </td>
                                 </tr>
                                <tr>
                                    <td>
                                       <div style={{fontSize: "11px", marginLeft: "-9px"}}>Responsable:</div>
                                   </td>
                                   <td>
                                       <div style={{fontSize: "11px", marginLeft: "30px"}}>{data.technician}</div>
                                 </td>
                                  </tr>
                                  <tr>
                                      <td>
                                         <div style={{fontSize: "11px", marginLeft: "-9px"}}>Tipo de Orden:</div>
                                     </td>
                                     <td>
                                         <div style={{fontSize: "11px", marginLeft: "30px"}}>{data.orderType}</div>
                                   </td>
                                    </tr>
                             </tbody>
                        </table>
           </div>
            </div>
            <div style={wrap}>
              <div style={divheardleft}>
              <div style={{marginTop: "-20px", fontFamily: "Calibri (Body)"}}><strong>Información General</strong></div>
              <h5 style={titleHeader}><u>Información de Equipo:</u></h5>
                  <table>
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Sistema:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.system.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                               <td>
                               <div style={divTdLeft}>Marca:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.brand.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Model:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.model.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Número de Serie:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.serialNumber.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Revisión SW:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{data.releaseSw.substr(0,40)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Fecha de Instalación:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.installationDate)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdLeft}>Garantia Vence:</div>
                              </td>
                              <td>
                               <div style={textFonsizeLeft}>{Util.getDateISO(data.warrantyExpires)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                  </div>
              <div style={divheardright}>
              <h5 style={titleHeader}><u>Información del Cliente:</u></h5>
                  <table >
                      <tbody>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Nombre:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.name.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Dirección:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                           <td>
                               <div style={divTdStyle}></div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.address.substr(47,47) != "" ? data.address.substr(47,47) : "."}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Teléfono :</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.phone}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Fax:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.fax}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>Contacto:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.contact.substr(0,47)}</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                               <div style={divTdStyle}>E-Mail:</div>
                              </td>
                              <td>
                               <div style={textFonsize}>{data.email.substr(0,47)}</div>
                              </td>
                          </tr>
                      </tbody>
                      </table>
                 </div>
          </div>
      </div>
          <div className="page-margin-report" >
            <div style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Descripción de Falla:</strong></u></div>
             {this.setTextArea(data.descriptionFailure, "failure")}
            <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Actividades:</strong></u></h4>
               <div style={{width: "100%"}}>
                   <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "325px", display: "inline-block"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Descripción</div></div>
                   <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "130px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Fecha</div></div>
                   <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Inicio</div></div>
                   <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "100px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Fin</div></div>
                   <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "240px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Notas</div></div>
               </div>
               {this.getTableActivity(dataAcv)}
                <h4 style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Partes:</strong></u></h4>
                    <div style={{width: "100%"}}>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderLeft: "1px solid black",  borderTop: "1px solid black", height: "26px", width: "100px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px" }}>Cantidad</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "145px", display: "inline-block", textAlign: "center"}}><div style={{fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Número</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "334px", display: "inline-block", textAlign: "left"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Descripción</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Antiguo NS</div></div>
                        <div style={{borderRight: "1px solid black", borderBottom: "1px solid black", borderTop: "1px solid black", height: "26px", width: "155px", display: "inline-block", textAlign: "center"}}><div style={{marginLeft: "5px", fontWeight: "bold", marginTop: "7px", fontSize: "11px"}}>Nuevo NS</div></div>
                    </div>
                     {this.getTablePart(datapart)}
               <div style={{marginBottom: "5px", marginTop: "25px"}} ><u><strong>Diagnóstico/observaciones:</strong></u></div>
                  {this.setTextArea(data.note, "diagnosis")}
              <div style={{marginBottom: "5px", marginTop: "100px"}}>
               <div  style={{width: "100%", borderCollapse: "collapse"}}>
                   <div style={{width: "50%", display: "inline-block", marginLeft: "-18px",  marginTop: "20px"}}>
                     <hr style={{backgroundColor: "black", height: "0px", width: "390px", marginLeft: "4%"}} />
                     <span style= {{marginLeft: "127px"}}><strong>Nombre, Firma, Sello y Fecha </strong></span>
                     <span style= {{marginLeft: "180px"}}><strong>Cliente</strong></span>
                   </div>
                   <div style= {{width: "50%", display: "inline-block", marginRight: "0px",  marginTop: "20px"}}>
                       <hr style= {{backgroundColor: "black", height: "0px", width: "385px", marginLeft: "4%"}} />
                       <span style= {{marginLeft: "127px"}}><strong>Nombre, Firma, Sello y Fecha </strong></span>
                       <span style= {{marginLeft: "180px"}}><strong>FSE</strong></span>
                   </div>
               </div>
              </div>

          </div>

        </div>
    }
 }
