import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField, PhoneField, FormPanel,Fieldset,FormColumn,NumberField } from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { PurchaseOrderDetails } from '/imports/api/purchaseOrderDetails/collection.js';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class PurchaseOrderDetail extends Component
{
    constructor(props)
    {
        super(props);

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.handlepaste = this.handlepaste.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);

    }

    componentDidMount()
    {
        document.getElementById("pasteId").addEventListener('paste', this.handlepaste);
        document.getElementById("pasteId").setAttribute("contentEditable", true);
    }

    componentWillUnmount()
    {
        document.getElementById("pasteId").removeEventListener('paste', this.handlepaste);
        document.getElementById("pasteId").setAttribute("contentEditable", false);
    }

    removeAllPurchaseOrderDetail(purchaseOrderId)
    {
        this.refs.confirmDialog.setTitle('Está seguro que desea eliminar todos los detalles?"');
        this.refs.confirmDialog.onOpenAndCustomValue(purchaseOrderId, 'removeAll');
    }

    removePurchaseOrderDetail(purchaseOrderDetail)
    {
        this.refs.confirmDialog.setTitle('Está seguro que desea eliminar el detalle?"');
        this.refs.confirmDialog.onOpenAndCustomValue(purchaseOrderDetail, 'removeSingle');
    }

    onOkConfirmDialog()
    {
        let self = this;
        let confirmType = this.refs.confirmDialog.getType();

        if(confirmType == "removeSingle")
        {
            let purchaseOrderDetail = this.refs.confirmDialog.getValue();

            Meteor.call("removePurchaseOrderDetail", purchaseOrderDetail._id, function(error, result){});
        }
        else
            if(confirmType == "removeAll")
            {
                let purchaseOrderId = this.refs.confirmDialog.getValue();

                Meteor.call("removeAllPurchaseOrderDetail", purchaseOrderId, function(error, result){});
            }
    }

    handlepaste(e)
    {
        let self = this;
        let id = e.currentTarget.getAttribute("id");

        if (e && e.clipboardData && e.clipboardData.types && e.clipboardData.getData && id == "pasteId")
        {
            // Check for 'text/html' in types list. See abligh's answer below for deatils on
            // why the DOMStringList bit is needed
            types = e.clipboardData.types;

            if (((types instanceof DOMStringList) && types.contains("text/html")) ||
            (types.indexOf && types.indexOf('text/html') !== -1))
            {
                // Extract data and pass it to callback
                arrayData = self.getRecordPaste(e.clipboardData.getData('text'));
                this.savePastedExcel(this.getDataCollection(arrayData));

                e.stopPropagation();
                e.preventDefault();
                return false;
            }
        }
    }

    getDataCollection(arrayData)
    {
        let data = [];

        if(arrayData.length > 0)
        {
            for(var i=0; i<arrayData.length; i++)
            {

                let quantity = arrayData[i][1];
                let  code = arrayData[i][2];
                let description = arrayData[i][3];

                data.push({
                    purchaseOrderId: this.props.purchaseOrderId,
                    quantity:quantity ,
                    code: code,
                    description: description
                });
            }
        }

        return data;
    }

    getRecordPaste(pastedData)
    {
        pastedData = pastedData.split("\n");

        for (var i = 0; i < pastedData.length; i++)
        {
            pastedData[i] = pastedData[i].split("\t");

            // Check if last row is a dummy row
            if (pastedData[pastedData.length - 1].length == 1 && pastedData[pastedData.length - 1][0] == "")
                pastedData.pop();
            //remove empty data
            if (pastedData.length == 1 && pastedData[0].length == 1 && (pastedData[0][0] == "" || pastedData[0][0] == "\r"))
                pastedData.pop();
        }

        return pastedData;
    }


    onRowDoubleClick(record)
    {
        if(this.props.status != 'Aprobado')
            this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit', record);
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Pos',
            //dataIndex: 'pos',
            minWidth: 10,
            align:'center',
            renderer: function(record)
            {
                return record.pos;
            }
        },
        {
            key: 2,
            header: 'Quantity',
            align:'center',
            minWidth: 30,
            //dataIndex: 'quantity'
            renderer: function(record)
            {
                return record.quantity;
            }
        },
        {
            key: 3,
            header: 'ABC Code',
            minWidth: 30,
            align:'center',
            //dataIndex: 'code'
            renderer: function(record)
            {
                return record.code;
            }
        },
        {
            key: 4,
            header: 'Description of Products',
            minWidth: 100,
            //dataIndex: 'description'
            renderer: function(record)
            {
                return record.description;
            }
        }];

        return columns;
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: [
            {
                text: 'Nuevo',
                cls: 'button-new',
                visible: this.props.status != 'In-House' && this.props.status != 'New',
                handler: function()
                {
                    self.openDialog('add', null);

                }
            },
            {
                text: 'Editar',
                cls: 'button-edit',
                visible: this.props.status != 'In-House' && this.props.status != 'New',
                handler: function()
                {
                    let record = self.refs.purchaseOrderDetailGrid.getSelectedRecord();

                    if(record != null)
                        self.openDialog('edit', record);
                    else
                        alert("Por favor seleccione un registro");
                }
            },
            {
                text: 'Eliminar',
                visible: this.props.status != 'In-House' && this.props.status != 'New',
                cls: 'button-delete',
                handler: function()
                {
                    let record = self.refs.purchaseOrderDetailGrid.getSelectedRecord();

                    if(record != null)
                        self.removePurchaseOrderDetail(record);
                    else
                        alert("Por favor seleccione un detalle.");
                }
            },
            {
                text: 'Eliminar Todo',
                visible: self.props.status != 'In-House' && self.props.status != 'New' && this.props.data.length > 1,
                cls: 'button-delete',
                handler: function()
                {
                    self.removeAllPurchaseOrderDetail(self.props.purchaseOrderId);
                }
            }]
        }];

        let toolbar = self.props.status != 'In-House' && self.props.status != 'New' ? <div><Toolbar className="north hbox" id="main-toolbar" items={items} /> </div>: "";

        return toolbar;
    }


    getGridPanel()
    {
        let data = this.props.data;
        let toolbar = null;

        return  (<GridPanel
        id="purchaseOrderDetailGrid"
        title="Detail"
        ref="purchaseOrderDetailGrid"
        columns={this.getColumns()}
        records={data}
        toolbar={toolbar}
        onRowDoubleClick={this.onRowDoubleClick} />);
    }

    savePastedExcel(array)
    {

        Meteor.call("addBacthPurchaseOrderDetail", array, function(error, result)
        {
            if(!error)
                console.log("succes")
            else
                throw alert(error);
        });
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
            purchaseOrderId: self.props.purchaseOrderId,
            code: fields.code.getValue(),
            description: fields.description.getValue(),
            quantity: fields.quantity.getValue()
            }

            if(recordId.length > 0)
                Meteor.call("updatePurchaseOrderDetail", recordId, data, function(error, result){});
            else
                Meteor.call("addPurchaseOrderDetail", data, function(error, result){});

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    getDialog()
    {
        let items = [
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        var toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (
            <Dialog ref="dialog" className="contact-dialog" title="Detail" width = "503px" modal>
                {toolbar}
                <FormPanel ref="form">
                    <Fieldset className="hbox">
                        <FormColumn className="hbox-purchases-column purchase-detail-dialog-column" style={{marginRight: "10px"}}>
                            <HiddenField ref="record" value="" />
                            <NumberField className = "form-field-left input" ref="quantity" label="Quantity" decimals={0} required />
                            <TextField ref="code" label="ABC Code" align = "left" required/>
                            <TextField ref="description" label="Description of Products" align = "left" required />
                        </FormColumn>
                    </Fieldset>
                </FormPanel>
            </Dialog>
        );
    }

    openDialog(action, record)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.code.setValue(record.code);
                fields.description.setValue(record.description);
                fields.quantity.setValue(record.quantity);
                fields.dialog.setTitle("Editar Detail");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.code.setValue('');
                fields.quantity.setValue('');
                fields.description.setValue('');
                fields.dialog.setTitle("Nuevo Detail");
                fields.dialog.open();
            }
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    getConfirmDialog()
    {
        return (<Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (
        <div>
            {this.getToolbar()}
            <div id = "pasteId">
                {this.getGridPanel()}
            </div>
            {this.getDialog()}
            {this.getConfirmDialog()}
        </div>
        );

    }
}
