import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { FormPanel, DisplayField, Container, Fieldset,
    FormColumn, NumberField, Toolbar, GridPanel,
    TextField, Dropdown, DateField, NoteField} from 'react-ui';
import { Util } from '/lib/common.js';
import { PurchaseOrderDetail } from './PurchaseOrderDetail.jsx'
import { PurchaseOrderDetailContainer } from '../../../containers/PurchaseOrderDetailContainer'
import { PurchaseOrderPartContainer } from '../../../containers/PurchaseOrderPartContainer'
import { EquipmentLookup } from '/imports/ui/components/lookup/equipment/EquipmentLookup.jsx';
import { PurchaseOrders } from '../../../../api/purchaseOrders/collection.js';
import { Models } from '../../../../api/models/collection.js';
import { Customers } from '../../../../api/customers/collection.js';
import { CustomerTypes } from '../../../../api/customerTypes/collection.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { AttachmentContainer } from '../../../containers/AttachmentContainer';
import { AttachmentForm } from '/imports/ui/components/src/AttachmentForm.jsx';
import { PurchaseOrderDetails } from '../../../../api/purchaseOrderDetails/collection.js';
import { TechnicianListSendEmailContainer } from '/imports/ui/containers/TechnicianContainerList.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class PurchaseOrderFormSystem extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            data: this.getDataStateProps(props.data, this.props.models),
            disabledModel: (props.isNew || props.data.status == "In-House"),
            disabledOrderClass: (props.isNew || props.data.status == "In-House"),
            disabledSerialNumber: (props.isNew || props.data.status == "In-House"),
            disabledField: props.data.status == "In-House",
            requiredField: this.isNew || this.props.customerType != "Stock",
            receptionEquipmentId: this.props.receptionEquipmentId
        };

        this.onChangeCustomer = this.onChangeCustomer.bind(this);
        this.onChangeBrand = this.onChangeBrand.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.toggleAprove  = this.toggleAprove.bind(this);
        this.onPurchaseOrderAdded = this.onPurchaseOrderAdded.bind(this);
        this.closeDialogTechnicianSendEmails = this.closeDialogTechnicianSendEmails.bind(this);
        this.getToast = this.getToast.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    componentWillReceiveProps(nextProps)
    {
        let models = Models.find({brandId: nextProps.data.brandId}).fetch() || [];

        this.setState({
            data: this.getDataStateProps(nextProps.data, models),
            disabledModel: (nextProps.isNew || nextProps.data.status == "In-House"),
            disabledOrderClass: (nextProps.isNew || nextProps.data.status == "In-House"),
            disabledField: nextProps.data.status == "In-House",
            receptionEquipmentId: this.props.receptionEquipmentId
        });
    }

    getDataStateProps(record, models)
    {
        let data = Object.assign({
        models: this.getModels(models || []),
        dataOrderClass: this.getOrderClass()
        }, record);

        return data;
    }

    getEquipmentData(seriealNumber)
    {
        return PurchaseOrders.findOne({"equipment.serialNumber" : seriealNumber});
    }

    getToolbarItems()
    {
        let self = this;

        let items =
        {
            back:
            {
                text: '',
                cls: 'button-back',
                handler: function()
                {
                    FlowRouter.go('/purchaseordersystems');
                }
            },
            new:
            {
                text: 'Nuevo',
                cls: 'button-new',
                handler: function()
                {
                FlowRouter.go('/purchaseordersystem/add');
                }
            },
            save:
            {
                text: 'Guardar',
                cls: 'button-save',
                //visible: this.props.data.status != 'Aprobado',
                handler: function()
                {
                    self.saveChanges("save");
                }
            },
            close:
            {
                key: 'close',
                text: 'Cerrar',
                cls: 'button-close',
                handler: function()
                {
                    FlowRouter.go('/purchaseordersystems');
                }
            },
            toogle:
            {
                text: self.props.data.status === "Dispotded" ? 'In-House' : 'Dispotded',
                cls: 'button-approval',
                visible: self.props.data.status != 'In-House',
                handler: function()
                {
                    if(self.props.data.status === "In-House")
                        self.getToast().showMessage("info", "La compra está en estado In-House.");
                    else
                        if(self.props.totalDetail === 0 && self.props.data.status === "Dispotded")
                            self.getToast().showMessage("info", "Usted no puede cambiar el estado esta compra porque no tiene detalles registrado.");
                    else
                        if(self.props.data.status == "In-Process" && (self.props.data.equipment.awb == "" || self.props.data.equipment.awb == null))
                            self.getToast().showMessage("info", "Para este cambio de estado necesita el siguiente campo (A. w. B #).");
                    else
                        if(self.props.data.status == "Dispotded" && (self.props.data.equipment.inhDate == "" || self.props.data.equipment.inhDate == null))
                            self.getToast().showMessage("info", "Para este cambio de estado necesita el siguiente campo (INH Date).");
                     else
                         if(!self.props.isNew)
                             self.toggleAprove();
                }
            },
            generate:
            {
                text: 'Crear Recepción',
                cls: 'button-generate',
                handler: function()
                {
                    if(self.props.data.hasReceptionEquipment === false)
                    {
                        if(self.props.data.status != "In-House")
                        {
                            self.getToast().showMessage("error", "Usted no puede crear una recepción de esta compra, antes la compra debe ser aprobada.");
                        }
                        else
                            self.toggleReceptionEquipment(self.props.data.status);
                    }
                    else
                        self.getToast().showMessage("info", "Existe una recepción para esta compra");
                }
            }
        }

        return items;
    }

    getToolbar()
    {
        let self = this;
        let actions = this.getToolbarItems();
        leftGroup = [], rightGroup = [];

        if(this.props.isNew)
            leftGroup.push(actions.save);
        else
        {
            leftGroup.push(actions.new);
            leftGroup.push(actions.save);
            rightGroup.push(actions.toogle);
            rightGroup.push(actions.generate);
        }

        leftGroup.push(actions.close);

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: leftGroup
        },
        {
            type: 'group',
            className: 'hbox-r',
            items: rightGroup
        }];

        return (<Toolbar className="north hbox" items={items} />);
    }

    saveChanges(button)
    {
        let self = this;
        let fields = this.refs;

        if(fields.form.isValid(fields))
        {
            let equipment = self.getPropertiesEquipment(fields);
            let properties = self.getProperties(fields, equipment);

            if(self.props.isNew)
                Meteor.call("addPurchaseOrder", properties, this.onPurchaseOrderAdded);
            else
            {
                Meteor.call("updatePurchaseOrder", this.props.data._id, properties, function(error, result)
                {
                    if(!error)
                    {
                        self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");

                        if(button == "saveAndClose")
                            FlowRouter.go('/purchaseordersystems');
                    }else
                        throw self.getToast().showMessage("error", error);
                });
            }
        }
        else
            this.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
    }

    getProperties(fields, equipment)
    {
        //The fields that are empty is why they are for part type purchases
        let record = {
            type: fields.type.getValue(),
            orderClass: fields.orderClass.getValue(),
            supplierId: fields.supplier.getValue(),
            date: fields.date.getValue(),
            serviceOrderNr: "",
            customerId: fields.customer.getValue(),
            systemId: fields.system.getValue(),
            brandId: fields.brand.getValue(),
            modelId: fields.model.getValue(),
            serialNumber: fields.serialNumber.getValue(),
            equipmentId: "",
            inboundSAPOrderNr: "",
            incoterms: "",
            inFreightForwarder: "",
            inAwb: "",
            inInhDate: "",
            note: fields.note.getValue(),
            source: "",
            foreignId: "",
            equipment: equipment
        };

        return record;
    }


    onPurchaseOrderAdded(error, result)
    {
        let self = this;

        if(!error)
        {
            self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
            FlowRouter.go('/purchaseordersystem/' + result);
        }
        else
            self.getToast().showMessage("error", error);
    }

    getPurchaseOrderType()
    {
        return [{value: "1", text: "Sistema"}, {value: "2", text: "Parte"}]
    }

    getOrderClass()
    {
        return [{value: "1", text: "Alquiler"}, {value: "2", text:"Venta"}, {value: "3", text: "Demo"}, {value: "4", text: "Loaner"}]
    }

    getCustomers()
    {
        this.props.customers.sort(Util.applySortObject);

        return this.props.customers.map(function(customer)
        {
            return {value: customer._id, text: customer.name};
        });
    }

    getSystems()
    {
        this.props.systems.sort(Util.applySortObject);

        return this.props.systems.map(function(system)
        {
            return {value: system._id, text: system.description};
        });
    }

    getBrands()
    {
        this.props.brands.sort(Util.applySortObject);

        return this.props.brands.map(function(brand)
        {
            return {value: brand._id, text: brand.description};
        });
    }

    getVendors()
    {
        this.props.vendors.sort(Util.applySortObject);

        return this.props.vendors.map(function(vendor)
        {
            return {value: vendor._id, text: vendor.name};
        });
    }

    getPeriods()
    {
        this.props.periods.sort(Util.applySortObject);

        return this.props.periods.map(function(period)
        {
            return {value: period._id, text: period.description};
        });
    }

    getModels(models)
    {
        models.sort(Util.applySortObject);

        return models.map(function(model)
        {
            return {value: model._id, text: model.description};
        });
    }

    getCollectionModels(fields)
    {
        return  Models.find({brandId: fields.brand.getValue()}).fetch() || [];
    }

    getDataOrderClass(fields)
    {
        return fields.type.getValue() == "" ? [] : this.getOrderClass();
    }

    // onChangeType(value)
    // {
    //   let fields = this.refs;
    //   let dataOrderClass = value == "" ? [] : (fields.type.getValue() == 2 ? this.getOrderType() : this.getOrderClass());
    //   this.setState({ data: this.setDataState(fields, this.getCollectionModels(fields), dataOrderClass, "type"),
    //                   disabledOrderClass: (value == "" ? true : false),
    //                 });
    // }

    onChangeCustomer(value)
    {
        let customer = Customers.findOne({_id: value}) || [];
        let customerTypes = CustomerTypes.findOne({_id: customer.customerTypeId});
        let type = customerTypes.description;

        let fields = this.refs;

        this.setState({ data: this.setDataState(fields, this.getCollectionModels(fields), this.getDataOrderClass(fields), "customer", value),
        disabledSerialNumber: (value == "" ? true : false),
        requiredField: type != "Stock"});
    }

    onChangeBrand(value)
    {
        let fields = this.refs;
        let models = Models.find({brandId: value}).fetch() || [];
        this.setState({data: this.setDataState(fields, models, this.getDataOrderClass(fields), "brand", value), disabledModel: (value == "" ? true : false)});
    }

    // onChangModel(value)
    // {
    //   if(value != "" && value != null && this.refs.type.getValue() == 1)
    //   {
    //     let model = Models.findOne({_id: value}) || [];
    //     this.refs.article.setValue(model.article);
    //     this.refs.descriptionSystem.setValue(model.description);
    //   }
    // }

    setDataState(fields, models, dataOrderClass, action, dropdownValue)
    {

        let equipment = this.setDataStateEquipment(fields);
        let part = {};

        let data =
        {
            type: fields.type.getValue(),
            models: this.getModels(models),
            dataOrderClass : dataOrderClass,
            modelId: fields.model.getValue(),
            brandId: action  == 'brand' ? dropdownValue : this.refs.brand.getValue(),
            systemId: fields.system.getValue(),
            customerId: action  == 'customer' ? dropdownValue : this.refs.customer.getValue(),
            orderClass: fields.orderClass.getValue(),
            supplierId: fields.supplier.getValue(),
            date: fields.date.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.date.getValue()) : null,
            serialNumber: fields.serialNumber != null ?  fields.serialNumber.getValue() : "",
            equipment: equipment,
            part: part
        };

        return data
    }

    setDataStateEquipment(fields)
    {
        let inhDate = fields.inhDate != null ? (fields.inhDate.getValue().toString() != "Invalid Date" ? fields.inhDate.getValue() : null ) : null;
        let estInstallDate = fields.estInstallDate != null ? (fields.estInstallDate.getValue().toString() != "Invalid Date" ? fields.estInstallDate.getValue() : null) : null;

        let data =
        {
            salesForce: fields.salesForce != null ?  fields.salesForce.getValue() : "",
            zohoCRM: fields.zohoCRM != null ?  fields.zohoCRM.getValue() : "",
            sAPOrderNr: fields.sAPOrderNr != null ? fields.sAPOrderNr.getValue() : "",
            systemWty: fields.systemWty != null ? fields.systemWty.getValue() : "",
            clientWty: fields.clientWty != null ? fields.clientWty.getValue() : "",
            estInstallDate: estInstallDate,
            mfrInvoiceNr: fields.mfrInvoiceNr != null ? fields.mfrInvoiceNr.getValue() : "",
            mfrInvoiceDate: fields.mfrInvoiceDate != null ? fields.mfrInvoiceDate.getValue() : "",
            freightForwarder: fields.freightForwarder != null ? fields.freightForwarder.getValue() : "",
            awb: fields.awb != null ? fields.awb.getValue() : "",
            inhDate: fields.inhDate != null ? fields.inhDate.getValue() : ""
        }

        return data;
    }

    generateReceptionEquipment(purchaseOrderId)
    {
        let fields = this.refs;
        let self = this;

        let record =
        {
            customerId: fields.customer.getValue(),
            systemId: fields.system.getValue(),
            brandId: fields.brand.getValue(),
            modelId: fields.model.getValue(),
            serialNumber: fields.serialNumber.getValue(),
            releaseSw: "",
            installationDate: new Date(),
            warranty: fields.clientWty.getValue(),
            warrantyExpires: Util.sumMonthDate(fields.estInstallDate.getValue(), fields.clientWty.getValue()),
            frequency: "",
            source: "purchaseOrder",
            foreignId: purchaseOrderId,
            technicianId: ""
        };

        let equipment = self.getPropertiesEquipment(fields);
        let properties = self.getProperties(fields, equipment);


        Meteor.call("addReceptionEquipment", record, function(error, result)
        {
            if(!error)
            {
                let resposeReceptionEquipmentId = result;

                self.SaveConfiguration(resposeReceptionEquipmentId, purchaseOrderId, properties);
                self.getToast().showMessage("info", "La recepción de equipo se genero correctamente");
            }
            else
                throw self.getToast().showMessage("error", error);
        });

    }

    SaveConfiguration(receptionEquipmentId, purchaseOrderId, properties)
    {
        let self = this;
        let dataDetail = PurchaseOrderDetails.find({purchaseOrderId: purchaseOrderId}).fetch();
        let dataConfigurations = [];

        dataDetail.forEach(function(item)
        {
            let data = {
                receptionEquipmentId: receptionEquipmentId,
                code: item.code,
                quantity: item.quantity,
                description: item.description
            }

            dataConfigurations.push(data);
        });

        if(dataConfigurations.length)
        {
            Meteor.call("addConfigurations", dataConfigurations, function(error, result){
                if(!error)
                {
                    Meteor.call("updatePurchaseOrder", purchaseOrderId, properties, function(error, result)
                    {
                        if(error)
                        {
                          throw self.getToast().showMessage("error", error);
                        }
                    });

                    self.setState({receptionEquipmentId: receptionEquipmentId},
                        function()
                        {
                            self.refs.technicianLisSendEmailDialog.open();
                        }
                    );

                }
                else
                {
                    console.log(error);
                    //self.getToast().showMessage("error", error);
                }

            });
        }
    }

    getToast()
    {
        return this.refs.toastrMsg;
    }

    getPropertiesEquipment()
    {
        let fields = this.refs;

        let record = {
            salesForce: fields.salesForce.getValue(),
            zohoCRM: fields.zohoCRM.getValue(),
            sAPOrderNr: fields.sAPOrderNr.getValue(),
            systemWty: fields.systemWty.getValue(),
            clientWty: fields.clientWty.getValue(),
            estInstallDate: fields.estInstallDate.getValue(),
            mfrInvoiceNr: fields.mfrInvoiceNr.getValue(),
            mfrInvoiceDate: fields.mfrInvoiceDate.getValue().toString() == "Invalid Date" ? "" : fields.mfrInvoiceDate.getValue(),
            freightForwarder: fields.freightForwarder.getValue(),
            awb: fields.awb.getValue().toString() == "Invalid Date" ? "" : fields.awb.getValue(),
            inhDate: fields.inhDate.getValue().toString() == "Invalid Date" ? "" : fields.inhDate.getValue()
        }

        return record;
    }

    closeDialogTechnicianSendEmails()
    {
        this.refs.technicianLisSendEmailDialog.close();
    }

    onClickReceptionEquipment()
    {
        FlowRouter.go('/receptionequipment/'+ this.state.receptionEquipmentId);
    }

    renderLinkReceptionEquipment()
    {
        let linkReceptionEquipment = "";

        if(this.state.receptionEquipmentId != 0 && !this.props.isNew && this.props.data.status == "In-House")
        {
            linkReceptionEquipment = (<a
                                         onClick={this.onClickReceptionEquipment.bind(this)}
                                         href=""
                                         style={{color: 'blue', textDecoration: 'underline'}}>
                                         {'Recepción # '+this.state.receptionEquipmentId}
                                     </a>);
        }

        return linkReceptionEquipment;

    }
    getRenderGeneral()
    {
        let data = this.props.data;
        let dataState = this.state.data;
        let dataEquipment = this.props.isNew ? {} : this.state.data.equipment;
        let disableModel = this.state.disabledModel;
        let disabledField = this.state.disabledField;
        let disabledOrderClass = this.state.disabledOrderClass;
        let disabledSerialNumber = this.state.disabledSerialNumber;
        let requiredField = this.state.requiredField;

        let componentDetail = (<Fieldset className="hbox box-form">
        <FormColumn  title="Detail" className="hbox-l border-puchase-detail">
            <PurchaseOrderDetailContainer purchaseOrderId = {data._id} status = {data.status} />
        </FormColumn>
        </Fieldset>);


        let technicianListSendEmailContainer = (
        <Dialog
            ref="technicianLisSendEmailDialog"
            title="Ingenieros"
            modal>
            <TechnicianListSendEmailContainer recordId={this.state.receptionEquipmentId} module = "PurchaseOrderTypePart" close={this.closeDialogTechnicianSendEmails}/>
        </Dialog>);

        return(<Container className="vbox center">
            <div style={{textAlign: "center", color: "#004990"}}>
            <span style={{fontSize: "18px"}}>Compra de Tipo Sistema - </span>
            <span style={{fontSize: "18px", color: "#565656"}}>{this.props.data.status} </span>
           </div>
           {this.getToolbar()}
            <FormPanel ref="form">
                <ToastrMessage ref="toastrMsg" />
                <Fieldset className="hbox box-form">
                    <FormColumn className="hbox-purchases-column purchase-width-column1 " >
                        <TextField ref="purchaseOrderId"  className = "purchase-margin-label"  label="Purchase Order Nr" value={data._id}  disabled />
                        <Dropdown ref="type" label="Type" items={this.getPurchaseOrderType()} value="1" required={true} emptyOption={true} disabled = {true} />
                        <Dropdown  ref="orderClass" label="Order Type" items={dataState.dataOrderClass} value={dataState.orderClass} required={true} emptyOption={true} disabled = {disabledField} />
                        <Dropdown  ref="supplier" label="Supplier" items={this.getVendors()} value={dataState.supplierId} required={true} emptyOption={true} disabled = {disabledField}/>
                        <DateField  ref="date" className = "purchase-input-date" label="Date" value={dataState.date != null ? Util.getDateISO(dataState.date) : null} disabled = {disabledField} />
                        {this.renderLinkReceptionEquipment()}
                    </FormColumn>
                    <FormColumn className="hbox-purchases-column purchase-width-column2" >
                        <Dropdown ref="customer" label="Client" items={this.getCustomers()} value={dataState.customerId} onChange={this.onChangeCustomer.bind(this)} required={requiredField} emptyOption={true} disabled = {disabledField} />
                        <Dropdown className="hbox-l" ref="system" label="System Type" items={this.getSystems()} value={dataState.systemId} required={this.state.requiredField} emptyOption={true} disabled = {disabledField} />
                        <Dropdown className="hbox-l" ref="brand" label="Manufacturer" items={this.getBrands()} value={dataState.brandId} onChange={this.onChangeBrand.bind(this)} required={requiredField} emptyOption={true} disabled = {disabledField} />
                        <Dropdown className="hbox-r" ref="model" label="Model Type" items={dataState.models} value={dataState.modelId} required={requiredField} emptyOption={true} disabled = {disableModel} />
                        <TextField  ref="serialNumber" label="System Nr" value={dataState.serialNumber} />
                    </FormColumn>
                    <FormColumn className = "hbox-purchases-column purchase-width-column3 " >
                        <TextField ref="salesForce" label="SalesForce"  value={dataEquipment.salesForce}  />
                        <TextField ref="zohoCRM" label="ZohoCRM" value={dataEquipment.zohoCRM}  required = {true} disabled = {disabledField}/>
                        <TextField ref="sAPOrderNr" label="SAP Order Nr" value={dataEquipment.sAPOrderNr} required = {true} disabled = {disabledField} />
                        <TextField ref="systemWty" label="System Wty (m)"  value={dataEquipment.systemWty} required = {true} disabled = {disabledField}/>
                        <TextField ref="clientWty" label="Client Wty (m)" value={dataEquipment.clientWty} required = {true} disabled = {disabledField} />
                    </FormColumn>
                    <FormColumn className = "hbox-purchases-column purchase-width-column4" >
                        <DateField ref="estInstallDate"  className = "purchase-input-date" value={dataEquipment.estInstallDate != null ? Util.getDateISO(dataEquipment.estInstallDate) : null} label="Est. Install Date" required = {true} />
                        <TextField ref="mfrInvoiceNr" label="Mfr Invoice Nr" value={dataEquipment.mfrInvoiceNr}  />
                        <DateField ref="mfrInvoiceDate" className = "purchase-input-date"  label="Mfr Invoice Date" value={dataEquipment.mfrInvoiceDate != null && dataEquipment.mfrInvoiceDate != "" ? Util.getDateISO(dataEquipment.mfrInvoiceDate) : null} />
                        <TextField ref="freightForwarder" label="Freight Forwarder" value={dataEquipment.freightForwarder} />
                        <TextField ref="awb" label="A. W. B #" value={dataEquipment.awb} disabled={this.props.data.status == "New"} />
                        <DateField ref="inhDate" className = "purchase-input-date" label="INH Date" value={dataEquipment.inhDate != null && dataEquipment.inhDate != "" ? Util.getDateISO(dataEquipment.inhDate) : null} disabled={this.props.data.status == "New" || this.props.data.status == "In-Process"}/>
                    </FormColumn>
                </Fieldset>
                <Fieldset className="hbox box-form" style={{marginTop: '6px'}}>
                    <FormColumn className="hbox-l hbox-purchases-column purchase-width-column1">
                        <NoteField ref="note" label="Notas" value={dataState.note} />
                    </FormColumn>
                </Fieldset>
                {componentDetail}
                {technicianListSendEmailContainer}
            </FormPanel>
            {this.getConfirmDialog()}
        </Container>);
    }

    validateFieldsBeforedReceptionEquipment()
    {
        let empty = false;
        let fields = [
        'purchaseOrderId',
        'type',
        'orderClass',
        'supplier',
        'date',
        'customer',
        'system',
        'brand',
        'model',
        'serialNumber',
        'salesForce',
        'zohoCRM',
        'sAPOrderNr',
        'systemWty',
        'clientWty',
        'estInstallDate',
        'mfrInvoiceNr',
        'mfrInvoiceDate',
        'freightForwarder',
        'awb',
        'inhDate'
        ];

        let self = this;
        let emptyFields = "";

        Object.keys(this.refs).forEach(function(key, position){

            for(let field of fields)
            {
                if(field == key)
                {
                    let value = ""+self.refs[key].getValue()+"";

                    if(value.trim() == "")
                    {
                        empty = true;
                        emptyFields += `${self.refs[key].getLabel()}, `;
                    }
                }
            }
        });

        if(empty)
        {
            let message = `Para crear recepción los siguientes campos deben contener valor: \n${emptyFields}`;
            this.getToast().showMessage("error", message);
        }

        return empty;
    }

    toggleReceptionEquipment(status)
    {
        let isFieldEmpty = this.validateFieldsBeforedReceptionEquipment();

        if(!isFieldEmpty)
        {
            this.refs.confirmDialog.setTitle("Está seguro que desea crear recepción?");
            this.refs.confirmDialog.setType("reception");
            this.refs.confirmDialog.onOpenAndValue(status);
        }
    }

    toggleAprove()
    {
        this.refs.confirmDialog.setTitle("Está seguro que desea cambiar el estado de esta compra?");
        this.refs.confirmDialog.setType("dispotded");
        this.refs.confirmDialog.onOpen();
    }

    onOkConfirmDialog()
    {
        if(this.refs.confirmDialog.getType() == 'dispotded')
            Meteor.call("setApprove", this.props.data._id, this.props.data.status === "In-Process" ? "Dispotded" : "In-House");
        else
            if(this.refs.confirmDialog.getType() == 'reception')
            {
                let self = this;
                let status = this.refs.confirmDialog.getValue();

                if(this.props.data.hasReceptionEquipment === false && status === "In-House")
                {
                    Meteor.call("setReceptionEquipment", this.props.data._id, true, function(error, result)
                    {
                        if(!error)
                            self.generateReceptionEquipment(self.props.data._id);
                        else
                            self.getToast().showMessage("error", error);
                    });
                }
            }
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        okLabel="Si"
        noLabel="No"
        />);
    }

    render()
    {
        return (this.getRenderGeneral());
    }
}
