import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { FormPanel, DisplayField, Container, Fieldset,
    FormColumn, NumberField, Toolbar, GridPanel,
    Label, TextField, Dropdown, DateField,
    NoteField} from 'react-ui';
import { Util } from '/lib/common.js';
import { PurchaseOrderDetail } from './PurchaseOrderDetail.jsx'
import { PurchaseOrderDetailContainer } from '../../../containers/PurchaseOrderDetailContainer'
import { PurchaseOrderPartContainer } from '../../../containers/PurchaseOrderPartContainer'
import { EquipmentLookup } from '/imports/ui/components/lookup/equipment/EquipmentLookup.jsx';
import { PurchaseOrders } from '/imports/api/purchaseOrders/collection.js';
import { Models } from '/imports/api/models/collection.js';
import { Customers } from '/imports/api/customers/collection.js';
import { CustomerTypes } from '/imports/api/customerTypes/collection.js';
import { ToastrMessage } from '/imports/ui/components/src/ToastrMessage.jsx';
import { AttachmentContainer } from '../../../containers/AttachmentContainer';
import { AttachmentForm } from '/imports/ui/components/src/AttachmentForm.jsx';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class PurchaseOrderFormPart extends Component
{
        constructor(props)
        {
            super(props);

            this.state =
            {
                data: this.getDataStateProps(props.data, this.props.models),
                disabledModel: (props.isNew || props.data.status == "In-House" || (props.data.foreignId != null && props.data.foreignId != "")),
                disabledSerialNumber: (props.isNew || props.data.status == "In-House" || (props.data.foreignId != null && props.data.foreignId != "")),
                disabledField: props.data.status == "In-House",
                disabledforeignFiled: (props.data.status == "In-House" || (props.data.foreignId != null && props.data.foreignId != "")),
                disabledDetailField: true,
                requiredField: this.isNew || this.props.customerType != "Stock",
                typeAttach: ""
            }

            this.onChangeCustomer = this.onChangeCustomer.bind(this);
            this.onChangeBrand = this.onChangeBrand.bind(this);
            this.onChangeOrderClass = this.onChangeOrderClass.bind(this);
            this.onEquipmentChanged = this.onEquipmentChanged.bind(this);
            this.saveChanges = this.saveChanges.bind(this);
            this.toggleAprove  = this.toggleAprove.bind(this);
            this.onPurchaseOrderAdded = this.onPurchaseOrderAdded.bind(this);
            this.onAttachClick = this.onAttachClick.bind(this);
            this.onSelectDetail = this.onSelectDetail.bind(this);
            this.onUpdateDetail = this.onUpdateDetail.bind(this);
            this.UpdateDetail = this.UpdateDetail.bind(this);
            this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
    }

    componentWillReceiveProps(nextProps)
    {
      let models = Models.find({brandId: nextProps.data.brandId}).fetch() || [];

       this.setState({data: this.getDataStateProps(nextProps.data, models),
                      disabledModel: (nextProps.isNew || nextProps.data.status == "In-House" || (nextProps.data.foreignId != null && nextProps.data.foreignId != "")),
                      disabledField: nextProps.data.status == "In-House",
                      disabledforeignFiled: (nextProps.data.status == "In-House" || (nextProps.data.foreignId != null && nextProps.data.foreignId != "")),
                      });
    }

    onAttachClick(value)
    {
        this.setState({typeAttach: value});
        if(!this.state.disabledDetailField)
            this.refs.attachmentDialog.open();
        }

    onUpdateDetail()
    {
        this.UpdateDetail();
    }

    onEquipmentChanged(record)
    {
        let fields = this.refs;
        let equipmentData = this.getEquipmentData(record["serialNumber"]);

      if(equipmentData != null)
      {
         fields.salesForce.setValue(equipmentData.equipment.salesForce);
         fields.awb.setValue(equipmentData.equipment.awb);
         fields.clientWty.setValue(equipmentData.equipment.clientWty);
         fields.estInstallDate.setValue(equipmentData.equipment.estInstallDate != null ? Util.getDateISO(equipmentData.equipment.estInstallDate) : null);
         fields.freightForwarder.setValue(equipmentData.equipment.freightForwarder);
         fields.inhDate.setValue(equipmentData.equipment.inhDate != null ? Util.getDateISO(equipmentData.equipment.inhDate) : null);
         fields.mfrInvoiceDate.setValue(equipmentData.equipment.mfrInvoiceDate != null ? Util.getDateISO(equipmentData.equipment.mfrInvoiceDate) : null);
         fields.mfrInvoiceNr.setValue(equipmentData.equipment.mfrInvoiceNr);
         fields.sAPOrderNr.setValue(equipmentData.equipment.sAPOrderNr);
         fields.systemWty.setValue(equipmentData.equipment.systemWty);
         fields.zohoCRM.setValue(equipmentData.equipment.zohoCRM);
      }
    }

    onSelectDetail(record)
    {

     let fields = this.refs;

    if(record != null && record.returnable)
     {
       let models = Models.find({brandId: fields.brand.getValue()}).fetch() || [];
       this.setState({data: this.setDataState(fields, models, record , '', ''), disabledDetailField : false});

     }else
     {
          let record =
          {
              recordDetailId: "",
              outboundRMANr: "",
              outDate: "",
              outFreightForwarder: "",
              outAwb: "",
              outPOD: "",
              outReceiptDate: "",
              attachRMA: 0,
              attachPOD: 0,
              attachReturnInvoice: 0,
          }

         let models = Models.find({brandId: fields.brand.getValue()}).fetch() || [];
         this.setState({data: this.setDataState(fields, models, record, '', ''), disabledDetailField : true});
     }
    }

    getDataStateProps(record, models)
    {
      let data = Object.assign({
           models: this.getModels(models || []),
           recordDetailId: "",
           outboundRMANr: "",
           outDate: "",
           outFreightForwarder: "",
           outAwb: "",
           outPOD: "",
           outReceiptDate: "",
           attachRMA: 0,
           attachPOD: 0,
           attachReturnInvoice: 0,
           }, record);

      return data;
    }

    getEquipmentData(seriealNumber)
    {
       let purchaseOrder = PurchaseOrders.findOne({serialNumber: seriealNumber});
       return purchaseOrder;
    }

    getToolbarItems()
    {
      let self = this;

      let items =
      {
        back: {
          text: '',
          cls: 'button-back',
          handler: function(){
            FlowRouter.go('/purchaseorderparts');
          }
        },
       new:{
          text: 'Nuevo',
          cls: 'button-new',
          handler: function()
          {
            FlowRouter.go('/purchaseorderpart/add');
          }
        },
       save: {
          text: 'Guardar',
          cls: 'button-save',
          visible: this.props.data.status != 'In-House',
          handler: function(){
            self.saveChanges("save");
          }
        },
        close:{
          key: 'close',
          text: 'Cerrar',
          cls: 'button-close',
          handler: function(){
            FlowRouter.go('/purchaseorderparts');
          }
        },
        print: {
          text: 'Imprimir',
          visible: this.props.data.status != 'In-House',
          cls: 'button-print',
          handler: function()
          {
            console.log('imprimir');
          }
        },
        toogle:
        {
            text: self.props.data.status === "Dispotded" ? 'In-House' : 'Dispotded',
            cls: 'button-approval',
            visible: self.props.data.status != 'In-House',
            handler: function()
            {
                if(self.props.data.status === "In-House")
                    self.getToast().showMessage("info", "La compra está en estado In-House.");
                else
                    if(self.props.data.status == "In-Process" && (self.state.data.inAwb == "" || self.state.data.inAwb == null))
                        self.getToast().showMessage("info", "Para este cambio de estado necesita el siguiente campo (A. w. B #) en Inbound SAP Order.");
                else
                    if(self.props.data.status == "Dispotded" && (self.state.data.inInhDate == "" || self.state.data.inInhDate == null))
                        self.getToast().showMessage("info", "Para este cambio de estado necesita el siguiente campo (INH Date) en Inbound SAP Order.");
                 else
                     if(!self.props.isNew)
                         self.toggleAprove();
            }
        },

       }

         return items;
    }

    getToolbar()
    {
        let self = this;
        let actions = this.getToolbarItems();
        leftGroup = [], rightGroup = [];

        if(this.props.isNew)
        {
            leftGroup.push(actions.save);
        }
        else
        {
            leftGroup.push(actions.new);
            leftGroup.push(actions.save);
            leftGroup.push(actions.print);
            rightGroup.push(actions.toogle);
        }

        leftGroup.push(actions.close);

        let items = [
                    {
                        type: 'group',
                        className: 'hbox-l',
                        items: leftGroup
                    },
                    {
                        type: 'group',
                        className: 'hbox-r',
                        items: rightGroup
                    }];

       let toolbar = <Toolbar className="north hbox" items={items} />;

       return toolbar;
   }

        toggleAprove()
        {
            this.refs.confirmDialog.setTitle("Está seguro que desea cambiar de estado esta compra?");
            this.refs.confirmDialog.setType("dispotded");
            this.refs.confirmDialog.onOpen();
        }

        UpdateDetail()
        {
          let self = this;
          let fields = this.refs;

          let  properties =
          {
               outboundRMANr: fields.outboundRMANr.getValue(),
               outDate: fields.outDate.getValue(),
               outFreightForwarder: fields.outFreightForwarder.getValue(),
               outAwb: fields.outAwb.getValue(),
               outPOD: fields.outPOD.getValue(),
               outReceiptDate: fields.outReceiptDate.getValue()
          }

          if(!self.props.isNew)

             Meteor.call("updatePurchaseOrderPart", fields.recordDetailId.getValue(), properties, function(error, result)
              {
                 if(!error)
                     self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
                   else
                    throw self.getToast().showMessage("error", error);
              });
    }

    saveChanges(button)
    {
        let self = this;
        let fields = this.refs;

        if(fields.form.isValid(fields))
        {
            let properties = self.getProperties(fields);

            if(self.props.isNew)
                Meteor.call("addPurchaseOrder", properties, this.onPurchaseOrderAdded);
            else
            {

                Meteor.call("updatePurchaseOrder", this.props.data._id, properties, function(error, result)
                {
                    if(!error)
                    {
                        self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");

                        if(button == "saveAndClose")
                        FlowRouter.go('/purchaseordersystems');

                    }else
                    {
                        throw self.getToast().showMessage("error", error);
                    }

                });
            }
        }
        else
        {
            this.getToast().showMessage("warning", "Por favor ingrese todos los campos requeridos.");
        }
    }

       getProperties(fields)
        {
           let record =
           {
              type: fields.type.getValue(),
              orderClass: fields.orderClass.getValue(),
              supplierId: fields.supplier.getValue(),
              date: fields.date.getValue(),
              serviceOrderNr: fields.serviceOrderNr.getValue(),
              customerId: fields.customer.getValue(),
              systemId: fields.system.getValue(),
              brandId: fields.brand.getValue(),
              modelId: fields.model.getValue(),
              serialNumber: fields.serialNumber.getTextValue(),
              equipmentId:  fields.serialNumber.getValue(),
              inboundSAPOrderNr: fields.inboundSAPOrderNr.getValue(),
              incoterms: fields.incoterms.getValue(),
              inFreightForwarder: fields.inFreightForwarder.getValue(),
              inAwb: fields.inAwb.getValue(),
              inInhDate: fields.inInhDate.getValue(),
              note: fields.note != null ?  fields.note.getValue() : "",
              equipment: {},
              hasReceptionEquipment: false
            };

            if(this.props.isNew)
            {
                record.source = "";
                record.foreignId = "";
            }


          return record;
        }

        onPurchaseOrderAdded(error, result)
        {
          let self = this;

          if(!error)
          {
            self.getToast().showMessage("succes", "Tus datos se han guardado correctamente!");
            FlowRouter.go('/purchaseorderpart/' + result);
          }
          else
            self.getToast().showMessage("error", error);
        }

        getPurchaseOrderType()
        {
          return [{value: "1", text: "Sistema"}, {value: "2", text: "Parte"}]
        }

        getCustomers()
        {
          this.props.customers.sort(Util.applySortObject);

          return this.props.customers.map(function(customer)
          {
            return {value: customer._id, text: customer.name};
          });
        }

        getSystems()
        {
            this.props.systems.sort(Util.applySortObject);

            return this.props.systems.map(function(system)
            {
                return {value: system._id, text: system.description};
            });
        }

        getBrands()
        {

         this.props.brands.sort(Util.applySortObject);

          return this.props.brands.map(function(brand)
          {
            return {value: brand._id, text: brand.description};
          });
        }

        getVendors()
        {
          this.props.vendors.sort(Util.applySortObject);

          return this.props.vendors.map(function(vendor)
          {
            return {value: vendor._id, text: vendor.name};
          });
        }

        getPeriods()
        {
          this.props.periods.sort(Util.applySortObject);

          return this.props.periods.map(function(period)
          {
            return {value: period._id, text: period.description};
          });
        }

        getModels(models)
        {
          models.sort(Util.applySortObject);

          return models.map(function(model)
          {
             return {value: model._id, text: model.description};
          });
        }

      getCollectionModels(fields)
        {
          return  Models.find({brandId: fields.brand.getValue()}).fetch() || [];
        }

      getDataOrderClass(fields)
        {
          return [{value: "Billed", text: "Billed"}, {value: "EquipmentWarranty", text: "Equipment Warranty"}, {value: "DOA", text: "DOA"}]
        }

      getIcoterms()
        {
          return [{value: "AIREXW", text: "AIR-EXW"},
                  {value: "AIRFCA", text: "AIR-FCA"},
                  {value: "AIRCPT", text: "AIR-CPT"},
                  {value: "AIRCIP", text: "AIR-CIP"},
                  {value: "SEAFOB", text: "SEA-FOB"},
                  {value: "SEACFR", text: "SEA-CFR"},
                  {value: "SEACIF", text: "SEA-CIF"}]
        }

      onChangeCustomer(value)
        {
           let customer = Customers.findOne({_id: value}) || [];
           let customerTypes = CustomerTypes.findOne({_id: customer.customerTypeId});
           let type = customerTypes.description;

           let fields = this.refs;
           this.setState({ data: this.setDataState(fields, this.getCollectionModels(fields), null, 'customer', value),
                           disabledSerialNumber: (value == "" ? true : false),
                           requiredField: type != "Stock"});
        }

      onChangeBrand(value)
      {
          let fields = this.refs;
          let models = Models.find({brandId: value}).fetch() || [];
          this.setState({data: this.setDataState(fields, models, null, 'brand', value), disabledModel: (value == "" ? true : false)});
      }

      onChangeOrderClass(value)
      {
        let fields = this.refs;
        this.setState({data: this.setDataState(fields, this.getCollectionModels(fields), null, 'orderClass', value)});
      }

    setDataState(fields, models, recordDetail, field, dropdownValue)
      {

        let equipment = this.setDataStateEquipment(fields);

         let recordDetailId = fields.recordDetailId != null ? fields.recordDetailId.getValue() : "";
         let outboundRMANr = fields.outboundRMANr != null ? fields.outboundRMANr.getValue() : "";
         let outDate = fields.outDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.outDate.getValue()) : null;
         let outFreightForwarder = fields.outFreightForwarder != null ? fields.outFreightForwarder.getValue() : "";
         let outAwb = fields.outAwb != null ? fields.outAwb.getValue() : "";
         let outPOD = fields.outPOD != null ? fields.outPOD.getValue() : "";
         let outReceiptDate = fields.outReceiptDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.outReceiptDate.getValue()) : null;
         let attachRMA: 0;
         let attachPOD: 0;

         let attachReturnInvoice: 0;

          if(recordDetail != null)
          {
             recordDetailId = recordDetail._id;
             outAwb = recordDetail.outAwb;
             outboundRMANr = recordDetail.outboundRMANr;
             outDate = recordDetail.outDate != "" ? Util.getDateISO(recordDetail.outDate) : null;
             outFreightForwarder = recordDetail.outFreightForwarder;
             outPOD = recordDetail.outPOD;
             outReceiptDate = recordDetail.outReceiptDate != "" ? Util.getDateISO(recordDetail.outReceiptDate) : null;
             attachRMA = recordDetail.attachRMA;
             attachPOD = recordDetail.attachPOD;
             attachReturnInvoice = recordDetail.attachReturnInvoice
          }

      let data =
         {
            type: fields.type.getValue(),
            models: this.getModels(models),
            modelId: fields.model.getValue(),
            brandId: field == 'brand' ? dropdownValue : this.refs.brand.getValue(),
            systemId: fields.system.getValue(),
            customerId: field == 'customer' ? dropdownValue : this.refs.customer.getValue(),
            orderClass: field == 'orderClass' ? dropdownValue : this.refs.orderClass.getValue(),
            supplierId: fields.supplier.getValue(),
            date: fields.date.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.date.getValue()) : null,
            serviceOrderNr: fields.serviceOrderNr != null ? fields.serviceOrderNr.getValue() : "",
            serialNumber: fields.serialNumber != null ?  fields.serialNumber.getValue() : "",
            inboundSAPOrderNr: fields.inboundSAPOrderNr != null ? fields.inboundSAPOrderNr.getValue() : "",
            incoterms: fields.incoterms != null ? fields.incoterms.getValue() : "",
            inFreightForwarder: fields.inFreightForwarder != null ? fields.inFreightForwarder.getValue() : "",
            inAwb: fields.inAwb != null ? fields.inAwb.getValue() : "",
            inInhDate: fields.inInhDate.getValue().toString() != "Invalid Date" ? Util.getDateISO(fields.inInhDate.getValue()) : null,
            recordDetailId: recordDetailId,
            outboundRMANr: outboundRMANr,
            outDate: outDate,
            outFreightForwarder: outFreightForwarder,
            outAwb: outAwb,
            outPOD: outPOD,
            outReceiptDate: outReceiptDate,
            attachRMA: attachRMA,
            attachPOD: attachPOD,
            attachReturnInvoice: attachReturnInvoice,
            equipment: equipment
          };

        return data
      }

    setDataStateEquipment(fields)
    {
      let inhDate = fields.inhDate != null ? (fields.inhDate.getValue().toString() != "Invalid Date" ? fields.inhDate.getValue() : null ) : null;
      let estInstallDate = fields.estInstallDate != null ? (fields.estInstallDate.getValue().toString() != "Invalid Date" ? fields.estInstallDate.getValue() : null) : null;
      let inInhDate = fields.inInhDate != null ? (fields.inInhDate.getValue().toString() != "Invalid Date" ? fields.inInhDate.getValue() : null) : null;

       let data =
       {
          salesForce: fields.salesForce != null ?  fields.salesForce.getValue() : "",
          zohoCRM: fields.zohoCRM != null ?  fields.zohoCRM.getValue() : "",
          sAPOrderNr: fields.sAPOrderNr != null ? fields.sAPOrderNr.getValue() : "",
          systemWty: fields.systemWty != null ? fields.systemWty.getValue() : "",
          clientWty: fields.clientWty != null ? fields.clientWty.getValue() : "",
          estInstallDate: estInstallDate,
          mfrInvoiceNr: fields.mfrInvoiceNr != null ? fields.mfrInvoiceNr.getValue() : "",
          mfrInvoiceDate: fields.mfrInvoiceDate != null ? fields.mfrInvoiceDate.getValue() : "",
          freightForwarder: fields.freightForwarder != null ? fields.freightForwarder.getValue() : "",
          awb: fields.awb != null ? fields.awb.getValue() : "",
          inhDate: fields.inhDate != null ? fields.inhDate.getValue() : "",
        }

        return data;
    }

    getToast()
    {
      return this.refs.toastrMsg;
    }

    getPropertiesEquipment()
        {
          let fields = this.refs;
          let record =
              {
                salesForce: fields.salesForce.getValue(),
                zohoCRM: fields.zohoCRM.getValue(),
                sAPOrderNr: fields.sAPOrderNr.getValue(),
                systemWty: fields.systemWty.getValue(),
                clientWty: fields.clientWty.getValue(),
                estInstallDate: fields.estInstallDate.getValue(),
                mfrInvoiceNr: fields.mfrInvoiceNr.getValue(),
                mfrInvoiceDate:fields.mfrInvoiceDate.getValue(),
                freightForwarder: fields.freightForwarder.getValue(),
                awb: fields.awb.getValue(),
                inhDate: fields.inhDate.getValue()

              }

             return record;
        }

    getRenderGeneral()
    {
      let data = this.props.data;
      let dataState = this.state.data;
      let disableModel = this.state.disabledModel;
      let disabledField = this.state.disabledField;
      let disabledforeignFiled = this.state.disabledforeignFiled;
      let disabledSerialNumber = this.state.disabledSerialNumber;
      let requiredField = this.state.requiredField;
      let disabledDetailField = this.state.disabledDetailField;
       console.log(this.state.data.inInhDate);
      let componentDetail = (
        <PurchaseOrderPartContainer
          ref= "partDetail"
          purchaseOrderId = {data._id}
          status = {data.status}
          onSelect={this.onSelectDetail}/>
    );

    return(<Container className="vbox center">
            <div style={{textAlign: "center", color: "#004990"}}>
            <span style={{fontSize: "18px"}}>Compra de Tipo Parte - </span>
            <span style={{fontSize: "18px", color: "#565656"}}>{this.props.data.status} </span>
           </div>
            {this.getToolbar()}
                <FormPanel ref="form">
                        <ToastrMessage ref="toastrMsg" />
                        <Fieldset className="hbox box-form">
                            <FormColumn className="hbox-purchases-column purchase-width-column1" >
                                <TextField ref="purchaseOrderId"  className = "purchase-margin-label"  label="Purchase Order Nr" value={data._id}  disabled />
                                <Dropdown ref="type" label="Type" items={this.getPurchaseOrderType()} value="2" required={true} emptyOption={true} disabled = {true} />
                                <Dropdown  ref="orderClass" label="Order Type" items={this.getDataOrderClass()} value={dataState.orderClass} onChange={this.onChangeOrderClass.bind(this)} required={true} emptyOption={true}  />
                                <Dropdown  ref="supplier" label="Supplier" items={this.getVendors()} value={dataState.supplierId} required={true} emptyOption={true} disabled = {disabledField}/>
                                <DateField  ref="date" className = "purchase-input-date" label="Date" value={dataState.date != null ? Util.getDateISO(dataState.date) : null} disabled = {disabledField} />
                                <TextField ref="serviceOrderNr" label="Service Order Nr"  value={dataState.serviceOrderNr} required={true} disabled = {disabledforeignFiled}/>
                            </FormColumn>
                            <FormColumn className="hbox-purchases-column purchase-width-column2" >
                                <Dropdown ref="customer" label="Client" items={this.getCustomers()} value={dataState.customerId} onChange={this.onChangeCustomer.bind(this)} required={requiredField} emptyOption={true} disabled = {disabledforeignFiled} />
                                <Dropdown className="hbox-l" ref="system" label="System Type" items={this.getSystems()} value={dataState.systemId} required={this.state.requiredField} emptyOption={true} disabled = {disabledforeignFiled} />
                                <Dropdown className="hbox-l" ref="brand" label="Manufacturer" items={this.getBrands()} value={dataState.brandId} onChange={this.onChangeBrand.bind(this)} required={requiredField} emptyOption={true} disabled = {disabledforeignFiled} />
                                <Dropdown className="hbox-r" ref="model" label="Model Type" items={dataState.models} value={dataState.modelId} required={requiredField} emptyOption={true} disabled = {disableModel} />
                                <EquipmentLookup ref="serialNumber"
                                    value={data.equipmentId}
                                    text={data.serialNumber}
                                    foreignId = {dataState.customerId}
                                    label="Serial Nr"
                                    disabled = {disabledSerialNumber}
                                    onSelect={this.onEquipmentChanged}
                                    required={requiredField}  />
                            </FormColumn>
                            <FormColumn className = "hbox-purchases-column purchase-width-column3 " >
                                <TextField ref="salesForce" label="SalesForce"  value={dataState.equipment.salesForce} disabled = {true}/>
                                <TextField ref="zohoCRM" label="ZohoCRM" value={dataState.equipment.zohoCRM}  disabled = {true} />
                                <TextField ref="sAPOrderNr" label="SAP Order Nr" value={dataState.equipment.sAPOrderNr}  disabled = {true} />
                                <TextField ref="systemWty" label="System Wty (m)"  value={dataState.equipment.systemWty} disabled = {true}/>
                                <TextField ref="clientWty" label="Client Wty (m)" value={dataState.equipment.clientWty}  disabled = {true} />
                            </FormColumn>
                            <FormColumn className = "hbox-purchases-column purchase-width-column4" >
                                <DateField ref="estInstallDate"  className = "purchase-input-date" value={dataState.equipment.estInstallDate != null ? Util.getDateISO(dataState.equipment.estInstallDate) : null} label="Est. Install Date" disabled = {true} />
                                <TextField ref="mfrInvoiceNr" label="Mfr Invoice Nr" value={dataState.equipment.mfrInvoiceNr}  disabled = {true} />
                                <DateField ref="mfrInvoiceDate" className = "purchase-input-date"  label="Mfr Invoice Date" value={dataState.equipment.mfrInvoiceDate != null ? dataState.equipment.mfrInvoiceDate : null}  disabled = {true} />
                                <TextField ref="freightForwarder" label="Freight Forwarder" value={dataState.equipment.freightForwarder} disabled = {true}/>
                                <TextField ref="awb" label="AWB#" value={dataState.equipment.awb} disabled = {true}/>
                                <DateField ref="inhDate" className = "purchase-input-date" label="INH Date" value={dataState.equipment.inhDate != null ? dataState.equipment.inhDate : null} disabled = {true}/>
                            </FormColumn>
                        </Fieldset>
                        {this.getComponentOrderType(dataState.orderClass, dataState.note)}
                        <Fieldset className="hbox box-form">
                            <FormColumn className="hbox-purchases-column purchase-width-column-in">
                                <TextField ref="inboundSAPOrderNr" label="Inbound SAP Order Nr" value={dataState.inboundSAPOrderNr}  />
                                <Dropdown ref="incoterms" label="Incoterms" items={this.getIcoterms()} value={dataState.incoterms} emptyOption={true}  />
                                <TextField ref="inFreightForwarder" label="Freight Forwarder" value={dataState.inFreightForwarder}  />
                                <TextField ref="inAwb" label="AWB#" value={dataState.inAwb} />
                                <DateField ref="inInhDate" className = "purchase-Out-input-date" label="INH Date" value={dataState.inInhDate != null && dataState.inInhDate != "" ? Util.getDateISO(dataState.inInhDate) : null} />
                            </FormColumn>
                            <FormColumn className="hbox-purchases-column purchase-width-column-out">
                                <HiddenField ref="recordDetailId" value={dataState.recordDetailId}  />
                                <TextField ref="outboundRMANr" label="Outbound RMA Nr" value={dataState.outboundRMANr} disabled = {disabledDetailField} />
                                <DateField ref="outDate" className = "purchase-Out-input-date" label="Date" value={dataState.outDate} disabled = {disabledDetailField} />
                                <TextField ref="outFreightForwarder" label="Freight Forwarder" value={dataState.outFreightForwarder} disabled = {disabledDetailField}/>
                                <TextField ref="outAwb" label="AWB#" value={dataState.outAwb} disabled = {disabledDetailField}/>
                                <TextField ref="outPOD" label="POD" value={dataState.outPOD} disabled = {disabledDetailField} />
                                <DateField ref="outReceiptDate" className = "purchase-Out-input-date" label="Receipt Date" value={dataState.outReceiptDate} disabled = {disabledDetailField} />
                                <div className="attach-item-return-invoice-purchase-detail">
                                    <span className = "attach-span-right-text-purchase-detail" >Return Invoice:</span>
                                    <a className = "attach-left-text-purchase-detail">{dataState.attachReturnInvoice}<i className="attach-icon-return-invoice-purchase-detail" onClick={this.onAttachClick.bind(this,"ReturnInvoice")} ></i> </a>
                                    <div>
                                        <button className = "button-Out-margin-purchase-detail" disabled = {disabledDetailField}  onClick = {this.onUpdateDetail.bind(this)} >Actualizar Detalle</button>
                                    </div>
                                </div>
                            </FormColumn>
                            <FormColumn className = "hbox-purchases-column" >
                                <div>
                                    <div className="attach-item-RMA-purchase-detail">
                                        <a className = "attach-left-text-purchase-detail" >{dataState.attachRMA}<i className="attach-icon-RMA-purchase-detail" onClick={this.onAttachClick.bind(this,"RMA")}></i></a>
                                    </div>
                                    <div className="attach-item-POD-purchase-detail">
                                        <a className = "attach-left-text-purchase-detail">{dataState.attachPOD}<i className="attach-icon-POD-purchase-detail" onClick={this.onAttachClick.bind(this,"POD")}></i> </a>
                                    </div>
                                </div>
                                <div>
                                </div>
                            </FormColumn>
                            <FormColumn className = "hbox-purchases-column purchase-width-column-detail" >
                                {componentDetail}
                            </FormColumn>
                        </Fieldset>
                    </FormPanel>
                    {this.getAttachmentDialog()}
                    {this.getConfirmDialog()}
                </Container>);
    }

    getComponentOrderType(type, note)
    {
     if(type != "EquipmentWarranty")
     {
        return (<Fieldset className="hbox box-form" style={{marginTop: '6px'}}>
            <FormColumn className="hbox-l hbox-purchases-column purchase-width-column1">
              <NoteField ref="note" label= {type == "DOA" ?  "Desc DOA Reason" : "Notas"}  value = {note}/>
            </FormColumn>
          </Fieldset>);
     }
    }

    getAttachmentDialog()
    {
     let AttachmentCompoent =  status != "In-House" ? <AttachmentForm foreignId={this.state.data.recordDetailId} collection= "purchaseOrderParts" fieldType = {this.state.typeAttach} /> : null;
       let title = "Adjuntar Archivos " + this.state.typeAttach;
       return (<Dialog ref="attachmentDialog">
         <FormPanel ref="form">
          <Fieldset className="hbox">
          <FormColumn  title= {title} className="hbox-r" >
         <AttachmentContainer foreignId= {this.state.data.recordDetailId} collection= "purchaseOrderParts" fieldType = {this.state.typeAttach} deleteFile = {status != "In-House"} />
          {AttachmentCompoent}
         </FormColumn>
        </Fieldset>
        </FormPanel>
       </Dialog>);
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        okLabel="Si"
        noLabel="No"
        />);
    }

    onOkConfirmDialog()
    {
        let self = this;

        if(this.refs.confirmDialog.getType() == 'dispotded')
            Meteor.call("setApprove", this.props.data._id, this.props.data.status === "In-Process" ? "Dispotded" : "In-House", function(error, result){
                if(!error)
                {
                   if(self.props.data.source == "serviceOrder" && result == "In-House")
                    {
                       Meteor.call("onApproveServiceOrder", self.props.data.foreignId, "Delivered Parts", function(error, result){
            			    if(!error)
            				   self.getToast().showMessage("succes", "Cambio de estado exitoso!");
            			    else
            				   self.getToast().showMessage("error", error);
            		     });
                    }
                }
            });
    }

        render()
        {
          return (this.getRenderGeneral());
        }
    }
