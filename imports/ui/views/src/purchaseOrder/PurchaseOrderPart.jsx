import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, TextField, EmailField,
    PhoneField, FormPanel, Fieldset, FormColumn,
    NumberField, CheckBox} from 'react-ui';
import { Util } from '/lib/common.js';
import { Dialog } from '/imports/ui/components/src/Dialog.jsx';
import { PurchaseOrderParts } from '/imports/api/purchaseOrderParts/collection.js';
import { HiddenField } from '/imports/ui/components/src/HiddenField.jsx';
import { CheckboxToggle } from '/imports/ui/components/src/CheckboxToggle.jsx';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';
import { PartSingleList } from '/imports/ui/views/src/purchaseOrder/PartSingleList.jsx';

export class PurchaseOrderPart extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {opened: this.props.opened || false}

        this.openDialog = this.openDialog.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onRowDoubleClick = this.onRowDoubleClick.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this);
        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.removePurchaseOrderPart = this.removePurchaseOrderPart.bind(this);
        this.closeDialogParts = this.closeDialogParts.bind(this);
    }

    removePurchaseOrderPart()
    {
        this.refs.confirmDialog.setTitle('Está seguro que desea eliminar parte existente?"');
        this.refs.confirmDialog.onOpen();
    }

    onOkConfirmDialog()
    {
        let self = this;
        let record = this.refs.purchaseOrderDetailGrid.getSelectedRecord();

        if(record != null)
            Meteor.call("removePurchaseOrderPart", record._id, function(error, result){});
        else
            alert("Por favor seleccione un detalle.");
    }

    onRowDoubleClick(record)
    {
        if(this.props.status != 'In-House')
            this.openRecord(record);
    }

    openRecord(record)
    {
        this.openDialog('edit', record);
    }

    onSelectionChange(record)
    {
        if(this.props.status != 'In-House')
            return this.props.onSelect && this.props.onSelect(record);
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header: 'Qty',
            align:'center',
            minWidth: 50,
            dataIndex: 'quantity'
        },
        {
            key: 2,
            header: 'ABC Code',
            minWidth: 30,
            align:'center',
            dataIndex: 'code'
        },
        {
            key: 3,
            header: 'Description of Products',
            minWidth: 100,
            dataIndex: 'description'
        },
        {
            key: 4,
            header: 'Returnable',
            minWidth: 30,
            align: 'center',
            renderer: function(record)
            {
                return record.returnable == true ? "Yes" : "No";
            }
        }];

        return columns;
    }

    getToobarTotal()
    {
        let items = [{
        type: 'group',
        className: 'hbox-r',
        items:[]
        }]

        return (<Toolbar className="north hbox" id="main-toolbar" items={items} />);
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            type: 'group',
            className: 'hbox-l',
            items: [{
                text: 'Partes existentes',
                cls: 'button-tools',
                visible: this.props.status == 'In-Process',
                handler: function()
                {
                    self.setState({opened: true})
                }
            },
            {
                text: 'Editar',
                cls: 'button-edit',
                visible: this.props.status == 'In-Process',
                handler: function()
                {
                    let record = self.refs.purchaseOrderDetailGrid.getSelectedRecord();

                    if(record != null)
                        self.openDialog('edit', record);
                    else
                        alert("Por favor seleccione un registro");
                }
            },
            {
                text: 'Eliminar',
                visible: this.props.status == 'In-Process',
                cls: 'button-delete',
                handler: function()
                {
                    self.removePurchaseOrderPart();
                }
            }]
        }];

        return (<Toolbar className="north hbox" id="main-toolbar" items={items} />);
    }


    getGridPanel()
    {
        let data = this.props.data;

        return  (<GridPanel
                    id="purchaseOrderDetailGridId"
                    title="Detail"
                    ref="purchaseOrderDetailGrid"
                    columns={this.getColumns()}
                    records={data}
                    toolbar={this.getToolbar()}
                    onRowDoubleClick={this.onRowDoubleClick}
                    onSelectionChange = {this.onSelectionChange} />);
    }

    saveChanges()
    {
        let self = this;
        let fields = self.refs;
        //let totalAmount = 0;

        if(fields.form.isValid(fields))
        {
            let recordId = fields.record.getValue();

            let data = {
                purchaseOrderId: self.props.purchaseOrderId,
                code: fields.code.getValue(),
                description: fields.description.getValue(),
                quantity: fields.quantity.getValue(),
                returnable: fields.returnable.getValue()
            }

            if(recordId.length > 0)
            {
                if(!fields.returnable.getValue())
                {
                    data.outboundRMANr = "",
                    data.outDate = "",
                    data.outFreightForwarder = "",
                    data.outAwb = "",
                    data.outPOD = "",
                    data.outReceiptDate = ""
                }

                Meteor.call("updatePurchaseOrderPart", recordId, data, function(error, result){});
            }
            else
            {
                data.outboundRMANr = "";
                data.outDate = "";
                data.outFreightForwarder = "";
                data.outAwb = "";
                data.outPOD = "";
                data.outReceiptDate = "";

                Meteor.call("addPurchaseOrderPart", data, function(error, result){});
            }

            fields.dialog.close();
        }
        else
            alert("Por favor ingrese todos los campos requeridos.");
    }

    getDialog()
    {
        let items = [{
            text: 'Guardar',
            cls: 'button-save',
            handler: this.saveChanges
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialog
        }];

        let toolbar = <Toolbar className="north hbox" id="main-toolbar" items={items} />;

        return (<Dialog ref="dialog" className="contact-dialog" title="Detail" width = "503px" modal>
        {toolbar}
        <FormPanel ref="form">
            <Fieldset className="hbox">
                <FormColumn className="hbox-purchases-column purchase-detail-dialog-column" style={{marginRight: "10px"}}>
                    <HiddenField ref="record" value="" />
                    <NumberField className = "form-field-left input" ref="quantity" label="Quantity" decimals={0} disabled={true} required  />
                    <TextField ref="code" label="ABC Code" align = "left" disabled={true} required />
                    <TextField ref="description" label="Description of Products" align = "left" disabled={true} required />
                    <CheckboxToggle key="1" ref="returnable" baseClass = "slider-purchase" lable = "Returnable" />
                </FormColumn>
            </Fieldset>
        </FormPanel>
        </Dialog>);
    }

    openDialog(action, record)
    {
        let self = this;
        let fields = self.refs;

        if(action == 'edit')
        {
            if(record != null)
            {
                fields.record.setValue(record._id);
                fields.code.setValue(record.code);
                fields.description.setValue(record.description);
                fields.quantity.setValue(record.quantity);
                fields.returnable.setValue(record.returnable);
                fields.dialog.setTitle("Editar Detail");
                fields.dialog.open();
            }
            else
                alert("Por favor seleccione un registro.");
        }
        else
            if(action == 'add')
            {
                fields.record.setValue('');
                fields.code.setValue('');
                fields.quantity.setValue('');
                fields.description.setValue('');
                fields.returnable.setValue(false);
                fields.dialog.setTitle("Nuevo Detail");
                fields.dialog.open();
            }
    }

    closeDialog()
    {
        this.refs.dialog.close();
    }

    closeDialogParts()
    {
        this.setState({opened: false});
    }

    getExistingPartsForm()
    {
        return (
        <PartSingleList
            opened={this.state.opened}
            purchaseOrderId={this.props.purchaseOrderId}
            close={this.closeDialogParts}
            data={this.props.partRecords}/>);

    }

    getConfirmDialog()
    {
        return (<Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        />);
    }

    render()
    {
        return (<div>
        {this.getGridPanel()}
        {this.getToobarTotal()}
        {this.getDialog()}
        {this.getExistingPartsForm()}
        {this.getConfirmDialog()}
        </div>);
    }
}
