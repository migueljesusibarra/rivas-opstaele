import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar, GridPanel, DialogPanel } from 'react-ui';

export class PartSingleList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            opened: this.props.opened || false,
            defaultChecked: false,
        };

        this.records = [];
        this.selectRecords = 0;
        this.onClickItem = this.onClickItem.bind(this);
        this.onCheckAllUnCheckAll = this.onCheckAllUnCheckAll.bind(this);
        this.SaveCheckAll = this.SaveCheckAll.bind(this);
        this.closeDialogParts = this.closeDialogParts.bind(this);
    }

    componentWillReceiveProps(nextProps)
    {
        this.setState({opened: nextProps.opened});
    }

    onClickItem(event)
    {
        let checked = event.target.checked;
        let totalRecords = this.records.length;

        if (!checked)
        {
            this.selectRecords--;
            this.refs.toolbar.unChecked();
        }
        else
        {
            this.selectRecords++;
            if(this.selectRecords == totalRecords)
                this.refs.toolbar.checked();
        }
    }

    onCheckAllUnCheckAll(value)
    {
        this.refs.toolbar.setChecked(value);
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');

        for (var i = 0; i < rowCheck.length; i++)
        {
            rowCheck[i].checked = value;
        }

        if(value)
            this.selectRecords = this.records.length;
        else
            this.selectRecords = 0;
    }

    setInitializeValue()
    {
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[type=checkbox]');
        let rowQuantity = table.querySelectorAll('input[name=number]');

        for (let i = 0; i < rowCheck.length; ++i)
        {
            rowCheck[i].checked = false;
        }

        for (let i = 0; i < rowQuantity.length; i++)
        {
            rowQuantity[i].value = 1;
        }

        this.refs.toolbar.unChecked();
        this.selectRecords = 0;
    }

    SaveCheckAll()
    {
        let self = this;
        let table = document.getElementById('partsGridId');
        let rowCheck = table.querySelectorAll('input[name=checkbox]');
        let rowQuantity = table.querySelectorAll('input[name=number]');
        let checked = false;

        let purchaseOrderParts = [];

        for (var i = 0; i < rowCheck.length; i++)
        {
            if(rowCheck[i].checked)
            {
                checked = true;

                let data = {
                  purchaseOrderId: self.props.purchaseOrderId,
                  partId: rowCheck[i].dataset.partid,
                  quantity: rowQuantity[i].value,
                  description: rowCheck[i].dataset.description,
                  code: rowCheck[i].dataset.number,
                  inStock: true
                };

                purchaseOrderParts.push(data);
            }
        }

        if(!checked)
            alert("Por favor seleccione un registro");
        else
            this.savePurchaseOrderPart(purchaseOrderParts);

    }

    savePurchaseOrderPart(data)
    {
        let self = this;

        Meteor.call("addPurchaseOrderParts", data, function(error, result)
        {
            if(!error)
                self.closeDialogParts();
            else
                throw alert(error);
        });
    }

    closeDialogParts()
    {
        this.setInitializeValue();
        this.props.close();
    }

    getToolbar()
    {
        let self = this;

        let items = [
        {
            text: 'Todo',
            type: 'check',
            cls: 'option-checked-tool-bar',
            handler: function(event)
            {
                let check = event.target.checked;
                self.onCheckAllUnCheckAll(check);
            }
        },
        {
            text: 'Guardar',
            cls: 'button-save',
            handler: this.SaveCheckAll
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close',
            handler: this.closeDialogParts
        }];

        let toolbar = (<Toolbar className="north hbox" id="main-toolbar" items={items} ref="toolbar"/>);

        return toolbar;
    }

    getColumns()
    {
        let self = this;

        let columns = [
        {
            key: 1,
            header:'',
            minWidth: 20,
            renderer: function(record)
            {
                return(<input
                type="checkbox"
                name="checkbox"
                key={'check-'+record._id}
                data-partid = {record._id}
                data-code = {record.code}
                data-description = {record.description}
                data-number = {record.number}
                defaultChecked = {self.state.defaultChecked}
                onClick={self.onClickItem}>
                </input>);
            }
        },
        {
            key: 2,
            header: 'Cantidad',
            minWidth: 100,
            renderer: function(record)
            {
                return(<input
                key={'number-'+record._id}
                defaultValue = {1}
                className = "width-quantity-previous"
                type="number"
                name="number" />);
            }
        },
        {
            key: 3,
            header: 'Número',
            dataIndex: 'number',
            align: 'center',
            minWidth: 200
        },
        {
            key: 4,
            header: 'Descripción',
            dataIndex: 'description',
            minWidth: 200
        }];

        return columns;
    }

    getGridPanel()
    {
        let data = this.props.data;
        this.records = data;

        let display = this.state.opened ? 'block' : 'none';

        return (
        <div className="overlay" style={{display: display}}>
            <DialogPanel
                className="vbox center"
                title="Partes Existentes"
                modal>
                <GridPanel
                id="partsGridId"
                ref="partsGrid"
                className="center"
                columns={this.getColumns()}
                records={data}
                toolbar={this.getToolbar()} />
            </DialogPanel>
        </div>);
    }


    render()
    {
        return(<div ref="partSingleList">{this.getGridPanel()}</div>)
    }
}
