import React, { Component } from 'react';

export class Confirm extends Component
{
    constructor(props)
    {
        super(props);

        this.state = this.empty();
        this.onCancel = this.onCancel.bind(this);
        this.onOk = this.onOk.bind(this);
    }

    empty()
    {
        let data = {
            opened: false,
            value: this.props.value || '',
            type: this.props.type || '',
            title: this.props.title || 'Esta seguro?',
            okLabel: this.props.okLabel || 'Si',
            noLabel: this.props.noLabel || 'No'

        };

        return data;
    }

    getBody()
    {
        let body = null;

        if(this.props.text)
        {
            body = <div className="confirm-body">{this.props.text || ''}</div>;
        }

        return body;
    }

    setTitle(title)
    {
        this.setState({title: title});
    }

    onOpen()
    {
        this.setState({opened: true});
    }

    onOpenAndCustomValue(value, type)
    {
        this.setState({
            opened: true,
            value: value,
            type: type
        });
    }

    onOpenAndValue(value)
    {
        this.setState({
            opened: true,
            value: value
        });
    }

    onClose()
    {
        this.setState({opened: false});
    }

    onOk()
    {
        if(this.props.onOk)
            this.props.onOk();

        this.onClose();
    }

    setValue(value)
    {
        this.setState({value: value});
    }

    getValue()
    {
        return this.state.value;
    }

    setType(type)
    {
        this.setState({type: type});
    }

    getType()
    {
        return this.state.type;
    }

    onCancel()
    {
        this.onClose();
    }

    render()
    {
        let display = this.state.opened ? '' : 'none';
        let height = this.props.text ? 150 : 100;

        return (
            <div
            className="confirm-overlay"
            style={{display: display}}>
                <div className="confirm-content" style={{height: height}}>
                    <div className="confirm-header">
                        <h4 className="confirm-title">
                            {this.state.title}
                        </h4>
                    </div>
                    {this.getBody()}
                    <div className="confirm-footer">
                        <button className="confirm-btn-ok" onClick={this.onOk}>{this.state.okLabel}</button>
                        <span> </span>
                        <button className="confirm-btn-cancel" onClick={this.onCancel}>{this.state.noLabel}</button>

                    </div>
                </div>
            </div>
        );
    }
}
