import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

export class SubMenu extends Component
{
    constructor(props)
    {
       super(props);

       this.onClick = this.onClick.bind(this);
    }

        onClick(e)
      {
          let clasess = this.props.className;
          let self = this;
          let linkMenu = e.target.parentElement.parentElement.getElementsByClassName('links')[0];
          let MenuDiv = document.getElementById(this.props.id);


           MenuDiv.removeAttribute('class');

           if( MenuDiv.hasAttribute('expanded') )
           {
               MenuDiv.removeAttribute('expanded');
               MenuDiv.classList.add('icon-expanded', clasess);
           }
           else
           {
               MenuDiv.setAttribute('expanded', 'true');
               MenuDiv.classList.add('icon-collapsed', clasess);
           }

           if(linkMenu.firstChild)
           {
               while(linkMenu.hasChildNodes())
               {
                   linkMenu.removeChild(linkMenu.lastChild);
               }
           }
           else
           {
               this.setLinkOptions(this.props.options, linkMenu);
           }
      }

    createLink(title, url, className)
    {
        let container = document.createElement("div"),
            link = document.createElement("a");

            link.title = title;
            link.href = url;
            link.text = title;
            container.className = className;

            container.appendChild(link);
            return container;
    }

    setLinkOptions(options, container)
      {
          let submenu = null,
              menuLen = 0
              i = 0;

          for(i = 0, menuLen = options.length; i < menuLen; i++)
          {
              let link = this.createLink(options[i].title, options[i].url, options[i].className);

              container.appendChild(link);
          }
      }
// "icon-expanded"
    render()
    {
        let clasess = ["icon-expanded", this.props.className];
        let url = "/" + this.props.href;

        return (<div className="links-menu">
                    <div id={this.props.id} className={clasess.join(' ')}><a onClick={this.onClick} href={url}>{this.props.text}</a></div>
                    <div className="links">
                    </div>
                </div>);
    }
}
