import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { TextField } from 'react-ui';
import { Comments } from '../../../api/comments/collection.js';


export class AttachmentForm extends Component{
    constructor(props)
    {
        super(props);
        this.state = {
            files: []
        };

        this.onPost = this.onPost.bind(this);
        this.onFileChosen = this.onFileChosen.bind(this);
        this.onRemoveFile = this.onRemoveFile.bind(this);
    }
    onFileChosen(event)
    {
        let files = this.state.files;
        
        for (let i = 0; i < event.target.files.length; i++) {
            files.push(event.target.files[i]);
        }

        this.setState({files: files});
    }
    onRemoveFile(event)
    {
        let files = this.state.files;
        let index = event.target.dataset.id;

        files.splice(index, 1);

        this.setState({files: files});
    }
    renderFiles()
    {
        let self = this;
        let index = 0;

        return this.state.files.map(function(file){
            return (<li key={"file" + file.size}>
                        <span style={{overflow:'hidden', textOverflow: 'ellipsis', display: 'block', marginRight: '60px'}}>{file.name}</span>
                        <a href="javascript:void(0);" data-id={index++} className="x-delete" onClick={self.onRemoveFile}>Remove</a>
                    </li>);
        });
    }
    isNonEmpty(value)
    {
        return value != null && value.toString().trim() != '';
    }
    onPost()
    {
        let timestamp = Math.floor(Date.now());
        let files = this.state.files;
        let commentId = this.addComment(files);
        
        for (let i = 0; i < files.length; i++) {
            this.uploadFile(commentId, files[i], i);
        }
    }
    addComment(files)
    {
        let index = 0;
        let attachments = (files || []).map(function(file){
            return {index: index++, name: file.name, type: file.type};
        });

        let value = this.refs.commentBox.value;

        if((value != null && value.length > 0) || files.length > 0)
        {
            let commentId = Random.id();
            let user = Meteor.user();
            let username = user.username;
            let properties = 
            {
                 _id: commentId,
                foreignId: this.props.foreignId,
                collection: this.props.collection,
                text: value,
                fieldType: this.props.fieldType,
                property: null,
                automatic: false,
                attachments: attachments,
            }

            Meteor.call("addComment", properties, function(error, result){
                    if(error)
                       throw alert(error);
                  });

            this.refs.commentBox.value = null;

            if(files.length > 0)
                this.setState({files: []});

            return commentId;
        }
    }
    uploadFile(commentId, file, index)
    {
        let uploader = new Slingshot.Upload("rivas-opstaele-attachments", {recordId: this.props.foreignId});
        let self = this;

        uploader.send(file, function (error, url) {
            if (error) {
                console.error('Error uploading ' + error);
            }
            else
            {
                let comment = Comments.findOne({_id: commentId});
                console.log(comment);
                let attachments = comment.attachments;

                attachments[index].url = url;

               Meteor.call("updateAttachments", commentId, attachments, function(error, result){
                    if(error)
                       throw alert(error);
                  });

            }
        });
    }
    render()
    {
        let user = Meteor.user();
        let self = this;
        let avatar = '/images/avatars/' + user.username + '.jpg';

        return (<div id="comment-container" className="x-south" style={{margin: '5px'}}>
                    <i className="x-comment-avatar" style={{backgroundImage:'url(' + avatar + ')'}}></i>
                    <div style={{overflow:'hidden', marginLeft:'40px', position:'relative'}}>
                        <textarea id="comment-input" ref="commentBox" className="x-textarea-autogrow" rows="1" style={{padding:'5px'}} placeholder="Descripción del adjunto..."></textarea>
                        <ul id="comment-files">
                            {this.state.files.length > 0 ? this.renderFiles() : null}
                        </ul>
                        <div id="comment-toolbar" style={{textAlign:'right'}}>
                            <button id="comment-attach" className="ui basic button" style={{fontWeight:'normal', padding:'4px 12px', fontSize:'12px', position:'relative', overflow:'hidden'}}>
                                <i className="icon-attachment" style={{verticalAlign:'middle', fontSize:'16px'}}></i> <span>Attach</span>
                                <input id="comment-file" type="file" multiple={true} style={{position:'absolute', top:'0', left:'0', right:'0', bottom:'0', opacity:'0', cursor:'pointer', width:'100%'}} onChange={this.onFileChosen} />
                            </button>
                            <button id="comment-post" className="ui blue button" style={{fontWeight:'normal', padding:'6px 12px', marginLeft: '10px', fontSize:'12px'}} onClick={this.onPost}>Post</button>
                        </div>
                    </div>
                </div>);
    }
}
