import React, { Component } from 'react';
import { DialogPanel } from 'react-ui';
import { DialogComponent } from 'react-ui';
import { Button } from 'react-ui';

export class Dialog extends Component
{
    static get defaultProps(){
        return {
            modal: false
        };
    }
    constructor(props)
    {
        super(props);

        this.state = {
            opened: this.props.opened || false,
            title: this.props.title
        };


        this.onHide = this.onHide.bind(this);
        this.onClose = this.onClose.bind(this);
    }
    onHide(event)
    {
        // Close the modal only if the click was directly on the overlay
        if(!this.props.modal && event.target.classList.contains('overlay'))
            this.close();
    }
    onClose(event)
    {
        this.close();
    }
    open()
    {
        this.setState({opened: true});
    }
    close()
    {
        this.setState({opened: false});
    }
    getFooter()
    {
        let footer = null;

        function createButton(item)
        {
            if(item.key == 'close')
                return (<Button className="btn btn-primary" id={"dialog-" + item.key} key={item.key} text={item.text} onClick={this.onClose.bind(this)} />);
            else
                return (<Button className="btn btn-danger" id={"dialog-" + item.key} key={item.key} text={item.text} onClick={item.handler} />);
        }

        let buttons = [];

        if(this.props.bbar){
            buttons = this.props.bbar.map(createButton.bind(this));
            footer = <div className="dialog-footer align-right">{buttons}</div>;
        }

        return footer;
    }

    setTitle(title)
    {
      this.setState({title:title});
    }
    render()
    {
        let display = this.state.opened ? 'block' : 'none';

        return (
            <div className="overlay" onClick={this.onHide} style={{display: display}}>
                <DialogPanel id={this.props.id} title={this.state.title} footer={this.getFooter()} className={this.props.className} style={{display: display}}>
                    {this.props.children}
                </DialogPanel>
            </div>
        );
    }
}

export class DialogLookup extends DialogComponent
{
    componentWillReceiveProps(nextProps)
    {
        this.setState({opened: nextProps.opened});
    }
    open(callback)
    {
        if(callback && typeof callback == 'function')
            this.onOpen = callback;

        this.setState({opened: true}, this.onOpen);
    }
    close(callback)
    {
        if(callback && typeof callback == 'function')
            this.onClose = callback;

        this.setState({opened: false}, this.onClose);
    }
    onOpen()
    {

    }
    onClose()
    {

    }
}

export class LookupDialog extends DialogLookup
{
    render()
    {
        let display = this.state.opened ? 'block' : 'none';

        return (
            <div className="overlay" onClick={this.onHide} style={{display: display}}>
                { this.state.opened ? this.props.children : null }
            </div>
        );
    }
}
