import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { CardItem } from './CardItem.jsx';

export class CardList extends Component
{
    constructor(props)
    {
        super(props);
    }

    getCardItems()
    {
        let cardItems = [];
        let totalRecords = this.props.records.length;

        for(let i = 0; i < totalRecords; i++)
        {
            cardItems.push(<div key={"div-card-item"+i} className="card-col-md-3"><CardItem record={this.props.records[i]}></CardItem></div>);
        }

        return cardItems;
    }

    render()
    {
        let cardItems = this.getCardItems();

        return (<div className="card-col-md-12">{cardItems}</div>);
    }
}
