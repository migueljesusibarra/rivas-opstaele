import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export class CardItem extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            mouseOver: false
        };

        this.toggleActive = this.toggleActive.bind(this);
        this.onMouseOver =  this.onMouseOver.bind(this);
        this.onMouseOut =  this.onMouseOut.bind(this);
    }

    toggleActive()
    {
        Meteor.call("setActive", this.props.record._id, this.props.record.active === false ? true : false);
    }
    
    onMouseOver()
    {
        this.setState({
            mouseOver: true
        });
    }

    onMouseOut()
    {
        this.setState({
            mouseOver: false
        });
    }

    render()
    {
        let avatar = '/images/' + (this.props.record.gender ? `avatar-${this.props.record.gender}.png` : 'avatar.png');

        return (
            <div className="card-box" onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}>
                <div className={this.state.mouseOver ? "content-top-action" : "content-top-action hidden"}>
                    <a href={"/user/" + this.props.record._id} className="button-edit"></a>
                </div>
                <div>
                    <a href="#">
                        <img className="img-circle-photo"  src={avatar} />
                    </a>
                    <div className="content-member-info">
                        <h4 className="text-title"><b>{this.props.record.profile.name || "No Name"}</b></h4>
                        <p className="text-muted">{this.props.record.typeName || "No Type"}</p>
                        <p className="text-dark"><small>{this.props.record.profile.email || "No Email"}</small></p>
                    </div>
                </div>
                <div className="content-bottom-action">

                </div>
            </div>
        );
    }
}
