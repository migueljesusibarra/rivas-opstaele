import React, { Component } from 'react'

export class ToastrMessage extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            message: "Please. Add a message"
        };
    }
    backgroundImage(type)
    {
        if (type == "info") return "url(/icons/info.png)";
        if (type == "warning") return "url(/icons/warning.png)";
        if (type == "succes") return "url(/icons/check.svg)";
        if (type == "error") return "url(/icons/error.png)";
    }
    getBorderMessage(type)
    {
        if (type == "info") return "solid 1px #bce8f1";
        if (type == "warning") return "solid 1px #faebcc";
        if (type == "succes") return "solid 1px #d6e9c6";
        if (type == "error") return "solid 1px #ebccd1";
    }

    getColorMessage(type)
    {
        if (type == "info") return "#3a87ad";
        if (type == "warning") return "#c09853";
        if (type == "succes") return "#468847";
        if (type == "error") return "#b94a48";
    }

    getBackgroundMessage(type)
    {
        if (type == "info") return "#d9edf7";
        if (type == "warning") return "#fcf8e3";
        if (type == "succes") return "#dff0d8";
        if (type == "error") return "#f2dede";

        return "#000000";
    }

    showMessage(type, message)
    {
        let snackbar = document.getElementById("snackbar");
        let infoColor = document.getElementById("closeToastMsg");
        let backgroundImage = document.getElementById("background-image");

        snackbar.className = "show";
        snackbar.style.border = this.getBorderMessage(type);
        snackbar.style.color = this.getColorMessage(type);
        snackbar.style.background = this.getBackgroundMessage(type);
        infoColor.style.background = this.getBackgroundMessage(type);
        infoColor.style.color = this.getColorMessage(type);
        backgroundImage.style.backgroundImage = this.backgroundImage(type);

        setTimeout(function()
        {
            snackbar.className = snackbar.className.replace("show", "");
        }, 5000);

        if(message != "")
        {
            this.setState({
                message:message
            });
        }
    }

    messageClose()
    {
        let x = document.getElementById("snackbar");

        x.className = x.className.replace("show", "");
    }

    render()
    {
        return(<div id="snackbar">
                    <a id="background-image"></a>
                    <button id= "closeToastMsg" className="toast-close"  onClick={this.messageClose.bind(this)} >x</button>
                    <p> {this.state.message}</p>
                </div>);
    }

}
