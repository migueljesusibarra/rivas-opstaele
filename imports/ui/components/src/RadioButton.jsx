import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export class RadioButton extends Component
{
   constructor(props)
   {
      super(props);
      this.state = {
        value: false
     }
      this.getValue = this.getValue.bind(this);
    };

    getValue()
    {
        return document.getElementById("passId").checked;
    }

    setValue(value)
    {
      return this.setState({value:value});
    }

   render()
   {
      let value = this.state.value;
      let id = "item"+(new Date()).getMilliseconds()+Math.floor(Math.random()*1000);
      let id2 = "item"+(new Date()).getMilliseconds()+Math.floor(Math.random()*1000);
      return(<div>
                <p>
                       <label style={{marginRight: "103px"}} >Pasar / Fallar: </label>
                        <input type = "radio"  defaultChecked={value} name = "passFail" key={id} id = "passId" />
                        <label className="test-performed-radio" >Pasar</label>
                        <input type = "radio" defaultChecked={value == false ? true:false} name = "passFail" key={id2} id = "failId"  />
                        <label className="test-performed-radio" >Fallar</label>
               </p>
         </div>
      )
   }
}
