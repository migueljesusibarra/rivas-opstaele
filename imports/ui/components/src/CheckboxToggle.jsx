import React, { Component } from 'react';
import ReactDOM from 'react-dom';


export class CheckboxToggle extends Component
{
   constructor(props)
   {
      super(props);
      this.state = {
        value: this.props.value,
        labelYes: this.props.labelYes || 'Yes',
        labelNo: this.props.labelNo || 'No'
     }
      this.getValue = this.getValue.bind(this);

    };

    componentWillReceiveProps(props)
    {
       this.setState({value: props.value});
    }

    getValue()
    {
        return document.getElementById("checkedId").checked;
    }

    setValue(value)
    {
      return this.setState({value:value});
    }

   render()
   {
      let value = this.state.value;
      let id = "item"+(new Date()).getMilliseconds()+Math.floor(Math.random()*1000);

      return(
         <div className="hbox form-field">
            <div className="form-field-label hbox-l">
               <label>{this.props.lable}</label>
            </div>
            <div className="form-field-value hbox-r compose-fields slider-boxs">
               <label className="switch">
                  <input type="checkbox" defaultChecked={value}  key={id} id="checkedId"></input>
                  <div className="slider round">
                    <span className="on">{this.state.labelYes}</span><span className="off">{this.state.labelNo}</span>
                  </div>
               </label>
            </div>
         </div>
      )
   }
}
