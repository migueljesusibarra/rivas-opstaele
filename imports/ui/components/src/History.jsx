import React, { Component } from 'react';
import { Toolbar, Fieldset, Container, List } from 'react-ui';
import moment from 'moment';

export class History extends Component
{
    constructor(props){
        super(props);
    }

    getHistory(records)
    {
        return records.map(function(item)
        {
            let date = moment(item.createdOn).format('MM/DD/YYYY h:m:ss A');

            // We started using the userDetail after the application recorded some history records
            let user = item.userDetail ? item.userDetail.createdBy : item.createdBy;

            if(item.type == "RECORD_UPDATED")
            {
                return (<div className="history" key={item._id}>
                            <span className="history-user">Modified by</span>
                            <span>{user}</span>
                            <span className="history-user">On</span>
                            <span>{date}</span>
                        </div>);
            }
            else
                if(item.type == 'RECORD_CREATED')
                {
                    return (<div className="history" key={item._id}>
                                <span className="history-user">Created by</span>
                                <span>{user}</span>
                                <span className="history-user">On</span>
                                <span>{date}</span>
                            </div>);
                }
        });
    }
    render()
    {
        return(
            <Container className="history-container vbox center">
                {this.props.records.length > 0 ?
                    this.getHistory(this.props.records)
                    :
                    <div className="history-result">No hay registros de historial para mostrar</div>
                }
                
            </Container>
        );
    }
}
