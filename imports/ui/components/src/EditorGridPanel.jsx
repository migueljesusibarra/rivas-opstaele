import  React, { Component } from 'react';
import { GridPanel } from 'react-ui';
import ReactDOM from 'react-dom';

export class EditorGridPanel extends GridPanel
{
    constructor(props)
    {
        super(props);

        this.onBlur = this.onBlur.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }
    onBlur(event)
    {
        let target = event.target;

        if(target.id == 'placeholder')
            this.removePlaceholder();
    }
    onKeyPress(event)
    {
        if(event.key == 'Enter')
        {
            let target = event.target;

            if(target.id == 'placeholder')
            {
                if(this.props.onEnter)
                    this.props.onEnter(event);

                event.target.blur();
            }
        }
    }
    showEditor()
    {
        let element = ReactDOM.findDOMNode(this);
        let target = element.querySelector(".grid-body > tbody > tr");
        let instance = null;

        if(target != null)
            instance = this.insertPlaceholder(target);
        else
        {
            target = document.querySelector(".grid-body > tbody");
            instance = this.appendPlaceholder(target);
        }
    }
    createPlaceholder(child)
    {
        let editor = <input id="placeholder" type="text" onKeyPress={this.onKeyPress} onBlur={this.onBlur} style={{width: '100%'}} placeholder="Introduzca un texto y pulse la tecla Intro" autoFocus/>;

        return ReactDOM.render(<td>{editor}</td>, child);
    }
    appendPlaceholder(target)
    {
        let child = document.createElement("tr");
        target.appendChild(child);

        return this.createPlaceholder(child);
    }
    insertPlaceholder(target)
    {
        let instance = null;

        let child = document.createElement("tr");

        target.parentNode.insertBefore(child, target);

        return this.createPlaceholder(child);
    }
    removePlaceholder()
    {
        let editor = document.getElementById("placeholder");
        let row = editor.closest("tr");

        row.parentNode.removeChild(row);
    }
    getSelectedRecord()
    {
        let grid = ReactDOM.findDOMNode(this);
        let row = grid.querySelector("tr.x-selected");

        if(row != null)
            return this.props.records[row.dataset.index];
        else
            return null;
    }
}
