import React, { Component } from 'react';

export class InlineEditor extends Component
{
    static get defaultProps(){
        return {
            clicksToEdit: 1
        };
    }
    constructor(props)
	{
		super(props);

		this.state = {
			adding: false,
            keepFocus: false
		};

        this.onChange = this.onChange.bind(this);
        this.onChanged = this.onChanged.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
      }
    setFocus(active)
    {
        if(this.state.adding == false)
            this.setState({adding: true, keepFocus: true});
        else
            this.refs.editor.focus();
    }
	onClick(event)
    {
        if(this.props.clicksToEdit == 1)
            this.onClickEvent(event);
	}
    onDoubleClick(event)
    {
        if(this.props.clicksToEdit == 2)
            this.onClickEvent(event);
    }
    onClickEvent(event)
    {
        if(this.props.onClick)
            this.props.onClick(event);

        this.setState({adding: true, width: event.target.closest(".inline-editor").getBoundingClientRect().width});
    }
	onBlur(event)
	{
        if(this.props.onBlur)
            this.props.onBlur(event);

        this.hideQuickForm();
	}
    onChange(event)
    {
        if(this.props.onChange)
            this.props.onChange(event);
    }
	onKeyPress(event)
	{
        if(this.props.onKeyPress)
            this.props.onKeyPress(event);

		if(event.key == 'Enter')
        	this.onChanged(event);
	}
	onChanged(event)
	{
        if(this.props.onChanged != null)
			this.props.onChanged(event);

		this.hideQuickForm();
	}
	hideQuickForm()
	{
        this.refs.editor.value = '';

        if(!this.state.keepFocus)
            this.setState({adding: false, width: null});
	}
	render()
	{
		let content = null;
        let classes = ['inline-editor', this.props.className].join(' ');

		if(!this.state.adding)
        {
            let text = this.props.text || this.props.placeholder;
            let color = this.props.text ? "#676767" : "#A9A9A9";

            content = <a href="javascript:void(0);" style={{color: color, textDecoration: 'none', display: 'block'}}>{text}</a>;
        }
        else
        {
			content = (	<input  ref="editor"
                                data-id={this.props["data-id"]}
                                type="text"
                                defaultValue={this.props.value}
                                placeholder={this.props.placeholder}
                                className=""
                                onBlur={this.onBlur}
                                onKeyPress={this.onKeyPress}
                                onChange={this.onChange}
                                autoFocus />
						);
        }

        let style = {};

        if(this.state.width)
            style.width = this.state.width + 'px';

		return (<div className={classes} style={style} onClick={this.onClick} onDoubleClick={this.onDoubleClick}>
				{content}
			    </div>);
	}
}
