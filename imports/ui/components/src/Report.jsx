import React, { Component } from 'react';
import { Toolbar } from 'react-ui';

export class Report extends Component
{
    constructor(props)
    {
      super(props);
      this.closeDialog = this.closeDialog.bind(this);
    }


    printContent()
     {
         let newWin = window.frames["printf"];
         let divElement = newWin.document.getElementsByClassName("page-margin-report")[0].innerHTML;
         let divElementhead = newWin.document.getElementsByClassName("rr-header")[0].innerHTML;
         let frmhtml = document.getElementById("ifrm");

         if(frmhtml != null)
            frmhtml.parentNode.removeChild(frmhtml);

         let ifrm = document.createElement('iframe');
         ifrm.setAttribute('id', 'ifrm');
         ifrm.setAttribute('name', 'ifrm');
         document.body.appendChild(ifrm);

         let ifrm2 = window.frames["ifrm"];
         ifrm2.document.write('<html><head><title></title>' +
         '<style type="text/css" media="print">' +
         '@page { size: A4; margin: 0;}'+
         'button { display: none; }'+
         'thead.head-report-reception { display: table-header-group;}' +
         'tbody.body-container-reception { page-break-after:always; }' +
         //'div.divheader { position: fixed; top:0px; }' +
         ' </style>'+
        //  '<style type="text/css" media="print">' +
        //  '@page {size: auto; margin: 2mm 4mm 0mm 0mm;}' +
        //  'div.divheader { position: fixed; top:0px;  } </style>' +
          '<style type="text/css" media="screen">' +
          'thead.tttt { display: block; } </style>');
         ifrm2.document.write('</head><body onload="window.print()"><form id="form1" runat="server">');
         ifrm2.document.write('<table style="width: 100%;">');
         ifrm2.document.write('<thead class="head-report-reception"><tr><td>' + divElementhead + '</td></tr></thead>');
         ifrm2.document.write('<tbody class="body-container-reception"><tr><td style="margin: 0 auto">' + divElement + '</td></tr></tbody>');
         ifrm2.document.write('<tfoot class="report-footer"><tr><td></td></tr></tfoot>');
          ifrm2.document.write('</table></form></body></html>');
         ifrm2.document.close();

    }

    closeDialog()
    {
        if(this.props.module == "reportreceptionequipment")
        {
           FlowRouter.go('/receptionequipment/' + this.props.record);

        }else if(this.props.module == "reportserviceorder")
        {
           FlowRouter.go('/serviceorder/' + this.props.record);
        }
        else if(this.props.module == "reportserviceprogram")
        {
           FlowRouter.go('/serviceorder/' + this.props.record);
        }
        else if(this.props.module == "reportcalendar")
        {
           FlowRouter.go('/calendar');
        }
    }



    render()
    {
        let items = [
        {
            text: 'Imprimir',
            cls: 'button-print',
            handler: this.printContent
        },
        {
            key: 'close',
            text: 'Cerrar',
            cls: 'button-close-report',
            handler: this.closeDialog
        }];

        let url = "/" + this.props.module + "/" + this.props.record;

        return (<div className="report-div">
                    <Toolbar className="north hbox" id="main-toolbar" items={items} />
                    <iframe  className="iframe-report" id="printf" name="printf" type="text/css" src={url}  />
                    </div>

        );
    }
}
