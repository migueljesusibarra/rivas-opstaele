import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export class Field extends Component
{
    constructor(props)
    {
        super(props);

        this.baseClass = "field";

        this.state = {
            value: this.processValue(props.value != null ? props.value : '')
        };

        this.getValue = this.getValue.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
    }
    componentDidMount()
    {
        this.validateField(this.state.value);
    }
    componentWillReceiveProps(props)
    {
        this.setState({value: this.processValue(props.value)});

        this.validateField(props.value);
    }
    processValue(value)
    {
        return value;
    }
    onFocus(event)
    {

    }
    onBlur(event)
    {
        this.validateField(this.getDOMValue(event.target));
    }
    addClass(className)
    {
        let node = ReactDOM.findDOMNode(this);

        if(!node.classList.contains(className))
            node.classList.add(className);
    }
    removeClass(className)
    {
        let node = ReactDOM.findDOMNode(this);
        node.classList.remove(className);
    }
    getInput(node)
    {
        return node.querySelector("input");
    }
    getDOMValue(input)
    {
        return input.value;
    }
    setDOMValue(value)
    {
        this.setState({value: value});
    }
    getValue()
    {
        return this.getDOMValue(this.getInput(ReactDOM.findDOMNode(this)));
    }
    setValue(value)
    {
        this.setDOMValue(value);
        this.validateField(value);
    }
    onChange(event) {
        let value = this.getDOMValue(event.target);

        this.setDOMValue(value);

        if (typeof this.props.onChange === "function") {
            this.props.onChange(value);
        }

        this.validateField(value);
    }
    validateField(value)
    {
        let isValid = true;

        // TODO: Refactor this code to make it more flexible for validating non required fields
        if(this.props.required === true){
            isValid = this.isNonEmpty(value) && this.checkValue(value);
        }
        else
            if(this.isNonEmpty(value))
                isValid = this.checkValue(value);

        this.setValid(isValid);
    }
    /**
      * @description: This method should implemented on each subclass
      * @return: boolean
      */
    isNonEmpty(value)
    {
        return value != null && value.toString().trim() != '';
    }
    isValid()
    {
        return this.state.isValid;
    }
    checkValue()
    {
        // Override in subclasses if it's necessary
        return true;
    }
    setValid(isValid)
    {
        if(!isValid)
            this.addClass(this.baseClass + '-invalid');
        else
            this.removeClass(this.baseClass + '-invalid');

        this.setState({isValid: isValid});
    }
    createInput(type, align)
    {
        let classes = [];

        return (
          <input
            className={classes.join(' ')}
            onChange={this.onChange.bind(this)}
            id={this.props.id}
            type={this.props.type}
            maxLength={this.props.maxLength}
            name={this.props.name}
            value={this.state.value}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
            readOnly={this.props.readOnly}
            autoFocus ={this.props.autoFocus}  />);
    }
    render()
    {
        let classes = [this.baseClass];
        let { align, required } = this.props;

        if(align && align.length > 0)
            classes.push(this.baseClass + '-' + align);

        if(required === true)
            classes.push(this.baseClass + '-required');

        return <div className={classes.join(' ')} style={this.props.style}>{this.createInput(this.props.type)}</div>;
    }
}
