import React, { Component, PropTypes } from 'react';
import { FormPanel, Toolbar, Field, TextField, GridPanel } from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { Dialog } from '/imports/ui/components/src/Dialog';
import { Util } from '/lib/common.js';

export class Lookup extends Component{
    constructor(props)
    {
        super(props);

        this.state = {
            records: this.props.records,
            value: this.props.value,
            text: this.props.text,
            search: null,
            opened: false,
            view: this.props.mode || 'list'
        };

        this.onCreate = this.onCreate.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.onDialogOpen = this.onDialogOpen.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
        this.onRecordSelected = this.onRecordSelected.bind(this);
    }
    componentWillReceiveProps(nextProps)
    {
        let state = {};

        if(nextProps.hasOwnProperty('value'))
            state.value = nextProps.value;
        if(nextProps.hasOwnProperty('text'))
            state.text = nextProps.text;
        if(nextProps.hasOwnProperty('records'))
            state.records = nextProps.records;

        this.setState(state);
    }
    shouldComponentUpdate(nextProps, nextState)
    {
        if(this.state.view != nextState.view || this.state.opened != nextState.opened)
            return true;
        else
            if(this.state.view == 'list' && this.state.search != nextState.search)
                return true;
            else
                if(!this.state.opened && (this.state.value != nextState.value || this.state.text != nextState.text))
                    return true;
                else
                    if(this.state.view == 'list' && this.state.records.length != nextState.records.length)
                        return true;
                    else
                        return false;
    }
    onDialogOpen()
    {
        this.openDialog();
    }
    openDialog()
    {
        this.setState({opened: true, view: 'list'});
    }
    onDialogClose(event)
    {
        this.closeDialog();
    }
    closeDialog()
    {
        this.setState({opened: false});
    }
    onRecordSelected(record)
    {
        if(this.props.onRecordChanged)
            this.props.onRecordChanged(record);

        this.closeDialog();
    }
    onCreate(event)
    {
        if(this.props.onCreate)
            this.props.onCreate(event);
    }
    onSearch(event)
    {
        let value = event.target.value.trim();
        let records = this.props.records;

        if(value != null && value.length > 0)
            records = this.getFilteredRecords(value);

        this.setState({search: value, records: records});
    }
    getFilteredRecords(value)
    {
        if(this.props.onSearch)
            return this.props.onSearch(this.props.records, value);
        else
            return this.props.records;
    }
    onTextChange(value)
    {
        if(value == "")
            this.setLookupValue(null, null);
    }
    getDefaultColumns()
    {
        return columns = [{
            key: 1,
            header: 'Id',
            renderer: function(record){
                return record.Id;
            }
        },
        {
            key: 2,
            header: 'Description',
            renderer: function(record)
            {
                return record.Description;
            }
        }];
    }
    setValue(value)
    {
        this.setState({value: value});
    }
    getValue()
    {
        return this.state.value;
    }
    setText(text)
    {
        this.setState({text: text});
    }
    getText()
    {
        return this.state.text;
    }
    setLookupValue(value, text)
    {
        console.log(value + ' - ' + text);
        this.setState({value: value, text: text});
    }
    setView(view)
    {
        this.setState({view: view});
    }
    getDialog()
    {
        let self = this;

        let bbar = [
        {
            key: 'ok',
            text: 'OK',
            handler: function()
            {
                let record = self.refs.gridLookup.getSelectedRecord();

                if(record != null)
                    self.onRecordSelected(record);
                else
                    alert('Please select a record.');
            }
        },
        {
            key: 'close',
            text: 'Cancel'
        }];

        let items = [];

        // Having this function means the client component wants to add the button to create a new record
        if(this.props.allowCreate)
            items.push(<a key={'new-button'} href="javascript:void(0);" onClick={this.onCreate} className="button-new-dealer">Create</a>);

        if(items.length > 0)
            items.push(<span key="separator-0" className="separator"></span>);

        items.push(<SearchField key={"search-field"} value={this.state.search} onSearch={this.onSearch}/>);

        let toolbar = items.length > 0 ? <Toolbar id="main-toolbar">{items}</Toolbar> : null;

        return (<Dialog ref="dialog" title={this.props.title ? this.props.title : 'Lookup'} bbar={bbar} modal={true} opened={true} onClose={this.onDialogClose}>
                    <GridPanel
                        ref="gridLookup"
                        toolbar={toolbar}
                        columns={this.props.columns}
                        records={this.state.records}
                        onRowDoubleClick={this.onRecordSelected}
                        />
                </Dialog>);
    }
    render()
    {
        let label = this.props.label;
        let required = this.props.required;

        return(
            <div className="hbox form-field-required">
                <div className="form-field-label hbox-l">
                    <label>{label}</label>
                    <img style={{cursor: "pointer", float: 'right', marginLeft: '10px'}} src="../../../../images/search.png" alt="Contract" onClick= {this.onDialogOpen} />
                </div>
                <div className="form-field-lookup form-field-value hbox-r">
                    <TextField ref="displayField" value={this.state.text} onChange={this.onTextChange} maxLength="50" plain={true} readOnly required={required} />
                    <hidden ref="valueField" value={this.state.value}/>
                </div>
                {this.state.opened ? this.getDialog() : null }
            </div>
        );
    }

}
