import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Comments } from '../../../api/comments/collection.js';
import { Confirm } from '/imports/ui/components/alert/Confirm.jsx';

export class AttachmentPanel extends Component
{
    constructor(props)
    {
        super(props);

        this.onOkConfirmDialog = this.onOkConfirmDialog.bind(this);
        this.onCommentDeleted = this.onCommentDeleted.bind(this);
    }

    onOkConfirmDialog()
    {
        let id = this.refs.confirmDialog.getValue();

        Meteor.call("deleteComment", id)
    }

    getConfirmDialog()
    {
        return (
        <Confirm
        ref="confirmDialog"
        onOk={this.onOkConfirmDialog}
        okLabel="Si"
        noLabel="No"
        />);
    }

    getUsername(createdBy)
    {
        return typeof createdBy == 'object' ? createdBy.username : createdBy;
    }

    onCommentDeleted(event)
    {
        this.refs.confirmDialog.setTitle("¿Seguro que quieres eliminar este comentario?");
        this.refs.confirmDialog.onOpenAndValue(event.target.closest(".x-comment").id);
    }

    formatDate(date)
    {
        return date.toLocaleDateString('en-US', {month: 'short'}) + ' ' + date.toLocaleDateString('en-us', {weekday: 'short'}) + " " + date.toLocaleTimeString("en-US", {day: '2-digit', hour:'2-digit', minute:'2-digit'});
    }

    getThumbnail(type)
    {
        if(type.indexOf('spread') != -1 || type.indexOf('excel') != -1)
            return '/images/excel.png'
        else
            if(type.indexOf('word') != -1)
                return '/images/word.png';
            else
                if(type.indexOf('pdf') != -1)
                    return '/images/pdf.png';
                else
                    return '/images/attachment.png';
    }
    summarizeAttachments()
    {
        // Display the summarized attachments section only if maybe they can't be seen at first sight
        if(this.props.records.length > 1 || this.props.data.childs.length > 1)
        {
            let attachments = [];

            this.props.records.forEach(function(comment){
		            if(comment.attachments != null)
	                attachments = attachments.concat(comment.attachments);
            });

            return <ul id="attachment-summary" className="x-attachment-list">{this.renderAttachments(attachments)}</ul>;
        }
        else
            return null;
    }
    renderAttachmentList(ref, attachments)
    {
        return (<ul className="x-attachment-list">
                    {this.renderAttachments(ref, attachments)}
                </ul>);
    }
    renderAttachments(ref, attachments)
    {
        if(attachments != null)
        {
            let self = this;
            let thumnail = null

            return attachments.map(function(attachment){
                // TODO: Add a placeholder depending on the file type when the attachment is not an image
                thumbnail = attachment.type.indexOf("image") != -1 ? attachment.url : self.getThumbnail(attachment.type);

                return (<li key={"attach-" + ref + attachment.index} className="x-attachment">
                    		<a href={attachment.url} className="x-thumbnail" target="_blank" style={{fontSize:'0px', display:'block'}}>
                                <img src={thumbnail} style={{display:'block'}}/>
                            </a>
                    		{
                                attachment.allowDelete ?
                            		<a href="#" className="x-delete" style={{position: 'absolute', top: 0, right: 0, backgroundColor: 'white', borderRadius: '70px', transform: 'none', padding: '2px', border: '1px solid lightgray', opacity: '0.9'}}>
                            			<i className="icon-delete" style={{fontSize: '18px', verticalAlign: 'middle'}}></i>
                            		</a>
                                : null
                            }
                    	</li>);
            });
        }
        else
            return null;
    }
    renderAutoComment(comment)
    {
        let { property, text, createdOn } = comment;

        return (<div style={{color:'#676767', marginLeft:'40px'}}>
                    {property != null ?
                        <div style={{display: 'inline-block'}}><span style={{fontStyle:'italic', color: 'gray', lineHeight: '20px'}}>{property.name}</span> changed to <span style={{fontStyle:'italic', color:'#909090'}}>{property.value}</span></div>
                        :
                        <span style={{color:'#676767', margin:'0px', fontStyle: 'italic', lineHeight: '20px'}}>{text}</span>
                    }
                    <span> on </span>
                    <span style={{color:'#B1B1B1', fontStyle: 'italic', fontSize: '13px'}}>{this.formatDate(comment.createdOn)}</span>
                </div>);
    }
    renderStandardComment(comment)
    {
        let user = Meteor.user();

        comment.allowReply = !comment.automatic;
        comment.allowDelete = !this.props.deleteFile ? false : (!comment.automatic && user.username == this.getUsername(comment.createdBy))

        return (<div style={{marginLeft: '40px'}}>
                    {comment.text ? <p style={{color:'#676767', margin:'0px'}}>{comment.text}</p> : null}
                    {this.renderAttachmentList(comment._id, comment.attachments)}
                    <div style={{fontSize:'11px', marginTop:'10px'}}>
                        <span style={{color:'#B1B1B1'}}>{this.formatDate(comment.createdOn)}</span>
                        { comment.allowDelete ? <a href="javascript:void(0);" className="x-comment-delete" style={{textDecoration: 'none', marginLeft: '10px'}} onClick={this.onCommentDeleted}>Delete</a> : null }
                    </div>
                </div>);
    }
    createComment(comment)
    {

        let path = '/images/avatars/' + comment.createdBy + '.jpg';

        return (<li id={comment._id} key={comment._id} className="x-comment">
            		<i className="x-comment-avatar" style={{backgroundImage:'url(' + path + ')'}}></i>
                    {comment.automatic ? this.renderAutoComment(comment) : this.renderStandardComment(comment)}
                </li>);
    }
    renderComments()
    {
        return <ul id="comment-list" className="center">{this.props.records.map(this.createComment.bind(this))}</ul>;
    }
    render()
    {
        return (
        <div>
            {this.renderComments()}
            {this.getConfirmDialog()}
        </div>);
    }
}
