import React, { Component } from 'react';
import { Field } from 'react-ui';

export class HiddenField extends Field
{
    constructor(props)
    {
        super(props);
    }
    getInput(node)
    {
        return node;
    }
    render()
    {
        return this.createInput(this.props.type);
    }
}

HiddenField.defaultProps = {
    type: 'hidden'
};
