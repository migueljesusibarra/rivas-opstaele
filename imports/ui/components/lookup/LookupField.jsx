import React, { Component } from 'react';
import { TextField } from 'react-ui';

export class LookupField extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            opened: false
        };

        this.onClear = this.onClear.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }
    onClear(event)
    {
        if(this.props.onClear)
            this.props.onClear();
    }
    onSearch(event)
    {
        if(!this.props.disabled)
        {
           this.setState({opened: true});
           this.props.onSearch && this.props.onSearch(event);
        }
    }
    render()
    {  
        return (
            <FormField>
                <FormLabel>
                    <label>{this.props.label}:</label>
                    <img style={{cursor: "pointer", float: 'right', marginLeft: '10px'}} src= "../../../../images/search.png" alt="Contract" onClick= {this.onSearch} />
                </FormLabel>
                <FormInput>
                    <TextField ref="displayField" value={this.props.text} onChange={this.onTextChange} onKeyPress={this.onKeyPress} maxLength="50" plain={true} readOnly required={this.props.required} disabled = {this.props.disabled}/>
                    {this.props.text ? <button className="lookup-clear" onClick={this.onClear}></button> : null}
                    <hidden ref="valueField" value={this.props.value}/>
                </FormInput>
                {this.props.children}
            </FormField>
        );
    }
}

export class FormField extends Component
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        return (
            <div className="hbox form-field">
                {this.props.children}
            </div>
        );
    }
}

export class FormLabel extends Component
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        return (
            <div className="form-field-label hbox-l">
                {this.props.children}
            </div>
        );
    }
}

export class FormInput extends Component
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        return (
            <div className="form-field-lookup form-field-value hbox-r">
                {this.props.children}
            </div>
        );
    }
}
