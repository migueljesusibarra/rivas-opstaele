import React, { Component } from 'react';
import { Toolbar, Panel, DialogPanel, GridPanel, Button,
    Container, TextField, Field, ZipField  } from 'react-ui';
import { FormPanel, Fieldset } from 'react-ui';
import { SearchField } from '/imports/ui/components/src/SearchField';
import { LookupDialog } from '/imports/ui/components/src/Dialog';

export class LookupList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            search: null,
            records: props.records
        };

        this.onSearch = this.onSearch.bind(this);
        this.onAccept = this.onAccept.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }
    onAccept()
    {
        let record = this.refs.grid.getSelectedRecord();

        if(record != null)
            this.props.onSelect && this.props.onSelect(record);
        else
            alert('Por favor seleccione un rigistro.');
    }

    onCancel()
    {
        this.props.onCancel && this.props.onCancel();
    }
    onSearch(event)
    {
        this.setState({search: event.target.value});
    }
    getToolbar()
    {
        return (<div>
                 <SearchField key={"searchfield"} className = "search-field-lookup" value={this.state.search}  placeholder=" Buscar" onSearch={this.onSearch}/>
               </div>
            // <Toolbar>
            //     {this.props.toolbar}
            //
            // </Toolbar>
        );
    }
    getFooter()
    {
        let footer = null;
        let items = [{
            key: 'ok',
            text: 'Aplicar',
            handler: this.onAccept
        },
        {
            key: 'close',
            text: 'Cancelar',
            handler: this.onCancel
        }];

        function createButton(item)
        {
            if(item.key == 'close')
                return (<Button id={"dialog-" + item.key} key={item.key} text={item.text} onClick={item.handler} />);
            else
                return (<Button id={"dialog-" + item.key} key={item.key} text={item.text} onClick={item.handler} />);
        }

        let buttons = [];

        buttons = items.map(createButton.bind(this));
        footer = <div className="dialog-footer align-right">{buttons}</div>;

        return footer;
    }
    getColumns()
    {
        return this.props.columns;
    }
    getRecords()
    {

        let search = this.state.search;

        if(search)
            return this.props.records.filter(c => this.props.onFilter(c, search));
        else
            return this.props.records;
    }
    render()
    {
        return (
            <DialogPanel
                title={this.props.title}
                footer={this.getFooter()}
                className="vbox center" style={{textAlign: "center"}} modal>
                <GridPanel
                ref="grid"
                className="center"
                toolbar={this.getToolbar()}
                columns={this.getColumns()}
                records={this.getRecords()}
                onRowDoubleClick={this.onAccept}
                />
            </DialogPanel>
        );
    }
}
