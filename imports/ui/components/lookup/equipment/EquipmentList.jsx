import React, { Component } from 'react';
import { LookupList } from '/imports/ui/components/lookup/LookupList';

export class EquipmentList extends Component
{

 getColumns(){

    let columns = [
        {
          key: 1,
          header: 'Id',
          minWidth: 40,
          dataIndex: '_id'
        },
      {
        key: 2,
        header: 'Serie',
        minWidth: 40,
        dataIndex: 'serialNumber'
      },
      {
        key: 3,
        header: 'Sistema',
        minWidth: 40,
        dataIndex: 'system'
      },
      {
        key: 4,
        header: 'Marca',
        minWidth: 40,
        dataIndex: 'brand'
      },
      {
        key: 5,
        header: 'Modelo',
        minWidth: 40,
        dataIndex: 'model'
      }
    ];

    return columns;

  }

  onFilter(record, search)
    {
      return record.serialNumber.search(new RegExp(search, "i")) != -1 ||
             record.system.search(new RegExp(search, "i")) != -1 ||
             record.brand.search(new RegExp(search, "i")) != -1 ||
             record.model.search(new RegExp(search, "i")) != -1 ;

    }
    render()
    {

        return (
                <LookupList
                    title="Lista de Equipos"
                    toolbar={this.props.toolbar}
                    columns={this.getColumns()}
                    records={this.props.records}
                    onSelect={this.props.onSelect}
                    onCancel={this.props.onCancel}
                    onFilter={this.onFilter}
                    />
        );
    }
}
