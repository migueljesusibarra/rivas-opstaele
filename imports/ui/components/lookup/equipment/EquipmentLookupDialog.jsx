import React, { Component } from 'react';
import { LookupDialog } from '/imports/ui/components/src/Dialog';
import { EquipmentLookupContainer } from '/imports/ui/containers/EquipmentLookupContainer';

export class EquipmentLookupDialog extends Component
{
    static get defaultProps()
    {
        return {
            mode: 'list'
        };
    }
    constructor(props)
    {
        super(props);

        this.state = {
            opened: false,
            mode: props.mode
        };

        this.onCreate = this.onCreate.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onModeChange = this.onModeChange.bind(this);

    }
    componentWillReceiveProps(nextProps)
    {
        this.setState({opened: nextProps.opened, mode: nextProps.mode});
    }
    onCreate()
    {
        this.setMode('form');
    }
    onSelect(record)
    {
        if(this.props.onSelect)
            this.props.onSelect(record);
    }
    onCancel()
    {
        if(this.state.mode == 'form')
            this.setMode('list');
        else
            if(this.props.onCancel)
                this.props.onCancel();
    }
    onModeChange()
    {
        if(this.props.onModeChange)
            this.props.onModeChange(this.state.mode);
    }
    setMode(mode)
    {
        this.setState({mode: mode}, this.onModeChange);
    }
    getPanel(mode)
    {
        if(mode == 'list')
        {
             return <EquipmentLookupContainer foreignId = {this.props.foreignId} onSelect={this.onSelect} onCancel={this.onCancel}/>;
        }       
    }
    render()
    {
        return (
            <LookupDialog opened={this.state.opened}>
                {this.getPanel(this.state.mode)}
            </LookupDialog>
        );
    }
}
