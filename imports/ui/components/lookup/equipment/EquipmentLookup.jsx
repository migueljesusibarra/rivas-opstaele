import React, { Component } from 'react';
import { LookupField } from '/imports/ui/components/lookup/LookupField';
import { EquipmentLookupDialog } from './EquipmentLookupDialog';

export class EquipmentLookup extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            opened: false,
            mode: 'list',
            value: props.value || null,
            text: props.text || null,
            record: props.record || null
        };

        this.onClear = this.onClear.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onModeChange = this.onModeChange.bind(this);
    }
    componentWillReceiveProps(nextProps)
    {
        this.setState({record: nextProps.record, value: nextProps.value, text: nextProps.text});
    }
    onModeChange(mode)
    {
        this.setState({mode: mode});
    }
    onClear()
    {
        this.setState({value: null, text: null});
    }
    onSearch(event)
    {
        this.setState({opened: true, mode: 'list'});
    }
    onSelect(record)
    {
        let serialNumber = record["serialNumber"];
        let self = this;
        let name = `${record.serialNumber}`;

        this.setState({opened: false, value: record._id, text: name, record: record}, function(){
            self.props.onSelect(record);
        });
    }
    onCancel()
    {
        this.setState({opened: false});
    }
    getValue()
    {
        return this.state.value;
    }
    getTextValue()
    {
       return this.state.text;
    }

    setValue(value, text)
    {  
       this.setState({value: value, text: text});
    }

    render()
    {
      let panel = null;

      return (<LookupField
                label={this.props.label}
                value={this.state.value}
                text={this.state.text}
                disabled = {this.props.disabled}
                required={this.props.required}
                onSearch={this.onSearch}
                onClear={this.onClear}>
                <EquipmentLookupDialog mode={this.state.mode} foreignId = {this.props.foreignId} opened={this.state.opened} onSelect={this.onSelect} onCancel={this.onCancel} onModeChange={this.onModeChange}/>
            </LookupField>
    );
    }
}
