import React, { Component } from 'react';
import { Button } from 'react-ui';
import { FormPanel } from 'react-ui';
import { Customers } from '/imports/api/customers';

export class LookupForm extends Component
{
    constructor(props)
    {
        super(props);

        this.onCreate = this.onCreate.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }
    onCreate()
    {
        if(this.props.onCreate)
            this.props.onCreate(this.refs.form);
    }
    onCancel()
    {
        if(this.props.onCancel)
            this.props.onCancel();
    }
    getFooter()
    {
        let footer = null;
        let items = [
          {
            key: 'ok',
            text: 'Create',
            handler: this.onCreate
        },
        {
            key: 'close',
            text: 'Cancel',
            handler: this.onCancel
        }];

        function createButton(item)
        {
            if(item.key == 'close')
                return (<Button id={"dialog-" + item.key} key={item.key} text={item.text} onClick={item.handler} />);
            else
                return (<Button id={"dialog-" + item.key} key={item.key} text={item.text} onClick={item.handler} />);
        }

        let buttons = [];

        buttons = items.map(createButton.bind(this));
        footer = <div className="dialog-footer align-right">{buttons}</div>;

        return footer;
    }
    render()
    {
        return (
            <FormPanel ref="form" title={this.props.title} footer={this.getFooter()}>
                {this.props.children}
            </FormPanel>
        );
    }
}
