import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

export class Navigation extends Component
{
  constructor(props) {
    super(props);
  }

  logout(e)
  {
      e.preventDefault();
      Meteor.logout(function() {
        FlowRouter.go('login');
      });
  }

  logOff()
  {
    if(Meteor.userId()){
        return(
            <div className="user-name">
               <span className="user-name-span">
               <img src="images/photo.jpg" alt="" /><a style={{marginLeft: '4px'}} onClick = { this.logout.bind(this) }>{this.getName(this.props.user)}!</a></span>
            </div>
        );
    }
    else{
         return(<div></div>);
    }
  }

  getName(user)
  {
      if(user != null)
      {
          if(user.profile != null)
              return user.profile.name != null ? user.profile.name : user.username;
          else
              return user.username;
      }
      else
          return '';
  }
  getProfile()
  {
    FlowRouter.go('/user/'+ Meteor.userId())
  }

   renderNavigation2(){

      return(<div style={{marginLeft:'-12px'}}><div className = "menu-background" >
        <ul className="nav">
            <li><a href="/customers"><i className="item-icon-customer"></i>Clientes</a></li>
            <li><a href="/technicians"><i className="item-icon-technicians"></i>Ingenieros</a>
            <ul className="subs">
                <li><a href= "/activities">Actividades</a></li>
            </ul>
            </li>
            <li><a><i className="item-icon-purchase"></i>Compras</a>
               <ul className="subs">
                    <li><a href="/purchaseordersystems">Sistema</a></li>
                    <li><a href= "/purchaseorderparts">Parte</a></li>
                </ul>
            </li>
            <li>
            <a href="/receptionequipments"><i className="item-icon-receptionEquipment">
               </i>Equipos</a>
               <ul className="subs">
                   <li><a href= "/testsPerformed">Pruebas de equipos</a></li>
              </ul>
               </li>
             <li><a href="/serviceorders"><i className="item-icon-service"></i>O. Service</a>
                 <ul className="subs">
                     <li><a href= "/preordens">Pre-Orden</a></li>
                     <li><a href= "/calendar">Calendar</a></li>
                  </ul>
             </li>
             <li><a href="/contracts"><i className="item-icon-contract"></i>Contratos</a></li>
             <li><a href= "/installequipments">
                 <i className="item-icon-protocol"></i>Base Instalada</a>
                 <ul className="subs">
                     <li><a href="/chartequipment">Gráfico</a></li>
                </ul>
                 </li>
             <li><a><i className="item-icon-setting"></i>Configuración</a>
                <ul className="subs">
                    <li><a href="/customertypes">Tipos de Cliente</a></li>
                    <li><a href="/systems">Sistemas</a></li>
                    <li><a href="/brands">Marcas</a></li>
                    <li><a href="/models">Modelos</a></li>
                    <li><a href="/parts">Partes</a></li>
                    <li><a href="/vendors">Proveedor</a></li>
                    <li><a href="/orderTypes">Tipos de ordenes</a></li>
                    <li><a href="/periods">Periods</a></li>
                    <li><a href="/protocols">Protocolos</a></li>
                    <li><a href="/emails">Correos</a></li>
                    <li><a href="/users">Usuarios</a></li>
                    <li><a href="/roles">Roles</a></li>
                </ul>
            </li>
            </ul>
         </div>
         <div className="user-name">
            <span className="user-name-span">
            <a style={{marginLeft: '8px', color:'#fff'}} onClick = { this.getProfile.bind(this)}>
                <img src="/img/photo.jpg" /> {this.getName(this.props.user)}
            </a><a onClick = { this.logout.bind(this)}>{" | " + "Salir"}</a></span>
        </div>
        </div>);
   }

    render()
    {
        return (this.renderNavigation2());
    }
  }
