import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

export class AppReport extends Component
{
    constructor(props) {
        super(props);
    }

    render()
    {
        return ( <div className="overflow-y-report">{this.props.content}</div>);
    }
}
