import { createContainer } from 'meteor/react-meteor-data';
import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import '../../startup/both/accounts.config.js';

export class App extends Component
{
    constructor(props) {
        super(props);
    }

    logout(e)
    {
        e.preventDefault();
        Meteor.logout(function() {
          FlowRouter.go('login');
        });
    }
    getName(user)
    {
        if(user != null)
        {
            if(user.profile != null)
                return user.profile.name != null ? user.profile.name : user.username;
            else
                return user.username;
        }
        else
            return '';
    }
    logOff()
    {
      if(Meteor.userId()){
          return(
              <div className="log-off">
                  <div className="test">
                      <p>Bienvenido {this.getName(this.props.user)}!</p>
                      <a href="#" onClick = { this.logout.bind(this) }>Desconectarse</a>
                  </div>
                  <img className="avatar" src="/img/avatar.png" />
              </div>);
      }
      else{
           return(<div></div>);
      }
    }
    isLogged()
    {
        return Meteor.userId() != null;
    }
    render()
    {
        miguel = this.props;
        if(this.isLogged())
        {
            return (<div id="react-app" className="vbox main">
                          {this.props.navigation}
                        <header id="main-header">
                            <div className="wrapper">
                                <div className="x-brand x-image">
                                    <a href="#"><img src="/img/logo2.png" /></a>
                                    <span></span>
                                </div>
                                </div>
                        </header>
                        <div id="main-container" className="hbox center">
                            <div id="main-content" className="vbox center">
                                {this.props.content}
                            </div>
                            {this.props.aside}
                        </div>
                        <footer id="main-footer">
                            <span>Todos los derechos reservados de rivas opstaele</span>
                        </footer>
                    </div>);
        }
        else
        {
            return this.props.content;
        }
    }
}
