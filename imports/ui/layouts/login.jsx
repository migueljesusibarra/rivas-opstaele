import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

export class Login extends Component
{
    componentDidMount()
    {
        // use meteor blaze to render login buttons
        this.view = Blaze.renderWithData(Template.loginButtons, {align: this.props.align}, ReactDOM.findDOMNode(this.refs.container));
    }
    componentWillUnmount()
    {
        Blaze.remove(this.view);
    }
    render()
    {
        return <span id="login" ref="container" />;
    }
}
