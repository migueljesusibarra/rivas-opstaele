import  React from 'react';
import { compose } from 'react-komposer';

const loadingMask = {
    loadingHandler: () => (<div style={{position: 'absolute', top: '50%', left: '50%'}}>Loading...</div>)
};

export const composeWithLoading = function(composer, component)
{
  return compose(getTrackerLoader(composer), loadingMask)(component);
};

export const composeDefault = function(composer, component)
{
    return compose(getTrackerLoader(composer))(component);
};

export const getTrackerLoader = function(reactiveMapper) {
  return (props, onData, env) => {
    let trackerCleanup = null;
    const handler = Tracker.nonreactive(() => {
      return Tracker.autorun(() => {

        // assign the custom clean-up function.
        trackerCleanup = reactiveMapper(props, onData, env);
      });
    });

    return () => {
      if(typeof trackerCleanup === 'function') trackerCleanup();
      return handler.stop();
    };
  };
};

export const Util =
{
  getDateISO: function(date)
    {
      if(typeof date == 'string')
      {
          var tmp = date.replace('ISODate("','').split('T');
          tmp = tmp[0].split('-');
          date = new Date(tmp[0], tmp[1] - 1, tmp[2]);
      }

      return date.toISOString().slice(0, 10);
    },

  getDepartments: function()
  {
      let departmentsArray = [{value:"1", text:"Boaco"},{value:"2", text:"Carazo"},
             {value:"3", text:"Chinandega"},{value:"4", text:"Chontales"},
             {value:"5", text:"Estelí"},{value:"6", text:"Granada"},
             {value:"7", text:"Jinotega"},{value:"8", text:"León"},
             {value:"9", text:"Madriz"},{value:"10", text:"Managua"},
             {value:"11", text:"Masaya"},{value:"12", text:"Matagalpa"},
             {value:"13", text:"Nueva Segovia"},{value:"14", text:"Rivas"},
             {value:"15", text:"Río San Juan"},
             {value:"16", text:"Región Autónoma de la Costa Caribe Norte"},{value:"17", text:"Región Autónoma de la Costa Caribe Sur"}]

     departmentsArray.sort(function(a, b)
     {
         let descriptionA = a.text;
         let descriptionB = b.text;

         var nameA = descriptionA.toUpperCase(); // ignore upper and lowercase
         var nameB = descriptionB.toUpperCase(); // ignore upper and lowercase

         if (nameA < nameB) {
             return -1;
         }

         if (nameA > nameB) {
             return 1;
         }
         // names must be equal
         return 0;
     });

    return departmentsArray;
  },
  getPeriods: function()
  {
    let data = [{value: 12, text: "Anual"},
                  {value: 2, text: "Bimestral"},
                  {value: 4, text: "Cuatrimestral"},
                  {value: 1, text: "Mensual"},
                  {value: 6, text: "Semestral"},
                  {value: 3, text: "Trimestral"}]

       return data;
  },
  searchObject: function(nameKey, myArray)
  {

   for (var i=0; i < myArray.length; i++)
    {
     if (myArray[i].value === nameKey) {
      return myArray[i];
     }
    }
  },

  isEmptyObject: function(obj)
  {
    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
  },

  isEmpty: function(value)
  {
   return (!value || !(/[^\s]+/.test(value)));
  },

  isEmail: function(email)
  {
    // var filter =   /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    if(filter.test(email)) {
      return true;
    }

    return false;
  },

  sumMonthDate: function(date, month)
  {
    let newDate = new Date(date);
    newDate.setMonth(newDate.getMonth() + parseInt(month));
    return newDate
  },

  sumDayDate: function(date, days)
  {
      let newDate = new Date(date);
      newDate.setDate(newDate.getDate() + parseInt(days));
      return newDate
  },

  formatMoney: function(n, currency) {
    return currency + "" + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  },

  spanishDate: function(d)
  {
      let weekday=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];

      let monthname=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

      return weekday[d.getDay()]+" "+d.getDate()+" de "+monthname[d.getMonth()]+" de "+d.getFullYear()

  },
  weekCount: function(year, month_number)
  {
      // month_number is in the range 1..12
      let firstOfMonth = new Date(year, month_number-1, 1);
      let lastOfMonth = new Date(year, month_number, 0);

      used = firstOfMonth.getDay() + lastOfMonth.getDate();

      return Math.ceil( used / 7);
  },
  applySortObject: function(a, b)
  {
      let descriptionA = a.text || a.name || a.description || "";
      let descriptionB = b.text || b.name || b.description || "";

      var nameA = descriptionA.toUpperCase(); // ignore upper and lowercase
      var nameB = descriptionB.toUpperCase(); // ignore upper and lowercase

      if (nameA < nameB) {
          return -1;
      }

      if (nameA > nameB) {
          return 1;
      }
      // names must be equal
      return 0;
  }
}
